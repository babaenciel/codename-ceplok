ALTER TABLE kamar CHANGE created created_at DATE;
ALTER TABLE kamar CHANGE updated updated_at DATE;

ALTER TABLE laboratorium CHANGE created created_at DATE;
ALTER TABLE laboratorium CHANGE updated updated_at DATE;

ALTER TABLE pemakaian_alkes CHANGE created created_at DATE;
ALTER TABLE pemakaian_alkes CHANGE updated updated_at DATE;

ALTER TABLE pemakaian_kamar CHANGE created created_at DATE;
ALTER TABLE pemakaian_kamar CHANGE updated updated_at DATE;

ALTER TABLE perawat CHANGE created created_at DATE;
ALTER TABLE perawat CHANGE updated updated_at DATE;

ALTER TABLE rawat_inap CHANGE created created_at DATE;
ALTER TABLE rawat_inap CHANGE updated updated_at DATE;

ALTER TABLE tarif_laboratorium CHANGE created created_at DATE;
ALTER TABLE tarif_laboratorium CHANGE updated updated_at DATE;

ALTER TABLE tarif_lainnya CHANGE created created_at DATE;
ALTER TABLE tarif_lainnya CHANGE updated updated_at DATE;

ALTER TABLE tarif_tindakan CHANGE created created_at DATE;
ALTER TABLE tarif_tindakan CHANGE updated updated_at DATE;

ALTER TABLE tarif_visit CHANGE created created_at DATE;
ALTER TABLE tarif_visit CHANGE updated updated_at DATE;

ALTER TABLE tindakan_lainnya CHANGE created created_at DATE;
ALTER TABLE tindakan_lainnya CHANGE updated updated_at DATE;

ALTER TABLE tindakan_perawat CHANGE created created_at DATE;
ALTER TABLE tindakan_perawat CHANGE updated updated_at DATE;

ALTER TABLE visit_dokter CHANGE created created_at DATE;
ALTER TABLE visit_dokter CHANGE updated updated_at DATE;