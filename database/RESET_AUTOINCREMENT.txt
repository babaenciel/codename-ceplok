ALTER TABLE dokter AUTO_INCREMENT = 1;
ALTER TABLE kamar AUTO_INCREMENT = 1;
ALTER TABLE katalog_obat AUTO_INCREMENT = 1;
ALTER TABLE kategori_obat AUTO_INCREMENT = 1;
ALTER TABLE laboratorium AUTO_INCREMENT = 1;
ALTER TABLE mutasi_obat AUTO_INCREMENT = 1;
ALTER TABLE mutasi_obat_detail AUTO_INCREMENT = 1;
ALTER TABLE obat_retur AUTO_INCREMENT = 1;
ALTER TABLE pasien AUTO_INCREMENT = 1;
ALTER TABLE pemakaian_alkes AUTO_INCREMENT = 1;
ALTER TABLE pemakaian_kamar AUTO_INCREMENT = 1;
ALTER TABLE penerimaan_obat AUTO_INCREMENT = 1;
ALTER TABLE penerimaan_obat_detail AUTO_INCREMENT = 1;
ALTER TABLE pengeluaran_obat AUTO_INCREMENT = 1;
ALTER TABLE perawat AUTO_INCREMENT = 1;
ALTER TABLE perubahan_persediaan_obat AUTO_INCREMENT = 1;
ALTER TABLE rawat_inap AUTO_INCREMENT = 1;
ALTER TABLE resep AUTO_INCREMENT = 1;
ALTER TABLE sp_detail AUTO_INCREMENT = 1;
ALTER TABLE supplier AUTO_INCREMENT = 1;
ALTER TABLE surat_permintaan AUTO_INCREMENT = 1;
ALTER TABLE tarif_laboratorium AUTO_INCREMENT = 1;
ALTER TABLE tarif_lainnya AUTO_INCREMENT = 1;
ALTER TABLE tarif_tindakan AUTO_INCREMENT = 1;
ALTER TABLE tarif_visit AUTO_INCREMENT = 1;
ALTER TABLE tindakan_lainnya AUTO_INCREMENT = 1;
ALTER TABLE tindakan_perawat AUTO_INCREMENT = 1;
ALTER TABLE user AUTO_INCREMENT = 1;
ALTER TABLE visit_dokter AUTO_INCREMENT = 1;
