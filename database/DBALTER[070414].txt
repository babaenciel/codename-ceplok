ALTER TABLE resep DROP FOREIGN KEY resep_ibfk_1;
ALTER TABLE resep DROP id_pasien;

ALTER TABLE resep ADD id_rawat_inap INT(9) NULL AFTER jenis_resep;
ALTER TABLE resep
    ADD CONSTRAINT resep_id_rawat_inap
    FOREIGN KEY(id_rawat_inap)
    REFERENCES rawat_inap(id_rawat_inap)
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE pengeluaran_obat ADD sisa INT(9) NULL AFTER id_katalog_obat;
ALTER TABLE pengeluaran_obat ADD tanggal_retur DATE NULL AFTER sisa;
ALTER TABLE pengeluaran_obat ADD sebagai_obat_pulang INT(1) NULL AFTER tanggal_retur;