<style type="text/css">
    #nota {
        font-size: 15px;        
    }

    #nota table {
        border-spacing: 0px;
    }
</style>

<div style="width: 700px" id="nota">
    <table style="margin-bottom: 10px;">
        <tr>
            <td>
                <img src="<?php echo base_url('resources/img/logo2.jpg'); ?>" style="width: 90px; height: 70px; margin-right: 20px;">
            </td>
            <td style="width: 550px">
                <div style="text-align: center; font-size: 20px;">RUMAH SAKIT</div>
                <div style="text-align: center; font-size: 20px;">BUDI SEHAT</div>
                <div style="text-align: center">Jln. WR. Supratman No 183, Cangkrep Lor, Purworejo</div>
                <div style="text-align: center">Telepon ( 0275 ) 3128272</div>
            </td>
        </tr>
    </table>

    <table>
        <tr>
            <td style="width: 700px;">
                <div style="border-top: 1px solid black;"></div>
            </td>
        </tr>
        <tr>
            <td style="width: 700px;">
                <div style="border-top: 1px solid black;"></div>
            </td>
        </tr>
    </table>

    <br>

    <table style="margin-left: 20px; margin-right: -20px;">
        <tr>  
            <td></td>
            <td><?php $this->load->view('elements/nomor_nota'); ?></td>
        </tr>        
        <tr>
            <td><h4 style="text-align: center; margin-top: -10px; margin-left: 60px;">TANDA BUKTI PEMBAYARAN TUNAI</h4></td>            
        </tr>        
        <tr>
            <td style="width: 600px; border-bottom: 1px dotted black; padding-top: 10px;">Sudah Terima Dari: <strong><?php echo $input['sudah_terima_dari']; ?></strong></td>
            <td style="width: 100px; "></td>
        </tr>
        <tr>
            <td style="width: 600px; border-bottom: 1px dotted black; padding-top: 10px;">Alamat: <strong><?php echo $input['alamat_dari']; ?></strong></td>
            <td style="width: 100px; "></td>
        </tr>
        <tr>
            <td style="width: 600px; border-bottom: 1px dotted black; padding-top: 10px;">Untuk Biaya Rawat Inap Pasien: <strong><?php echo $input['untuk']; ?></strong></td>
            <td style="width: 100px; "></td>
        </tr>
        <?php if(false): ?>
        <tr>
            <td style="width: 600px; border-bottom: 1px dotted black; padding-top: 10px;">Alamat: <strong><?php echo $input['alamat_untuk']; ?></strong></td>
            <td style="width: 100px; "></td>
        </tr>
        <?php endif; ?>
        <tr>
            <?php if(!empty($input['atas_permintaan_sendiri'])): ?>
                <td style="width: 600px; border-bottom: 1px dotted black; padding-top: 10px;">Atas Permintaan Sendiri: <strong>Ya</strong></td>
            <?php else: ?>
                <td style="width: 600px; border-bottom: 1px dotted black; padding-top: 10px;">Atas Permintaan Sendiri: <strong>Tidak</strong></td>
            <?php endif; ?>

            <td style="width: 100px; "></td>
        </tr>

        <tr>
            <td><h4 style="text-align: center; margin-top: 20px; margin-left: 60px;">RINCIAN</h4></td>
        </tr>

        <?php if(isset($input['kamar'])): ?>
            <tr>
                <td style="padding-top: 10px;"><strong>Kamar</strong></td>
                <td style="padding-top: 10px;"></td>
            </tr>

            <?php foreach($input['kamar'] as $keys=>$rows): ?>
                <tr>
                    <td style="width: 500px; border-bottom: 1px dotted black; padding-top: 10px;"><?php echo $input['nama_kamar'][$keys]; ?></td>
                    <td style="width: 500px; padding-top: 10px;"><?php echo format_number($rows, TRUE); ?></td>
                </tr>
            <?php endforeach; ?>

            <tr>
                <td style="padding-top: 10px;"></td>
                <td style="padding-top: 10px; font-weight: bold;"><?php echo format_number($input['total_kamar'], TRUE); ?></td>
            </tr>
        <?php endif; ?>

        <tr>
            <td></td>
        </tr>

        <?php if(isset($input['obat'])): ?>
            <tr>
                <td style="padding-top: 10px;"><strong>Obat</strong></td>
                <td style="padding-top: 10px;"></td>
            </tr>

            <?php foreach($input['obat'] as $keys=>$rows): ?>
                <tr>
                    <td style="width: 500px; border-bottom: 1px dotted black; padding-top: 10px;"><?php echo $input['obat_desc'][$keys]; ?></td>
                    <td style="width: 500px; padding-top: 10px;"><?php echo format_number($rows, TRUE); ?></td>
                </tr>
            <?php endforeach; ?>

            <tr>
                <td style="padding-top: 10px;"></td>
                <td style="padding-top: 10px; font-weight: bold;"><?php echo format_number($input['total_obat'], TRUE); ?></td>
            </tr>
        <?php endif; ?>

        <tr>
            <td></td>
        </tr>

        <?php if(isset($input['alkes'])): ?>
            <tr>
                <td style="padding-top: 10px;"><strong>Alat Kesehatan</strong></td>
                <td style="padding-top: 10px;"></td>
            </tr>

            <?php foreach($input['alkes'] as $keys=>$rows): ?>
                <tr>
                    <td style="width: 500px; border-bottom: 1px dotted black; padding-top: 10px;"><?php echo $input['nama_alkes'][$keys]; ?></td>
                    <td style="width: 500px; padding-top: 10px;"><?php echo format_number($rows, TRUE); ?></td>
                </tr>
            <?php endforeach; ?>

            <tr>
                <td style="padding-top: 10px;"></td>
                <td style="padding-top: 10px; font-weight: bold;"><?php echo format_number($input['total_alkes'], TRUE); ?></td>
            </tr>
        <?php endif; ?>

        <tr>
            <td></td>
        </tr>

        <?php if(isset($input['obat_pulang'])): ?>
            <tr>
                <td style="padding-top: 10px;"><strong>Obat Pulang</strong></td>
                <td style="padding-top: 10px;"></td>
            </tr>

            <?php foreach($input['obat_pulang'] as $keys=>$rows): ?>
                <tr>
                    <td style="width: 500px; border-bottom: 1px dotted black; padding-top: 10px;"><?php echo $input['obat_pulang_desc'][$keys]; ?></td>
                    <td style="width: 500px; padding-top: 10px;"><?php echo format_number($rows, TRUE); ?></td>
                </tr>
            <?php endforeach; ?>

            <tr>
                <td style="padding-top: 10px;"></td>
                <td style="padding-top: 10px; font-weight: bold;"><?php echo format_number($input['total_obat_pulang'], TRUE); ?></td>
            </tr>
        <?php endif; ?>

        <tr>
            <td></td>
        </tr>

        <?php if(isset($input['gizi'])): ?>
            <tr>
                <td style="padding-top: 10px;"><strong>Gizi</strong></td>
                <td style="padding-top: 10px;"></td>
            </tr>

            <?php foreach($input['gizi'] as $keys=>$rows): ?>
                <tr>
                    <td style="width: 500px; border-bottom: 1px dotted black; padding-top: 10px;"><?php echo $input['nama_gizi'][$keys]; ?></td>
                    <td style="width: 500px; padding-top: 10px;"><?php echo format_number($rows, TRUE); ?></td>
                </tr>
            <?php endforeach; ?>

            <tr>
                <td style="padding-top: 10px;"></td>
                <td style="padding-top: 10px; font-weight: bold;"><?php echo format_number($input['total_gizi'], TRUE); ?></td>
            </tr>
        <?php endif; ?>

        <tr>
            <td></td>
        </tr>

        <?php if(isset($input['visit_dokter'])): ?>
            <tr>
                <td style="padding-top: 10px;"><strong>Visit Dokter</strong></td>
                <td style="padding-top: 10px;"></td>
            </tr>

            <?php foreach($input['visit_dokter'] as $keys=>$rows): ?>
                <tr>
                    <td style="width: 500px; border-bottom: 1px dotted black; padding-top: 10px;"><?php echo $input['nama_visit_dokter'][$keys]; ?></td>
                    <td style="width: 500px; padding-top: 10px;"><?php echo format_number($rows, TRUE); ?></td>
                </tr>
            <?php endforeach; ?>

            <tr>
                <td style="padding-top: 10px;"></td>
                <td style="padding-top: 10px; font-weight: bold;"><?php echo format_number($input['total_visit_dokter'], TRUE); ?></td>
            </tr>
        <?php endif; ?>

        <tr>
            <td></td>
        </tr>

        <?php if(isset($input['laboratorium'])): ?>
            <tr>
                <td style="padding-top: 10px;"><strong>Laboratorium</strong></td>
                <td style="padding-top: 10px;"></td>
            </tr>

            <?php foreach($input['laboratorium'] as $keys=>$rows): ?>
                <tr>
                    <td style="width: 500px; border-bottom: 1px dotted black; padding-top: 10px;"><?php echo $input['nama_laboratorium'][$keys]; ?></td>
                    <td style="width: 500px; padding-top: 10px;"><?php echo format_number($rows, TRUE); ?></td>
                </tr>
            <?php endforeach; ?>

            <tr>
                <td style="padding-top: 10px;"></td>
                <td style="padding-top: 10px; font-weight: bold;"><?php echo format_number($input['total_laboratorium'], TRUE); ?></td>
            </tr>
        <?php endif; ?>

        <tr>
            <td></td>
        </tr>

        <?php if(isset($input['fisioterapi'])): ?>
            <tr>
                <td style="padding-top: 10px;"><strong>Fisioterapi</strong></td>
                <td style="padding-top: 10px;"></td>
            </tr>

            <?php foreach($input['fisioterapi'] as $keys=>$rows): ?>
                <tr>
                    <td style="width: 500px; border-bottom: 1px dotted black; padding-top: 10px;"><?php echo $input['nama_fisioterapi'][$keys]; ?></td>
                    <td style="width: 500px; padding-top: 10px;"><?php echo format_number($rows, TRUE); ?></td>
                </tr>
            <?php endforeach; ?>

            <tr>
                <td style="padding-top: 10px;"></td>
                <td style="padding-top: 10px; font-weight: bold;"><?php echo format_number($input['total_fisioterapi'], TRUE); ?></td>
            </tr>
        <?php endif; ?>

        <tr>
            <td></td>
        </tr>

        <?php if(isset($input['rontgen'])): ?>
            <tr>
                <td style="padding-top: 10px;"><strong>Rontgen</strong></td>
                <td style="padding-top: 10px;"></td>
            </tr>

            <?php foreach($input['rontgen'] as $keys=>$rows): ?>
                <tr>
                    <td style="width: 500px; border-bottom: 1px dotted black; padding-top: 10px;"><?php echo $input['nama_rontgen'][$keys]; ?></td>
                    <td style="width: 500px; padding-top: 10px;"><?php echo format_number($rows, TRUE); ?></td>
                </tr>
            <?php endforeach; ?>

            <tr>
                <td style="padding-top: 10px;"></td>
                <td style="padding-top: 10px; font-weight: bold;"><?php echo format_number($input['total_rontgen'], TRUE); ?></td>
            </tr>
        <?php endif; ?>

        <tr>
            <td></td>
        </tr>

        <?php if(isset($input['tindakan'])): ?>
            <tr>
                <td style="padding-top: 10px;"><strong>Tindakan</strong></td>
                <td style="padding-top: 10px;"></td>
            </tr>

            <?php foreach($input['tindakan'] as $keys=>$rows): ?>
                <tr>
                    <td style="width: 500px; border-bottom: 1px dotted black; padding-top: 10px;"><?php echo $input['nama_tindakan'][$keys]; ?></td>
                    <td style="width: 500px; padding-top: 10px;"><?php echo format_number($rows, TRUE); ?></td>
                </tr>
            <?php endforeach; ?>

            <tr>
                <td style="padding-top: 10px;"></td>
                <td style="padding-top: 10px; font-weight: bold;"><?php echo format_number($input['total_tindakan'], TRUE); ?></td>
            </tr>
        <?php endif; ?>

        <tr><td style="padding: 10px"></td></tr>     

        <tr>
            <td style="padding-top: 10px; border-bottom: 1px dotted black; padding-top: 10px;"><strong>Administrasi</strong></td>
            <td style="padding-top: 10px; font-weight: bold;"><?php echo format_number($input['administrasi'], TRUE); ?></td>
        </tr>

        <?php if(!empty($input['bpjs_total'])): ?>
            <tr>
                <td style="padding-top: 10px; border-bottom: 1px dotted black; padding-top: 10px;"><strong>BPJS (<?php echo $input['bpjs_hari'] . " Hari x " . format_number($input['bpjs_tarif']); ?>)</strong></td>
                <td style="padding-top: 10px; font-weight: bold;"><?php echo format_number($input['bpjs_total'], TRUE); ?></td>
            </tr>
        <?php endif; ?>

        <tr>
            <td style="padding-top: 10px; border-bottom: 1px dotted black; padding-top: 10px;"><strong>Total</strong></td>
            <td style="padding-top: 10px; font-weight: bold;"><?php echo format_number($input['total_keseluruhan'], TRUE); ?></td>
        </tr>
    </table>

    <table style="margin-top: 50px;">
        <tr>
            <td style="width: 350px;">
                <div style="text-align: center">Mengetahui</div> <br>
                <div style="text-align: center">Kepala Rumah Sakit</div>
                <div style="text-align: center">Budi Sehat</div>
                <div style="text-align: center"><br></div>
                <div style="text-align: center"><br></div>
                <div style="text-align: center"><br></div>
                <div style="text-align: center"><br></div>  
                <div style="text-align: center"><br></div>                
            </td>
            <td style="width: 300px;">
                <div style="text-align: center">Purworejo, <?php echo date('d-m-Y'); ?></div> <br>
                <div style="text-align: center">Yang membuat rekening</div>
                <div style="text-align: center"><br></div>
                <div style="text-align: center"><br></div>
                <div style="text-align: center"><br></div>
                <div style="text-align: center"><br></div>
                <div style="text-align: center"><br></div>  
                <div style="text-align: center"><?php echo $input['pembuat_rekening']; ?></div>
            </td>
        </tr>
    </table>
</div>