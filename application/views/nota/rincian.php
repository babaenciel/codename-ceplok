<div id="main-container">
    <div class="padding-md">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php $this->load->view('elements/error_message'); ?>
                <form action="<?php echo site_url('nota/cetak_umum'); ?>" method="POST" id="formToggleLine" class="form-horizontal no-margin">
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Sudah terima dari</label>
                        <div class="col-lg-5">
                            <?php
                                echo form_input(
                                    'sudah_terima_dari',
                                    (set_value('sudah_terima_dari')) ? set_value('sudah_terima_dari') : $form['sudah_terima_dari'],
                                    'class="form-control"'
                                );
                            ?>
                        </div><!-- /.col -->
                    </div><!-- /form-group -->
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Alamat</label>
                        <div class="col-lg-5">
                            <?php
                                echo form_input(
                                    'alamat_dari',
                                    (set_value('alamat_dari')) ? set_value('alamat_dari') : $form['alamat_dari'],
                                    'class="form-control"'
                                );
                            ?>
                        </div><!-- /.col -->
                    </div><!-- /form-group -->
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Untuk pembayaran ongkos-ongkos</label>
                        <div class="col-lg-5">
                            <?php
                                echo form_input(
                                    'untuk',
                                    (set_value('untuk')) ? set_value('untuk') : $form['untuk'],
                                    'class="form-control"'
                                );
                            ?>
                        </div><!-- /.col -->
                    </div><!-- /form-group -->
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Alamat</label>
                        <div class="col-lg-5">
                            <?php
                                echo form_input(
                                    'alamat_untuk',
                                    (set_value('alamat_untuk')) ? set_value('alamat_untuk') : $form['alamat_untuk'],
                                    'class="form-control"'
                                );
                            ?>
                        </div><!-- /.col -->
                    </div><!-- /form-group -->

                    <br><br>

                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Keterangan</th>
                                <th>Jumlah</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>
                                    Kamar / Perawatan <br>
                                    Tgl 20-02-2014 s/d tgl 21-02-2014 = 1 hari @ 25.000
                                </td>
                                <td>Rp 125.000</td>
                            </tr>
                            <tr>
                                <td>Obat</td>
                                <td>Rp 150.000</td>
                            </tr>
                            <tr>
                                <td>Visit Dokter</td>
                                <td>Rp 150.000</td>
                            </tr>
                            <tr>
                                <td>Laboratorium</td>
                                <td>Rp 150.000</td>
                            </tr>
                            <tr>
                                <td>Administrasi</td>
                                <td><input type="text" name="administrasi" value="15000"></td>
                            </tr>
                            <tr>
                                <td>Alat</td>
                                <td>Rp 150.000</td>
                            </tr>
                            <tr>
                                <td>Tindakan</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>----- Persalinan</td>
                                <td>Rp 150.000</td>
                            </tr>
                            <tr>
                                <td>----- Operasi Kecil</td>
                                <td>Rp 150.000</td>
                            </tr>
                            <tr>
                                <td>----- Cathether</td>
                                <td>Rp 150.000</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td style="float:right">Jumlah</td>
                                <td>Rp 1.000.000</td>
                            </tr>
                            <tr>
                                <td style="float:right">Sudah Bayar</td>
                                <td><input type="text" name="bayar" value="1000000"></td>
                            </tr>
                            <tr>
                                <td style="float:right">Sisa</td>
                                <td>Rp 1.000.000</td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div><!-- /panel -->
    </div><!-- /.padding-md -->
</div><!-- /main-container -->