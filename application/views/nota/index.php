<?php //dump($this->input->post());?>

<div id="main-container">
    <div class="padding-md">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php $this->load->view('elements/error_message'); ?>
                <?php $this->load->view('elements/nomor_nota'); ?>

                <form action="" method="POST" id="form-nota" class="form-horizontal no-margin">
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Sudah terima dari</label>
                        <div class="col-lg-5">
                            <?php
                                echo form_input(
                                    'sudah_terima_dari',
                                    (set_value('sudah_terima_dari')) ? set_value('sudah_terima_dari') : $form['sudah_terima_dari'],
                                    'class="form-control"'
                                );
                            ?>
                        </div><!-- /.col -->
                    </div><!-- /form-group -->
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Alamat</label>
                        <div class="col-lg-5">
                            <?php
                                echo form_input(
                                    'alamat_dari',
                                    (set_value('alamat_dari')) ? set_value('alamat_dari') : $form['alamat_dari'],
                                    'class="form-control"'
                                );
                            ?>
                        </div><!-- /.col -->
                    </div><!-- /form-group -->
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Untuk Biaya Rawat Inap Pasien: </label>
                        <div class="col-lg-5">
                            <?php
                                echo form_input(
                                    'untuk',
                                    (set_value('untuk')) ? set_value('untuk') : $form['untuk'],
                                    'class="form-control"'
                                );
                            ?>
                        </div><!-- /.col -->
                    </div><!-- /form-group -->
                    <?php if(false): ?>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Alamat</label>
                        <div class="col-lg-5">
                            <?php
                                echo form_input(
                                    'alamat_untuk',
                                    (set_value('alamat_untuk')) ? set_value('alamat_untuk') : $form['alamat_untuk'],
                                    'class="form-control"'
                                );
                            ?>
                        </div><!-- /.col -->
                    </div><!-- /form-group -->
                    <?php endif; ?>
                    <div class="form-group">
                        <label class="col-lg-2 control-label"></label>
                        <div class="col-lg-5">
                            <?php
                                echo form_checkbox(
                                    'atas_permintaan_sendiri',
                                    '1',
                                    (set_checkbox('atas_permintaan_sendiri', '1')) ? set_checkbox('atas_permintaan_sendiri', '1') : $form['atas_permintaan_sendiri'],
                                    'class="form-control" id="atas-permintaan-sendiri"'
                                );
                            ?>

                            <span class="custom-checkbox"></span> Atas Permintaan Sendiri
                            <div class="clearfix"></div>
                        </div><!-- /.col -->
                    </div><!-- /form-group -->

                    <br><br>

                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="col-md-9" style="text-align: center">Keterangan</th>
                                <th class="col-md-3">Jumlah</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php if(!empty($kamar)): ?>
                                <tr>
                                    <td>
                                        Kamar / Perawatan
                                    </td>
                                    <td id="total-kamar"><?php echo format_number($kamar_total, TRUE); ?></td>
                                    <input type="hidden" name="total_kamar" value="<?php echo $kamar_total; ?>" id="total-kamar-field">
                                </tr>

                                <?php foreach($kamar as $rows): ?>
                                    <tr>
                                        <td>
                                            <i class="fa fa-long-arrow-right fa-lg"></i> <?php echo $rows->nama_kamar; ?>
                                            Tgl <?php echo conv_date_format($rows->tgl_mulai, 'd-m-Y'); ?> s/d <?php echo $rows->tgl_akhir ? 'Tgl ' . conv_date_format($rows->tgl_akhir, 'd-m-Y') : '-'; ?>
                                            = <?php echo $rows->diff_date; ?> hari @ <span class="tarif-kamar-satuan-text"><?php echo format_number($rows->tarif_kamar, TRUE); ?></span>
                                        </td>
                                        <td><input type="text" name="kamar[]" value="<?php echo $rows->total_satu_kamar; ?>" class="form-format-number form-kamar"></td>
                                        <input type="hidden" name="nama_kamar[]" value="<?php echo $rows->nama_kamar.' Tgl '.conv_date_format($rows->tgl_mulai, 'd-m-Y').' s/d Tgl '.conv_date_format($rows->tgl_akhir, 'd-m-Y').' = '.$rows->diff_date.' hari @ ['.format_number($rows->tarif_kamar, TRUE).']'; ?>">
                                        <input type="hidden" name="banyak_kamar[]" value="<?php echo $rows->diff_date; ?>" class="banyak-kamar-satuan">
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <?php if(!empty($obat)): ?>
                                <tr>
                                    <td>Obat</td>
                                    <td id="total-obat"><?php echo format_number($obat_total, TRUE); ?></td>
                                    <input type="hidden" name="total_obat" value="<?php echo $obat_total; ?>" id="total-obat-field">
                                </tr>

                                <?php foreach($obat as $rows): ?>
                                    <tr>
                                        <td>
                                            <i class="fa fa-long-arrow-right fa-lg"></i> <span class="tarif-obat-satuan-text"><?php echo $rows->obat_desc_clean; ?></span>
                                        </td>
                                        <td><input type="text" name="obat[]" value="<?php echo $rows->total_satu_obat; ?>" class="form-format-number form-obat"></td>
                                        <input type="hidden" name="obat_desc[]" value="<?php echo $rows->obat_desc; ?>">
                                        <input type="hidden" name="banyak_obat[]" value="<?php echo $rows->satuan; ?>" class="banyak-obat-satuan">
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <?php if(!empty($obat_pulang)): ?>
                                <tr>
                                    <td>Obat Pulang</td>
                                    <td id="total-obat-pulang"><?php echo format_number($obat_pulang_total, TRUE); ?></td>
                                    <input type="hidden" name="total_obat_pulang" value="<?php echo $obat_pulang_total; ?>" id="total-obat-pulang-field">
                                </tr>

                                <?php foreach($obat_pulang as $rows): ?>
                                    <tr>
                                        <td>
                                            <i class="fa fa-long-arrow-right fa-lg"></i> <span class="tarif-obat-pulang-satuan-text"><?php echo $rows->obat_pulang_desc_clean; ?></span>
                                        </td>
                                        <td><input type="text" name="obat_pulang[]" value="<?php echo $rows->total_satu_obat_pulang; ?>" class="form-format-number form-obat-pulang"></td>
                                        <input type="hidden" name="obat_pulang_desc[]" value="<?php echo $rows->obat_pulang_desc; ?>">
                                        <input type="hidden" name="banyak_obat_pulang[]" value="<?php echo $rows->satuan; ?>" class="banyak-obat-pulang-satuan">
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <?php if(!empty($alkes)): ?>
                                <tr>
                                    <td>Alat</td>
                                    <td id="total-alkes"><?php echo format_number($alkes_total, TRUE); ?></td>
                                    <input type="hidden" name="total_alkes" value="<?php echo $alkes_total; ?>" id="total-alkes-field">
                                </tr>

                                <?php foreach($alkes as $rows): ?>
                                    <tr>
                                        <td>
                                            <i class="fa fa-long-arrow-right fa-lg"></i> <?php echo $rows->alkes_desc; ?>
                                        </td>
                                        <td><input type="text" name="alkes[]" value="<?php echo $rows->total_satu_alkes; ?>" class="form-format-number form-alkes"></td>
                                        <input type="hidden" name="nama_alkes[]" value="<?php echo $rows->alkes_desc; ?>">
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <?php if(!empty($gizi)): ?>
                                <tr>
                                    <td>
                                        Gizi
                                    </td>
                                    <td id="total-gizi"><?php echo format_number($gizi_total, TRUE); ?></td>
                                    <input type="hidden" name="total_gizi" value="<?php echo $gizi_total; ?>" id="total-gizi-field">
                                </tr>

                                <?php foreach($gizi as $rows): ?>
                                    <tr>
                                        <td>
                                            <i class="fa fa-long-arrow-right fa-lg"></i> <?php echo $rows->nama_gizi; ?>
                                            Tgl <?php echo conv_date_format($rows->tgl_mulai, 'd-m-Y'); ?> s/d <?php echo $rows->tgl_akhir ? 'Tgl ' . conv_date_format($rows->tgl_akhir, 'd-m-Y') : '-'; ?>
                                            = <?php echo $rows->diff_date; ?> hari @ <?php echo format_number($rows->tarif, TRUE); ?>
                                        </td>
                                        <td><input type="text" name="gizi[]" value="<?php echo $rows->total_satu_gizi; ?>" class="form-format-number form-gizi"></td>
                                        <input type="hidden" name="nama_gizi[]" value="<?php echo $rows->nama_gizi.' Tgl '.conv_date_format($rows->tgl_mulai, 'd-m-Y').' s/d Tgl '.conv_date_format($rows->tgl_akhir, 'd-m-Y').' = '.$rows->diff_date.' hari @ '.format_number($rows->tarif, TRUE); ?>">
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <?php if(!empty($visit_dokter)): ?>
                                <tr>
                                    <td>Visit Dokter</td>
                                    <td id="total-visit"><?php echo format_number($visit_dokter_total, TRUE); ?></td>
                                    <input type="hidden" name="total_visit_dokter" value="<?php echo $visit_dokter_total; ?>" id="total-visit-field">
                                </tr>

                                <?php foreach($visit_dokter as $rows): ?>
                                    <tr>
                                        <td>
                                            <i class="fa fa-long-arrow-right fa-lg"></i> <?php echo $rows->jenis_dokter . ' ' . $rows->count_layanan . 'x @ ' ?> <?php echo format_number($rows->tarif, TRUE); ?>
                                        </td>
                                        <td><input type="text" name="visit_dokter[]" value="<?php echo $rows->total_tarif; ?>" class="form-format-number form-visit"></td>
                                        <input type="hidden" name="nama_visit_dokter[]" value="<?php echo $rows->jenis_dokter . ' ' . $rows->count_layanan . 'x @ ' ?> <?php echo format_number($rows->tarif, TRUE); ?>">
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <?php if(!empty($laboratorium)): ?>
                                <tr>
                                    <td>Laboratorium</td>
                                    <td id="total-laboratorium"><?php echo format_number($laboratorium_total, TRUE); ?></td>
                                    <input type="hidden" name="total_laboratorium" value="<?php echo $laboratorium_total; ?>" id="total-laboratorium-field">
                                </tr>

                                <?php foreach($laboratorium as $rows): ?>
                                    <tr>
                                        <td>
                                            <i class="fa fa-long-arrow-right fa-lg"></i> <?php echo $rows->nama_layanan . ' ' . $rows->count_layanan . 'x @ ' ?> <?php echo format_number($rows->tarif, TRUE); ?>
                                        </td>
                                        <td><input type="text" name="laboratorium[]" value="<?php echo $rows->total_tarif; ?>" class="form-format-number form-laboratorium"></td>
                                        <input type="hidden" name="nama_laboratorium[]" value="<?php echo $rows->nama_layanan . ' ' . $rows->count_layanan . 'x @ ' ?> <?php echo format_number($rows->tarif, TRUE); ?>">
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <?php if(!empty($fisioterapi)): ?>
                                <tr>
                                    <td>Fisioterapi</td>
                                    <td id="total-fisioterapi"><?php echo format_number($fisioterapi_total, TRUE); ?></td>
                                    <input type="hidden" name="total_fisioterapi" value="<?php echo $fisioterapi_total; ?>" id="total-fisioterapi-field">
                                </tr>

                                <?php foreach($fisioterapi as $rows): ?>
                                    <tr>
                                        <td>
                                            <i class="fa fa-long-arrow-right fa-lg"></i> <?php echo $rows->nama_terapi . ' ' . $rows->count_layanan . 'x @ ' ?> <?php echo format_number($rows->tarif, TRUE); ?>
                                        </td>
                                        <td><input type="text" name="fisioterapi[]" value="<?php echo $rows->total_tarif; ?>" class="form-format-number form-fisioterapi"></td>
                                        <input type="hidden" name="nama_fisioterapi[]" value="<?php echo $rows->nama_terapi . ' ' . $rows->count_layanan . 'x @ ' ?> <?php echo format_number($rows->tarif, TRUE); ?>">
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <?php if(!empty($rontgen)): ?>
                                <tr>
                                    <td>Rontgen</td>
                                    <td id="total-rontgen"><?php echo format_number($rontgen_total, TRUE); ?></td>
                                    <input type="hidden" name="total_rontgen" value="<?php echo $rontgen_total; ?>" id="total-rontgen-field">
                                </tr>

                                <?php foreach($rontgen as $rows): ?>
                                    <tr>
                                        <td>
                                            <i class="fa fa-long-arrow-right fa-lg"></i> <?php echo $rows->nama_layanan . ' ' . $rows->count_layanan . 'x @ ' ?> <?php echo format_number($rows->tarif, TRUE); ?>
                                        </td>
                                        <td><input type="text" name="rontgen[]" value="<?php echo $rows->total_tarif; ?>" class="form-format-number form-rontgen"></td>
                                        <input type="hidden" name="nama_rontgen[]" value="<?php echo $rows->nama_layanan . ' ' . $rows->count_layanan . 'x @ ' ?> <?php echo format_number($rows->tarif, TRUE); ?>">
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <?php if(!empty($tindakan)): ?>
                                <tr>
                                    <td>Tindakan</td>
                                    <td id="total-tindakan"><?php echo format_number($tindakan_total, TRUE); ?></td>
                                    <input type="hidden" name="total_tindakan" value="<?php echo $tindakan_total; ?>" id="total-tindakan-field">
                                </tr>

                                <?php foreach($tindakan as $rows): ?>
                                    <?php
                                        if($rows->tindakan_lainnya == 0) {
                                            $nama_tindakan = $rows->nama_tindakan . ' ' . $rows->count_tindakan . 'x @ ' . format_number($rows->tarif, TRUE);
                                        }else {
                                            $nama_tindakan = $rows->nama_tindakan;
                                        }
                                    ?>
                                    <tr>
                                        <td>
                                            <i class="fa fa-long-arrow-right fa-lg"></i> <?php echo $nama_tindakan; ?>
                                        </td>
                                        <td><input type="text" name="tindakan[]" value="<?php echo $rows->total_tarif; ?>" class="form-format-number form-tindakan"></td>
                                        <input type="hidden" name="nama_tindakan[]" value="<?php echo $nama_tindakan; ?>">
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <tr>
                                <td>Administrasi</td>
                                <td><input type="text" name="administrasi" value="<?php echo $administrasi; ?>" class="form-format-number" id="administrasi"></td>
                            </tr>

                            <tr>
                                <td>BPJS</td>
                                <td></td>
                            </tr>

                            <tr>
                                <td>
                                    <i class="fa fa-long-arrow-right fa-lg"></i> Hari
                                </td>
                                <td>
                                    <input type="text" name="bpjs_hari" class="form-format-number form-bpjs-hari" style="margin-bottom:10px">
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <i class="fa fa-long-arrow-right fa-lg"></i> Tarif per Hari
                                </td>
                                <td>
                                    <input type="text" name="bpjs_tarif" class="form-format-number form-bpjs-tarif" style="margin-bottom:10px">
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <i class="fa fa-long-arrow-right fa-lg"></i> Total (Hari * Tarif per Hari)
                                </td>
                                <td>
                                    <input type="text" name="bpjs_total" class="form-format-number form-bpjs-total" style="margin-bottom:10px">
                                </td>
                            </tr>
                            <!-- <tr>
                                <td>----- Persalinan</td>
                                <td>Rp 150.000</td>
                            </tr>
                            <tr>
                                <td>----- Operasi Kecil</td>
                                <td>Rp 150.000</td>
                            </tr>
                            <tr>
                                <td>----- Cathether</td>
                                <td>Rp 150.000</td>
                            </tr> -->
                            <tr>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td style="float:right">Jumlah</td>
                                <td id="total-keseluruhan"><?php echo format_number($jumlah_nota, TRUE); ?></td>
                                <input type="hidden" name="total_keseluruhan" value="<?php echo $jumlah_nota; ?>" id="total-keseluruhan-field">
                            </tr>
                            <!-- <tr>
                                <td style="float:right">Sudah Bayar</td>
                                <td><input type="text" name="bayar" value=""></td>
                            </tr>
                            <tr>
                                <td style="float:right">Sisa</td>
                                <td></td>
                            </tr> -->
                        </tbody>
                    </table>

                    <div class="form-group">

                        <div class="pull-right" style="margin-right: 50px">
                            <a class="btn btn-primary cetak" id="cetak-umum">Cetak Umum</a>
                            <a class="btn btn-primary cetak" id="cetak-rincian">Cetak Rincian</a>
                        </div><!-- /.col -->
                    </div><!-- /form-group -->
                </form>
            </div>
        </div><!-- /panel -->
    </div><!-- /.padding-md -->
</div><!-- /main-container -->

<script type="text/javascript">
    // UNTUK FORMAT NUMBER
    $("document").ready(function(){
        $('.form-format-number').each(function(index, elm) {
            $newval = formatprice($(elm).val());
            $(elm).val($newval);
        });
    });

    $(".form-format-number").keyup(function() {
        var $newval = formatprice($(this).val());
        // console.log($(this).attr('class'));
        hitung_total($(this).val(), $(this));
        $(this).val($newval);
    });

    $("input[type=submit]").click(function (e) {
        $('.form-format-number').each(function(index, elm) {
            var value = $(elm).val();
            value = value.replace(/\./g, "");
            $(elm).val(value);
        });
    });

    $('#form-nota').submit(function(e) {
        // e.preventDefault();
    });

    $('.cetak').on('click', function(e) {
            e.preventDefault();

            var mybutton = $(this);
            $.prompt("Apakah Anda yakin ingin mencetak nota ini?", {
                buttons: { "Yakin": true, "Batal": false },
                submit: function(e, v, m, f) {
                    if (v) {
                        $('.form-format-number').each(function(index, elm) {
                            var value = $(elm).val();
                            value = value.replace(/\./g, "");
                            $(elm).val(value);
                        });

                        if(mybutton.attr('id') === 'cetak-umum') {
                            $('#form-nota').attr('action', '<?php echo site_url("nota/cetak_umum/".$id); ?>');
                        }else if(mybutton.attr('id') === 'cetak-rincian') {
                            $('#form-nota').attr('action', '<?php echo site_url("nota/cetak_rincian/".$id); ?>');
                        }

                        $('#form-nota').submit();
                        // var href = mybutton.attr('action');
                        // $('#form-nota').submit();

                        $('.form-format-number').each(function(index, elm) {
                            $newval = formatprice($(elm).val());
                            $(elm).val($newval);
                        });
                    }
                }
            });
        });

    function formatprice($oldval){
        var nStr = $oldval + '';
        nStr = nStr.replace( /\./g, "");
        var x = nStr.split( '.' );
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while ( rgx.test(x1) ) {
            x1 = x1.replace( rgx, '$1' + '.' + '$2' );
        }

        return x1 + x2;
    }

    function hitung_total(value, element) {
        var total = 0;
        value = parseInt(value.replace(/\./g, ""));

        if(element.is('.form-alkes')) {
            $('.form-alkes').each(function(index, elm) {
                total += parseInt($(this).val().replace(/\./g, ""));
                // console.log($(this).val());
            });

            $('#total-alkes-field').val(total);
            total = 'Rp ' + formatprice(total);
            $('#total-alkes').html(total);
        }else if(element.is('.form-kamar')) {
            $('.form-kamar').each(function(index, elm) {
                total += parseInt($(this).val().replace(/\./g, ""));
            });

            $('#total-kamar-field').val(total);
            var totalFormatted = 'Rp ' + formatprice(total);
            $('#total-kamar').html(totalFormatted);

            // change text tarif kamar
            var banyak_kamar_satuan = element.parent().parent().find('input[name="banyak_kamar[]"]').val();
            var tarif_kamar_satuan = value / banyak_kamar_satuan;
            tarif_kamar_satuan = tarif_kamar_satuan | 0;
            tarif_kamar_satuan = 'Rp ' + formatprice(tarif_kamar_satuan);
            element.parent().parent().find('.tarif-kamar-satuan-text').html(tarif_kamar_satuan);

            // change input kamar
            var nama_kamar = element.parent().parent().find('input[name="nama_kamar[]"]');
            var text_replaced = nama_kamar.val().replace(/\[(.+?)\]/g, "["+tarif_kamar_satuan+"]")
            nama_kamar.val(text_replaced);
        }else if(element.is('.form-obat')) {
            $('.form-obat').each(function(index, elm) {
                total += parseInt($(this).val().replace(/\./g, ""));
                // console.log($(this).val());
            });

            $('#total-obat-field').val(total);
            total = 'Rp ' + formatprice(total);
            $('#total-obat').html(total);

            // change text tarif obat
            var banyak_obat_satuan = element.parent().parent().find('input[name="banyak_obat[]"]').val();
            var tarif_obat_satuan = value / banyak_obat_satuan;
            tarif_obat_satuan = tarif_obat_satuan | 0;
            tarif_obat_satuan = 'Rp ' + formatprice(tarif_obat_satuan);
            var obat_desc = element.parent().parent().find('input[name="obat_desc[]"]');
            var new_val = obat_desc.val().replace(/\[(.+?)\]/g, "["+tarif_obat_satuan+"]");
            obat_desc.val(new_val);
            var clean = obat_desc.val().replace('[', ' ');
            clean = clean.replace(']', ' ');
            element.parent().parent().find('.tarif-obat-satuan-text').html(clean);

        }else if(element.is('.form-obat-pulang')) {
            $('.form-obat-pulang').each(function(index, elm) {
                total += parseInt($(this).val().replace(/\./g, ""));
                // console.log($(this).val());
            });

            $('#total-obat-pulang-field').val(total);
            total = 'Rp ' + formatprice(total);
            $('#total-obat-pulang').html(total);

            // change text tarif obat
            var banyak_obat_pulang_satuan = element.parent().parent().find('input[name="banyak_obat_pulang[]"]').val();
            var tarif_obat_pulang_satuan = value / banyak_obat_pulang_satuan;
            tarif_obat_pulang_satuan = tarif_obat_pulang_satuan | 0;
            tarif_obat_pulang_satuan = 'Rp ' + formatprice(tarif_obat_pulang_satuan);
            var obat_pulang_desc = element.parent().parent().find('input[name="obat_pulang_desc[]"]');
            var new_val = obat_pulang_desc.val().replace(/\[(.+?)\]/g, "["+tarif_obat_pulang_satuan+"]");
            obat_pulang_desc.val(new_val);
            var clean = obat_pulang_desc.val().replace('[', ' ');
            clean = clean.replace(']', ' ');
            element.parent().parent().find('.tarif-obat-pulang-satuan-text').html(clean);
        }else if(element.is('.form-laboratorium')) {
            $('.form-laboratorium').each(function(index, elm) {
                total += parseInt($(this).val().replace(/\./g, ""));
                // console.log($(this).val());
            });

            $('#total-laboratorium-field').val(total);
            total = 'Rp ' + formatprice(total);
            $('#total-laboratorium').html(total);
        }else if(element.is('.form-tindakan')) {
            $('.form-tindakan').each(function(index, elm) {
                total += parseInt($(this).val().replace(/\./g, ""));
                // console.log($(this).val());
            });

            $('#total-tindakan-field').val(total);
            total = 'Rp ' + formatprice(total);
            $('#total-tindakan').html(total);
        }else if(element.is('.form-visit')) {
            $('.form-visit').each(function(index, elm) {
                total += parseInt($(this).val().replace(/\./g, ""));
                // console.log($(this).val());
            });

            $('#total-visit-field').val(total);
            total = 'Rp ' + formatprice(total);
            $('#total-visit').html(total);
        }else if(element.is('.form-gizi')) {
            $('.form-gizi').each(function(index, elm) {
                total += parseInt($(this).val().replace(/\./g, ""));
                // console.log($(this).val());
            });

            $('#total-gizi-field').val(total);
            total = 'Rp ' + formatprice(total);
            $('#total-gizi').html(total);
        }else if(element.is('.form-fisioterapi')) {
            $('.form-fisioterapi').each(function(index, elm) {
                total += parseInt($(this).val().replace(/\./g, ""));
                // console.log($(this).val());
            });

            $('#total-fisioterapi-field').val(total);
            total = 'Rp ' + formatprice(total);
            $('#total-fisioterapi').html(total);
        }else if(element.is('.form-rontgen')) {
            $('.form-rontgen').each(function(index, elm) {
                total += parseInt($(this).val().replace(/\./g, ""));
                // console.log($(this).val());
            });

            $('#total-rontgen-field').val(total);
            total = 'Rp ' + formatprice(total);
            $('#total-rontgen').html(total);
        }else if(element.is(".form-bpjs-hari") || element.is(".form-bpjs-tarif")) {
            var hari = parseInt($(".form-bpjs-hari").val().replace(/\./g, ""));
            var tarif = parseInt($(".form-bpjs-tarif").val().replace(/\./g, ""));
            var bpjs_total = hari * tarif;

            if(isNaN(bpjs_total)) {
                bpjs_total = 0;
            }

            $(".form-bpjs-total").val(formatprice(bpjs_total));
        }

        hitung_total_keseluruhan();
        // console.log(value);
    }

    function hitung_total_keseluruhan() {
        // console.log('masuk');
        var total_kamar = $('#total-kamar').text();
        if(total_kamar == '') {
            total_kamar = 0;
        }else {
            total_kamar = total_kamar.replace(/\./g, "");
            total_kamar = total_kamar.replace('Rp ', '');
        }

        var total_obat = $('#total-obat').text();
        if(total_obat == '') {
            total_obat = 0;
        }else {
            total_obat = total_obat.replace(/\./g, "");
            total_obat = total_obat.replace('Rp ', '');
        }

        var total_obat_pulang = $('#total-obat-pulang').text();
        if(total_obat_pulang == '') {
            total_obat_pulang = 0;
        }else {
            total_obat_pulang = total_obat_pulang.replace(/\./g, "");
            total_obat_pulang = total_obat_pulang.replace('Rp ', '');
        }

        var total_alkes = $('#total-alkes').text();
        if(total_alkes == '') {
            total_alkes = 0;
        }else {
            total_alkes = total_alkes.replace(/\./g, "");
            total_alkes = total_alkes.replace('Rp ', '');
        }

        var total_laboratorium = $('#total-laboratorium').text();
        if(total_laboratorium == '') {
            total_laboratorium = 0;
        }else {
            total_laboratorium = total_laboratorium.replace(/\./g, "");
            total_laboratorium = total_laboratorium.replace('Rp ', '');
        }

        var total_visit = $('#total-visit').text();
        if(total_visit == '') {
            total_visit = 0;
        }else {
            total_visit = total_visit.replace(/\./g, "");
            total_visit = total_visit.replace('Rp ', '');
        }

        var total_tindakan = $('#total-tindakan').text();
        if(total_tindakan == '') {
            total_tindakan = 0;
        }else {
            total_tindakan = total_tindakan.replace(/\./g, "");
            total_tindakan = total_tindakan.replace('Rp ', '');
        }

        var total_gizi = $('#total-gizi').text();
        if(total_gizi == '') {
            total_gizi = 0;
        }else {
            total_gizi = total_gizi.replace(/\./g, "");
            total_gizi = total_gizi.replace('Rp ', '');
        }

        var total_fisioterapi = $('#total-fisioterapi').text();
        if(total_fisioterapi == '') {
            total_fisioterapi = 0;
        }else {
            total_fisioterapi = total_fisioterapi.replace(/\./g, "");
            total_fisioterapi = total_fisioterapi.replace('Rp ', '');
        }

        var total_rontgen = $('#total-rontgen').text();
        if(total_rontgen == '') {
            total_rontgen = 0;
        }else {
            total_rontgen = total_rontgen.replace(/\./g, "");
            total_rontgen = total_rontgen.replace('Rp ', '');
        }

        var administrasi = $('#administrasi').val();
        administrasi = administrasi.replace(/\./g, "");

        var bpjs = $('.form-bpjs-total').val();
        if(bpjs) {
            bpjs = bpjs.replace(/\./g, "");
        } else {
            bpjs = 0;
        }

        // bpjs = bpjs.replace(/\./g, "");
        // administrasi = administrasi.replace('Rp ', '');

        // console.log(parseInt(total_kamar));
        // console.log(parseInt(total_obat));
        // console.log(parseInt(total_obat_pulang));
        // console.log(parseInt(total_alkes));
        // console.log(parseInt(total_laboratorium));
        // console.log(parseInt(total_tindakan));
        // console.log(parseInt(total_visit));
        // console.log(parseInt(total_gizi));
        // console.log(parseInt(total_fisioterapi));
        // console.log(parseInt(total_rontgen));
        // console.log(parseInt(administrasi));
        // console.log(parseInt(bpjs));


        var total_keseluruhan = parseInt(total_kamar) + parseInt(total_obat) +
            parseInt(total_obat_pulang) + parseInt(total_alkes) +
            parseInt(total_laboratorium) + parseInt(total_visit) +
            parseInt(total_tindakan) + parseInt(total_gizi) + parseInt(total_fisioterapi) +
            parseInt(total_rontgen) + parseInt(administrasi) - parseInt(bpjs);

        $('#total-keseluruhan-field').val(total_keseluruhan);
        total_keseluruhan = 'Rp ' + formatprice(total_keseluruhan);
            $('#total-keseluruhan').html(total_keseluruhan);
    }

    // END UNTUK FORMAT NUMBER

    // ALKES
    // $('.form-alkes').on('input', function() {
    //     console.log($(this).val());
    // });
</script>