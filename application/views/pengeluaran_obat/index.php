<div id="main-container">
	<div class="padding-md">
		<div class="panel panel-default table-responsive">
			<div class="padding-md clearfix">
				 <a href="<?php echo site_url('pengeluaran_obat/forecast'); ?>" class="btn btn-primary"><i class="fa fa-lg fa-bar-chart-o" style="padding-right: 5px;"></i>Lakukan Peramalan Kebutuhan Obat</a>
				<div style="margin-bottom: 20px;"></div>
				<div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
					<table class="table table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
						<thead>
							<tr role="row">
								<th>Nama Dagang</th>
								<th>Nama Generik</th>
								<th>Jenis</th>
								<th>Kelas Terapi</th>
								<th>VED</th>
								<th>Satuan</th>
								<th>Jumlah Terjual</th>
								<th>Action</th>
							</tr>
						</thead>

						<tbody>
							<?php foreach($data as $rows): ?>
								<tr>
									<td><?php echo $rows->nama_obat; ?></td>
									<td><?php echo $rows->nama_generik; ?></td>
									<td><?php echo $rows->nama_kategori?></td>
									<td><?php echo $rows->kelas_terapi;?></td>
									<td><?php echo $rows->ved; ?></td>
									<td><?php echo $rows->satuan; ?></td>
									<td><?php echo $rows->total; ?></td>
									<td>
										<a class="detail" title="Chart" rel="tooltip" href="<?php echo site_url('pengeluaran_obat/chart/'.$rows->id_katalog_obat); ?>">
	                                        <i class="fa fa-lg fa-bar-chart-o"></i></a>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>

				<div class="clearfix"></div>
				<br>
				<!-- <a href="<?php //echo site_url(); ?>/report/monthly" class="btn btn-primary"><i class="fa fa-lg fa-print" style="padding-right: 5px;"></i>Cetak Laporan</a> -->
			</div><!-- /.padding-md -->
		</div><!-- /panel -->
	</div><!-- /.padding-md -->
</div><!-- /main-container -->