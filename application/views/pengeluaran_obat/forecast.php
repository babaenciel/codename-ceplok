<div id="main-container">
	<div class="padding-md">
		<div class="panel panel-default table-responsive">
			<div class="padding-md clearfix">

				<div style="margin-bottom: 20px;"></div>
				<div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
					<table class="table table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
						<thead>
							<tr role="row">
								<th>Nama Obat</th>
								<th>Kategori</th>
								<th>Satuan</th>
								<th>Harga</th>
								<th>Kebutuhan Obat</th>
								<th>SS</th>
								<th>ROP</th>
								<th>EOQ</th>
							</tr>
						</thead>

						<tbody>

							<?php foreach($dt_pengeluaran as $rows): ?>
								<tr>
									<td><?php echo $rows->nama_obat; ?></td>
									<td><?php echo $rows->nama_kategori?></td>
									<td><?php echo $rows->satuan; ?></td>
									<td><?php echo $rows->harga_satuan; ?></td>
									<td><?php echo $forcast_obat[$rows->id_katalog_obat]; ?></td>
									<td><?php echo $SS_obat[$rows->id_katalog_obat]; ?></td>
									<td><?php echo $ROP_obat[$rows->id_katalog_obat]; ?></td>
									<td><?php echo $EOQ_obat[$rows->id_katalog_obat]; ?></td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>

				<div class="clearfix"></div>
				<br>
				<a href="<?php echo site_url(); ?>/pengeluaran_obat/forecast_save" class="btn btn-primary"><i class="fa fa-lg fa-save" style="padding-right: 5px;"></i>Simpan Hasil Peramalan</a>

			</div><!-- /.padding-md -->
		</div><!-- /panel -->
	</div><!-- /.padding-md -->
</div><!-- /main-container -->