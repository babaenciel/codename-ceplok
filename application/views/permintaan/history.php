<div id="main-container">
    <div class="padding-md">
        <div class="panel panel-default table-responsive">
            <div class="padding-md clearfix">
                <div style="margin-bottom: 20px;"></div>
                <div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
                    <table class="table table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
                        <thead>
                            <tr role="row">
                                <th>
                                    <div>Nomor SP</div>
                                </th>
                                <th>
                                    <div>Tanggal SP</div>
                                </th>
                                <th>
                                    <div>Action</div>
                                </th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach($data as $rows): ?>
                                <tr>
                                    <td><?php echo $rows->id_surat_permintaan; ?></td>
                                    <td><?php echo conv_date_format($rows->tanggal_sp, 'd-m-Y'); ?></td>
                                    <td>
                                        <a class="detail" title="Detail" rel="tooltip" href="<?php echo site_url('permintaan/detail_history/'.$rows->id_surat_permintaan); ?>">
                                            <i class="fa fa-lg fa-eye"></i></a>
                                        <a class="delete" title="Delete" rel="tooltip" href="<?php echo site_url('permintaan/delete/'.$rows->id_surat_permintaan); ?>">
                                            <i class="fa fa-lg fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
            </div><!-- /.padding-md -->
        </div><!-- /panel -->
    </div><!-- /.padding-md -->
</div><!-- /main-container -->