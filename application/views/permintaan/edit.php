<div id="main-container">
	<div class="padding-md">
		<div class="panel panel-default">
			<div class="panel-body">
				<?php echo $this->load->view('elements/error_message'); ?>
				<form action="<?php site_url('permintaan/edit/'.$id); ?>" method="POST" id="formToggleLine" class="form-horizontal no-margin">
					<div class="form-group">
						<label class="col-lg-2 control-label">Reorder Point</label>
						<div class="col-lg-5">
							<input name="reorder_point" class="form-control" type="text" value="<?php echo (set_value('reorder_point')) ? set_value('reorder_point') : $data->reorder_point ?>">
						</div><!-- /.col -->
					</div><!-- /form-group -->
					<div class="form-group">
						<label class="col-lg-2 control-label">Economic Order Quantity</label>
						<div class="col-lg-5">
							<input name="economic_order_quantity" class="form-control" type="text" value="<?php echo (set_value('economic_order_quantity')) ? set_value('economic_order_quantity') : $data->economic_order_quantity ?>">
						</div><!-- /.col -->
					</div><!-- /form-group -->
					<br>
					<div class="form-group">
						<label class="col-lg-2 control-label"></label>
						<div class="col-lg-5">
							<div class="input-group">
								<input type="submit" class="form-control btn btn-primary">
							</div>
						</div>
					</div>
				</form>
			</div>
		</div><!-- /panel -->
	</div><!-- /.padding-md -->
</div><!-- /main-container -->