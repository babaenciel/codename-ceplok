<div id="main-container">
    <div class="padding-md">
        <div class="panel panel-default table-responsive">
            <div class="padding-md clearfix">
                <div style="margin-bottom: 20px;"></div>
                <div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
                    <table class="table table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
                        <thead>
                            <tr role="row">
                                <th>
                                    <div>Nama Dagang</div>
                                </th>
                                <th>
                                    <div>Nama Generik</div>
                                </th>
                                <th>
                                    <div>Jenis</div>
                                </th>
                                <th>
                                    <div>Kelas Terapi</div>
                                </th>
                                <th>
                                    <div>Berat Netto</div>
                                </th>
                                <th>
                                    <div>Reorder Point</div>
                                </th>
                                <th>
                                    <div>Economic Order Quantity</div>
                                </th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach($data as $rows): ?>
                                <tr>
                                    <td><?php echo $rows->nama_obat; ?></td>
                                    <td><?php echo $rows->nama_generik; ?></td>
                                    <td><?php echo $rows->nama_kategori; ?></td>
                                    <td><?php echo $rows->kelas_terapi; ?></td>
                                    <td><?php echo $rows->berat_netto; ?></td>
                                    <td><?php echo $rows->reorder_point; ?></td>
                                    <td><?php echo $rows->economic_order_quantity; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
            </div><!-- /.padding-md -->
        </div><!-- /panel -->
    </div><!-- /.padding-md -->
</div><!-- /main-container -->