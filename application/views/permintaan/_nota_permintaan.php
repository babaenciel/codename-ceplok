<style type="text/css">
    #nota {
        font-size: 13px;
    }

    #nota table {
        border-spacing: 0px;
    }
</style>

<div style="width: 700px" id="nota">
    <table style="margin-bottom: 20px;">
        <tr>
            <td>
                <img src="<?php echo base_url('resources/img/logo2.jpg'); ?>" style="width: 90px; height: 70px; margin-right: 20px;">
            </td>
            <td style="width: 550px">
                <div style="text-align: center; font-size: 20px;">RUMAH SAKIT</div>
                <div style="text-align: center; font-size: 20px;">BUDI SEHAT</div>
                <div style="text-align: center">Jln. WR. Supratman No 183, Cangkrep Lor, Purworejo</div>
                <div style="text-align: center">Telepon ( 0275 ) 3128272</div>
            </td>
        </tr>
    </table>

    <table>
        <tr>
            <td style="width: 700px;">
                <div style="border-top: 1px solid black;"></div>
            </td>
        </tr>
        <tr>
            <td style="width: 700px;">
                <div style="border-top: 1px solid black;"></div>
            </td>
        </tr>
    </table>

    <br>

    <table>
        <tr>
            <td><h4 style="text-align: center">SURAT PERMINTAAN PEMBELIAN</h4></td>
        </tr>
        <tr>
            <td style="width: 700px; border-bottom: 1px dotted black; padding-top: 10px;">No. SP: <strong><?php echo $no_sp; ?></strong></td>
        </tr>
        <tr>
            <td style="width: 700px; border-bottom: 1px dotted black; padding-top: 10px;">Tanggal: <strong><?php echo $tanggal_sp; ?></strong></td>
        </tr>
        <tr>
            <td><h4 style="text-align: center; margin-top: 20px">RINCIAN</h4></td>
        </tr>
    </table>

    <table border="1" style="width: 700px">
        <tr>
            <td>Nomor</td>
            <td>Nama</td>
            <td>Jenis</td>
            <td>Jumlah Satuan</td>
            <td>Qty Per Dus</td>
            <td>Jumlah Dus</td>
            <td>Supplier</td>
            <td>Harga Beli Per Dus</td>
            <td>Medrep</td>
            <td>Telp. Medrep</td>
            <td>Lead Time</td>
        </tr>

        <?php foreach($daftar_permintaan as $keys => $rows): ?>
            <?php if(!empty($rows->nama_supplier)): ?>
                <?php
                    $nama_supplier = explode('/', $rows->nama_supplier);
                    $harga_beli_per_dus = explode('/', $rows->harga_beli_per_dus);
                    $nama_medrep = explode('/', $rows->nama_medrep);
                    $telepon_medrep = explode('/', $rows->telepon_medrep);
                    $quantity_per_dus = explode('/', $rows->quantity_per_dus);
                    $jumlah_dus = explode('/', $rows->jumlah_dus);
                    $first = TRUE;
                ?>

                <?php foreach($nama_supplier as $keys2 => $rows2): ?>
                    <?php if($first): ?>
                        <tr>
                            <td><?php echo ($keys + 1); ?></td>
                            <td><?php echo $rows->nama_obat; ?></td>
                            <td><?php echo $rows->nama_kategori; ?></td>
                            <td><?php echo $rows->economic_order_quantity; ?></td>
                            <td><?php echo $quantity_per_dus[$keys2]; ?></td>
                            <td><?php echo $jumlah_dus[$keys2]; ?></td>
                            <td><?php echo $rows2; ?></td>
                            <td><?php echo $harga_beli_per_dus[$keys2]; ?></td>
                            <td><?php echo $nama_medrep[$keys2]; ?></td>
                            <td><?php echo $telepon_medrep[$keys2]; ?></td>
                            <td><?php echo $rows->lead_time; ?></td>
                        </tr>
                        <?php $first = FALSE; ?>
                    <?php else: ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><?php echo $quantity_per_dus[$keys2]; ?></td>
                            <td><?php echo $jumlah_dus[$keys2]; ?></td>
                            <td><?php echo $rows2; ?></td>
                            <td><?php echo $harga_beli_per_dus[$keys2]; ?></td>
                            <td><?php echo $nama_medrep[$keys2]; ?></td>
                            <td><?php echo $telepon_medrep[$keys2]; ?></td>
                            <td><?php echo $rows->lead_time; ?></td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td><?php echo ($keys + 1); ?></td>
                    <td><?php echo $rows->nama_obat; ?></td>
                    <td><?php echo $rows->nama_kategori; ?></td>
                    <td><?php echo $rows->economic_order_quantity; ?></td>
                    <td><?php echo !empty($rows->nama_supplier) ? $rows->nama_supplier : ''; ?></td>
                    <td><?php echo !empty($rows->harga_beli_per_dus) ? $rows->harga_beli_per_dus : ''; ?></td>
                    <td><?php echo !empty($rows->nama_medrep) ? $rows->nama_medrep : ''; ?></td>
                    <td><?php echo !empty($rows->telepon_medrep) ? $rows->telepon_medrep : ''; ?></td>
                    <td><?php echo !empty($rows->lead_time) ? $rows->lead_time : ''; ?></td>
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>
    </table>

    <table style="margin-top: 100px;">
        <tr>
            <td style="width: 350px;">
                <div style="text-align: center">Mengetahui</div> <br>
                <div style="text-align: center">Kepala Klinik Umum Rawat Inap</div>
                <div style="text-align: center">Budi Sehat</div>
                <div style="text-align: center"><br></div>
                <div style="text-align: center"><br></div>
                <div style="text-align: center"><br></div>
                <div style="text-align: center"><br></div>  
                <div style="text-align: center"><br></div>                
            </td>
            <td style="width: 300px;">
                <div style="text-align: center">Purworejo, <?php echo date('d-m-Y'); ?></div> <br>
                <div style="text-align: center">Yang membuat rekening</div>
                <div style="text-align: center"><br></div>
                <div style="text-align: center"><br></div>
                <div style="text-align: center"><br></div>
                <div style="text-align: center"><br></div>
                <div style="text-align: center"><br></div>  
                <div style="text-align: center"><?php echo $pembuat_nota; ?></div>
            </td>
        </tr>
    </table>
</div>