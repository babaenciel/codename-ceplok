<div id="main-container">
	<div class="padding-md">
		<div class="panel panel-default table-responsive">
			<div class="padding-md clearfix">
				<div style="margin-bottom: 20px;"></div>
				<div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
					<form method="POST" action="<?php echo site_url('permintaan/cetak'); ?>" id="form-permintaan">
						<table class="table table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
							<thead>
								<tr role="row">
									<th>
										<label class="label-checkbox">
											<input type="checkbox" id="chk-all">
											<span class="custom-checkbox" style="margin-right: 23px;"></span>
										</label>
									</th>
									<th>
										<div>Nama Dagang</div>
									</th>
									<th>
										<div>Nama Generik</div>
									</th>
									<th>
										<div>Jenis</div>
									</th>	
									<th>
										<div>Kelas Terapi</div>
									</th>									
									<th>
										<div>Reorder Point</div>
									</th>
									<th>
										<div>Economic Order Quantity</div>
									</th>
									<th>
										<div>Action</div>
									</th>
								</tr>
							</thead>

							<tbody>
								<?php foreach($data as $rows): ?>
									<tr>
										<td>
											<label class="label-checkbox">
												<input type="checkbox" class="chk-row" name="check[]" value="<?php echo $rows->id_katalog_obat?>">
												<span class="custom-checkbox"></span>
											</label>
										</td>
										<td><?php echo $rows->nama_obat; ?></td>
										<td><?php echo $rows->nama_generik; ?></td>
										<td><?php echo $rows->nama_kategori; ?></td>
										<td><?php echo $rows->kelas_terapi; ?></td>
										<td><?php echo $rows->reorder_point; ?></td>
										<td><?php echo $rows->economic_order_quantity; ?></td>
										<td class="">
											<?php //if(date_diff_with_today($rows->created_at)->m < 6): ?>
												<a href="<?php echo site_url('permintaan/edit/'.$rows->id_katalog_obat); ?>"><i class="fa fa-lg fa-pencil"></i></a>
											<?php //endif; ?>
											<!-- <a href="#"><i class="fa fa-lg fa-print"></i></a> -->
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</form>
			</div><!-- /.padding-md -->
		</div><!-- /panel -->
		<div class="panel-body">
			<div class="row">
				<div class="col-md-4 col-sm-4">
					<a class="btn btn-primary btn-sm cetak-permintaan" href="#">Cetak</a>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div>
	</div><!-- /.padding-md -->
</div><!-- /main-container -->

<script type="text/javascript">
	$('a.cetak-permintaan').on('click', function(e) {
        e.preventDefault();
        var mybutton = $(this);
        $.prompt("Apakah Anda yakin ingin mencetak permintaan obat tersebut?", {
            buttons: { "Yakin": true, "Batal": false },
            submit: function(e, v, m, f) {
                if (v) {
                	$('#form-permintaan').submit();
                    // var href = mybutton.attr('href');
                    // window.location = href;

                }
            }
        });
    });
</script>