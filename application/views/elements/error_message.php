<?php if(isset($errors)): ?>
	<div class="alert alert-danger">
		<?php if(is_array($errors)): ?>
			<?php foreach($errors as $error): ?>
				<?php echo $error . '<br>'; ?>
			<?php endforeach; ?>
		<?php else: ?>
			<?php echo $errors; ?>
		<?php endif; ?>
	</div>
<?php endif; ?>

<?php if($this->session->flashdata('errors')): ?>
    <div class="alert alert-danger">
    	<?php if(is_array($this->session->flashdata('errors'))): ?>
			<?php foreach($this->session->flashdata('errors') as $error): ?>
				<?php echo $error . '<br>'; ?>
			<?php endforeach; ?>
		<?php else: ?>
			<?php echo $this->session->flashdata('errors'); ?>
		<?php endif; ?>
    </div>
<?php endif; ?>

<?php if($this->session->flashdata('success')): ?>
    <div class="alert alert-success">
    	<?php if(is_array($this->session->flashdata('success'))): ?>
			<?php foreach($this->session->flashdata('success') as $success): ?>
				<?php echo $success . '<br>'; ?>
			<?php endforeach; ?>
		<?php else: ?>
			<?php echo $this->session->flashdata('success'); ?>
		<?php endif; ?>
    </div>
<?php endif; ?>