<div id="main-container">
	<div class="padding-md">
		<div class="panel panel-default table-responsive">
			<div class="padding-md clearfix">
				<h5><strong>Nama Pasien: <?php echo $nama_pasien; ?></strong></h5>
				<h5><strong>Nama Dokter: <?php echo $nama_dokter; ?></strong></h5>
				<br>
				<a href="<?php echo site_url('resep/'.$jenis.'/edit/'.$id); ?>" class="btn btn-primary"><i class="fa fa-lg fa-plus" style="padding-right: 5px;"></i>Tambah Obat</a>
				<div style="margin-bottom: 20px;"></div>
				<div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
					<table class="table table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
						<thead>
							<tr role="row">
								<th>
									<div>Nama Dagang</div>
								</th>
								<th>
									<div>Nama Generik</div>
								</th>
								<th>
									<div>Jenis</div>
								</th>
								<th>
									<div>Kelas Terapi</div>
								</th>
								<th>
									<div>Jumlah</div>
								</th>
								<!-- <th>
									<div>Action</div>
								</th> -->
							</tr>
						</thead>

						<tbody>
							<?php foreach($data as $rows): ?>
								<tr class="odd">
									<td><?php echo $rows->nama_obat; ?></td>
									<td><?php echo $rows->nama_generik; ?></td>
									<td><?php echo $rows->nama_kategori; ?></td>
									<td><?php echo $rows->kelas_terapi; ?></td>
									<td class=""><?php echo $rows->jumlah_pengeluaran . ' ' . $rows->satuan; ?></td>
									<!-- <td class="">
										<a class="update" title="Edit" rel="tooltip" href="<?php echo site_url('resep/'.$jenis.'/edit/'.$id); ?>">
	                                        <i class="fa fa-pencil fa-lg"></i></a>
	                                    <a class="delete" title="Hapus" rel="tooltip" href="<?php echo site_url('resep_detail/'.$jenis.'/delete/'.$rows->id_pengeluaran_obat); ?>">
	                                        <i class="fa fa-trash-o fa-lg"></i></a>
									</td> -->
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
			</div><!-- /.padding-md -->
		</div><!-- /panel -->
	</div><!-- /.padding-md -->
</div><!-- /main-container -->