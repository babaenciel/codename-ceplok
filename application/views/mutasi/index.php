<div id="main-container">
	<div class="padding-md">
		<div class="panel panel-default table-responsive">
			<div class="padding-md clearfix">
				<a href="<?php echo site_url('mutasi/'.$gudang.'/add'); ?>" class="btn btn-primary"><i class="fa fa-lg fa-plus" style="padding-right: 5px;"></i>Input Form Pengambilan</a>
				<div style="margin-bottom: 20px;"></div>
				<div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
					<table class="table table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
						<thead>
							<tr role="row">
								<th>
									<div>Nomor Mutasi</div>
								</th>
								<th>
									<div>Tanggal Pengambilan</div>
								</th>
								<th>
									<div>Tanggal Input</div>
								</th>
								<th>
									<div>Action</div>
								</th>
							</tr>
						</thead>

						<tbody>
							<?php foreach($data as $rows): ?>
								<tr>
									<td><?php echo $rows->id_mutasi_obat; ?></td>
									<td><?php echo conv_date_format($rows->tanggal_mutasi, 'd-m-Y'); ?></td>
									<td><?php echo conv_date_format($rows->created_at, 'd-m-Y'); ?></td>
									<td class="">
										<a class="detail" title="Detail" rel="tooltip" href="<?php echo site_url('mutasi_detail/'.$gudang.'/index/'.$rows->id_mutasi_obat); ?>">
	                                        <i class="fa fa-lg fa-eye"></i></a>
										<a class="update" title="Edit" rel="tooltip" href="<?php echo site_url('mutasi/'.$gudang.'/edit/'.$rows->id_mutasi_obat); ?>">
	                                        <i class="fa fa-pencil fa-lg"></i></a>
	                                    <a class="delete" title="Hapus" rel="tooltip" href="<?php echo site_url('mutasi/'.$gudang.'/delete/'.$rows->id_mutasi_obat); ?>">
	                                        <i class="fa fa-trash-o fa-lg"></i></a>
									</td>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>
			</div><!-- /.padding-md -->
		</div><!-- /panel -->
	</div><!-- /.padding-md -->
</div><!-- /main-container -->