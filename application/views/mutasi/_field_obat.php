<div class="form-group">
	<label class="col-lg-2 control-label">Nama Obat</label>
	<div class="col-lg-5">
		<?php
			echo form_dropdown(
				'obat[]',
				$obat_dropdown,
				(isset($form_repopulate['obat'][$i])) ? $form_repopulate['obat'][$i] : $form['obat[]'][$i],
				'class="form-control chzn-select"'
			);
		?>
	</div><!-- /.col -->

	<div class="col-lg-2">
		<?php
			echo form_input(
				'jumlah[]',
				(isset($form_repopulate['jumlah'][$i])) ? $form_repopulate['jumlah'][$i] : $form['jumlah[]'][$i],
				'class="form-control" id="jumlah" placeholder="Jumlah"'
			);
		?>
	</div>

	<div class="col-lg-1">
		<a href="#" class="delete-field"><i class="fa fa-lg fa-minus-circle" style="font-size: 20px; padding-top: 10px;"></i> </a>
	</div>
</div><!-- /form-group -->

<script type="text/javascript">
	$(".chzn-select").chosen();
</script>

<script type="text/javascript">
	$('.delete-field').on('click', function(e) {
		e.preventDefault();
		$(this).parent().parent().remove();
	});
</script>