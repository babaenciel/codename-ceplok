<div class="form-group">
	<label class="col-lg-2 control-label">Nomor Mutasi</label>
	<div class="col-lg-5">
		<div class="input-group">
			<?php
				echo form_input(
					'id_mutasi',
					(set_value('id_mutasi')) ? set_value('id_mutasi') : $form['id_mutasi'],
					'class="form-control"'
				);
			?>
		</div>
	</div><!-- /.col -->
</div><!-- /form-group -->

<div class="form-group">
	<label class="col-lg-2 control-label">Tanggal Mutasi</label>
	<div class="col-lg-5">
		<div class="input-group">
			<?php
				echo form_input(
					'tanggal_mutasi',
					(set_value('tanggal_mutasi')) ? set_value('tanggal_mutasi') : $form['tanggal_mutasi'],
					'class="form-control datepicker"'
				);
			?>
			<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
		</div>
	</div><!-- /.col -->
</div><!-- /form-group -->
<br>
<br>

<div id="group-obat">
	<?php if(isset($lebih_dari_satu)): ?>
		<?php for($i = 0; $i < $total_field_obat; $i++): ?>
			<?php $data['i'] = $i;?>
			<?php $this->load->view('mutasi/_field_obat', $data); ?>
		<?php endfor; ?>
	<?php else: ?>
		<?php $data['i'] = 0; ?>
		<?php $this->load->view('mutasi/_field_obat', $data); ?>
	<?php endif; ?>

</div>

<div class="form-group">
	<label class="col-lg-2 control-label"></label>
	<div class="col-lg-5">
		<a href="#" id="tambah-kolom-button"><i class="fa fa-lg fa-plus" style="padding-right: 5px"></i>Tambah Kolom</a>
	</div><!-- /.col -->
</div><!-- /form-group -->

<br>
<div class="form-group">
	<label class="col-lg-2 control-label"></label>
	<div class="col-lg-5">
		<div class="input-group">
			<input type="submit" class="form-control btn btn-primary">
		</div>
	</div>
</div>

<!-- <div class="form-group">
	<label class="col-lg-2 control-label">Form in modal</label>
	<div class="col-lg-10">
		<a href="http://websdevp.com/Endless/form_element.html#formModal" class="btn btn-success" data-toggle="modal">Form In Modal</a>
	</div>
</div> -->

<script type="text/javascript">
	$('#tambah-kolom-button').on('click', function(e) {
		e.preventDefault();
		$.ajax({
			url: '<?php echo site_url("mutasi/get_field_obat"); ?>',
			success: function(data) {
				var data = $.parseJSON(data);
				console.log(data);
				$('#group-obat').append(data.view);
			}
		})
	});
</script>