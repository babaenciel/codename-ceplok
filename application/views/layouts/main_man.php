<!DOCTYPE html>
<!-- saved from url=(0038)http://websdevp.com/Endless/table.html -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Endless Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>resources/endless/css/bootstrap.min.css" rel="stylesheet">

	<!-- Font Awesome-->
	<link href="<?php echo base_url(); ?>resources/font-awesome/css/font-awesome.min.css" rel="stylesheet">

	<!-- Datatable -->
	<link href="<?php echo base_url(); ?>resources/endless/css/jquery.dataTables_themeroller.css" rel="stylesheet">

	<!-- Endless -->
	<link href="<?php echo base_url(); ?>resources/endless/css/endless.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>resources/endless/css/endless-skin.css" rel="stylesheet">

	<!-- Jquery -->
	<script src="<?php echo base_url(); ?>resources/endless/js/jquery-1.10.2.min.js"></script>
  </head>

  <body class="">
	<!-- Overlay Div -->
	<div id="overlay">
		<div class="overlay-inner">
			<div id="followingBallsG">
				<div id="followingBallsG_1" class="followingBallsG">
				</div>
				<div id="followingBallsG_2" class="followingBallsG">
				</div>
				<div id="followingBallsG_3" class="followingBallsG">
				</div>
				<div id="followingBallsG_4" class="followingBallsG">
				</div>
			</div>
		</div>
	</div>

	<div id="wrapper" class="">
		<div id="top-nav" class="fixed skin-4">
			<h4 class="pull-left"><?php echo $this->title; ?></h4>
			<ul class="nav-notification clearfix">
				<li class="profile dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="http://websdevp.com/Endless/table.html#">
						<strong>Ringgo</strong>
						<span><i class="fa fa-chevron-down"></i></span>
					</a>
					<ul class="dropdown-menu">
						<li>
							<a class="clearfix" href="http://websdevp.com/Endless/table.html#">
								<img src="<?php echo base_url(); ?>resources/endless/user.jpg" alt="User Avatar">
								<div class="detail">
									<strong>John Doe</strong>
									<p class="grey">John_Doe@email.com</p>
								</div>
							</a>
						</li>
						<li><a tabindex="-1" href="http://websdevp.com/Endless/profile.html" class="main-link"><i class="fa fa-edit fa-lg"></i> Edit profile</a></li>
						<li><a tabindex="-1" href="http://websdevp.com/Endless/gallery.html" class="main-link"><i class="fa fa-picture-o fa-lg"></i> Photo Gallery</a></li>
						<li><a tabindex="-1" href="http://websdevp.com/Endless/table.html#" class="theme-setting"><i class="fa fa-cog fa-lg"></i> Setting</a></li>
						<li class="divider"></li>
						<li><a tabindex="-1" href="http://websdevp.com/Endless/login.html" class="main-link"><i class="fa fa-lock fa-lg"></i> Log out</a></li>
					</ul>
				</li>
			</ul>
		</div><!-- /top-nav-->

		<aside class="fixed skin-4">
			<div class="brand">
				<p>Klinik Budi Sehat</p>
			</div><!-- /brand -->

			<img src="<?php echo base_url(); ?>resources/img/logo2.jpg" style="width: 100px" class="center-block img-thumbnail">
			<br>
			<div class="sidebar-inner">
				<div class="main-menu">
					<ul>
						<li class="">
							<a href="<?php echo site_url(); ?>/sample/man_grafik">
								<span class="menu-icon"><i class="fa fa-home fa-lg"></i></span>
								<span class="text">Nilai Persediaan</span>
								<span class="menu-hover"></span>
							</a>
						</li>
					</ul>
				</div><!-- /main-menu -->
			</div><!-- /sidebar-inner -->
		</aside>

		<?php echo $content; ?>
	</div><!-- /wrapper -->

	<a href="" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

	<!-- Bootstrap -->
    <script src="<?php echo base_url(); ?>resources/endless/js/bootstrap.min.js"></script>

	<!-- Datatable -->
	<script src="<?php echo base_url(); ?>resources/endless/js/jquery.dataTables.min.js"></script>

	<!-- Cookie -->
	<script src="<?php echo base_url(); ?>resources/endless/js/jquery.cookie.min.js"></script>

	<!-- Endless -->
	<script src="<?php echo base_url(); ?>resources/endless/js/endless.js"></script>

	<script src="<?php echo base_url(); ?>resources/endless/js/bootstrap-datepicker.min.js"></script>
	<!-- <script src="<?php echo base_url(); ?>resources/endless/js/bootstrap-slider.min.js"></script> -->
	<script src="<?php echo base_url(); ?>resources/endless/js/bootstrap-timepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>resources/endless/js/chosen.jquery.min.js"></script>
	<!-- <script src="<?php echo base_url(); ?>resources/endless/js/bootstrap-wysihtml5.js"></script> -->
	<!-- <script src="<?php echo base_url(); ?>resources/endless/js/dropzone.min.js"></script> -->
	<script src="<?php echo base_url(); ?>resources/endless/js/endless_form.js"></script>
	<script src="<?php echo base_url(); ?>resources/endless/js/jquery.maskedinput.min.js"></script>
	<!-- <script src="<?php echo base_url(); ?>resources/endless/js/jquery.tagsinput.min.js"></script> -->
	<!-- <script src="<?php echo base_url(); ?>resources/endless/js/wysihtml5-0.3.0.min.js"></script> -->

	<script>
		$(function	()	{
			$('#dataTable').dataTable( {
				"bJQueryUI": true,
				"sPaginationType": "full_numbers"
			});

			$('#chk-all').click(function()	{
				if($(this).is(':checked'))	{
					$('#responsiveTable').find('.chk-row').each(function()	{
						$(this).prop('checked', true);
						$(this).parent().parent().parent().addClass('selected');
					});
				}
				else	{
					$('#responsiveTable').find('.chk-row').each(function()	{
						$(this).prop('checked' , false);
						$(this).parent().parent().parent().removeClass('selected');
					});
				}
			});
		});
	</script>



</body></html>