<li class="">
	<a href="<?php echo site_url('pasien'); ?>">
		<span class="menu-icon"><i class="fa fa-users fa-lg"></i></span>
		<span class="text">Pasien</span>
		<span class="menu-hover"></span>
	</a>
</li>
<li class="">
	<a href="<?php echo site_url('pasien_rawat_inap'); ?>">
		<span class="menu-icon"><i class="fa fa-users fa-lg"></i></span>
		<span class="text">Pasien Rawat Inap</span>
		<span class="menu-hover"></span>
	</a>
</li>
<li class="">
	<a href="<?php echo site_url('obat_retur'); ?>">
		<span class="menu-icon"><i class="fa fa-lg fa-medkit"></i></span>
		<span class="text">Obat Retur</span>
		<span class="menu-hover"></span>
	</a>
</li>
<li class="">
	<a href="<?php echo site_url('obat'); ?>">
		<span class="menu-icon"><i class="fa fa-home fa-lg"></i></span>
		<span class="text">Gudang Pusat</span>
		<span class="menu-hover"></span>
	</a>
</li>
<?php if(false): ?>
<li class="">
	<a href="<?php echo site_url('pendapatan'); ?>">
		<span class="menu-icon"><i class="fa fa-money fa-lg"></i></span>
		<span class="text">Pendapatan</span>
		<span class="menu-hover"></span>
	</a>
</li>
<?php endif; ?>