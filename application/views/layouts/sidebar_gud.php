<li class="">
	<a href="<?php echo site_url(); ?>/obat">
		<span class="menu-icon"><i class="fa fa-home fa-lg"></i></span>
		<span class="text">Persediaan Obat</span>
		<span class="menu-hover"></span>
	</a>
</li>
<li>
	<a href="http://websdevp.com/Endless/table.html#" class="dropdown-toggle" data-toggle="dropdown">
		<span class="menu-icon">
			<i class="fa fa-file-text fa-lg"></i>
		</span>
		<span class="text">
			Mutasi
		</span>
		<span class="menu-hover"></span>
	</a>
	<ul class="dropdown-menu">
		<li class="">
			<a href="<?php echo site_url('mutasi/rawat_inap/index'); ?>">
				<span class="menu-icon"><i class="fa fa-file-o fa-lg"></i></span>
				<span class="text">Mutasi Rawat Inap</span>
				<span class="menu-hover"></span>
			</a>
		</li>
		<li class="">
			<a href="<?php echo site_url('mutasi/rawat_jalan/index'); ?>">
				<span class="menu-icon"><i class="fa fa-file-o fa-lg"></i></span>
				<span class="text">Mutasi Rawat Jalan</span>
				<span class="menu-hover"></span>
			</a>
		</li>
	</ul>
</li>
<li>
	<a href="http://websdevp.com/Endless/table.html#" class="dropdown-toggle" data-toggle="dropdown">
		<span class="menu-icon">
			<i class="fa fa-file-text fa-lg"></i>
		</span>
		<span class="text">
			Resep
		</span>
		<span class="menu-hover"></span>
	</a>
	<ul class="dropdown-menu">
		<li class="">
			<a href="<?php echo site_url('resep/rawat_inap/index'); ?>">
				<span class="menu-icon"><i class="fa fa-file-o fa-lg"></i></span>
				<span class="text">Resep Rawat Inap</span>
				<span class="menu-hover"></span>
			</a>
		</li>
		<li class="">
			<a href="<?php echo site_url('resep/rawat_jalan/index'); ?>">
				<span class="menu-icon"><i class="fa fa-file-o fa-lg"></i></span>
				<span class="text">Resep Rawat Jalan</span>
				<span class="menu-hover"></span>
			</a>
		</li>
	</ul>
</li>
<li>
	<a href="http://websdevp.com/Endless/table.html#" class="dropdown-toggle" data-toggle="dropdown">
		<span class="menu-icon">
			<i class="fa fa-file-text fa-lg"></i>
		</span>
		<span class="text">
			Permintaan
		</span>
		<span class="menu-hover"></span>
	</a>
	<ul class="dropdown-menu">
		<li class="">
			<a href="<?php echo site_url('permintaan'); ?>">
				<span class="menu-icon"><i class="fa fa-file-o fa-lg"></i></span>
				<span class="text">Daftar Permintaan</span>
				<span class="menu-hover"></span>
			</a>
		</li>
		<li class="">
			<a href="<?php echo site_url('permintaan/history'); ?>">
				<span class="menu-icon"><i class="fa fa-files-o fa-lg"></i></span>
				<span class="text">History Permintaan</span>
				<span class="menu-hover"></span>
			</a>
		</li>
	</ul>
</li>

<li class="">
	<a href="<?php echo site_url('obat_retur'); ?>">
		<span class="menu-icon"><i class="fa fa-lg fa-medkit"></i></span>
		<span class="text">Obat Retur</span>
		<span class="menu-hover"></span>
	</a>
</li>

<li class="">
	<a href="<?php echo site_url('administration/supplier_manage'); ?>">
		<span class="menu-icon"><i class="fa fa-lg fa-truck"></i></span>
		<span class="text">Supplier</span>
		<span class="menu-hover"></span>
	</a>
</li>

<li class="">
	<a href="<?php echo site_url('pengeluaran_obat'); ?>">
		<span class="menu-icon"><i class="fa fa-lg fa-bar-chart-o"></i></span>
		<span class="text">Pengeluaran Obat</span>
		<span class="menu-hover"></span>
	</a>
</li>