<!DOCTYPE html>
<!-- saved from url=(0038)http://websdevp.com/Endless/table.html -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Rumah Sakit Budi Sehat</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>resources/endless/css/bootstrap.min.css" rel="stylesheet">

	<!-- Font Awesome-->
	<link href="<?php echo base_url(); ?>resources/font-awesome/css/font-awesome.min.css" rel="stylesheet">

	<!-- Datatable -->
	<link href="<?php echo base_url(); ?>resources/endless/css/jquery.dataTables_themeroller.css" rel="stylesheet">

	<!-- Endless -->
	<link href="<?php echo base_url(); ?>resources/endless/css/endless.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>resources/endless/css/endless-skin.css" rel="stylesheet">

	<link href="<?php echo base_url(); ?>resources/endless/css/jquery.minicolors.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>resources/endless/css/bootstrap-wysihtml5.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>resources/endless/css/bootstrap-timepicker.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>resources/endless/css/datepicker.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>resources/endless/css/chosen.min.css" rel="stylesheet">

	<link href="<?php echo base_url(); ?>resources/jquery-impromptu/jquery-impromptu.css" rel="stylesheet">

	<!-- Jquery -->
	<script src="<?php echo base_url(); ?>resources/endless/js/jquery-1.10.2.min.js"></script>

	<style type="text/css">
		.fa {
			margin-right: 3px;
		}

		.autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
		.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
		.autocomplete-selected { background: #F0F0F0; }
		.autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
	</style>
  </head>

  <body class="">
	<!-- Overlay Div -->
	<div id="overlay">
		<div class="overlay-inner">
			<div id="followingBallsG">
				<div id="followingBallsG_1" class="followingBallsG">
				</div>
				<div id="followingBallsG_2" class="followingBallsG">
				</div>
				<div id="followingBallsG_3" class="followingBallsG">
				</div>
				<div id="followingBallsG_4" class="followingBallsG">
				</div>
			</div>
		</div>
	</div>

	<!-- <a href="" id="theme-setting-icon"><i class="fa fa-cog fa-lg"></i></a>
	<div id="theme-setting">
		<div class="title">
			<strong class="no-margin">Skin Color</strong>
		</div>
		<div class="theme-box">
			<a class="theme-color" style="background:#323447" id="default"></a>
			<a class="theme-color" style="background:#efefef" id="skin-1"></a>
			<a class="theme-color" style="background:#a93922" id="skin-2"></a>
			<a class="theme-color" style="background:#3e6b96" id="skin-3"></a>
			<a class="theme-color" style="background:#635247" id="skin-4"></a>
		</div>
	</div> -->

	<div id="wrapper" class="">
		<div id="top-nav" class="fixed skin-4">
			<h4 class="pull-left"><?php echo $this->title; ?></h4>
			<ul class="nav-notification clearfix">
				<?php if($this->session->userdata('user')->jabatan === USER_GUDANG): ?>
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo site_url(); ?>sample/pg_table_pemesanan">
							<i class="fa fa-bell fa-lg"></i>
							<span class="notification-label"><?php echo get_count_permintaan_obat(); ?></span>
						</a>
						<ul class="dropdown-menu notification dropdown-3">
							<li>
								<a href="<?php echo site_url('permintaan'); ?>">
									<span class="notification-icon bg-warning">
										<i class="fa fa-warning"></i>
									</span>
									<span class="m-left-xs">Ada <?php echo get_count_permintaan_obat(); ?> jenis obat yang perlu dibeli</span>
								</a>
							</li>
						</ul>
					</li>
				<?php endif; ?>
				<li class="profile dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="http://websdevp.com/Endless/table.html#">
						<strong><?php echo $this->session->userdata('user')->username; ?></strong>
						<span><i class="fa fa-chevron-down"></i></span>
					</a>
					<ul class="dropdown-menu">
						<li>
							<a class="clearfix" href="http://websdevp.com/Endless/table.html#">
								<div class="detail">
									<strong><?php echo $this->session->userdata('user')->username; ?></strong>
									<p class="grey"><?php echo $this->session->userdata('user')->nip; ?></p>
								</div>
							</a>
						</li>
						<!-- <li><a tabindex="-1" href="http://websdevp.com/Endless/profile.html" class="main-link"><i class="fa fa-edit fa-lg"></i> Edit profile</a></li>
						<li><a tabindex="-1" href="http://websdevp.com/Endless/gallery.html" class="main-link"><i class="fa fa-picture-o fa-lg"></i> Photo Gallery</a></li>
						<li><a tabindex="-1" href="http://websdevp.com/Endless/table.html#" class="theme-setting"><i class="fa fa-cog fa-lg"></i> Setting</a></li> -->
						<li class="divider"></li>
						<li><a tabindex="-1" href="<?php echo site_url(); ?>/login/logout" class="main-link"><i class="fa fa-lock fa-lg"></i> Log out</a></li>
					</ul>
				</li>
			</ul>
		</div><!-- /top-nav-->

		<aside class="fixed skin-4">
			<div class="brand">
				<p>Rumah Sakit</p>
				<p>Budi Sehat</p>
			</div><!-- /brand -->			
			<br>
			<img src="<?php echo base_url(); ?>resources/img/logo2.jpg" style="width: 100px" class="center-block img-thumbnail">
			<br>
			<div class="sidebar-inner">
				<div class="main-menu">
					<ul>
						<?php echo $sidebar; ?>
					</ul>
				</div><!-- /main-menu -->
			</div><!-- /sidebar-inner -->
		</aside>

		<?php echo $content; ?>
	</div><!-- /wrapper -->

	<a href="" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

	<!-- Bootstrap -->
    <script src="<?php echo base_url(); ?>resources/endless/js/bootstrap.min.js"></script>

	<!-- Datatable -->
	<script src="<?php echo base_url(); ?>resources/endless/js/jquery.dataTables.min.js"></script>

	<!-- Cookie -->
	<script src="<?php echo base_url(); ?>resources/endless/js/jquery.cookie.min.js"></script>

	<!-- Endless -->
	<script src="<?php echo base_url(); ?>resources/endless/js/endless.js"></script>

	<script src="<?php echo base_url(); ?>resources/endless/js/bootstrap-datepicker.min.js"></script>
	<!-- <script src="<?php echo base_url(); ?>resources/endless/js/bootstrap-slider.min.js"></script> -->
	<script src="<?php echo base_url(); ?>resources/endless/js/bootstrap-timepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>resources/endless/js/chosen.jquery.min.js"></script>
	<!-- <script src="<?php echo base_url(); ?>resources/endless/js/bootstrap-wysihtml5.js"></script> -->
	<!-- <script src="<?php echo base_url(); ?>resources/endless/js/dropzone.min.js"></script> -->
	<script src="<?php echo base_url(); ?>resources/endless/js/endless_form.js"></script>
	<script src="<?php echo base_url(); ?>resources/endless/js/jquery.maskedinput.min.js"></script>
	<!-- <script src="<?php echo base_url(); ?>resources/endless/js/jquery.tagsinput.min.js"></script> -->
	<!-- <script src="<?php echo base_url(); ?>resources/endless/js/wysihtml5-0.3.0.min.js"></script> -->

	<script src="<?php echo base_url(); ?>resources/jquery-impromptu/jquery-impromptu.js"></script>

	<script>
		$(function	()	{
			$('#dataTable').dataTable( {
				"bJQueryUI": true,
				"sPaginationType": "full_numbers"
			});

            $('#datatable-penerimaan').dataTable( {
                "bJQueryUI": true,
                "aaSorting": [[ 1, "desc" ]],
                "sPaginationType": "full_numbers"
            });

			$('#chk-all').click(function()	{
				if($(this).is(':checked'))	{
					$('#dataTable').find('.chk-row').each(function()	{
						$(this).prop('checked', true);
						$(this).parent().parent().parent().addClass('selected');
					});
				}
				else	{
					$('#dataTable').find('.chk-row').each(function()	{
						$(this).prop('checked' , false);
						$(this).parent().parent().parent().removeClass('selected');
					});
				}
			});
		});
	</script>

	<script type="text/javascript">
        $('a.delete').on('click', function(e) {
            e.preventDefault();
            var mybutton = $(this);
            $.prompt("Apakah Anda yakin ingin menghapus?", {
                buttons: { "Yakin": true, "Batal": false },
                submit: function(e, v, m, f) {
                    if (v) {
                        var href = mybutton.attr('href');
                        window.location = href;
                    }
                }
            });
        });

        $('a.status-hidup').on('click', function(e) {
            e.preventDefault();
            var mybutton = $(this);
            $.prompt("Apakah Anda yakin ingin mengubah status menjadi HIDUP?", {
                buttons: { "Yakin": true, "Batal": false },
                submit: function(e, v, m, f) {
                    if (v) {
                        var href = mybutton.attr('href');
                        window.location = href;
                    }
                }
            });
        });

        $('a.status-mati').on('click', function(e) {
            e.preventDefault();
            var mybutton = $(this);
            $.prompt("Apakah Anda yakin ingin mengubah status menjadi MENINGGAL?", {
                buttons: { "Yakin": true, "Batal": false },
                submit: function(e, v, m, f) {
                    if (v) {
                        var href = mybutton.attr('href');
                        window.location = href;
                    }
                }
            });
        });
    </script>

</body></html>