<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>resources/endless/css/bootstrap.min.css" rel="stylesheet">

	<!-- Font Awesome -->
	<link href="<?php echo base_url(); ?>resources/endless/css/font-awesome.min.css" rel="stylesheet">

	<!-- resources/Endless -->
	<link href="<?php echo base_url(); ?>resources/endless/css/endless.css" rel="stylesheet">

  </head>

  <body>
	<div class="login-wrapper">
		<div class="text-center">
			<h2 class="fadeInUp animation-delay8" style="font-weight:bold">
				<span class="text-success">Rumah Sakit</span> <span style="color:#ccc; text-shadow:0 1px #fff">Budi Sehat</span>
			</h2>
		</div>
		<div class="login-widget animation-delay1">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					<?php if(isset($errors)): ?>
						<div class="alert alert-danger"><?php echo $errors; ?></div>
					<?php endif; ?>
				</div>
				<div class="panel-body">
					<form class="form-login" method="post" action="<?php echo site_url('login'); ?>">
						<div class="form-group">
							<label>Username</label>
							<input type="text" name="username" placeholder="Username" class="form-control input-sm bounceIn animation-delay2" >
						</div>
						<div class="form-group">
							<label>Password</label>
							<input type="password" name="password" placeholder="Password" class="form-control input-sm bounceIn animation-delay4">
						</div>
                        
						<hr/>

						<input type="submit" value="Login" class="btn btn-success btn-sm bounceIn animation-delay5 login-link pull-right">
					</form>
				</div>
			</div><!-- /panel -->
		</div><!-- /login-widget -->
	</div><!-- /login-wrapper -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <!-- Jquery -->
	<script src="js/jquery-1.10.2.min.js"></script>

    <!-- Bootstrap -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

	<!-- Cookie -->
	<script src='js/jquery.cookie.min.js'></script>

	<!-- Endless -->
	<script src="js/endless/endless.js"></script>
  </body>
</html>
