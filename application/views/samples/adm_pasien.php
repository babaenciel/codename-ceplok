<div id="main-container">
	<div class="padding-md">
		<div class="panel panel-default table-responsive">
			<div class="padding-md clearfix">
				<a href="<?php echo site_url(); ?>/sample/adm_form_pasien" class="btn btn-primary"><i class="fa fa-lg fa-plus" style="padding-right: 5px;"></i>Tambah Pasien</a>
				<a href="<?php echo site_url(); ?>/sample/adm_form_pasien_rawat_inap" class="btn btn-primary"><i class="fa fa-lg fa-plus" style="padding-right: 5px;"></i>Daftarkan Pasien Rawat Inap</a>
				<div style="margin-bottom: 20px;"></div>
				<div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
					<table class="table table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
						<thead>
							<tr role="row">
								<th>
									<div>No</div>
								</th>
								<th>
									<div>Nama Pasien</div>
								</th>
								<th>
									<div>Alamat</div>
								</th>
								<th>
									<div>Umur</div>
								</th>
								<th>
									<div>Status</div>
								</th>
								<th style="width: 170px">
									<div>Action</div>
								</th>
							</tr>
						</thead>

						<tbody role="alert" aria-live="polite" aria-relevant="all">
							<tr class="odd">
								<td class="">#1</td>
								<td class="">Heru Suheru 1</td>
								<td class="">Palur</td>
								<td class="">30</td>
								<td><span class="label label-success">Sedang Inap</span></td>
								<td class="">
									<a class="detail" title="Detail" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-eye"></i></a>
									<a class="update" title="Edit" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Hapus" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
                                    <a class="detail" title="Visit Dokter" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_visit_dokter">
                                        <i class="fa fa-lg fa-user-md"></i></a>
                                    <a class="detail" title="Obat dan Alkes" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_obat_dan_alkes">
                                        <i class="fa fa-lg fa-medkit"></i></a>
                                    <a class="detail" title="Laboratorium" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_laboratorium">
                                        <i class="fa fa-lg fa-hospital-o"></i></a>
                                    <a class="detail" title="Tindakan" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_tindakan">
                                        <i class="fa fa-lg fa-stethoscope"></i></a>
								</td>
							</tr>
							<tr class="even">
								<td class=" sorting_1">#2</td>
								<td class="">Heru Suheru 2</td>
								<td class=" ">Gemolong</td>
								<td class=" ">24</td>
								<td><span class="label label-success">Sedang Inap</span></td>
								<td class=" ">
									<a class="detail" title="Detail" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-eye"></i></a>
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
                                    <a class="detail" title="Visit Dokter" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_visit_dokter">
                                        <i class="fa fa-lg fa-user-md"></i></a>
                                    <a class="detail" title="Obat dan Alkes" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_obat_dan_alkes">
                                        <i class="fa fa-lg fa-medkit"></i></a>
                                    <a class="detail" title="Laboratorium" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_laboratorium">
                                        <i class="fa fa-lg fa-hospital-o"></i></a>
                                    <a class="detail" title="Tindakan" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_tindakan">
                                        <i class="fa fa-lg fa-stethoscope"></i></a>

								</td>
							</tr>
							<tr class="odd">
								<td class=" sorting_1">#3</td>
								<td class="">Heru Suheru 3</td>
								<td class=" ">Solo</td>
								<td class=" ">100</td>
								<td><span class="label label-success">Sedang Inap</span></td>
								<td class=" ">
									<a class="detail" title="Detail" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-eye"></i></a>
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
                                    <a class="detail" title="Visit Dokter" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_visit_dokter">
                                        <i class="fa fa-lg fa-user-md"></i></a>
                                    <a class="detail" title="Obat dan Alkes" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_obat_dan_alkes">
                                        <i class="fa fa-lg fa-medkit"></i></a>
                                    <a class="detail" title="Laboratorium" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_laboratorium">
                                        <i class="fa fa-lg fa-hospital-o"></i></a>
                                    <a class="detail" title="Tindakan" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_tindakan">
                                        <i class="fa fa-lg fa-stethoscope"></i></a>

								</td>
							</tr>
							<tr class="even">
								<td class=" sorting_1">#4</td>
								<td class="">Heru Suheru 4</td>
								<td class=" ">Bandung</td>
								<td class=" ">21</td>
								<td></td>
								<td class=" ">
									<a class="detail" title="Detail" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-eye"></i></a>
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
                                    <a class="detail" title="Visit Dokter" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_visit_dokter">
                                        <i class="fa fa-lg fa-user-md"></i></a>
                                    <a class="detail" title="Obat dan Alkes" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_obat_dan_alkes">
                                        <i class="fa fa-lg fa-medkit"></i></a>
                                    <a class="detail" title="Laboratorium" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_laboratorium">
                                        <i class="fa fa-lg fa-hospital-o"></i></a>
                                    <a class="detail" title="Tindakan" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_tindakan">
                                        <i class="fa fa-lg fa-stethoscope"></i></a>

								</td>
							</tr>
							<tr class="odd">
								<td class="">#5</td>
								<td class="">Heru Suheru 5</td>
								<td class="">Palur</td>
								<td class="">30</td>
								<td></td>
								<td class="">
									<a class="detail" title="Detail" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-eye"></i></a>
									<a class="update" title="Edit" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Hapus" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
                                    <a class="detail" title="Visit Dokter" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_visit_dokter">
                                        <i class="fa fa-lg fa-user-md"></i></a>
                                    <a class="detail" title="Obat dan Alkes" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_obat_dan_alkes">
                                        <i class="fa fa-lg fa-medkit"></i></a>
                                    <a class="detail" title="Laboratorium" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_laboratorium">
                                        <i class="fa fa-lg fa-hospital-o"></i></a>
                                    <a class="detail" title="Tindakan" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_tindakan">
                                        <i class="fa fa-lg fa-stethoscope"></i></a>

								</td>
							</tr>
							<tr class="even">
								<td class=" sorting_1">#6</td>
								<td class="">Heru Suheru 6</td>
								<td class=" ">Gemolong</td>
								<td class=" ">24</td>
								<td></td>
								<td class=" ">
									<a class="detail" title="Detail" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-eye"></i></a>
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
                                    <a class="detail" title="Visit Dokter" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_visit_dokter">
                                        <i class="fa fa-lg fa-user-md"></i></a>
                                    <a class="detail" title="Obat dan Alkes" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_obat_dan_alkes">
                                        <i class="fa fa-lg fa-medkit"></i></a>
                                    <a class="detail" title="Laboratorium" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_laboratorium">
                                        <i class="fa fa-lg fa-hospital-o"></i></a>
                                    <a class="detail" title="Tindakan" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_tindakan">
                                        <i class="fa fa-lg fa-stethoscope"></i></a>

								</td>
							</tr>
							<tr class="odd">
								<td class=" sorting_1">#7</td>
								<td class="">Heru Suheru 7</td>
								<td class=" ">Solo</td>
								<td class=" ">100</td>
								<td></td>
								<td class=" ">
									<a class="detail" title="Detail" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-eye"></i></a>
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
                                    <a class="detail" title="Visit Dokter" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_visit_dokter">
                                        <i class="fa fa-lg fa-user-md"></i></a>
                                    <a class="detail" title="Obat dan Alkes" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_obat_dan_alkes">
                                        <i class="fa fa-lg fa-medkit"></i></a>
                                    <a class="detail" title="Laboratorium" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_laboratorium">
                                        <i class="fa fa-lg fa-hospital-o"></i></a>
                                    <a class="detail" title="Tindakan" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_tindakan">
                                        <i class="fa fa-lg fa-stethoscope"></i></a>

								</td>
							</tr>
							<tr class="even">
								<td class=" sorting_1">#8</td>
								<td class="">Heru Suheru 8</td>
								<td class=" ">Bandung</td>
								<td class=" ">21</td>
								<td></td>
								<td class=" ">
									<a class="detail" title="Detail" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-eye"></i></a>
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
                                    <a class="detail" title="Visit Dokter" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_visit_dokter">
                                        <i class="fa fa-lg fa-user-md"></i></a>
                                    <a class="detail" title="Obat dan Alkes" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_obat_dan_alkes">
                                        <i class="fa fa-lg fa-medkit"></i></a>
                                    <a class="detail" title="Laboratorium" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_laboratorium">
                                        <i class="fa fa-lg fa-hospital-o"></i></a>
                                    <a class="detail" title="Tindakan" rel="tooltip" href="<?php echo site_url(); ?>/sample/adm_tindakan">
                                        <i class="fa fa-lg fa-stethoscope"></i></a>

								</td>
							</tr>
						</tbody>
					</table>
			</div><!-- /.padding-md -->
		</div><!-- /panel -->
	</div><!-- /.padding-md -->
</div><!-- /main-container -->