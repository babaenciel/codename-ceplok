<div id="main-container">
	<div class="padding-md">
		<div class="panel panel-default table-responsive">
			<div class="padding-md clearfix">
				<a href="<?php echo site_url(); ?>/sample/adm_form_nota" class="btn btn-primary"><i class="fa fa-lg fa-plus" style="padding-right: 5px;"></i>Buat Nota</a>
				<div style="margin-bottom: 20px;"></div>
				<div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
					<table class="table table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
						<thead>
							<tr role="row">
								<th>
									<div>No</div>
								</th>
								<th>
									<div>Nama Pasien</div>
								</th>
								<th>
									<div>Alamat</div>
								</th>
								<th>
									Status
								</th>
								<th>
									<div>Action</div>
								</th>
							</tr>
						</thead>

						<tbody role="alert" aria-live="polite" aria-relevant="all">
							<tr class="odd">
								<td class="">#1</td>
								<td class="">Heru Suheru 1</td>
								<td class="">Palur</td>
								<td><span class="label label-warning">Belum Tercetak</span></td>
								<td class="">
									<a class="print" title="Cetak" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-print"></i></a>
									<a class="detail" title="Detail" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-eye"></i></a>
									<a class="update" title="Edit" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Hapus" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
								</td>
							</tr>
							<tr class="even">
								<td class=" sorting_1">#2</td>
								<td class="">Heru Suheru 2</td>
								<td class=" ">Gemolong</td>
								<td><span class="label label-success">Tercetak</span></td>
								<td class=" ">
									<a class="print" title="Cetak" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-print"></i></a>
									<a class="detail" title="Detail" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-eye"></i></a>
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
								</td>
							</tr>
							<tr class="odd">
								<td class=" sorting_1">#3</td>
								<td class="">Heru Suheru 3</td>
								<td class=" ">Solo</td>
								<td><span class="label label-success">Tercetak</span></td>
								<td class=" ">
									<a class="print" title="Cetak" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-print"></i></a>
									<a class="detail" title="Detail" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-eye"></i></a>
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
								</td>
							</tr>
							<tr class="even">
								<td class=" sorting_1">#4</td>
								<td class="">Heru Suheru 4</td>
								<td class=" ">Bandung</td>
								<td><span class="label label-success">Tercetak</span></td>
								<td class=" ">
									<a class="print" title="Cetak" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-print"></i></a>
									<a class="detail" title="Detail" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-eye"></i></a>
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
								</td>
							</tr>
							<tr class="odd">
								<td class="">#1</td>
								<td class="">Heru Suheru 1</td>
								<td class="">Palur</td>
								<td><span class="label label-warning">Belum Tercetak</span></td>
								<td class="">
									<a class="print" title="Cetak" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-print"></i></a>
									<a class="detail" title="Detail" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-eye"></i></a>
									<a class="update" title="Edit" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Hapus" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
								</td>
							</tr>
							<tr class="even">
								<td class=" sorting_1">#2</td>
								<td class="">Heru Suheru 2</td>
								<td class=" ">Gemolong</td>
								<td><span class="label label-success">Tercetak</span></td>
								<td class=" ">
									<a class="print" title="Cetak" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-print"></i></a>
									<a class="detail" title="Detail" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-eye"></i></a>
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
								</td>
							</tr>
							<tr class="odd">
								<td class=" sorting_1">#3</td>
								<td class="">Heru Suheru 3</td>
								<td class=" ">Solo</td>
								<td><span class="label label-success">Tercetak</span></td>
								<td class=" ">
									<a class="print" title="Cetak" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-print"></i></a>
									<a class="detail" title="Detail" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-eye"></i></a>
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
								</td>
							</tr>
							<tr class="even">
								<td class=" sorting_1">#4</td>
								<td class="">Heru Suheru 4</td>
								<td class=" ">Bandung</td>
								<td><span class="label label-success">Tercetak</span></td>
								<td class=" ">
									<a class="print" title="Cetak" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-print"></i></a>
									<a class="detail" title="Detail" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-eye"></i></a>
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
								</td>
							</tr>
						</tbody>
					</table>
			</div><!-- /.padding-md -->
		</div><!-- /panel -->
	</div><!-- /.padding-md -->
</div><!-- /main-container -->