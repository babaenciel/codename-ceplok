<div id="main-container">
	<div class="padding-md">
		<div class="panel panel-default table-responsive">
			<div class="padding-md clearfix">
				<a href="<?php echo site_url(); ?>/sample/adm_form" class="btn btn-primary"><i class="fa fa-lg fa-plus" style="padding-right: 5px;"></i>Tambah Jenis Obat</a>
				<div style="margin-bottom: 20px;"></div>
				<div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
					<table class="table table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
						<thead>
							<tr role="row">
								<th>
									<div>No</div>
								</th>
								<th>
									<div>Nama Obat</div>
								</th>
								<th>
									<div>Jumlah Pusat</div>
								</th>
								<th>
									<div>Jumlah Rawat Inap</div>
								</th>
								<th>
									<div>Jumlah Rawat Jalan</div>
								</th>
								<th>
									<div>Action</div>
								</th>
							</tr>
						</thead>

						<tbody>
							<tr class="odd">
								<td class="">#1001</td>
								<td class="">Leather Bag</td>
								<td class="">$89</td>
								<td class="">30</td>
								<td class="">187</td>
								<td class="">
									<a class="detail" title="Detail" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-eye"></i></a>
									<a class="update" title="Edit" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Hapus" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
                                    <a class="add" title="Tambah Jumlah" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-plus-circle"></i></a>
								</td>
							</tr>
							<tr class="even">
								<td class=" sorting_1">#1002</td>
								<td class="">Brown Sunglasses <span class="label label-success m-left-xs">Best Seller</span> </td>
								<td class=" ">$120</td>
								<td class=" ">0</td>
								<td class=" ">861</td>
								<td class=" ">
									<a class="detail" title="Detail" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-eye"></i></a>
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
                                    <a class="add" title="Tambah Jumlah" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-plus-circle"></i></a>
								</td>
							</tr>
							<tr class="odd">
								<td class=" sorting_1">#1003</td>
								<td class="">Casual Boots</td>
								<td class=" ">$99</td>
								<td class=" ">100</td>
								<td class=" ">12</td>
								<td class=" ">
									<a class="detail" title="Detail" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-eye"></i></a>
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
                                    <a class="add" title="Tambah Jumlah" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-plus-circle"></i></a>
								</td>
							</tr>
							<tr class="even">
								<td class=" sorting_1">#1004</td>
								<td class="">Lambskin Sport Coat</td>
								<td class=" ">$4000</td>
								<td class=" ">7</td>
								<td class=" ">41</td>
								<td class=" ">
									<a class="detail" title="Detail" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-eye"></i></a>
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
                                    <a class="add" title="Tambah Jumlah" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-plus-circle"></i></a>
								</td>
							</tr>
							<tr class="odd">
								<td class=" sorting_1">#1005</td>
								<td class="">Summer Dress</td>
								<td class=" ">$310</td>
								<td class=" ">25</td>
								<td class=" ">39</td>
								<td class=" ">
									<a class="detail" title="Detail" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-eye"></i></a>
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
                                    <a class="add" title="Tambah Jumlah" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-plus-circle"></i></a>
								</td>
							</tr>
							<tr class="even">
								<td class=" sorting_1">#1006</td>
								<td class="">Duffle Coat</td>
								<td class=" ">$80</td>
								<td class=" ">0</td>
								<td class=" ">20</td>
								<td class=" ">
									<a class="detail" title="Detail" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-eye"></i></a>
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
                                    <a class="add" title="Tambah Jumlah" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-plus-circle"></i></a>
								</td>
							</tr>
							<tr class="odd">
								<td class=" sorting_1">#1007</td>
								<td class="">Universal Camera Case</td>
								<td class=" ">$35</td>
								<td class=" ">30</td>
								<td class=" ">3</td>
								<td class=" ">
									<a class="detail" title="Detail" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-eye"></i></a>
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
                                    <a class="add" title="Tambah Jumlah" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-plus-circle"></i></a>
								</td>
							</tr>
							<tr class="even">
								<td class=" sorting_1">#1008</td>
								<td class="">Leopard Rose Dress</td>
								<td class=" ">$1500</td>
								<td class=" ">10</td>
								<td class=" ">1</td>
								<td class=" ">
									<a class="detail" title="Detail" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-eye"></i></a>
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
                                    <a class="add" title="Tambah Jumlah" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-plus-circle"></i></a>
								</td>
							</tr>
							<tr class="odd">
								<td class=" sorting_1">#1009</td>
								<td class="">Casual stripe</td>
								<td class=" ">$50</td>
								<td class=" ">30</td>
								<td class=" ">25</td>
								<td class=" ">
									<a class="detail" title="Detail" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-eye"></i></a>
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
                                    <a class="add" title="Tambah Jumlah" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-plus-circle"></i></a>
								</td>
							</tr>
							<tr class="even">
								<td class=" sorting_1">#1010</td>
								<td class="">Italian shirt</td>
								<td class=" ">$70</td>
								<td class=" ">40</td>
								<td class=" ">4</td>
								<td class=" ">
									<a class="detail" title="Detail" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-eye"></i></a>
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
                                    <a class="add" title="Tambah Jumlah" rel="tooltip" href="#">
                                        <i class="fa fa-lg fa-plus-circle"></i></a>
								</td>
							</tr>
						</tbody>
					</table>
			</div><!-- /.padding-md -->
		</div><!-- /panel -->
	</div><!-- /.padding-md -->
</div><!-- /main-container -->