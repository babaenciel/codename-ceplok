<script type="text/javascript" src="<?php echo base_url(); ?>resources/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>resources/flot/jquery.flot.time.min.js"></script>

<div id="main-container">
	<div class="padding-md">
		<div class="panel panel-default table-responsive">
			<div class="panel-body">
				<div class="padding-md clearfix">
					<form class="form-inline" role="form">
					  	<div class="form-group">
					    	<label class="sr-only" for="exampleInputEmail2">Pilih Tahun</label>
					    	<select class="form-control">
								<option>-- Pilih Tahun --</option>
								<option>2013</option>
								<option>2014</option>
								<option>2015</option>
							</select>
					  	</div>

					  	<button type="submit" class="btn btn-primary" style="margin-left: 10px">Submit</button>
					</form>

					<div class="clearfix"></div>
					<br>
					<br>

					<div id="placeholder1" style="width: 800px; height: 300px; margin: 0 auto;"></div>
					<div id="placeholder2" style="width: 800px; height: 300px; margin: 0 auto;"></div>
					<div id="placeholder3" style="width: 800px; height: 300px; margin: 0 auto;"></div>
				</div>
			</div>
		</div><!-- /panel -->
	</div><!-- /.padding-md -->
</div><!-- /main-container -->

<script type="text/javascript">
	//function to format money
    Number.prototype.formatMoney = function(c, d, t){
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
       return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };

	// var d1 = [[0, 3], [1, 3], [2, 5], [3, 7], [4, 8], [5, 10], [6, 11], [7, 9], [8, 5], [9, 13]];
	var data1 = [{
		label: 'Nilai Persediaan Gudang Pusat',
		color: 'red',
		data: [
			// [(1328054400 * 1000), 1500000],
			// [(1330560000 * 1000), 1500000],
			// [(1333238400 * 1000), 1500000],
			// [(1335830400 * 1000), 1500000],
			// [(1338508800 * 1000), 1500000],
			// [(1341100800 * 1000), 1500000],
			// [(1343779200 * 1000), 1500000],
			// [(1346457600 * 1000), 1500000],
			// [(1349049600 * 1000), 1500000],
			// [(1351728000 * 1000), 1500000],
			// [(1354320000 * 1000), 1500000],
			[(1356998400 * 1000), 1600000],
			[(1359676800 * 1000), 2000000],
			[(1362096000 * 1000), 1900000],
			[(1364774400 * 1000), 3400000],
			[(1367366400 * 1000), 1900000],
			[(1370044800 * 1000), 1600000],
			[(1372636800 * 1000), 2000000],
			[(1375315200 * 1000), 1900000],
			[(1377993600 * 1000), 3400000],
			[(1380585600 * 1000), 1900000],
			[(1383264000 * 1000), 2700000],
			[(1385856000 * 1000), 1500000],
			// [(1388534400 * 1000), 1500000],
			// [(1391212800 * 1000), 1500000],
			// [(1393632000 * 1000), 1500000],
			// [(1396310400 * 1000), 1500000],
			// [(1398902400 * 1000), 1500000],
			// [(1401580800 * 1000), 1500000],
			// [(1404172800 * 1000), 1500000],
			// [(1406851200 * 1000), 1500000],
			// [(1409529600 * 1000), 1500000],
			// [(1412121600 * 1000), 1500000],
			// [(1414800000 * 1000), 1500000],
			// [(1417392000 * 1000), 1500000],
		]
	}];

	var data2 = [{
		label: 'Nilai Persediaan Gudang Rawat Inap',
		color: "green",
		data: [
			[(1356998400 * 1000), 1600000],
			[(1359676800 * 1000), 2000000],
			[(1362096000 * 1000), 1900000],
			[(1364774400 * 1000), 3400000],
			[(1367366400 * 1000), 1900000],
			[(1370044800 * 1000), 1600000],
			[(1372636800 * 1000), 2000000],
			[(1375315200 * 1000), 1900000],
			[(1377993600 * 1000), 3400000],
			[(1380585600 * 1000), 1900000],
			[(1383264000 * 1000), 2700000],
			[(1385856000 * 1000), 1500000],
		]
	}];

	var data3 = [{
		label: 'Nilai Persediaan Gudang Rawat Jalan',
		color: "blue",
		data: [
			[(1356998400 * 1000), 1600000],
			[(1359676800 * 1000), 2000000],
			[(1362096000 * 1000), 1900000],
			[(1364774400 * 1000), 3400000],
			[(1367366400 * 1000), 1900000],
			[(1370044800 * 1000), 1600000],
			[(1372636800 * 1000), 2000000],
			[(1375315200 * 1000), 1900000],
			[(1377993600 * 1000), 3400000],
			[(1380585600 * 1000), 1900000],
			[(1383264000 * 1000), 2700000],
			[(1385856000 * 1000), 1500000],
		]
	}]


	$.plot($("#placeholder1"), data1, {
		series: {
			bars: {
				barWidth: 1000 * 60 * 60 * 24 * 10,
				show: true
			}
		},
		yaxis: {
			tickDecimals: 0,
            tickFormatter: function (v, axis) {
                return (v).formatMoney(axis.tickDecimals, '.', '.');
            }
		},
		xaxis: {
			mode: "time",
			timeformat: "%b"
		},
	});

	$.plot($("#placeholder2"), data2, {
		series: {
			bars: {
				barWidth: 1000 * 60 * 60 * 24 * 10,
				show: true
			}
		},
		yaxis: {
			tickDecimals: 0,
            tickFormatter: function (v, axis) {
                return (v).formatMoney(axis.tickDecimals, '.', '.');
            }
		},
		xaxis: {
			mode: "time",
			timeformat: "%b"
		},
	});

	$.plot($("#placeholder3"), data3, {
		series: {
			bars: {
				barWidth: 1000 * 60 * 60 * 24 * 10,
				show: true
			}
		},
		yaxis: {
			tickDecimals: 0,
            tickFormatter: function (v, axis) {
                return (v).formatMoney(axis.tickDecimals, '.', '.');
            }
		},
		xaxis: {
			mode: "time",
			timeformat: "%b"
		},
	});
</script>