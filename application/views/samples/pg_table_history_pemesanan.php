<div id="main-container">
	<div class="padding-md">
		<div class="panel panel-default table-responsive">
			<div class="padding-md clearfix">
				<div style="margin-bottom: 20px;"></div>
				<div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
					<table class="table table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
						<thead>
							<tr role="row">
								<th>
									<div>No</div>
								</th>
								<th>
									<div>Tanggal Pemesanan</div>
								</th>
								<th>
									<div>Action</div>
								</th>
							</tr>
						</thead>

						<tbody>
							<tr class="odd">
								<td class="">#1001</td>
								<td class="">20 Desember 2013</td>
								<td class="">
									<a class="detail" title="Detail" rel="tooltip" href="<?php echo site_url(); ?>/sample/pg_table_detail_history_pemesanan">
                                        <i class="fa fa-lg fa-eye"></i></a>
								</td>
							</tr>
							<tr class="even">
								<td class=" sorting_1">#1002</td>
								<td class="">20 Desember 2013</td>
								<td class=" ">
									<a class="detail" title="Detail" rel="tooltip" href="<?php echo site_url(); ?>/sample/pg_table_detail_history_pemesanan">
                                        <i class="fa fa-lg fa-eye"></i></a>
								</td>
							</tr>
							<tr class="odd">
								<td class=" sorting_1">#1003</td>
								<td class="">20 Desember 2013</td>
								<td class=" ">
									<a class="detail" title="Detail" rel="tooltip" href="<?php echo site_url(); ?>/sample/pg_table_detail_history_pemesanan">
                                        <i class="fa fa-lg fa-eye"></i></a>
								</td>
							</tr>
							<tr class="even">
								<td class=" sorting_1">#1004</td>
								<td class="">20 Desember 2013</td>
								<td class=" ">
									<a class="detail" title="Detail" rel="tooltip" href="<?php echo site_url(); ?>/sample/pg_table_detail_history_pemesanan">
                                        <i class="fa fa-lg fa-eye"></i></a>
								</td>
							</tr>
							<tr class="odd">
								<td class=" sorting_1">#1005</td>
								<td class="">20 Desember 2013</td>
								<td class=" ">
									<a class="detail" title="Detail" rel="tooltip" href="<?php echo site_url(); ?>/sample/pg_table_detail_history_pemesanan">
                                        <i class="fa fa-lg fa-eye"></i></a>
								</td>
							</tr>
							<tr class="even">
								<td class=" sorting_1">#1006</td>
								<td class="">20 Desember 2013</td>
								<td class=" ">
									<a class="detail" title="Detail" rel="tooltip" href="<?php echo site_url(); ?>/sample/pg_table_detail_history_pemesanan">
                                        <i class="fa fa-lg fa-eye"></i></a>
								</td>
							</tr>
							<tr class="odd">
								<td class=" sorting_1">#1007</td>
								<td class="">20 Desember 2013</td>
								<td class=" ">
									<a class="detail" title="Detail" rel="tooltip" href="<?php echo site_url(); ?>/sample/pg_table_detail_history_pemesanan">
                                        <i class="fa fa-lg fa-eye"></i></a>
								</td>
							</tr>
							<tr class="even">
								<td class=" sorting_1">#1008</td>
								<td class="">20 Desember 2013</td>
								<td class=" ">
									<a class="detail" title="Detail" rel="tooltip" href="<?php echo site_url(); ?>/sample/pg_table_detail_history_pemesanan">
                                        <i class="fa fa-lg fa-eye"></i></a>
								</td>
							</tr>
							<tr class="odd">
								<td class=" sorting_1">#1009</td>
								<td class="">20 Desember 2013</td>
								<td class=" ">
									<a class="detail" title="Detail" rel="tooltip" href="<?php echo site_url(); ?>/sample/pg_table_detail_history_pemesanan">
                                        <i class="fa fa-lg fa-eye"></i></a>
								</td>
							</tr>
							<tr class="even">
								<td class=" sorting_1">#1010</td>
								<td class="">20 Desember 2013</td>
								<td class=" ">
									<a class="detail" title="Detail" rel="tooltip" href="<?php echo site_url(); ?>/sample/pg_table_detail_history_pemesanan">
                                        <i class="fa fa-lg fa-eye"></i></a>
								</td>
							</tr>
						</tbody>
					</table>
			</div><!-- /.padding-md -->
		</div><!-- /panel -->
	</div><!-- /.padding-md -->
</div><!-- /main-container -->