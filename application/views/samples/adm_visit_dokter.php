<div id="main-container">
	<div class="padding-md">
		<div class="panel panel-default table-responsive">
			<div class="padding-md clearfix">
				<a href="<?php echo site_url(); ?>/sample/adm_form_visit_dokter" class="btn btn-primary"><i class="fa fa-lg fa-plus" style="padding-right: 5px;"></i>Tambah Visit Dokter</a>
				<div style="margin-bottom: 20px;"></div>
				<div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
					<table class="table table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
						<thead>
							<tr role="row">
								<th>
									<div>No</div>
								</th>
								<th>
									<div>Nama Dokter</div>
								</th>
								<th>
									<div>Jenis Dokter</div>
								</th>
								<th>
									<div>Tarif</div>
								</th>
								<th>
									<div>Action</div>
								</th>
							</tr>
						</thead>

						<tbody role="alert" aria-live="polite" aria-relevant="all">
							<tr class="odd">
								<td class="">#1</td>
								<td class="">Dokter Budi 1</td>
								<td class="">Dokter Jenis 1</td>
								<td class="">30</td>
								<td class="">
									<a class="update" title="Edit" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Hapus" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
								</td
							</tr>
							<tr class="even">
								<td class=" sorting_1">#2</td>
								<td class="">Dokter Budi 2</td>
								<td class=" ">Dokter Jenis 2</td>
								<td class=" ">24</td>
								<td class=" ">
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
								</td>
							</tr>
							<tr class="odd">
								<td class=" sorting_1">#3</td>
								<td class="">Dokter Budi 3</td>
								<td class=" ">Dokter Jenis 4</td>
								<td class=" ">100</td>
								<td class=" ">
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
								</td>
							</tr>
							<tr class="even">
								<td class=" sorting_1">#4</td>
								<td class="">Dokter Budi 4</td>
								<td class=" ">Dokter Jenis 3</td>
								<td class=" ">21</td>
								<td class=" ">
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
								</td>
							</tr>
							<tr class="odd">
								<td class="">#5</td>
								<td class="">Dokter Budi 5</td>
								<td class="">Dokter Jenis 1</td>
								<td class="">30</td>
								<td class="">
									<a class="update" title="Edit" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Hapus" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
								</td>
							</tr>
							<tr class="even">
								<td class=" sorting_1">#6</td>
								<td class="">Dokter Budi 6</td>
								<td class=" ">Dokter Jenis 2</td>
								<td class=" ">24</td>
								<td class=" ">
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
								</td>
							</tr>
							<tr class="odd">
								<td class=" sorting_1">#7</td>
								<td class="">Dokter Budi 7</td>
								<td class=" ">Dokter Jenis 4</td>
								<td class=" ">100</td>
								<td class=" ">
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
								</td>
							</tr>
							<tr class="even">
								<td class=" sorting_1">#8</td>
								<td class="">Dokter Budi 8</td>
								<td class=" ">Dokter Jenis 3</td>
								<td class=" ">21</td>
								<td class=" ">
									<a class="update" title="Update" rel="tooltip" href="#">
                                        <i class="fa fa-pencil fa-lg"></i></a>
                                    <a class="delete" title="Delete" rel="tooltip" href="#">
                                        <i class="fa fa-trash-o fa-lg"></i></a>
								</td>
							</tr>
						</tbody>
					</table>
			</div><!-- /.padding-md -->
		</div><!-- /panel -->
	</div><!-- /.padding-md -->
</div><!-- /main-container -->