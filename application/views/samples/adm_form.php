<div id="main-container">
	<div class="padding-md">
		<div class="panel panel-default">
			<div class="panel-body">
				<form id="formToggleLine" class="form-horizontal no-margin">
					<div class="form-group">
						<label class="col-lg-2 control-label">Jenis</label>
						<div class="col-lg-5">
							<select class="form-control chzn-select" style="display: none;">
								<option>-- Pilih Jenis --</option>
								<option>Injeksi - Ampul</option>
								<option>Injeksi - Vial</option>
								<option>Infus - Plabot</option>
								<option>Table - Tablet</option>
								<option>Syrup - Botol</option>
								<option>Drops - Botol</option>
								<option>Kapsul - Kapsul</option>
								<option>Suppo - Rectal</option>
								<option>Salep - Buah</option>
								<option>Sachet - Sachet</option>
							</select>
						</div><!-- /.col -->
					</div><!-- /form-group -->
					<div class="form-group">
						<label class="col-lg-2 control-label">Nama Obat</label>
						<div class="col-lg-5">
							<input class="form-control" type="text" placeholder="input here...">
						</div><!-- /.col -->
					</div><!-- /form-group -->
					<div class="form-group">
						<label class="col-lg-2 control-label">Berat Netto</label>
						<div class="col-lg-5">
							<input class="form-control" type="text" placeholder="input here...">
						</div><!-- /.col -->
					</div><!-- /form-group -->
					<div class="form-group">
						<label class="col-lg-2 control-label">Supplier</label>
						<div class="col-lg-5">
							<select class="form-control chzn-select" style="display: none;">
								<option>-- Pilih Supplier --</option>
								<option>Alabama</option>
								<option>Alaska</option>
								<option>Arizona</option>
								<option>Arkansas</option>
								<option>California</option>
								<option>Colorado</option>
								<option>Connecticut</option>
								<option>Delaware</option>
								<option>District Of Columbia</option>
								<option>Florida</option>
								<option>Georgia</option>
								<option>Hawaii</option>
								<option>Idaho</option>
								<option>Illinois</option>
								<option>Indiana</option>
								<option>Iowa</option>
								<option>Kansas</option>
								<option>Kentucky</option>
								<option>Louisiana</option>
								<option>Maine</option>
								<option>Maryland</option>
								<option>Massachusetts</option>
								<option>Michigan</option>
								<option>Minnesota</option>
								<option>Mississippi</option>
								<option>Missouri</option>
								<option>Montana</option>
								<option>Nebraska</option>
								<option>Nevada</option>
								<option>New Hampshire</option>
								<option>New Jersey</option>
								<option>New Mexico</option>
								<option>New York</option>
								<option>North Carolina</option>
								<option>North Dakota</option>
								<option>Ohio</option>
								<option>Oklahoma</option>
								<option>Oregon</option>
								<option>Pennsylvania</option>
								<option>Rhode Island</option>
								<option>South Carolina</option>
								<option>South Dakota</option>
								<option>Tennessee</option>
								<option>Texas</option>
								<option>Utah</option>
								<option>Vermont</option>
								<option>Virginia</option>
								<option>Washington</option>
								<option>West Virginia</option>
								<option>Wisconsin</option>
								<option>Wyoming</option>
							</select>
						</div><!-- /.col -->
					</div><!-- /form-group -->
					<div class="form-group">
						<label class="col-lg-2 control-label">VED</label>
						<div class="col-lg-5">
							<input class="form-control" type="text" placeholder="input here...">
						</div><!-- /.col -->
					</div><!-- /form-group -->
					<div class="form-group">
						<label class="col-lg-2 control-label">Lead Time</label>
						<div class="col-lg-5">
							<input class="form-control" type="text" placeholder="input here...">
						</div><!-- /.col -->
					</div><!-- /form-group -->
					<div class="form-group">
						<label class="col-lg-2 control-label">Tanggal Nota</label>
						<div class="col-lg-5">
							<div class="input-group">
								<input type="text" value="<?php echo date('d/m/Y');?>" class="datepicker form-control">
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
						</div><!-- /.col -->
					</div><!-- /form-group -->

					<div class="form-group">
						<label class="col-lg-2 control-label">Jumlah Dus</label>
						<div class="col-lg-5">
							<input class="form-control" type="text" placeholder="input here...">
						</div><!-- /.col -->
					</div><!-- /form-group -->

					<div class="form-group">
						<label class="col-lg-2 control-label">Quantity Per Dus</label>
						<div class="col-lg-5">
							<input class="form-control" type="text" placeholder="input here...">
						</div><!-- /.col -->
					</div><!-- /form-group -->

					<div class="form-group">
						<label class="col-lg-2 control-label">Harga Per Dus</label>
						<div class="col-lg-5">
							<input class="form-control" type="text" placeholder="input here...">
						</div><!-- /.col -->
					</div><!-- /form-group -->

					<div class="form-group">
						<label class="col-lg-2 control-label">Jumlah Satuan</label>
						<div class="col-lg-5">
							<p class="form-control-static">20</p>
						</div><!-- /.col -->
					</div><!-- /form-group -->

					<div class="form-group">
						<label class="col-lg-2 control-label">Harga Satuan</label>
						<div class="col-lg-5">
							<p class="form-control-static">Rp5.000.000</p>
						</div><!-- /.col -->
					</div><!-- /form-group -->
					<br>
					<div class="form-group">
						<label class="col-lg-2 control-label"></label>
						<div class="col-lg-5">
							<div class="input-group">
								<input type="submit" class="form-control btn btn-primary">
							</div>
						</div>
					</div>

					<!-- <div class="form-group">
						<label class="col-lg-2 control-label">Form in modal</label>
						<div class="col-lg-10">
							<a href="http://websdevp.com/Endless/form_element.html#formModal" class="btn btn-success" data-toggle="modal">Form In Modal</a>
						</div>
					</div> -->
				</form>
			</div>
		</div><!-- /panel -->

		<!-- /Modal -->
		<div class="modal fade" id="formModal">
  			<div class="modal-dialog">
    			<div class="modal-content">
      				<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h4>Modal with form</h4>
      				</div>
				    <div class="modal-body">
						<form>
							<div class="form-group">
								<label>Username</label>
								<input type="text" class="form-control input-sm" placeholder="Email Address">
							</div>
							<div class="form-group">
								<label>Password</label>
								<input type="password" class="form-control input-sm" placeholder="Password">
							</div>
							<div class="form-group">
								 <label class="label-checkbox">
								 <input type="checkbox" class="regular-checkbox">
								 <span class="custom-checkbox"></span>
									 Remember me
								</label>
							</div>
							<div class="form-group text-right">
								<a href="http://websdevp.com/Endless/form_element.html#" class="btn btn-success">Sign in</a>
								<a href="http://websdevp.com/Endless/form_element.html#" class="btn btn-success">Sign up</a>
							</div>
						</form>
				    </div>
			  	</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	</div><!-- /.padding-md -->
</div><!-- /main-container -->