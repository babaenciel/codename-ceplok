<div id="main-container">
	<div class="padding-md">
		<div class="panel panel-default table-responsive">
			<div class="padding-md clearfix">
				<div style="margin-bottom: 20px;"></div>
				<div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
					<table class="table table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
						<thead>
							<tr role="row">
								<th>
									<div>No</div>
								</th>
								<th>
									<div>Nama Obat</div>
								</th>
								<th>
									<div>Jumlah</div>
								</th>
							</tr>
						</thead>

						<tbody>
							<tr class="odd">
								<td class="">#1001</td>
								<td class="">Alinamin</td>
								<td class="">89</td>
							</tr>
							<tr class="even">
								<td class=" sorting_1">#1002</td>
								<td class="">Adona</td>
								<td class=" ">120</td>
							</tr>
							<tr class="odd">
								<td class=" sorting_1">#1003</td>
								<td class="">Aminophylline</td>
								<td class=" ">99</td>
							</tr>
							<tr class="even">
								<td class=" sorting_1">#1004</td>
								<td class="">Cefotaxime</td>
								<td class=" ">4000</td>
							</tr>
							<tr class="odd">
								<td class=" sorting_1">#1005</td>
								<td class="">Ceftriaxone</td>
								<td class=" ">310</td>
							</tr>
							<tr class="even">
								<td class=" sorting_1">#1006</td>
								<td class="">Combivent</td>
								<td class=" ">80</td>
							</tr>
							<tr class="odd">
								<td class=" sorting_1">#1007</td>
								<td class="">Cercul</td>
								<td class=" ">35</td>
							</tr>
							<tr class="even">
								<td class=" sorting_1">#1008</td>
								<td class="">Dexamethasone</td>
								<td class=" ">1500</td>
							</tr>
							<tr class="odd">
								<td class=" sorting_1">#1009</td>
								<td class="">Farsix</td>
								<td class=" ">50</td>
							</tr>
							<tr class="even">
								<td class=" sorting_1">#1010</td>
								<td class="">Ottogenta</td>
								<td class=" ">70</td>
							</tr>
						</tbody>
					</table>
			</div><!-- /.padding-md -->
		</div><!-- /panel -->
	</div><!-- /.padding-md -->
</div><!-- /main-container -->