<div id="main-container">
	<div class="padding-md">
		<div class="panel panel-default table-responsive">
			<div class="padding-md clearfix">
				<div style="margin-bottom: 20px;"></div>
				<div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
					<table class="table table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
						<thead>
							<tr role="row">
								<th>

								</th>
								<th>
									<div>No</div>
								</th>
								<th>
									<div>Nama Obat</div>
								</th>
								<th>
									<div>Reorder Point</div>
								</th>
								<th>
									<div>Economic Order Quantity</div>
								</th>
								<th>
									<div>Action</div>
								</th>
							</tr>
						</thead>

						<tbody>
							<tr class="odd">
								<td>
									<label class="label-checkbox">
										<input type="checkbox" class="chk-row">
										<span class="custom-checkbox"></span>
									</label>
								</td>
								<td class="">#1001</td>
								<td class="">Alinamin</td>
								<td class="">89</td>
								<td class="">89</td>
								<td class="">
									<a href="#">Cetak</a>
								</td>
							</tr>
							<tr class="even">
								<td>
									<label class="label-checkbox">
										<input type="checkbox" class="chk-row">
										<span class="custom-checkbox"></span>
									</label>
								</td>
								<td class=" sorting_1">#1002</td>
								<td class="">Adona</td>
								<td class=" ">120</td>
								<td class=" ">120</td>
								<td class=" ">
									<a href="#">Cetak</a>
								</td>
							</tr>
							<tr class="odd">
								<td>
									<label class="label-checkbox">
										<input type="checkbox" class="chk-row">
										<span class="custom-checkbox"></span>
									</label>
								</td>
								<td class=" sorting_1">#1003</td>
								<td class="">Aminophylline</td>
								<td class=" ">99</td>
								<td class=" ">99</td>
								<td class=" ">
									<a href="#">Cetak</a>
								</td>
							</tr>
							<tr class="even">
								<td>
									<label class="label-checkbox">
										<input type="checkbox" class="chk-row">
										<span class="custom-checkbox"></span>
									</label>
								</td>
								<td class=" sorting_1">#1004</td>
								<td class="">Cefotaxime</td>
								<td class=" ">4000</td>
								<td class=" ">4000</td>
								<td class=" ">
									<a href="#">Cetak</a>
								</td>
							</tr>
							<tr class="odd">
								<td>
									<label class="label-checkbox">
										<input type="checkbox" class="chk-row">
										<span class="custom-checkbox"></span>
									</label>
								</td>
								<td class=" sorting_1">#1005</td>
								<td class="">Ceftriaxone</td>
								<td class=" ">310</td>
								<td class=" ">310</td>
								<td class=" ">
									<a href="#">Cetak</a>
								</td>
							</tr>
							<tr class="even">
								<td>
									<label class="label-checkbox">
										<input type="checkbox" class="chk-row">
										<span class="custom-checkbox"></span>
									</label>
								</td>
								<td class=" sorting_1">#1006</td>
								<td class="">Combivent</td>
								<td class=" ">80</td>
								<td class=" ">80</td>
								<td class=" ">
									<a href="#">Cetak</a>
								</td>
							</tr>
							<tr class="odd">
								<td>
									<label class="label-checkbox">
										<input type="checkbox" class="chk-row">
										<span class="custom-checkbox"></span>
									</label>
								</td>
								<td class=" sorting_1">#1007</td>
								<td class="">Cercul</td>
								<td class=" ">35</td>
								<td class=" ">35</td>
								<td class=" ">
									<a href="#">Cetak</a>
								</td>
							</tr>
							<tr class="even">
								<td>
									<label class="label-checkbox">
										<input type="checkbox" class="chk-row">
										<span class="custom-checkbox"></span>
									</label>
								</td>
								<td class=" sorting_1">#1008</td>
								<td class="">Dexamethasone</td>
								<td class=" ">1500</td>
								<td class=" ">1500</td>
								<td class=" ">
									<a href="#">Cetak</a>
								</td>
							</tr>
							<tr class="odd">
								<td>
									<label class="label-checkbox">
										<input type="checkbox" class="chk-row">
										<span class="custom-checkbox"></span>
									</label>
								</td>
								<td class=" sorting_1">#1009</td>
								<td class="">Farsix</td>
								<td class=" ">50</td>
								<td class=" ">50</td>
								<td class=" ">
									<a href="#">Cetak</a>
								</td>
							</tr>
							<tr class="even">
								<td>
									<label class="label-checkbox">
										<input type="checkbox" class="chk-row">
										<span class="custom-checkbox"></span>
									</label>
								</td>
								<td class=" sorting_1">#1010</td>
								<td class="">Ottogenta</td>
								<td class=" ">70</td>
								<td class=" ">70</td>
								<td class=" ">
									<a href="#">Cetak</a>
								</td>
							</tr>
						</tbody>
					</table>
			</div><!-- /.padding-md -->
		</div><!-- /panel -->
		<div class="panel-body">
			<div class="row">
				<div class="col-md-4 col-sm-4">
					<a class="btn btn-primary btn-sm">Cetak</a>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div>
	</div><!-- /.padding-md -->
</div><!-- /main-container -->