<div class="form-group">
    <label class="col-lg-2 control-label">Nama Layanan Laboratorium</label>
    <div class="col-lg-5">
        <?php
            echo form_dropdown(
                'layanan',
                $layanan_dropdown,
                (set_value('layanan')) ? set_value('layanan') : $form['layanan'],
                'class="form-control chzn-select"'
            );
        ?>
    </div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
    <label class="col-lg-2 control-label">Tanggal</label>
    <div class="col-lg-5">
        <?php
            echo form_input(
                'tanggal_laboratorium',
                (set_value('tanggal_laboratorium')) ? set_value('tanggal_laboratorium') : $form['tanggal_laboratorium'],
                'class="form-control datepicker"'
            );
        ?>
    </div><!-- /.col -->
</div><!-- /form-group -->
<?php if(false): ?>
<div class="form-group">
    <label class="col-lg-2 control-label">Waktu</label>
    <div class="col-lg-5">
        <?php
            echo form_input(
                'waktu_laboratorium',
                (set_value('waktu_laboratorium')) ? set_value('waktu_laboratorium') : $form['waktu_laboratorium'],
                'class="form-control" placeholder="HH:MM"'
            );
        ?>
    </div><!-- /.col -->
</div><!-- /form-group -->
<?php endif; ?>
<br>
<div class="form-group">
    <label class="col-lg-2 control-label"></label>
    <div class="col-lg-5">
        <div class="input-group">
            <input type="submit" class="form-control btn btn-primary">
        </div>
    </div>
</div>
