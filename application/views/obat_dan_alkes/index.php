<div id="main-container">
    <div class="padding-md">
        <div class="panel panel-default table-responsive">
            <div class="padding-md clearfix">
                <a href="<?php echo site_url('resep/add/'.$id); ?>" class="btn btn-primary"><i class="fa fa-lg fa-plus" style="padding-right: 5px;"></i>Tambah Obat Dan Alkes</a>
                <div style="margin-bottom: 20px;"></div>
                <div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
                    <table class="table table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
                        <thead>
                            <tr role="row">
                                <th>
                                    <div>No. Resep</div>
                                </th>   
                                <th>
                                    <div>Nama Obat/Alkes</div>
                                </th>
                                <th>
                                    <div>Jumlah</div>
                                </th>
                                <th>
                                    <div>Tarif</div>
                                </th>
                                <th>
                                    <div>Tanggal</div>
                                </th>                                
                                <th>
                                    <div>Status</div>
                                </th>
                                <th>
                                    <div>Action</div>
                                </th>
                            </tr>
                        </thead>

                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php foreach($data as $rows): ?>
                                <tr>
                                    <td><?php echo $rows->nomor_resep; ?></td>
                                    <td class=""><?php echo $rows->nama_obat; ?></td>
                                    <td class=""><?php echo $rows->jumlah_pengeluaran; ?></td>
                                    <td class=""><?php echo format_number($rows->jumlah_pengeluaran * $rows->harga_satuan, TRUE); ?></td>
                                    <td class=""><?php echo conv_date_format($rows->tanggal_resep, 'd-m-Y'); ?></td>                                    
                                    <td><?php echo ($rows->sebagai_obat_pulang == 1) ? '<span class="label label-success">Obat Pulang</span>' : '' ?></td>
                                    <td class="">
                                        <a class="update" title="Edit" rel="tooltip" href="<?php echo site_url('resep/edit/'.$rows->id_resep.'/1'); ?>">
                                            <i class="fa fa-pencil fa-lg"></i></a>
                                        <!-- <a class="delete" title="Hapus" rel="tooltip" href="<?php echo site_url('resep/delete/'.$rows->id_pengeluaran_obat); ?>">
                                            <i class="fa fa-trash-o fa-lg"></i></a> -->
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
            </div><!-- /.padding-md -->
        </div><!-- /panel -->
    </div><!-- /.padding-md -->
</div><!-- /main-container -->