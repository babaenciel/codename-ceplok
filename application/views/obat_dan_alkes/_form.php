<div class="form-group">
    <label class="col-lg-2 control-label"></label>
    <div class="col-lg-5">
        <?php
            echo form_checkbox(
                'sebagai_obat_pulang',
                '1',
                (set_checkbox('sebagai_obat_pulang', '1')) ? set_checkbox('sebagai_obat_pulang', '1') : $form['sebagai_obat_pulang'],
                'class="form-control" id="sebagai-obat-pulang"'
            );
        ?>

        <span class="custom-checkbox"></span> Sebagai Obat Pulang
        <div class="clearfix"></div>
    </div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
    <label class="col-lg-2 control-label">Nama Obat/Alkes</label>
    <div class="col-lg-5">
        <?php
            echo form_dropdown(
                'obat_dan_alkes',
                $obat_dan_alkes_dropdown,
                (set_value('obat_dan_alkes')) ? set_value('obat_dan_alkes') : $form['obat_dan_alkes'],
                'class="form-control chzn-select"'
            );
        ?>
    </div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
    <label class="col-lg-2 control-label">Jumlah</label>
    <div class="col-lg-5">
        <?php
            echo form_input(
                'jumlah',
                (set_value('jumlah')) ? set_value('jumlah') : $form['jumlah'],
                'class="form-control"'
            );
        ?>
    </div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
    <label class="col-lg-2 control-label">Tanggal</label>
    <div class="col-lg-5">
        <?php
            echo form_input(
                'tanggal',
                (set_value('tanggal')) ? set_value('tanggal') : $form['tanggal'],
                'class="form-control datepicker"'
            );
        ?>
    </div><!-- /.col -->
</div><!-- /form-group -->
<br>
<div class="form-group">
    <label class="col-lg-2 control-label"></label>
    <div class="col-lg-5">
        <div class="input-group">
            <input type="submit" class="form-control btn btn-primary">
        </div>
    </div>
</div>