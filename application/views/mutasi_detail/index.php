<div id="main-container">
	<div class="padding-md">
		<div class="panel panel-default table-responsive">
			<div class="padding-md clearfix">
				<a href="<?php echo site_url('mutasi/'.$gudang.'/edit/'.$id); ?>" class="btn btn-primary"><i class="fa fa-lg fa-plus" style="padding-right: 5px;"></i>Tambah Obat</a>
				<div style="margin-bottom: 20px;"></div>
				<div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
					<table class="table table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
						<thead>
							<tr role="row">
								<th>
									<div>Nama Dagang</div>
								</th>
								<th>
									<div>Nama Generik</div>
								</th>
								<th>
									<div>Jenis</div>
								</th>
								<th>
									<div>Kelas Terapi</div>
								</th>
								<th>
									<div>Jumlah</div>
								</th>
								<!-- <th>
									<div>Action</div>
								</th> -->
							</tr>
						</thead>

						<tbody>
							<?php foreach($data as $rows): ?>
								<tr>
									<td><?php echo $rows->nama_obat; ?></td>
									<td><?php echo $rows->nama_generik; ?></td>
									<td><?php echo $rows->nama_kategori; ?></td>
									<td><?php echo $rows->kelas_terapi; ?></td>
									<td><?php echo $rows->jumlah_mutasi . ' ' . $rows->satuan; ?></td>
									<!-- <td class="">
										<a class="update" title="Edit" rel="tooltip" href="<?php echo site_url('mutasi/'.$gudang.'/edit/'.$id); ?>">
	                                        <i class="fa fa-pencil fa-lg"></i></a>
	                                    <a class="delete" title="Hapus" rel="tooltip" href="<?php echo site_url('mutasi_detail/'.$gudang.'/delete/'.$rows->id_mutasi_obat_detail); ?>">
	                                        <i class="fa fa-trash-o fa-lg"></i></a>
									</td> -->
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
			</div><!-- /.padding-md -->
		</div><!-- /panel -->
	</div><!-- /.padding-md -->
</div><!-- /main-container -->