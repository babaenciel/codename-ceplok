<?php if(false): ?>
<div class="form-group">
    <label class="col-lg-2 control-label">Nama Perawat</label>
    <div class="col-lg-5">
        <?php
            echo form_dropdown(
                'perawat',
                $perawat_dropdown,
                (set_value('perawat')) ? set_value('perawat') : $form['perawat'],
                'class="form-control chzn-select"'
            );
        ?>
    </div><!-- /.col -->
</div><!-- /form-group -->
<?php endif; ?>
<div class="form-group">
    <label class="col-lg-2 control-label">Jenis Tindakan</label>
    <div class="col-lg-5">
        <?php
            echo form_checkbox(
                'tindakan_lainnya_checkbox',
                '1',
                (set_checkbox('tindakan_lainnya_checkbox', '1')) ? set_checkbox('tindakan_lainnya_checkbox', '1') : $form['tindakan_lainnya_checkbox'],
                'class="form-control" id="tindakan-lainnya-checkbox"'
            );
        ?>
        <span class="custom-checkbox"></span> Tindakan Lainnya
        <div class="clearfix"></div>
        <br>

        <div id="tindakan-dropdown">
            <?php
                echo form_dropdown(
                    'jenis_tindakan',
                    $jenis_tindakan_dropdown,
                    (set_value('jenis_tindakan')) ? set_value('jenis_tindakan') : $form['jenis_tindakan'],
                    'class="form-control chzn-select"'
                );
            ?>
        </div>

        <div class="clearfix"></div>
        <br>

        <?php
            echo form_input(
                'tindakan_lainnya',
                (set_value('tindakan_lainnya')) ? set_value('tindakan_lainnya') : $form['tindakan_lainnya'],
                'class="form-control" id="tindakan-lainnya" style="display:none" placeholder="Masukkan Nama Tindakan Lainnya"'
            );
        ?>

        <?php
            echo form_input(
                'tarif',
                (set_value('tarif')) ? set_value('tarif') : $form['tarif'],
                'class="form-control" id="tarif" style="display:none" placeholder="Masukkan Tarif Tindakan Lainnya"'
            );
        ?>
    </div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
    <label class="col-lg-2 control-label">Tanggal</label>
    <div class="col-lg-5">
        <?php
            echo form_input(
                'tanggal_tindakan',
                (set_value('tanggal_tindakan')) ? set_value('tanggal_tindakan') : $form['tanggal_tindakan'],
                'class="form-control datepicker"'
            );
        ?>
    </div><!-- /.col -->
</div><!-- /form-group -->
<?php if(false): ?>
<div class="form-group">
    <label class="col-lg-2 control-label">Waktu</label>
    <div class="col-lg-5">
        <?php
            echo form_input(
                'waktu_tindakan',
                (set_value('waktu_tindakan')) ? set_value('waktu_tindakan') : $form['waktu_tindakan'],
                'class="form-control" placeholder="HH:MM"'
            );
        ?>
    </div><!-- /.col -->
</div><!-- /form-group -->
<?php endif; ?>
<br>
<div class="form-group">
    <label class="col-lg-2 control-label"></label>
    <div class="col-lg-5">
        <div class="input-group">
            <input type="submit" class="form-control btn btn-primary">
        </div>
    </div>
</div>

<!-- <div class="form-group">
    <label class="col-lg-2 control-label">Form in modal</label>
    <div class="col-lg-10">
        <a href="http://websdevp.com/Endless/form_element.html#formModal" class="btn btn-success" data-toggle="modal">Form In Modal</a>
    </div>
</div> -->

<script type="text/javascript">
    $('#tindakan-lainnya-checkbox').on('click', function() {
        // console.log($(this).val());
        $('#tindakan-lainnya').toggle(this.checked);
        $('#tarif').toggle(this.checked);

        if(this.checked) {
            $('#tindakan-dropdown').hide();
        }else {
            $('#tindakan-dropdown').show();
        }
    });

    if($('#tindakan-lainnya-checkbox').is(':checked')) {
        $('#tindakan-lainnya').show();
        $('#tarif').show();
        $('#tindakan-dropdown').hide();
    }else {
        $('#tindakan-lainnya').hide();
        $('#tarif').hide();
        $('#tindakan-dropdown').show();
    }
</script>