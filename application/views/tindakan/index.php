<div id="main-container">
    <div class="padding-md">
        <div class="panel panel-default table-responsive">
            <div class="padding-md clearfix">
                <a href="<?php echo site_url('tindakan/add/'.$id); ?>" class="btn btn-primary"><i class="fa fa-lg fa-plus" style="padding-right: 5px;"></i>Tambah Tindakan</a>
                <div style="margin-bottom: 20px;"></div>
                <div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
                    <table class="table table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
                        <thead>
                            <tr role="row">                                
                                <th>
                                    <div>Nama Tindakan</div>
                                </th>
                                <th>
                                    <div>Tarif</div>
                                </th>
                                <th>
                                    <div>Tanggal</div>
                                </th>                                
                                <th>
                                    <div>Action</div>
                                </th>
                            </tr>
                        </thead>

                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php foreach($data as $rows): ?>
                                <tr>                                    
                                    <td class="">
                                        <?php if($rows->tindakan_lainnya == 0): ?>
                                            <?php echo $rows->nama_tindakan . ' - ' . $rows->jenis_kamar; ?>
                                        <?php else: ?>
                                            <?php echo $rows->nama_tindakan; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td class=""><?php echo format_number($rows->tarif, TRUE); ?></td>
                                    <td class=""><?php echo conv_date_format($rows->tgl_tindakan, 'd-m-Y'); ?></td>                                    
                                    <td class="">
                                        <a class="update" title="Edit" rel="tooltip" href="<?php echo site_url('tindakan/edit/'.$rows->id_tindakan_perawat); ?>">
                                            <i class="fa fa-pencil fa-lg"></i></a>
                                        <a class="delete" title="Hapus" rel="tooltip" href="<?php echo site_url('tindakan/delete/'.$rows->id_tindakan_perawat); ?>">
                                            <i class="fa fa-trash-o fa-lg"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
            </div><!-- /.padding-md -->
        </div><!-- /panel -->
    </div><!-- /.padding-md -->
</div><!-- /main-container -->