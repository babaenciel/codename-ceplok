<div class="form-group">
    <label class="col-lg-2 control-label">Nama Layanan Fisioterapi</label>
    <div class="col-lg-5">
        <?php
            echo form_dropdown(
                'layanan',
                $layanan_dropdown,
                (set_value('layanan')) ? set_value('layanan') : $form['layanan'],
                'class="form-control chzn-select"'
            );
        ?>
    </div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
    <label class="col-lg-2 control-label">Tanggal</label>
    <div class="col-lg-5">
        <?php
            echo form_input(
                'tanggal_fisioterapi',
                (set_value('tanggal_fisioterapi')) ? set_value('tanggal_fisioterapi') : $form['tanggal_fisioterapi'],
                'class="form-control datepicker"'
            );
        ?>
    </div><!-- /.col -->
</div><!-- /form-group -->
<br>
<div class="form-group">
    <label class="col-lg-2 control-label"></label>
    <div class="col-lg-5">
        <div class="input-group">
            <input type="submit" class="form-control btn btn-primary">
        </div>
    </div>
</div>
