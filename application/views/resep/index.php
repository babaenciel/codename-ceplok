<div id="main-container">
	<div class="padding-md">
		<div class="panel panel-default table-responsive">
			<div class="padding-md clearfix">
				<a href="<?php echo site_url('resep/'.$jenis.'/add'); ?>" class="btn btn-primary">
					<i class="fa fa-lg fa-plus" style="padding-right: 5px;"></i>
					Input Resep <?php echo (($jenis == 'rawat_inap') ? 'Rawat Inap' : 'Rawat Jalan'); ?>
				</a>
				<div style="margin-bottom: 20px;"></div>
				<div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
					<table class="table table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
						<thead>
							<tr role="row">
								<th>
									<div>Nomor Resep</div>
								</th>
								<th>
									<div>Nama Pasien</div>
								</th>
								<th>
									<div>Nama Dokter</div>
								</th>
								<th>
									<div>Tanggal Resep Dibuat</div>
								</th>
								<th>
									<div>Action</div>
								</th>
							</tr>
						</thead>

						<tbody>
							<?php foreach($data as $rows): ?>
								<tr>
									<td>
										<?php echo $rows->nomor_resep; ?>
									</td>
									<td>
										<?php if($jenis == 'rawat_inap'): ?>
											<?php echo $rows->nama_pasien; ?>
										<?php else: ?>
											<?php echo $rows->nama_pasien_rawat_jalan; ?>
										<?php endif; ?>
									</td>
									<td><?php echo $rows->nama_dokter; ?></td>
									<td><?php echo conv_date_format($rows->tanggal_resep, 'd-m-Y'); ?></td>
									<td class="">
										<a class="detail" title="Detail" rel="tooltip" href="<?php echo site_url('resep_detail/'.$jenis.'/index/'.$rows->id_resep); ?>">
	                                        <i class="fa fa-lg fa-eye"></i></a>
										<a class="update" title="Edit" rel="tooltip" href="<?php echo site_url('resep/'.$jenis.'/edit/'.$rows->id_resep); ?>">
	                                        <i class="fa fa-pencil fa-lg"></i></a>
	                                    <a class="delete" title="Hapus" rel="tooltip" href="<?php echo site_url('resep/'.$jenis.'/delete/'.$rows->id_resep); ?>">
	                                        <i class="fa fa-trash-o fa-lg"></i></a>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
			</div><!-- /.padding-md -->
		</div><!-- /panel -->
	</div><!-- /.padding-md -->
</div><!-- /main-container -->