<div id="main-container">
	<div class="padding-md">
		<div class="panel panel-default">
			<div class="panel-body">
				<?php echo $this->load->view('elements/error_message'); ?>
				<?php if($id !== NULL): ?>
					<form action="<?php echo site_url('resep/'.$jenis.'/add/'.$id); ?>" method="POST" id="formToggleLine" class="form-horizontal no-margin">
				<?php else: ?>
					<form action="<?php echo site_url('resep/'.$jenis.'/add'); ?>" method="POST" id="formToggleLine" class="form-horizontal no-margin">
				<?php endif; ?>
					<?php $this->load->view('resep/_form'); ?>

				</form>
			</div>
		</div><!-- /panel -->
	</div><!-- /.padding-md -->
</div><!-- /main-container -->