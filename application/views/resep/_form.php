<div class="form-group">
	<label class="col-lg-2 control-label">Nomor Resep</label>
	<div class="col-lg-5">
		<div class="input-group">
			<?php
				echo form_input(
					'nomor_resep',
					(set_value('nomor_resep')) ? set_value('nomor_resep') : $form['nomor_resep'],
					'class="form-control"'
				);
			?>
		</div>
	</div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
	<label class="col-lg-2 control-label">Nama Dokter</label>
	<div class="col-lg-5">
		<?php
			echo form_dropdown(
				'nama_dokter',
				$dokter_dropdown,
				( set_value('nama_dokter') ? set_value('nama_dokter') : $form['nama_dokter'] ),
				'class="form-control chzn-select"'
			);
		?>
	</div><!-- /.col -->
</div><!-- /form-group -->

<div class="form-group">
	<label class="col-lg-2 control-label">Nama Pasien</label>
	<div class="col-lg-5">
		<?php if($jenis == 'rawat_inap'): ?>
			<?php
				echo form_dropdown(
					'pasien',
					$pasien_dropdown,
					( set_value('pasien') ? set_value('pasien') : $form['pasien'] ),
					'class="form-control chzn-select"'
				);
			?>
		<?php elseif($jenis == 'rawat_jalan'): ?>
			<?php
				echo form_input(
					'pasien',
					( set_value('pasien') ? set_value('pasien') : $form['pasien'] ),
					'class="form-control"'
				);
			?>
		<?php endif; ?>
	</div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
	<label class="col-lg-2 control-label">Tanggal Resep</label>
	<div class="col-lg-5">
		<div class="input-group">
			<?php
				echo form_input(
					'tanggal_resep',
					(set_value('tanggal_resep')) ? set_value('tanggal_resep') : $form['tanggal_resep'],
					'class="form-control datepicker"'
				);
			?>
			<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
		</div>
	</div><!-- /.col -->
</div><!-- /form-group -->
<?php if(false): ?>
<div class="form-group">
	<label class="col-lg-2 control-label">Waktu Resep</label>
	<div class="col-lg-5">
		<div class="input-group">
			<?php
				echo form_input(
					'waktu_resep',
					(set_value('waktu_resep')) ? set_value('waktu_resep') : $form['waktu_resep'],
					'class="form-control" placeholder="HH:MM"'
				);
			?>
		</div>
	</div><!-- /.col -->
</div><!-- /form-group -->
<?php endif; ?>
<br>
<br>

<div id="group-obat">
	<?php if(isset($lebih_dari_satu)): ?>
		<?php foreach($i as $rows): ?>
			<?php $data['i'] = $rows; ?>
			<?php $this->load->view('resep/_field_obat', $data); ?>
		<?php endforeach; ?>
		<input type="hidden" value="<?php echo $last_count; ?>" id="value-i">
	<?php else: ?>
		<?php $data['i'] = 0; ?>
		<input type="hidden" value="<?php echo $data['i']; ?>" id="value-i">
		<?php $this->load->view('resep/_field_obat', $data); ?>
	<?php endif; ?>
</div>

<div class="form-group">
	<label class="col-lg-2 control-label"></label>
	<div class="col-lg-5">
		<a href="#" id="tambah-kolom-button"><i class="fa fa-lg fa-plus" style="padding-right: 5px"></i>Tambah Kolom</a>
	</div><!-- /.col -->
</div><!-- /form-group -->

<br>
<div class="form-group">
	<label class="col-lg-2 control-label"></label>
	<div class="col-lg-5">
		<div class="input-group">
			<input type="submit" class="form-control btn btn-primary">
		</div>
	</div>
</div>

<!-- <div class="form-group">
	<label class="col-lg-2 control-label">Form in modal</label>
	<div class="col-lg-10">
		<a href="http://websdevp.com/Endless/form_element.html#formModal" class="btn btn-success" data-toggle="modal">Form In Modal</a>
	</div>
</div> -->

<script type="text/javascript">
	$('#tambah-kolom-button').on('click', function(e) {
		e.preventDefault();

		var i = parseInt($('#value-i').val());
		i += 1;

		$.ajax({
			type: 'POST',
			url: '<?php echo site_url("resep/get_field_obat"); ?>',
			data: {
				'i' : i,
				'jenis' : '<?php echo $jenis; ?>'
			},
			success: function(data) {
				var data = $.parseJSON(data);

				$('#value-i').val(i);
				$('#group-obat').append(data.view);
			}
		})
	});
</script>