<div class="form-group">
	<label class="col-lg-2 control-label">Nama Obat</label>
	<div class="col-lg-5">
		<?php
			echo form_dropdown(
				'obat['.$i.']',
				$obat_dropdown,
				(isset($form_repopulate['obat'][$i])) ? $form_repopulate['obat'][$i] : $form['obat'][$i],
				'class="form-control chzn-select"'
			);
		?>
	</div><!-- /.col -->

	<div class="col-lg-2">
		<?php
			echo form_input(
				'jumlah['.$i.']',
				(isset($form_repopulate['jumlah'][$i])) ? $form_repopulate['jumlah'][$i] : $form['jumlah'][$i],
				'class="form-control" id="jumlah" placeholder="Jumlah"'
			);
		?>
	</div>

	<div class="col-lg-1">
		<?php
			echo form_checkbox(
				'gudang_lain['.$i.']',
				'checked',
				// form repopulate gudang_lain ini aku tak tahu pasti mengapa bisa berhasil
				// untuk sementara biarkan saja
				(isset($form_repopulate['gudang_lain'][$i])) ? $form_repopulate['gudang_lain'][$i] : $form['gudang_lain'][$i]
			);
		?>
		<?php if($jenis == 'rawat_inap'): ?>
			<span class="custom-checkbox"></span> <br>Ambil Dari Gudang Rawat Jalan
		<?php else: ?>
			<span class="custom-checkbox"></span> <br>Ambil Dari Gudang Rawat Inap
		<?php endif; ?>
	</div>

	<div class="col-lg-1">
		<?php
			echo form_checkbox(
				'obat_pulang['.$i.']',
				'checked',
				// form repopulate obat pulang ini aku tak tahu pasti mengapa bisa berhasil
				// untuk sementara biarkan saja
				(isset($form_repopulate['obat_pulang'][$i])) ? $form_repopulate['obat_pulang'][$i] : $form['obat_pulang'][$i]
			);
		?>

		<span class="custom-checkbox"></span> <br>Sebagai Obat Pulang
	</div>

	<div class="col-lg-1">
		<a href="#" class="delete-field"><i class="fa fa-lg fa-minus-circle" style="font-size: 20px; padding-top: 10px;"></i> </a>
	</div>
</div><!-- /form-group -->

<script type="text/javascript">
	$(".chzn-select").chosen();
</script>

<script type="text/javascript">
	$('.delete-field').on('click', function(e) {
		e.preventDefault();

		// var i = parseInt($('#i-deleted').val());
		// console.log(i);
		// var i_group = $('#i-deleted-group').val() + ',' + i;
		// $('#i-deleted-group').val(i_group);
		// console.log($('#i-deleted-group').val());

		$(this).parent().parent().remove();
	});
</script>