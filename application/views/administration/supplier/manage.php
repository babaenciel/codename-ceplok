<div id="main-container">
    <div class="padding-md">
        <div class="panel panel-default table-responsive">
            <div class="padding-md clearfix">
                <a href="<?php echo site_url('administration/supplier_add'); ?>" class="btn btn-primary"><i class="fa fa-lg fa-plus" style="padding-right: 5px;"></i>Tambah Supplier</a>
                <div style="margin-bottom: 20px;"></div>
                <div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
                    <table class="table table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
                        <thead>
                            <tr role="row">
                                <th>
                                    <div>Kode Pabrik</div>
                                </th>
                                <th>
                                    <div>Nama Medrep</div>
                                </th>
                                <th>
                                    <div>No. Telepon Medrep</div>
                                </th>
                                <th style="width: 180px">
                                    <div>Action</div>
                                </th>
                            </tr>
                        </thead>

                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php foreach($data as $rows): ?>
                                <tr>
                                    <td><?php echo $rows->nama_supplier; ?></td>
                                    <td><?php echo $rows->nama_medrep; ?></td>
                                    <td><?php echo $rows->telepon_medrep; ?></td>
                                    <td>
                                        <a class="update" title="Edit" rel="tooltip" href="<?php echo site_url('administration/supplier_edit/'.$rows->id_supplier); ?>">
                                            <i class="fa fa-pencil fa-lg"></i></a>
                                        <a class="delete" title="Hapus" rel="tooltip" href="<?php echo site_url('administration/supplier_delete/'.$rows->id_supplier); ?>">
                                            <i class="fa fa-trash-o fa-lg"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
            </div><!-- /.padding-md -->
        </div><!-- /panel -->
    </div><!-- /.padding-md -->
</div><!-- /main-container -->