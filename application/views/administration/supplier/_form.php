<div class="form-group">
    <label class="col-lg-2 control-label">Kode Pabrik</label>
    <div class="col-lg-5">
        <?php
            echo form_input(
                'nama_supplier',
                (set_value('nama_supplier')) ? set_value('nama_supplier') : $form['nama_supplier'],
                'class="form-control"'
            );
        ?>
    </div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
    <label class="col-lg-2 control-label">Nama Medrep</label>
    <div class="col-lg-5">
        <?php
            echo form_input(
                'nama_medrep',
                (set_value('nama_medrep')) ? set_value('nama_medrep') : $form['nama_medrep'],
                'class="form-control"'
            );
        ?>
    </div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
    <label class="col-lg-2 control-label">No. Telepon Medrep</label>
    <div class="col-lg-5">
        <?php
            echo form_input(
                'telepon_medrep',
                (set_value('telepon_medrep')) ? set_value('telepon_medrep') : $form['telepon_medrep'],
                'class="form-control"'
            );
        ?>
    </div><!-- /.col -->
</div><!-- /form-group -->

<br>

<div class="form-group">
    <label class="col-lg-2 control-label"></label>
    <div class="col-lg-5">
        <div class="input-group">
            <input type="submit" class="form-control btn btn-primary">
        </div>
    </div>
</div>

<!-- <div class="form-group">
    <label class="col-lg-2 control-label">Form in modal</label>
    <div class="col-lg-10">
        <a href="http://websdevp.com/Endless/form_element.html#formModal" class="btn btn-success" data-toggle="modal">Form In Modal</a>
    </div>
</div> -->