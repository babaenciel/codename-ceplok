<div class="form-group">
	<label class="col-lg-2 control-label">Kategori</label>
	<div class="col-lg-5">
		<?php
			echo form_dropdown(
				'jenis',
				$jenis_dropdown,
				(set_value('jenis')) ? set_value('jenis') : $form['jenis'],
				'class="form-control chzn-select"'
			);
		?>
			<!-- <option>-- Pilih Jenis --</option>
			<option>Injeksi - Ampul</option>
			<option>Injeksi - Vial</option>
			<option>Infus - Plabot</option>
			<option>Table - Tablet</option>
			<option>Syrup - Botol</option>
			<option>Drops - Botol</option>
			<option>Kapsul - Kapsul</option>
			<option>Suppo - Rectal</option>
			<option>Salep - Buah</option>
			<option>Sachet - Sachet</option> -->
	</div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
	<label class="col-lg-2 control-label">Obat / Alkes</label>
	<div class="col-lg-5">
		<?php
			echo form_dropdown(
				'jenis_obat',
				$jenis_obat_dropdown,
				(set_value('jenis_obat')) ? set_value('jenis_obat') : $form['jenis_obat'],
				'class="form-control chzn-select"'
			);
		?>
	</div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
	<label class="col-lg-2 control-label">Nama Dagang</label>
	<div class="col-lg-5">
		<?php
			echo form_input(
				'nama',
				(set_value('nama')) ? set_value('nama') : $form['nama'],
				'class="form-control"'
			);
		?>
	</div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
	<label class="col-lg-2 control-label">Nama Generik</label>
	<div class="col-lg-5">
		<?php
			echo form_input(
				'nama_generik',
				(set_value('nama_generik')) ? set_value('nama_generik') : $form['nama_generik'],
				'class="form-control"'
			);
		?>
	</div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
	<label class="col-lg-2 control-label">Kelas Terapi</label>
	<div class="col-lg-5">
		<?php
			echo form_input(
				'kelas_terapi',
				(set_value('kelas_terapi')) ? set_value('kelas_terapi') : $form['kelas_terapi'],
				'class="form-control"'
			);
		?>
	</div><!-- /.col -->
</div><!-- /form-group -->
<?php if(false): ?>
<div class="form-group">
	<label class="col-lg-2 control-label">Berat Netto</label>
	<div class="col-lg-5">
		<?php
			echo form_input(
				'berat_netto',
				(set_value('berat_netto')) ? set_value('berat_netto') : $form['berat_netto'],
				'class="form-control"'
			);
		?>
	</div><!-- /.col -->
</div><!-- /form-group -->
<?php endif; ?>
<div class="form-group">
	<label class="col-lg-2 control-label">VED</label>
	<div class="col-lg-5">
		<?php
			echo form_input(
				'ved',
				(set_value('ved')) ? set_value('ved') : $form['ved'],
				'class="form-control"'
			);
		?>
	</div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
	<label class="col-lg-2 control-label">Lead Time</label>
	<div class="col-lg-5">
		<div class="input-group">
			<?php
				echo form_input(
					'lead_time',
					(set_value('lead_time')) ? set_value('lead_time') : $form['lead_time'],
					'class="form-control"'
				);
			?>

			<span class="input-group-addon">Minggu</span>
		</div>
	</div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
	<label class="col-lg-2 control-label">Reorder Point</label>
	<div class="col-lg-5">
		<?php
			echo form_input(
				'reorder_point',
				(set_value('reorder_point')) ? set_value('reorder_point') : $form['reorder_point'],
				'class="form-control"'
			);
		?>
	</div>
</div>
<!-- <div class="form-group">
	<label class="col-lg-2 control-label">Quantity Per Dus</label>
	<div class="col-lg-5">
		<?php
			echo form_input(
				'quantity_per_dus',
				(set_value('quantity_per_dus')) ? set_value('quantity_per_dus') : $form['quantity_per_dus'],
				'class="form-control" id="quantity-per-dus"'
			);
		?>
	</div>
</div>
<div class="form-group">
	<label class="col-lg-2 control-label">Harga Jual Per Dus</label>
	<div class="col-lg-5">
		<?php
			echo form_input(
				'harga_per_dus',
				(set_value('harga_per_dus')) ? set_value('harga_per_dus') : $form['harga_per_dus'],
				'class="form-control" id="harga-per-dus"'
			);
		?>
	</div>
</div> -->
<div class="form-group">
	<label class="col-lg-2 control-label">Harga Jual Satuan</label>
	<div class="col-lg-5">
		<?php
			echo form_input(
				'harga_satuan',
				(set_value('harga_satuan')) ? set_value('harga_satuan') : $form['harga_satuan'],
				'class="form-control" id="harga-satuan"'
			);
		?>
	</div>
</div>

<?php if($this->uri->segment(2) == 'edit'): ?>
	<div class="form-group">
		<label class="col-lg-2 control-label">Jumlah Pusat</label>
		<div class="col-lg-5">
			<?php
				echo form_input(
					'jumlah_pusat',
					(set_value('jumlah_pusat')) ? set_value('jumlah_pusat') : $form['jumlah_pusat'],
					'class="form-control"'
				);
			?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-lg-2 control-label">Jumlah Rawat Inap</label>
		<div class="col-lg-5">
			<?php
				echo form_input(
					'jumlah_rawat_inap',
					(set_value('jumlah_rawat_inap')) ? set_value('jumlah_rawat_inap') : $form['jumlah_rawat_inap'],
					'class="form-control"'
				);
			?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-lg-2 control-label">Jumlah Rawat Jalan</label>
		<div class="col-lg-5">
			<?php
				echo form_input(
					'jumlah_rawat_jalan',
					(set_value('jumlah_rawat_jalan')) ? set_value('jumlah_rawat_jalan') : $form['jumlah_rawat_jalan'],
					'class="form-control"'
				);
			?>
		</div>
	</div>
<?php endif; ?>

<div class="form-group">
	<label class="col-lg-2 control-label">Catatan</label>
	<div class="col-lg-5">		
		<textarea name="catatan" class="form-control" rows="4"><?php echo (set_value('catatan')) ? set_value('catatan') : $form['catatan']; ?></textarea>
	</div><!-- /.col -->
</div><!-- /form-group -->

<!-- <div class="form-group">
	<label class="col-lg-2 control-label">Nomor Faktur</label>
	<div class="col-lg-5">
		<?php
			echo form_input(
				'nomor_faktur',
				(set_value('nomor_faktur')) ? set_value('nomor_faktur') : $form['nomor_faktur'],
				'class="form-control" id="nomor-faktur"'
			);
		?>
	</div>
</div>
<div class="form-group">
	<label class="col-lg-2 control-label">Tanggal Penerimaan</label>
	<div class="col-lg-5">
		<div class="input-group">
			<?php
				echo form_input(
					'tanggal_penerimaan',
					(set_value('tanggal_penerimaan')) ? set_value('tanggal_penerimaan') : $form['tanggal_penerimaan'],
					'class="form-control datepicker" id="tanggal-penerimaan"'
				);
			?>
			<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
		</div>
	</div>
</div>
<div class="form-group">
	<label class="col-lg-2 control-label">Supplier</label>
	<div class="col-lg-5">
		<?php
			echo form_dropdown(
				'supplier',
				$supplier_dropdown,
				(set_value('supplier')) ? set_value('supplier') : $form['supplier'],
				'class="form-control chzn-select"'
			);
		?>
	</div>
</div>
<div class="form-group">
	<label class="col-lg-2 control-label">Jumlah Dus</label>
	<div class="col-lg-5">
		<?php
			echo form_input(
				'jumlah_dus',
				(set_value('jumlah_dus')) ? set_value('jumlah_dus') : $form['jumlah_dus'],
				'class="form-control" id="jumlah-dus"'
			);
		?>
	</div>
</div>

<div class="form-group">
	<label class="col-lg-2 control-label">Quantity Per Dus</label>
	<div class="col-lg-5">
		<?php
			echo form_input(
				'quantity_per_dus',
				(set_value('quantity_per_dus')) ? set_value('quantity_per_dus') : $form['quantity_per_dus'],
				'class="form-control" id="quantity-per-dus"'
			);
		?>
	</div>
</div>

<div class="form-group">
	<label class="col-lg-2 control-label">Harga Per Dus</label>
	<div class="col-lg-5">
		<?php
			echo form_input(
				'harga_per_dus',
				(set_value('harga_per_dus')) ? set_value('harga_per_dus') : $form['harga_per_dus'],
				'class="form-control" id="harga-per-dus"'
			);
		?>
	</div>
</div>

<div class="form-group">
	<label class="col-lg-2 control-label">Jumlah Satuan</label>
	<div class="col-lg-5">
		<p class="form-control-static" id="jumlah-satuan"><?php echo (set_value('jumlah_satuan')) ? format_number(set_value('jumlah_satuan')) : format_number($form['jumlah_satuan']); ?></p>
		<?php
			echo form_hidden(
				'jumlah_satuan',
				(set_value('jumlah_satuan')) ? set_value('jumlah_satuan') : $form['jumlah_satuan'],
				'class="form-control" id="jumlah-satuan-input"'
			);
		?>
	</div>
</div>

<div class="form-group">
	<label class="col-lg-2 control-label">Harga Satuan</label>
	<div class="col-lg-5">
		<p class="form-control-static" id="harga-satuan"><?php echo (set_value('harga_satuan')) ? format_number(set_value('harga_satuan')) : format_number($form['harga_satuan']); ?></p>
		<?php
			echo form_hidden(
				'harga_satuan',
				(set_value('harga_satuan')) ? set_value('harga_satuan') : $form['harga_satuan'],
				'class="form-control" id="harga-satuan-input"'
			);
		?>
	</div>
</div> -->
<br>
<div class="form-group">
	<label class="col-lg-2 control-label"></label>
	<div class="col-lg-5">
		<div class="input-group">
			<input type="submit" class="form-control btn btn-primary">
		</div>
	</div>
</div>

<!-- <div class="form-group">
	<label class="col-lg-2 control-label">Form in modal</label>
	<div class="col-lg-10">
		<a href="http://websdevp.com/Endless/form_element.html#formModal" class="btn btn-success" data-toggle="modal">Form In Modal</a>
	</div>
</div> -->


<script type="text/javascript">
	// function addSeparator(nStr, inD, outD, sep)
	// {
	// 	nStr += '';
	// 	var dpos = nStr.indexOf(inD);
	// 	var nStrEnd = '';
	// 	if (dpos != -1) {
	// 		nStrEnd = outD + nStr.substring(dpos + 1, nStr.length);
	// 		nStr = nStr.substring(0, dpos);
	// 	}
	// 	var rgx = /(\d+)(\d{3})/;
	// 	while (rgx.test(nStr)) {
	// 		nStr = nStr.replace(rgx, '$1' + sep + '$2');
	// 	}
	// 	return nStr + nStrEnd;
	// }

	// // bind('input') untuk trigger tiap change dan copy paste ke field
	// $('#jumlah-dus').bind('input', function() {
	// 	hitung_jumlah_satuan();
	// });

	// $('#quantity-per-dus').bind('input', function() {
	// 	hitung_jumlah_satuan();
	// 	hitung_harga_satuan();
	// });

	// $('#harga-per-dus').bind('input', function() {
	// 	hitung_harga_satuan();
	// });

	// function hitung_jumlah_satuan() {
	// 	var jumlah_satuan = $('#jumlah-dus').val() * $('#quantity-per-dus').val()
	// 	jumlah_satuan_separator = addSeparator(jumlah_satuan, '.', ',', '.');
	// 	$('#jumlah-satuan').html(jumlah_satuan_separator);
	// 	$('input[name=jumlah_satuan]').val(jumlah_satuan);
	// 	// console.log($('#jumlah-satuan-input').val());
	// 	// console.log(jumlah_satuan);
	// }

	// function hitung_harga_satuan() {
	// 	if($('#quantity-per-dus').val() == 0) {
	// 		$('#harga-satuan').html(0);
	// 		$('#harga-satuan-input').val(0);
	// 	}else {
	// 		var harga_satuan = $('#harga-per-dus').val() / $('#quantity-per-dus').val();
	// 		harga_satuan_separator = addSeparator(harga_satuan, '.', ',', '.');
	// 		$('#harga-satuan').html(harga_satuan_separator);
	// 		$('input[name=harga_satuan]').val(harga_satuan);
	// 		// console.log($('input[name=harga_satuan]').val());
	// 		// console.log(harga_satuan);
	// 	}
	// }
</script>

<script type="text/javascript">
	// $('#nomor-faktur').autocomplete({
	//     serviceUrl: '<?php echo site_url("obat/get_nomor_faktur"); ?>',
	//     onSearchComplete: function (query, suggestions) {
	//     	// console.log(query);
	//     	// console.log(suggestions);
	//     },
	//     onSelect: function (suggestion) {
	//     	// console.log(suggestion.tanggal);
	//     	$('#tanggal-penerimaan').val(suggestion.tanggal);
	//     }
	// });
</script>