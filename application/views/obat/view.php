<div id="main-container">
	<div class="padding-md">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4><?php echo $data->nama_obat . ' - ' . $data->nama_generik; ?></h4>
			</div>
			<table class="table table-hover table-striped">
				<thead>
					<tr>
						<th width="150px"></th>
						<th width="200px"></th>
						<th width="150px"></th>
						<th width="300px"></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Jenis</td>
						<td><?php echo $data->nama_kategori; ?></td>
						<td>VED</td>
						<td><?php echo $data->ved; ?></td>
					</tr>
					<tr>
						<td>Satuan</td>
						<td><?php echo $data->satuan; ?></td>
						<td>ABC</td>
						<td><?php echo $data->abc; ?></td>
					</tr>
					<tr>
						<td>Obat / Alkes</td>
						<td><?php echo ucfirst($data->jenis); ?></td>
						<td>Lead Time</td>
						<td><?php echo $data->lead_time; ?></td>
					</tr>
					<tr>
						<td>Jumlah Pusat</td>
						<td><?php echo $data->jumlah_pusat; ?></td>
						<td>Safety Stock</td>
						<td><?php echo $data->safety_stock; ?></td>
					</tr>
					<tr>
						<!-- <td>Quantity Per Dus</td>
						<td><?php echo $data->quantity_per_dus; ?></td> -->
						<td>Jumlah Rawat Inap</td>
						<td><?php echo $data->jumlah_rawat_inap; ?></td>
						<td>Reorder Point</td>
						<td><?php echo $data->reorder_point; ?></td>
					</tr>
					<tr>
						<td>Jumlah Rawat Jalan</td>
						<td><?php echo $data->jumlah_rawat_jalan; ?></td>
						<td>Economic Order Quantity</td>
						<td><?php echo $data->economic_order_quantity; ?></td>
					</tr>
					<tr>
						<td>Kelas Terapi</td>
						<td><?php echo $data->kelas_terapi; ?></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<!-- <td>Harga Jual Per Dus</td>
						<td><?php echo format_number($data->harga_per_dus, TRUE); ?></td> -->
						<td>Catatan</td>
						<td><?php echo $data->catatan;?></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<!-- <td>Harga Jual Satuan</td>
						<td><?php echo format_number($data->harga_satuan, TRUE); ?></td> -->
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			</table>

			<div class="panel-heading">
				<h4></h4>
			</div>
			<table class="table table-hover table-striped">
				<thead>
					<tr>
						<th>Kode Pabrik</th>
						<th>Quantity Per Dus</th>
						<th>Harga Beli Per Dus</th>
						<th>Harga Jual Satuan</th>
						<th>Harga Beli Satuan</th>
					</tr>
				</thead>

				<tbody>
					<?php foreach($supplier as $rows): ?>
						<tr>
							<td><?php echo $rows->nama_supplier; ?></td>
							<td><?php echo $rows->quantity_per_dus; ?></td>
							<td><?php echo format_number($rows->harga_per_dus, TRUE); ?></td>
							<td><?php echo format_number($rows->harga_jual_satuan, TRUE); ?></td>
							<td><?php echo format_number($rows->harga_satuan, TRUE); ?></td>
						</tr>
					<?php endforeach?>
				</tbody>
			</table>
		</div><!-- /panel -->
	</div><!-- /.col -->
</div>