<div id="main-container">
	<div class="padding-md">
		<div class="panel panel-default table-responsive">
			<div class="padding-md clearfix">
				<a href="<?php echo site_url(); ?>/obat/add" class="btn btn-primary"><i class="fa fa-lg fa-plus" style="padding-right: 5px;"></i>Tambah Jenis Obat</a>
				<div style="margin-bottom: 20px;"></div>
				<div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
					<table class="table table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
						<thead>
							<tr role="row">
								<th>Nama Obat</th>
								<th>Jenis</th>
								<th>Jumlah Pusat</th>
								<th>Jumlah Rawat Inap</th>
								<th>Jumlah Rawat Jalan</th>
								<th>Action</th>
							</tr>
						</thead>

						<tbody>
							<?php foreach($data as $rows): ?>
								<tr>
									<td><?php echo $rows->nama_obat; ?></td>
									<td><?php echo $rows->nama_kategori?></td>
									<td><?php echo $rows->jumlah_pusat; ?></td>
									<td><?php echo $rows->jumlah_rawat_inap; ?></td>
									<td><?php echo $rows->jumlah_rawat_jalan; ?></td>
									<td>
										<a class="detail" title="Detail" rel="tooltip" href="<?php echo site_url('obat/view/'.$rows->id_katalog_obat); ?>">
	                                        <i class="fa fa-lg fa-eye"></i></a>
										<a class="update" title="Edit" rel="tooltip" href="<?php echo site_url('obat/edit/'.$rows->id_katalog_obat); ?>">
	                                        <i class="fa fa-pencil fa-lg"></i></a>
	                                    <a class="delete" title="Hapus" rel="tooltip" href="<?php echo site_url('obat/delete/'.$rows->id_katalog_obat); ?>">
	                                        <i class="fa fa-trash-o fa-lg"></i></a>
	                                    <a class="add" title="Tambah Penerimaan" rel="tooltip" href="<?php echo site_url('penerimaan/add/'.$rows->id_katalog_obat); ?>">
	                                        <i class="fa fa-lg fa-plus-circle"></i></a>
	                                    <a class="add" title="History Penerimaan" rel="tooltip" href="<?php echo site_url('penerimaan/index/'.$rows->id_katalog_obat); ?>">
	                                        <i class="fa fa-file-o fa-lg"></i></a>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
			</div><!-- /.padding-md -->
		</div><!-- /panel -->
	</div><!-- /.padding-md -->
</div><!-- /main-container -->