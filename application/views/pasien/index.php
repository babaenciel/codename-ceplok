<div id="main-container">
	<div class="padding-md">
		<div class="panel panel-default table-responsive">
			<div class="padding-md clearfix">
				<a href="<?php echo site_url(); ?>/pasien/add" class="btn btn-primary"><i class="fa fa-lg fa-plus" style="padding-right: 5px;"></i>Pendaftaran Pasien</a>
				<div style="margin-bottom: 20px;"></div>
				<?php echo $this->load->view('elements/error_message'); ?>
				<div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
					<table class="table table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
						<thead>
							<tr role="row">
								<th style="width: 100px">
									<div>Nomor Pasien</div>
								</th>
								<th>
									<div>Nama Pasien</div>
								</th>
								<th>
									<div>Jenis Kelamin</div>
								</th>
								<th>
									<div>Alamat</div>
								</th>
								<th>
									<div>Umur</div>
								</th>
								<th style="width: 180px">
									<div>Action</div>
								</th>
							</tr>
						</thead>

						<tbody role="alert" aria-live="polite" aria-relevant="all">
							<?php foreach($data as $rows): ?>
								<tr>
									<td><?php echo $rows->id_pasien; ?></td>
									<td>
										<?php echo $rows->nama_pasien; ?>
										<?php if($rows->status == PASIEN_STATUS_MATI): ?>
											<span style="color: red"> (Meninggal)</span>
										<?php endif; ?>
									</td>
									<td><?php echo ($rows->jenis_kelamin == 'L') ? 'Laki-Laki' : 'Perempuan'; ?></td>
									<td><?php echo $rows->alamat_pasien; ?></td>
									<td><?php echo $rows->umur_pasien; //echo get_age($rows->tanggal_lahir); ?></td>
									<td>
										<a class="detail" title="Detail" rel="tooltip" href="<?php echo site_url('pasien_rawat_inap/view/'.$rows->id_pasien); ?>">
                                            <i class="fa fa-lg fa-eye"></i></a>
										<a class="update" title="Edit Data Pasien" rel="tooltip" href="<?php echo site_url('pasien/edit/'.$rows->id_pasien); ?>">
	                                        <i class="fa fa-pencil fa-lg"></i></a>
	                                    <a class="delete" title="Hapus" rel="tooltip" href="<?php echo site_url('pasien/delete/'.$rows->id_pasien); ?>">
	                                        <i class="fa fa-trash-o fa-lg"></i></a>
	                                    <?php if($rows->status == PASIEN_STATUS_HIDUP): ?>
		                                    <a class="status-mati" title="Meninggal" rel="tooltip" href="<?php echo site_url('pasien/change_status/'.$rows->id_pasien.'/'.PASIEN_STATUS_MATI); ?>">
		                                        <i class="fa fa-heart fa-lg"></i></a>
		                                <?php else: ?>
		                                	<a class="status-hidup" title="Hidup" rel="tooltip" href="<?php echo site_url('pasien/change_status/'.$rows->id_pasien.'/'.PASIEN_STATUS_HIDUP); ?>">
		                                        <i class="fa fa-heart-o fa-lg"></i></a>
		                            	<?php endif; ?>
									</td>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>
			</div><!-- /.padding-md -->
		</div><!-- /panel -->
	</div><!-- /.padding-md -->
</div><!-- /main-container -->