<div id="main-container">
    <div class="padding-md">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div style="font-size: 20px">Data Pasien</div>
            </div>
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th width="200px"></th>
                        <th></th>
                        <th width="200px"></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Nomor</td>
                        <td><?php echo $data->id_pasien; ?></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Nama</td>
                        <td><?php echo $data->nama_pasien; ?></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Nomor BPJS</td>
                        <td><?php echo $data->nomor_bpjs; ?></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Jenis Kelamin</td>
                        <td><?php echo ($data->jenis_kelamin == 'L') ? 'Laki-Laki' : 'Perempuan'; ?></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td><?php echo $data->alamat_pasien; ?></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Tanggal Lahir</td>
                        <td><?php echo !empty($data->tanggal_lahir) ? conv_date_format($data->tanggal_lahir, 'd-m-Y') : ''; ?></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Umur</td>
                        <td><?php echo $data->umur_pasien; //echo get_age($data->tanggal_lahir); ?></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Tinggi Badan</td>
                        <td><?php echo $data->tinggi_badan; ?></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Berat Badan</td>
                        <td><?php echo $data->berat_badan; ?></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Sistole</td>
                        <td><?php echo $data->sistole; ?></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Diastole</td>
                        <td><?php echo $data->diastole; ?></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Respiratory Rate</td>
                        <td><?php echo $data->respiratory_rate; ?></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Heart Rate</td>
                        <td><?php echo $data->heart_rate; ?></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Telepon</td>
                        <td><?php echo $data->telepon; ?></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Nama Keluarga</td>
                        <td><?php echo $data->nama_keluarga; ?></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>No Telp Keluarga</td>
                        <td><?php echo $data->telepon_keluarga; ?></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div><!-- /panel -->

        <div class="panel panel-default table-responsive">
            <div class="panel-heading">
                <div style="font-size: 20px">History Rawat Inap</div>
            </div>
            <div class="padding-md clearfix">
                <div style="margin-bottom: 10px;"></div>
                <div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
                    <table class="table table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
                        <thead>
                            <tr role="row">
                                <th>Tanggal Masuk</th>
                                <th>Tanggal Keluar</th>
                                <th>Dokter</th>
                                <th>Diagnosa</th>
                                <th>Total Biaya Terbayar</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach($rawat_inap as $rows): ?>
                                <tr>
                                    <td><?php echo !empty($rows->tgl_masuk) ? conv_date_format($rows->tgl_masuk, 'd-m-Y') : ''; ?></td>
                                    <td><?php echo !empty($rows->tgl_keluar) ? conv_date_format($rows->tgl_keluar, 'd-m-Y') : ''; ?></td>
                                    <td><?php echo $rows->nama_dokter; ?></td>
                                    <td><?php echo $rows->catatan_medik; ?></td>
                                    <td><?php echo format_number($rows->total_biaya, TRUE); ?></td>
                                    <td>
                                        <a class="detail" title="Detail" rel="tooltip" href="<?php echo site_url('pasien_rawat_inap/history_pelayanan/'.$rows->id_rawat_inap); ?>">
                                            <i class="fa fa-lg fa-eye"></i></a>
                                        <a class="print" title="Cetak Nota" rel="tooltip" href="<?php echo site_url('nota/index/'.$rows->id_rawat_inap); ?>">
                                            <i class="fa fa-lg fa-print"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
            </div><!-- /.padding-md -->
        </div><!-- /panel -->
    </div><!-- /.col -->
</div>