<div id="main-container">
	<div class="padding-md">
		<div class="panel panel-default">
			<div class="panel-body">
				<?php echo $this->load->view('elements/error_message'); ?>
				<form method="POST" action="<?php echo site_url('pasien/add'); ?>" id="formToggleLine" class="form-horizontal no-margin">
					<?php $this->load->view('pasien/_form', $form); ?>
				</form>
			</div>
		</div><!-- /panel -->
	</div><!-- /.padding-md -->
</div><!-- /main-container -->