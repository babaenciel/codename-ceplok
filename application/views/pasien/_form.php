<?php //if($this->uri->segment(2) === 'edit'): ?>
<div class="form-group">
	<label class="col-lg-2 control-label">ID Pasien</label>
	<div class="col-lg-5">
		<?php
			echo form_input(
				'id_pasien',
				(set_value('id_pasien')) ? set_value('id_pasien') : $form['id_pasien'],
				'class="form-control"'
			);
		?>
	</div><!-- /.col -->
</div><!-- /form-group -->
<?php //endif; ?>
<div class="form-group">
	<label class="col-lg-2 control-label">Nama Pasien</label>
	<div class="col-lg-5">
		<?php
			echo form_input(
				'nama',
				(set_value('nama')) ? set_value('nama') : $form['nama'],
				'class="form-control"'
			);
		?>
	</div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
	<label class="col-lg-2 control-label">Nomor BPJS</label>
	<div class="col-lg-5">
		<?php
			echo form_input(
				'nomor_bpjs',
				(set_value('nomor_bpjs')) ? set_value('nomor_bpjs') : $form['nomor_bpjs'],
				'class="form-control"'
			);
		?>
	</div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
	<label class="col-lg-2 control-label">Jenis Kelamin</label>
	<div class="col-lg-5">
		<label class="label-radio inline">
			<?php
				if($form['jenis_kelamin'] == 'L') {
					echo form_radio(
						'jenis_kelamin',
						'L',
						(set_radio('jenis_kelamin', 'L')) ? set_radio('jenis_kelamin', 'L') : 'checked'
					);
				}else {
					echo form_radio(
						'jenis_kelamin',
						'L',
						(set_radio('jenis_kelamin', 'L')) ? set_radio('jenis_kelamin', 'L') : ''
					);
				}
			?>
			<span class="custom-radio"></span> Laki-laki
		</label>
		<label class="label-radio inline">
			<?php
				if($form['jenis_kelamin'] == 'P') {
					echo form_radio(
						'jenis_kelamin',
						'P',
						(set_radio('jenis_kelamin', 'P')) ? set_radio('jenis_kelamin', 'P') : 'checked'
					);
				}else {
					echo form_radio(
						'jenis_kelamin',
						'P',
						(set_radio('jenis_kelamin', 'P')) ? set_radio('jenis_kelamin', 'P') : ''
					);
				}

			?>
			<span class="custom-radio"></span> Perempuan
		</label>
	</div><!-- /.col -->
</div><!-- /form-group -->
<!-- <div class="form-group">
	<label class="col-lg-2 control-label">Tanggal Lahir</label>
	<div class="col-lg-5">
		<input name="tanggal_lahir" class="form-control" type="text">
	</div>
</div> -->
<div class="form-group">
	<label class="col-lg-2 control-label">Alamat</label>
	<div class="col-lg-5">
		<?php
			echo form_input(
				'alamat',
				(set_value('alamat')) ? set_value('alamat') : $form['alamat'],
				'class="form-control"'
			);
		?>
	</div><!-- /.col -->
</div><!-- /form-group -->

<div class="form-group">
	<label class="col-lg-2 control-label">Telepon</label>
	<div class="col-lg-2">
		<?php
			echo form_input(
				'telepon',
				(set_value('telepon')) ? set_value('telepon') : $form['telepon'],
				'class="form-control"'
			);
		?>
	</div><!-- /.col -->
</div><!-- /form-group -->

<div class="form-group">
	<label class="col-lg-2 control-label">Tanggal Lahir</label>
	<div class="col-lg-2">
		<div class="input-group">
			<?php
				echo form_input(
					'tanggal_lahir',
					(set_value('tanggal_lahir')) ? set_value('tanggal_lahir') : $form['tanggal_lahir'],
					'class="form-control datepicker" id="birthdate"'
				);
			?>
		</div>
	</div><!-- /.col -->
</div><!-- /form-group -->

<div class="form-group">
	<label class="col-lg-2 control-label">Umur</label>
	<div class="col-lg-2">
		<div class="input-group">
			<?php
			echo form_input(
				'umur',
				(set_value('umur')) ? set_value('umur') : $form['umur'],
				'class="form-control" id="age"'
			);
		?>
			<span class="input-group-addon">Tahun</span>
		</div>
	</div><!-- /.col -->
</div><!-- /form-group -->

<div class="form-group">
	<label class="col-lg-2 control-label">Tinggi Badan</label>
	<div class="col-lg-2">
		<div class="input-group">
			<?php
			echo form_input(
				'tinggi_badan',
				(set_value('tinggi_badan')) ? set_value('tinggi_badan') : $form['tinggi_badan'],
				'class="form-control"'
			);
		?>
			<span class="input-group-addon">Cm</span>
		</div>
	</div><!-- /.col -->
</div><!-- /form-group -->

<div class="form-group">
	<label class="col-lg-2 control-label">Berat Badan</label>
	<div class="col-lg-2">
		<div class="input-group">
			<?php
			echo form_input(
				'berat_badan',
				(set_value('berat_badan')) ? set_value('berat_badan') : $form['berat_badan'],
				'class="form-control"'
			);
		?>
			<span class="input-group-addon">Kg</span>
		</div>
	</div><!-- /.col -->
</div><!-- /form-group -->

<div class="form-group">
	<label class="col-lg-2 control-label">Sistole</label>
	<div class="col-lg-2">
		<div class="input-group">
			<?php
			echo form_input(
				'sistole',
				(set_value('sistole')) ? set_value('sistole') : $form['sistole'],
				'class="form-control"'
			);
		?>
			<span class="input-group-addon">mmHg</span>
		</div>
	</div><!-- /.col -->
</div><!-- /form-group -->

<div class="form-group">
	<label class="col-lg-2 control-label">Diastole</label>
	<div class="col-lg-2">
		<div class="input-group">
			<?php
			echo form_input(
				'diastole',
				(set_value('diastole')) ? set_value('diastole') : $form['diastole'],
				'class="form-control"'
			);
		?>
			<span class="input-group-addon">mmHg</span>
		</div>
	</div><!-- /.col -->
</div><!-- /form-group -->

<div class="form-group">
	<label class="col-lg-2 control-label">Respiratory Rate</label>
	<div class="col-lg-2">
		<div class="input-group">
			<?php
			echo form_input(
				'respiratory_rate',
				(set_value('respiratory_rate')) ? set_value('respiratory_rate') : $form['respiratory_rate'],
				'class="form-control"'
			);
		?>
			<span class="input-group-addon">per Menit</span>
		</div>
	</div><!-- /.col -->
</div><!-- /form-group -->

<div class="form-group">
	<label class="col-lg-2 control-label">Heart Rate</label>
	<div class="col-lg-2">
		<div class="input-group">
			<?php
			echo form_input(
				'heart_rate',
				(set_value('heart_rate')) ? set_value('heart_rate') : $form['heart_rate'],
				'class="form-control"'
			);
		?>
			<span class="input-group-addon">bpm</span>
		</div>
	</div><!-- /.col -->
</div><!-- /form-group -->

<div class="form-group">
	<label class="col-lg-2 control-label">Nama Keluarga</label>
	<div class="col-lg-2">
		<div class="input-group">
			<?php
			echo form_input(
				'nama_keluarga',
				(set_value('nama_keluarga')) ? set_value('nama_keluarga') : $form['nama_keluarga'],
				'class="form-control"'
			);
		?>
		</div>
	</div><!-- /.col -->
</div><!-- /form-group -->

<div class="form-group">
	<label class="col-lg-2 control-label">No Telp Keluarga</label>
	<div class="col-lg-2">
		<div class="input-group">
			<?php
			echo form_input(
				'telepon_keluarga',
				(set_value('telepon_keluarga')) ? set_value('telepon_keluarga') : $form['telepon_keluarga'],
				'class="form-control"'
			);
		?>
		</div>
	</div><!-- /.col -->
</div><!-- /form-group -->

<!-- <div class="form-group">
	<label class="col-lg-2 control-label">Nomor Rekam Medik</label>
	<div class="col-lg-5">
		<input name="nomor_rekam_medik" class="form-control" type="text">
	</div>
</div> -->
<br>
<div class="form-group">
	<label class="col-lg-2 control-label"></label>
	<div class="col-lg-5">
		<div class="input-group">
			<input type="submit" class="form-control btn btn-primary">
		</div>
	</div>
</div>

<!-- <div class="form-group">
	<label class="col-lg-2 control-label">Form in modal</label>
	<div class="col-lg-10">
		<a href="http://websdevp.com/Endless/form_element.html#formModal" class="btn btn-success" data-toggle="modal">Form In Modal</a>
	</div>
</div> -->

<script type="text/javascript">
	$(document).ready(function() {
		$('#birthdate').trigger('change');
	});

	$('#birthdate').on('changeDate', function() {
		var birthDate = $(this).val();
		if(birthDate) {
			var dateSplit = birthDate.split('-');
			var newDate = dateSplit[2]+'-'+dateSplit[1]+'-'+dateSplit[0];
			var age = getAge(newDate);
			$('#age').val(age);
		}
	});

	function getAge(dateString) {
	    var today = new Date();
	    var birthDate = new Date(dateString);
	    var age = today.getFullYear() - birthDate.getFullYear();
	    var m = today.getMonth() - birthDate.getMonth();
	    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
	        age--;
	    }
	    return age;
	}

	$('#age').on('change', function() {
		var age = $(this).val();
		var today = new Date();
		var yearBirth = today.getFullYear() - age;
		$('#birthdate').val(today.getDate() + '-' + (today.getMonth()+1) + '-' + yearBirth);
		// console.log($)
		// console.log(today.getDate() + '-' + today.getMonth());
		// console.log(yearBirth);
	})
</script>