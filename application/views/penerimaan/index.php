<div id="main-container">
	<div class="padding-md">
		<div class="panel panel-default table-responsive">
			<div class="padding-md clearfix">
				<div style="margin-bottom: 20px;"></div>
				<div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
					<table class="table table-striped dataTable" id="datatable-penerimaan" aria-describedby="dataTable_info">
						<thead>
							<tr role="row">
								<th>Nomor Faktur</th>
								<th>Tanggal Penerimaan</th>
								<th>Jumlah Penerimaan</th>
								<th>Quantity Per Dus</th>
								<th>Harga Beli Per Dus</th>
								<th>Harga Beli Satuan</th>
								<!-- <th>Harga Jual Satuan</th> -->
								<th>Kode Pabrik</th>
								<th>Action</th>
							</tr>
						</thead>

						<tbody>
							<?php foreach($data as $rows): ?>
								<tr>
									<td><?php echo $rows->nomor_faktur; ?></td>
									<td><?php echo conv_date_format($rows->tanggal_penerimaan, 'd-m-Y'); ?></td>
									<td><?php echo $rows->jumlah_penerimaan; ?></td>
									<td><?php echo $rows->quantity_per_dus?></td>
									<td><?php echo format_number($rows->harga_per_dus, TRUE); ?></td>
									<td><?php echo format_number($rows->harga_satuan, TRUE); ?></td>
									<td><?php echo $rows->nama_supplier; ?></td>
									<td>
										<a class="update" title="Edit" rel="tooltip" href="<?php echo site_url('penerimaan/edit/'.$rows->id_penerimaan_obat_detail); ?>">
	                                        <i class="fa fa-pencil fa-lg"></i></a>
	                                    <a class="delete" title="Hapus" rel="tooltip" href="<?php echo site_url('penerimaan/delete/'.$rows->id_penerimaan_obat_detail); ?>">
	                                        <i class="fa fa-trash-o fa-lg"></i></a>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
			</div><!-- /.padding-md -->
		</div><!-- /panel -->
	</div><!-- /.padding-md -->
</div><!-- /main-container -->