<div id="main-container">
	<div class="padding-md">
		<div class="panel panel-default">
			<div class="panel-body">
				<?php $this->load->view('elements/error_message'); ?>
				<form action="<?php echo site_url('penerimaan/add/'.$id_katalog_obat); ?>" method="POST" id="formToggleLine" class="form-horizontal no-margin">
					<?php echo $this->load->view('penerimaan/_form'); ?>
				</form>
			</div>
		</div><!-- /panel -->
	</div><!-- /.padding-md -->
</div><!-- /main-container -->