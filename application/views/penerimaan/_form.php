<script src="<?php echo base_url(); ?>resources/jquery-autocomplete/jquery.autocomplete.min.js"></script>

<div class="form-group">
	<label class="col-lg-2 control-label">Nomor Faktur</label>
	<div class="col-lg-5">
		<?php
			echo form_input(
				'nomor_faktur',
				(set_value('nomor_faktur')) ? set_value('nomor_faktur') : $form['nomor_faktur'],
				'class="form-control" id="nomor-faktur"'
			);
		?>
	</div>
</div>
<div class="form-group">
	<label class="col-lg-2 control-label">Kode Pabrik</label>
	<div class="col-lg-5">
		<?php
			echo form_dropdown(
				'supplier',
				$supplier_dropdown,
				(set_value('supplier')) ? set_value('supplier') : $form['supplier'],
				'class="form-control chzn-select"'
			);
		?>

		<div class="clearfix"></div>
		<div style="margin-top: 10px">
			<a href="<?php echo site_url('administration/supplier_add'); ?>" class="fa fa-plus" style="font-size: 15px"> Tambah Supplier </a>
		</div>

	</div>
</div>
<div class="form-group">
	<label class="col-lg-2 control-label">Tanggal Penerimaan</label>
	<div class="col-lg-5">
		<div class="input-group">
			<?php
				echo form_input(
					'tanggal_penerimaan',
					(set_value('tanggal_penerimaan')) ? set_value('tanggal_penerimaan') : $form['tanggal_penerimaan'],
					'class="form-control datepicker" id="tanggal-penerimaan"'
				);
			?>
			<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
		</div>
	</div>
</div>

<div class="form-group">
	<label class="col-lg-2 control-label">Jumlah Dus</label>
	<div class="col-lg-5">
		<?php
			echo form_input(
				'jumlah_dus',
				(set_value('jumlah_dus')) ? set_value('jumlah_dus') : $form['jumlah_dus'],
				'class="form-control" id="jumlah-dus"'
			);
		?>
	</div>
</div>

<div class="form-group">
	<label class="col-lg-2 control-label">Quantity Per Dus</label>
	<div class="col-lg-5">
		<?php
			echo form_input(
				'quantity_per_dus',
				(set_value('quantity_per_dus')) ? set_value('quantity_per_dus') : $form['quantity_per_dus'],
				'class="form-control" id="quantity-per-dus"'
			);
		?>
	</div>
</div>

<div class="form-group">
	<label class="col-lg-2 control-label">Harga Beli Per Dus</label>
	<div class="col-lg-5">
		<?php
			echo form_input(
				'harga_per_dus',
				(set_value('harga_per_dus')) ? set_value('harga_per_dus') : $form['harga_per_dus'],
				'class="form-control" id="harga-per-dus"'
			);
		?>
	</div>
</div>

<div class="form-group">
	<label class="col-lg-2 control-label">Jumlah Satuan</label>
	<div class="col-lg-5">
		<p class="form-control-static" id="jumlah-satuan"><?php echo (set_value('jumlah_satuan')) ? format_number(set_value('jumlah_satuan')) : format_number($form['jumlah_satuan']); ?></p>
		<?php
			echo form_hidden(
				'jumlah_satuan',
				(set_value('jumlah_satuan')) ? set_value('jumlah_satuan') : $form['jumlah_satuan'],
				'class="form-control" id="jumlah-satuan-input"'
			);
		?>
	</div>
</div>

<div class="form-group">
	<label class="col-lg-2 control-label">Harga Beli Satuan</label>
	<div class="col-lg-5">
		<p class="form-control-static" id="harga-satuan"><?php echo (set_value('harga_satuan')) ? format_number(set_value('harga_satuan')) : format_number($form['harga_satuan']); ?></p>
		<?php
			echo form_hidden(
				'harga_satuan',
				(set_value('harga_satuan')) ? set_value('harga_satuan') : $form['harga_satuan'],
				'class="form-control" id="harga-satuan-input"'
			);
		?>
	</div>
</div>

<br>

<div class="form-group">
	<label class="col-lg-2 control-label"></label>
	<div class="col-lg-5">
		<div class="input-group">
			<input type="submit" class="form-control btn btn-primary">
		</div>
	</div>
</div>

<!-- <div class="form-group">
	<label class="col-lg-2 control-label">Form in modal</label>
	<div class="col-lg-10">
		<a href="http://websdevp.com/Endless/form_element.html#formModal" class="btn btn-success" data-toggle="modal">Form In Modal</a>
	</div>
</div> -->

<script type="text/javascript">
	function addSeparator(nStr, inD, outD, sep)
	{
		nStr += '';
		var dpos = nStr.indexOf(inD);
		var nStrEnd = '';
		if (dpos != -1) {
			nStrEnd = outD + nStr.substring(dpos + 1, nStr.length);
			nStr = nStr.substring(0, dpos);
		}
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(nStr)) {
			nStr = nStr.replace(rgx, '$1' + sep + '$2');
		}
		return nStr + nStrEnd;
	}

	// bind('input') untuk trigger tiap change dan copy paste ke field
	$('#jumlah-dus').bind('input', function() {
		hitung_jumlah_satuan();
	});

	$('#quantity-per-dus').bind('input', function() {
		hitung_jumlah_satuan();
		hitung_harga_satuan();
	});

	$('#harga-per-dus').bind('input', function() {
		hitung_harga_satuan();
	});

	function hitung_jumlah_satuan() {
		var jumlah_satuan = $('#jumlah-dus').val() * $('#quantity-per-dus').val()
		jumlah_satuan_separator = addSeparator(jumlah_satuan, '.', ',', '.');
		$('#jumlah-satuan').html(jumlah_satuan_separator);
		$('input[name=jumlah_satuan]').val(jumlah_satuan);
		// console.log($('input[name=jumlah_satuan]').val());
		// console.log(jumlah_satuan);
	}

	function hitung_harga_satuan() {
		if($('#quantity-per-dus').val() == 0) {
			$('#harga-satuan').html(0);
			$('#harga-satuan-input').val(0);
		}else {
			var harga_satuan = $('#harga-per-dus').val() / $('#quantity-per-dus').val();
			harga_satuan_separator = addSeparator(harga_satuan, '.', ',', '.');
			$('#harga-satuan').html(harga_satuan_separator);
			$('input[name=harga_satuan]').val(harga_satuan);
			// console.log($('input[name=harga_satuan]').val());
			// console.log(harga_satuan);
		}
	}
</script>

<script type="text/javascript">
	$('#nomor-faktur').autocomplete({
	    serviceUrl: '<?php echo site_url("obat/get_nomor_faktur"); ?>',
	    onSearchComplete: function (query, suggestions) {
	    	// console.log(query);
	    	// console.log(suggestions);
	    },
	    onSelect: function (suggestion) {
	    	// console.log(suggestion.tanggal);
	    	$('#tanggal-penerimaan').val(suggestion.tanggal);
	    }
	});
</script>