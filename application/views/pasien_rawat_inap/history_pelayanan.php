<div id="main-container">
    <div class="padding-md">
        <div class="panel panel-default table-responsive">
            <div class="padding-md clearfix">
                <div style="margin-bottom: 20px;"></div>
                <div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
                    <table class="table table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
                        <thead>
                            <tr role="row">
                                <th>
                                    <div>Jenis Pelayanan</div>
                                </th>
                                <th>
                                    <div>Nama Pelayanan</div>
                                </th>
                                <th>
                                    <div>Tanggal Pelayanan</div>
                                </th>
                            </tr>
                        </thead>

                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <!-- -------------------------- KAMAR -------------------------- -->

                            <?php //dump($history['kamar']); ?>
                            <?php if(isset($history['kamar'])): ?>
                                <?php foreach($history['kamar'] as $keys=>$rows): ?>
                                    <tr>
                                        <td>Kamar</td>
                                        <td><?php echo $rows->nama_kamar . ' - ' . $rows->jenis_kamar; ?></td>
                                        <?php $tgl_akhir = !empty($rows->tgl_akhir) ? conv_date_format($rows->tgl_akhir, 'd-m-Y') : ''; ?>                                
                                        <td><?php echo conv_date_format($rows->tgl_mulai, 'd-m-Y') . ' - ' . $tgl_akhir; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <!-- -------------------------- OBAT -------------------------- -->

                            <?php //dump($history['obat']); ?>
                            <?php if(isset($history['obat'])): ?>
                                <?php foreach($history['obat'] as $keys=>$rows): ?>
                                    <tr>
                                        <td>Obat</td>                                        
                                        <?php $jumlah_obat = $rows->jumlah_pengeluaran - $rows->sisa; ?>
                                        <td><?php echo $rows->nama_obat . ': ' . $jumlah_obat . ' ' . $rows->satuan; ?></td>
                                        <td><?php echo conv_date_format($rows->tanggal_resep, 'd-m-Y'); ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <!-- -------------------------- OBAT PULANG -------------------------- -->

                            <?php //dump($history['obat_pulang']); ?>
                            <?php if(isset($history['obat_pulang'])): ?>
                                <?php foreach($history['obat_pulang'] as $keys=>$rows): ?>
                                    <tr>
                                        <td>Obat Pulang</td>
                                        <?php $jumlah_obat = $rows->jumlah_pengeluaran - $rows->sisa; ?>
                                        <td><?php echo $rows->nama_obat . ': ' . $jumlah_obat . ' ' . $rows->satuan; ?></td>
                                        <td><?php echo conv_date_format($rows->tanggal_resep, 'd-m-Y'); ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <!-- -------------------------- ALKES -------------------------- -->

                            <?php //dump($history['alkes']); ?>
                            <?php if(isset($history['alkes'])): ?>
                                <?php foreach($history['alkes'] as $keys=>$rows): ?>
                                    <tr>
                                        <td>Alkes</td>
                                        <td><?php echo $rows->nama_obat . ': ' . $rows->jumlah_pengeluaran . ' ' . $rows->satuan; ?></td>
                                        <td><?php echo conv_date_format($rows->tanggal_resep, 'd-m-Y'); ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <!-- -------------------------- GIZI -------------------------- -->

                            <?php //dump($history['visit_dokter']); ?>
                            <?php if(isset($history['gizi'])): ?>
                                <?php foreach($history['gizi'] as $keys=>$rows): ?>                                     
                                    <tr>
                                        <td>Gizi</td>
                                        <td><?php echo $rows->nama_gizi . ' - ' . $rows->jenis_kamar ?></td>        
                                        <?php $tgl_akhir = !empty($rows->tgl_akhir) ? conv_date_format($rows->tgl_akhir, 'd-m-Y') : ''; ?>                                
                                        <td><?php echo conv_date_format($rows->tgl_mulai, 'd-m-Y') . ' - ' . $tgl_akhir; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <!-- -------------------------- VISIT DOKTER -------------------------- -->

                            <?php //dump($history['visit_dokter']); ?>
                            <?php if(isset($history['visit_dokter'])): ?>
                                <?php foreach($history['visit_dokter'] as $keys=>$rows): ?>
                                    <tr>
                                        <td>Visit Dokter</td>
                                        <td><?php echo $rows->nama_dokter . ' - ' . $rows->jenis_dokter . ' - ' . $rows->jenis_kamar; ?></td>
                                        <td><?php echo conv_date_format($rows->tanggal_visit, 'd-m-Y'); ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <!-- -------------------------- TINDAKAN -------------------------- -->

                            <?php //dump($history['tindakan']); ?>
                            <?php if(isset($history['tindakan'])): ?>
                                <?php foreach($history['tindakan'] as $keys=>$rows): ?>                                
                                    <tr>
                                        <td>Tindakan</td>                                    
                                        <td>
                                            <?php if($rows->tindakan_lainnya == 0): ?>
                                                <?php echo $rows->nama_tindakan . ' - ' . $rows->jenis_kamar; ?>
                                            <?php else: ?>
                                                <?php echo $rows->nama_tindakan; ?>
                                            <?php endif; ?>                                            
                                        </td>
                                        <td><?php echo conv_date_format($rows->tgl_tindakan, 'd-m-Y'); ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <!-- -------------------------- LABORATORIUM -------------------------- -->
                            <?php //dump($history['laboratorium']); ?>
                            <?php if(isset($history['laboratorium'])): ?>
                                <?php foreach($history['laboratorium'] as $keys=>$rows): ?>
                                    <tr>
                                        <td>Laboratorium</td>
                                        <td><?php echo $rows->nama_layanan . ' - ' . $rows->jenis_kamar; ?></td>
                                        <td><?php echo conv_date_format($rows->tgl_laboratorium, 'd-m-Y'); ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <!-- -------------------------- RONTGEN -------------------------- -->                            
                            <?php if(isset($history['rontgen'])): ?>
                                <?php foreach($history['rontgen'] as $keys=>$rows): ?>
                                    <tr>
                                        <td>Rontgen</td>
                                        <td><?php echo $rows->nama_layanan . ' - ' . $rows->jenis_kamar; ?></td>
                                        <td><?php echo conv_date_format($rows->tgl_rontgen, 'd-m-Y'); ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
            </div><!-- /.padding-md -->
        </div><!-- /panel -->
    </div><!-- /.padding-md -->
</div><!-- /main-container -->