<div id="main-container">
	<div class="padding-md">
		<div class="panel panel-default">
			<div class="panel-body">
                <?php echo $this->load->view('elements/error_message'); ?>
				<form action="<?php echo site_url('pasien_rawat_inap/add'); ?>" method="POST" id="formToggleLine" class="form-horizontal no-margin">
					<?php $this->load->view('pasien_rawat_inap/_form', $form); ?>
				</form>
			</div>
		</div><!-- /panel -->
	</div><!-- /.padding-md -->
</div><!-- /main-container -->