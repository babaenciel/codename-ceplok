<div class="form-group">
	<label class="col-lg-2 control-label">Nama Pasien</label>
	<div class="col-lg-5">
		<?php
			echo form_dropdown(
				'pasien',
				$pasien_dropdown,
				( set_value('pasien') ? set_value('pasien') : $form['pasien'] ),
				'class="form-control chzn-select"'
			);
		?>
	</div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
	<label class="col-lg-2 control-label">Tanggal Masuk</label>
	<div class="col-lg-5">
		<?php
			echo form_input(
				'tanggal_masuk',
				(set_value('tanggal_masuk')) ? set_value('tanggal_masuk') : $form['tanggal_masuk'],
				'class="form-control datepicker"'
			);
		?>
	</div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
	<label class="col-lg-2 control-label">Tanggal Keluar</label>
	<div class="col-lg-5">
		<?php
			echo form_input(
				'tanggal_keluar',
				(set_value('tanggal_keluar')) ? set_value('tanggal_keluar') : $form['tanggal_keluar'],
				'class="form-control datepicker"'
			);
		?>
	</div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
	<label class="col-lg-2 control-label">Keluhan</label>
	<div class="col-lg-5">
		<textarea name="keluhan" class="form-control" rows="4"><?php echo (set_value('keluhan')) ? set_value('keluhan') : $form['keluhan']; ?></textarea>
	</div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
	<label class="col-lg-2 control-label">Diagnosa</label>
	<div class="col-lg-1">
		<?php
			echo form_input(
				'kode_diagnosa',
				(set_value('kode_diagnosa')) ? set_value('kode_diagnosa') : $form['kode_diagnosa'],
				'class="form-control"'
			);
		?>
	</div>		
	<div class="col-lg-5">		
		<textarea name="catatan_medik" class="form-control" rows="4"><?php echo (set_value('catatan_medik')) ? set_value('catatan_medik') : $form['catatan_medik']; ?></textarea>
	</div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
	<label class="col-lg-2 control-label">Kesadaran</label>
	<div class="col-lg-5">
		<?php
			echo form_dropdown(
				'kesadaran',
				$kesadaran_dropdown,
				( set_value('kesadaran') ? set_value('kesadaran') : $form['kesadaran'] ),
				'class="form-control chzn-select"'
			);
		?>
	</div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
	<label class="col-lg-2 control-label">Nama Dokter</label>
	<div class="col-lg-5">
		<?php
			echo form_dropdown(
				'dokter',
				$dokter_dropdown,
				( set_value('dokter') ? set_value('dokter') : $form['dokter'] ),
				'class="form-control chzn-select"'
			);
		?>
	</div><!-- /.col -->
</div><!-- /form-group -->
<br>
<div class="form-group">
	<label class="col-lg-2 control-label"></label>
	<div class="col-lg-5">
		<div class="input-group">
			<input type="submit" class="form-control btn btn-primary">
		</div>
	</div>
</div>

<!-- <div class="form-group">
	<label class="col-lg-2 control-label">Form in modal</label>
	<div class="col-lg-10">
		<a href="http://websdevp.com/Endless/form_element.html#formModal" class="btn btn-success" data-toggle="modal">Form In Modal</a>
	</div>
</div> -->
