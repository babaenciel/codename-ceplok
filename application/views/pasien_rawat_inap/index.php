<div id="main-container">
    <div class="padding-md">
        <div class="panel panel-default table-responsive">
            <div class="padding-md clearfix">
                <a href="<?php echo site_url(); ?>/pasien_rawat_inap/add" class="btn btn-primary"><i class="fa fa-lg fa-plus" style="padding-right: 5px;"></i>Daftarkan Pasien Rawat Inap</a>
                <div style="margin-bottom: 20px;"></div>
                <div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
                    <table class="table table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
                        <thead>
                            <tr role="row">
                                <th style="width: 100px">
                                    <div>Nomor Pasien</div>
                                </th>
                                <th style="width: 100px">
                                    <div>Status</div>
                                </th>
                                <th>
                                    <div>Nama Pasien</div>
                                </th>
                                <th>
                                    <div>Jenis Kelamin</div>
                                </th>
                                <th>
                                    <div>Alamat</div>
                                </th>
                                <th>
                                    <div>Umur</div>
                                </th>
                                <th>
                                    Kamar
                                </th>
                                <th style="width: 170px">
                                    <div>Action</div>
                                </th>
                                <th>
                                    Cetak
                                </th>
                            </tr>
                        </thead>

                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php foreach($data as $rows): ?>
                                <tr>
                                    <td style="text-align:center">
                                        <?php echo $rows->id_pasien_ordered; ?>
                                    </td>
                                    <td>
                                        <?php if($rows->status == 1): ?>
                                            <span class="label label-success">Sedang Inap</span>
                                        <?php endif;?>
                                    </td>
                                    <td><?php echo $rows->nama_pasien; ?></td>
                                    <td><?php echo ($rows->jenis_kelamin == 'L') ? 'Laki-Laki' : 'Perempuan'; ?></td>
                                    <td><?php echo $rows->alamat_pasien; ?></td>
                                    <td><?php echo $rows->umur_pasien; ?></td>
                                    <td><?php echo $rows->nama_kamar . ' - ' . $rows->jenis_kamar; ?></td>
                                    <td>
                                        <a class="detail" title="Detail" rel="tooltip" href="<?php echo site_url('pasien_rawat_inap/view/'.$rows->id_pasien_ordered); ?>">
                                            <i class="fa fa-lg fa-eye"></i></a>
                                        <a class="update" title="Edit Data Pasien Rawat Inap" rel="tooltip" href="<?php echo site_url('pasien_rawat_inap/edit/'.$rows->id_rawat_inap); ?>">
                                            <i class="fa fa-pencil-square-o fa-lg"></i></a>
                                        <a class="pulang" title="Pulang" rel="tooltip" href="<?php echo site_url('pasien_rawat_inap/pulang/'.$rows->id_rawat_inap); ?>">
                                            <i class="fa fa-undo fa-lg"></i></a>
                                        <br>
                                        <a class="detail" title="Kamar" rel="tooltip" href="<?php echo site_url('pemakaian_kamar/index/'.$rows->id_rawat_inap); ?>">
                                            <i class="fa fa-lg fa-home" style="padding-top: 10px"></i></a>
                                        <a class="detail" title="Visit Dokter" rel="tooltip" href="<?php echo site_url('visit_dokter/index/'.$rows->id_rawat_inap); ?>">
                                            <i class="fa fa-lg fa-user-md" style="padding-top: 10px"></i></a>
                                        <a class="detail" title="Obat dan Alkes" rel="tooltip" href="<?php echo site_url('obat_dan_alkes/index/'.$rows->id_rawat_inap); ?>">
                                            <i class="fa fa-lg fa-medkit" style="padding-top: 10px"></i></a>
                                        <a class="detail" title="Laboratorium" rel="tooltip" href="<?php echo site_url('laboratorium/index/'.$rows->id_rawat_inap); ?>">
                                            <i class="fa fa-lg fa-hospital-o" style="padding-top: 10px"></i></a>
                                        <a class="detail" title="Tindakan" rel="tooltip" href="<?php echo site_url('tindakan/index/'.$rows->id_rawat_inap); ?>">
                                            <i class="fa fa-lg fa-stethoscope" style="padding-top: 10px"></i></a>                                        
                                        <br>
                                        <a class="detail" title="Gizi" rel="tooltip" href="<?php echo site_url('gizi/index/'.$rows->id_rawat_inap); ?>">
                                            <i class="fa fa-lg fa-cutlery" style="padding-top: 10px"></i></a>
                                        <a class="detail" title="Fisioterapi" rel="tooltip" href="<?php echo site_url('fisioterapi/index/'.$rows->id_rawat_inap); ?>">
                                            <i class="fa fa-lg fa-male" style="padding-top: 10px"></i></a>
                                            <a class="detail" title="Rontgen" rel="tooltip" href="<?php echo site_url('rontgen/index/'.$rows->id_rawat_inap); ?>">
                                            <i class="fa fa-lg fa-wifi" style="padding-top: 10px"></i></a>
                                        <a class="detail" title="Obat Retur" rel="tooltip" href="<?php echo site_url('obat_retur/add/'.$rows->id_pasien_ordered); ?>">
                                            <i class="fa fa-lg fa-suitcase" style="padding-top: 10px"></i></a>
                                    </td>
                                    <td>
                                        <a class="print" title="Cetak Nota" rel="tooltip" href="<?php echo site_url('nota/index/'.$rows->id_rawat_inap); ?>">
                                            <i class="fa fa-lg fa-print"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
            </div><!-- /.padding-md -->
        </div><!-- /panel -->
    </div><!-- /.padding-md -->
</div><!-- /main-container -->

<script type="text/javascript">
    // var mystates = {
    //     state0: {
    //         html:'Apakah anda yakin ingin memulangkan pasien?',
    //         buttons: { Tidak: false, Ya: true },
    //         focus: 1,
    //         submit:function(e,v,m,f){
    //             if(v){
    //                 e.preventDefault();
    //                 $.prompt.goToState('state1');
    //                 return false;
    //             }
    //             $.prompt.close();
    //         }
    //     },
    //     state1: {
    //         html:'Apakah anda ingin mencetak nota?',
    //         buttons: { Tidak: false, Ya: true },
    //         focus: 1,
    //         submit:function(e,v,m,f){
    //             e.preventDefault();
    //             if(v==0)
    //                 $.prompt.close();
    //             else if(v==-1)
    //                 $.prompt.goToState('state0');
    //         }
    //     }
    // };

    $('a.pulang').on('click', function(e) {
        e.preventDefault();
        var mybutton1 = $(this);

        $.prompt({
            state0: {
                html:'Apakah anda yakin ingin memulangkan pasien?',
                buttons: { Tidak: false, Ya: true },
                focus: 1,
                submit:function(e,v,m,f){
                    if(v){
                        e.preventDefault();
                        pulangkan_pasien(mybutton1.attr('href'));
                        $.prompt.goToState('state1');
                        return false;
                    }
                    $.prompt.close();
                }
            },
            state1: {
                html:'Apakah anda ingin mencetak nota?',
                buttons: { Tidak: false, Ya: true },
                focus: 1,
                submit:function(e,v,m,f){
                    e.preventDefault();
                    if(v) {
                        var href = mybutton1.parent().next().find('.print').attr('href');
                        window.location = href;

                        $.prompt.close();
                    }
                    else {
                        var href = '<?php echo site_url("pasien_rawat_inap"); ?>';
                        window.location = href;

                        $.prompt.close();
                    }
                }
            }
        });
    });

    function pulangkan_pasien(url) {
        $.ajax({
            url: url,
            error:function() {
                alert('error');
            }
        });
    }
</script>