<?php foreach($pemakaian_obat as $keys=>$rows): ?>
    <div class="form-group">
        <label class="col-lg-2 control-label">Nama Obat</label>
        <div class="col-lg-3">
            <?php
                echo form_input(
                    'obat['.$keys.']',
                    $rows['nama_obat'],
                    'class="form-control" readonly'
                );

                echo form_hidden(
                    'obat_id['.$keys.']',
                    $rows['id_katalog_obat']
                );

                echo form_hidden(
                    'id_pengeluaran_obat['.$keys.']',
                    $rows['id_pengeluaran_obat']
                );
            ?>
        </div><!-- /.col -->
        <div class="col-lg-2">
            <?php
                echo form_input(
                    'kategori['.$keys.']',
                    $rows['nama_kategori'],
                    'class="form-control" readonly'
                );
            ?>
        </div>

        <div class="col-lg-2">
            <?php
                echo form_input(
                    'sisa_pengeluaran['.$rows['id_katalog_obat'].']',
                    'Obat tersedia: ' . $rows['sisa_pengeluaran'],
                    'class="form-control" id="sisa-pengeluaran" placeholder="Jumlah Pengeluaran" disabled'
                );
            ?>
        </div>

        <div class="col-lg-2">
            <?php
                echo form_input(
                    'jumlah['.$rows['id_katalog_obat'].']',
                    (isset($form_repopulate[$rows['id_katalog_obat']]['jumlah'])) ? $form_repopulate[$rows['id_katalog_obat']]['jumlah'] : $rows['sisa'],
                    'class="form-control" id="jumlah-retur" placeholder="Jumlah Retur"'
                );
            ?>
        </div>
    </div><!-- /form-group -->
<?php endforeach; ?>

<br>
<div class="form-group">
    <label class="col-lg-2 control-label"></label>
    <div class="col-lg-5">
        <div class="input-group">
            <input type="submit" class="form-control btn btn-primary">
        </div>
    </div>
</div>