<div id="main-container">
    <div class="padding-md">
        <div class="panel panel-default table-responsive">
            <div class="padding-md clearfix">
                <div style="margin-bottom: 20px;"></div>
                <p style="font-size: 17px">Silahkan memilih pasien rawat inap yang akan di retur obat</p>
                <div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
                    <table class="table table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
                        <thead>
                            <tr role="row">
                                <th style="width: 100px">
                                    <div>Nomor Pasien</div>
                                </th>
                                <th style="width: 100px">
                                    <div>Status</div>
                                </th>
                                <th>
                                    <div>Nama Pasien</div>
                                </th>
                                <th>
                                    <div>Jenis Kelamin</div>
                                </th>
                                <th>
                                    <div>Alamat</div>
                                </th>
                                <th>
                                    <div>Umur</div>
                                </th>
                                <th style="width: 150px">
                                    <div>Action</div>
                                </th>
                            </tr>
                        </thead>

                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php foreach($data as $rows): ?>
                                <tr>
                                    <td style="text-align:center">
                                        <?php echo $rows->id_pasien_ordered; ?>
                                    </td>
                                    <td>
                                        <?php if($rows->status == 1): ?>
                                            <span class="label label-success">Sedang Inap</span>
                                        <?php endif;?>
                                    </td>
                                    <td><?php echo $rows->nama_pasien; ?></td>
                                    <td><?php echo ($rows->jenis_kelamin == 'L') ? 'Laki-Laki' : 'Perempuan'; ?></td>
                                    <td><?php echo $rows->alamat_pasien; ?></td>
                                    <td><?php echo $rows->umur_pasien; ?></td>
                                    <td>
                                        <a class="detail" title="Obat Retur" rel="tooltip" href="<?php echo site_url('obat_retur/add/'.$rows->id_pasien_ordered); ?>">
                                            <i class="fa fa-lg fa-medkit"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
            </div><!-- /.padding-md -->
        </div><!-- /panel -->
    </div><!-- /.padding-md -->
</div><!-- /main-container -->