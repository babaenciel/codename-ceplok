<?php //dump($form_repopulate['jumlah'][26][0]); ?>
<?php //dump($form['jumlah'][]); ?>
<?php foreach($pemakaian_obat as $rows): ?>
    <div class="form-group">
        <label class="col-lg-2 control-label">Nama Obat</label>
        <div class="col-lg-5">
            <?php
                echo form_input(
                    'obat[]',
                    $rows->nama_obat,
                    'class="form-control" readonly'
                );
            ?>
        </div><!-- /.col -->

        <div class="col-lg-2">
            <?php
                echo form_input(
                    'jumlah['.$rows->id_katalog_obat.'][]',
                    (isset($form_repopulate['jumlah'][$rows->id_katalog_obat][0])) ? $form_repopulate['jumlah'][$rows->id_katalog_obat][0] : $form['jumlah'][$rows->id_katalog_obat][0],
                    'class="form-control" id="jumlah-retur" placeholder="Jumlah Retur"'
                );
            ?>
        </div>
    </div><!-- /form-group -->
<?php endforeach; ?>

<br>
<div class="form-group">
    <label class="col-lg-2 control-label"></label>
    <div class="col-lg-5">
        <div class="input-group">
            <input type="submit" class="form-control btn btn-primary">
        </div>
    </div>
</div>