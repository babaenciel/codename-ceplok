<div id="main-container">
    <div class="padding-md">
        <div class="panel panel-default table-responsive">
            <div class="padding-md clearfix">
                <form action="<?php echo site_url('pendapatan/index'); ?>" method="POST">
                    <div class="col-lg-2">
                        <?php
                            echo form_dropdown(
                                'bulan',
                                $bulan,
                                (set_value('bulan') ? set_value('bulan') : date('m')),
                                'class="chzn-select"'
                            );
                        ?>
                    </div>
                    <div class="col-lg-2">
                        <?php
                            echo form_dropdown(
                                'tahun',
                                $tahun,
                                (set_value('tahun') ? set_value('tahun') : date('Y')),
                                'class="chzn-select"'
                            );
                        ?>
                    </div>

                    <div class="col-lg-4">
                        <input type="submit" class="submit" id="pusat" value="Lihat Pendapatan" style="display: inline">
                    </div>
                </form>

                <div class="clearfix"></div>
                <br>
                <br>

                <div class="col-lg-6">
                    <?php if($_POST): ?>
                    <div class="well text-center">
                        <h5>
                            TOTAL PENDAPATAN BULAN <?php echo strtoupper($bulan[set_value('bulan')]); ?>:
                            <strong style="font-size: 20px; margin-left: 10px;">
                                <?php echo format_number($total_pendapatan, TRUE); ?>
                            </strong>
                        </h5>
                        <h5>
                            TOTAL PASIEN DIRAWAT BULAN <?php echo strtoupper($bulan[set_value('bulan')]); ?>
                            <strong style="font-size: 20px; margin-left: 10px;">
                                <?php echo $total_pasien_dirawat; ?>
                            </strong>
                        </h5>
                        <h5>
                            TOTAL PASIEN PERNAH DIRAWAT KESELURUHAN
                            <strong style="font-size: 20px; margin-left: 10px;">
                                <?php echo $total_pasien_keseluruhan; ?>
                            </strong>
                        </h5>
                    </div>
                    <?php endif; ?>
                </div>
            </div><!-- /.padding-md -->
        </div><!-- /panel -->
    </div><!-- /.padding-md -->
</div><!-- /main-container -->