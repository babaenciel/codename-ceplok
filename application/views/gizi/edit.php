<div id="main-container">
    <div class="padding-md">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php $this->load->view('elements/error_message'); ?>
                <form action="<?php echo site_url('gizi/edit/'.$id); ?>" method="POST" id="formToggleLine" class="form-horizontal no-margin">
                    <?php $this->load->view('gizi/_form'); ?>
                </form>
            </div>
        </div><!-- /panel -->
    </div><!-- /.padding-md -->
</div><!-- /main-container -->