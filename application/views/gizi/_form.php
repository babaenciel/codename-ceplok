<div class="form-group">
    <label class="col-lg-2 control-label">Nama Gizi</label>
    <div class="col-lg-5">
        <?php
            echo form_dropdown(
                'gizi',
                $gizi_dropdown,
                (set_value('gizi')) ? set_value('gizi') : $form['gizi'],
                'class="form-control chzn-select"'
            );
        ?>
    </div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
    <label class="col-lg-2 control-label">Tanggal Mulai</label>
    <div class="col-lg-5">
        <?php
            echo form_input(
                'tanggal_mulai',
                (set_value('tanggal_mulai')) ? set_value('tanggal_mulai') : $form['tanggal_mulai'],
                'class="form-control datepicker"'
            );
        ?>
    </div><!-- /.col -->
</div><!-- /form-group -->
<?php if(isset($edit)): ?>
<div class="form-group">
    <label class="col-lg-2 control-label">Tanggal Akhir</label>
    <div class="col-lg-5">
        <?php
            echo form_input(
                'tanggal_akhir',
                (set_value('tanggal_akhir')) ? set_value('tanggal_akhir') : $form['tanggal_akhir'],
                'class="form-control datepicker"'
            );
        ?>
    </div><!-- /.col -->
</div><!-- /form-group -->
<?php endif; ?>
<?php if(false): ?>
<div class="form-group">
    <label class="col-lg-2 control-label">Waktu</label>
    <div class="col-lg-5">
        <?php
            echo form_input(
                'waktu_laboratorium',
                (set_value('waktu_laboratorium')) ? set_value('waktu_laboratorium') : $form['waktu_laboratorium'],
                'class="form-control" placeholder="HH:MM"'
            );
        ?>
    </div><!-- /.col -->
</div><!-- /form-group -->
<?php endif; ?>
<br>
<div class="form-group">
    <label class="col-lg-2 control-label"></label>
    <div class="col-lg-5">
        <div class="input-group">
            <input type="submit" class="form-control btn btn-primary">
        </div>
    </div>
</div>
