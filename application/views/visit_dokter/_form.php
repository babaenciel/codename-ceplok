<div class="form-group">
    <label class="col-lg-2 control-label">Nama Dokter</label>
    <div class="col-lg-5">
        <?php
            echo form_dropdown(
                'dokter',
                $dokter_dropdown,
                (set_value('dokter')) ? set_value('dokter') : $form['dokter'],
                'class="form-control chzn-select"'
            );
        ?>
    </div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
    <label class="col-lg-2 control-label">Jenis Visit</label>
    <div class="col-lg-5">
        <?php
            echo form_dropdown(
                'jenis_visit',
                $jenis_visit_dropdown,
                (set_value('jenis_visit')) ? set_value('jenis_visit') : $form['jenis_visit'],
                'class="form-control chzn-select"'
            );
        ?>
    </div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
    <label class="col-lg-2 control-label">Tanggal Visit</label>
    <div class="col-lg-5">
        <?php
            echo form_input(
                'tanggal_visit',
                (set_value('tanggal_visit')) ? set_value('tanggal_visit') : $form['tanggal_visit'],
                'class="form-control datepicker"'
            );
        ?>
    </div><!-- /.col -->
</div><!-- /form-group -->
<?php if(false): ?>
<div class="form-group">
    <label class="col-lg-2 control-label">Waktu Visit</label>
    <div class="col-lg-5">
        <?php
            echo form_dropdown(
                'waktu_visit',
                array('pagi' => 'Pagi', 'sore' => 'Sore'),
                (set_value('waktu_visit')) ? set_value('waktu_visit') : $form['waktu_visit'],
                'class="form-control chzn-select"'
            );
        ?>
    </div><!-- /.col -->
</div><!-- /form-group -->
<?php endif; ?>
<br>
<div class="form-group">
    <label class="col-lg-2 control-label"></label>
    <div class="col-lg-5">
        <div class="input-group">
            <input type="submit" class="form-control btn btn-primary">
        </div>
    </div>
</div>

<!-- <div class="form-group">
    <label class="col-lg-2 control-label">Form in modal</label>
    <div class="col-lg-10">
        <a href="http://websdevp.com/Endless/form_element.html#formModal" class="btn btn-success" data-toggle="modal">Form In Modal</a>
    </div>
</div> -->