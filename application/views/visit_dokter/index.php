<div id="main-container">
    <div class="padding-md">
        <div class="panel panel-default table-responsive">
            <div class="padding-md clearfix">
                <a href="<?php echo site_url('visit_dokter/add/'.$id); ?>" class="btn btn-primary"><i class="fa fa-lg fa-plus" style="padding-right: 5px;"></i>Tambah Visit Dokter</a>
                <div style="margin-bottom: 20px;"></div>
                <div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
                    <table class="table table-striped dataTable" id="dataTable" aria-describedby="dataTable_info">
                        <thead>
                            <tr role="row">
                                <th>
                                    <div>Nama Dokter</div>
                                </th>
                                <th>
                                    <div>Jenis Visit</div>
                                </th>
                                <th>
                                    <div>Tanggal Visit</div>
                                </th>                                
                                <th>
                                    <div>Tarif</div>
                                </th>
                                <th>
                                    <div>Action</div>
                                </th>
                            </tr>
                        </thead>

                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php foreach($data as $rows): ?>
                                <tr>
                                    <td class=""><?php echo $rows->nama_dokter; ?></td>
                                    <td class=""><?php echo $rows->jenis_dokter . '-' . $rows->jenis_kamar; ?></td>
                                    <td><?php echo conv_date_format($rows->tanggal_visit, 'd-m-Y'); ?></td>                                    
                                    <td class=""><?php echo format_number($rows->tarif, TRUE); ?></td>
                                    <td class="">
                                        <a class="update" title="Edit" rel="tooltip" href="<?php echo site_url('visit_dokter/edit/'.$rows->id_visit_dokter); ?>">
                                            <i class="fa fa-pencil fa-lg"></i></a>
                                        <a class="delete" title="Hapus" rel="tooltip" href="<?php echo site_url('visit_dokter/delete/'.$rows->id_visit_dokter); ?>">
                                            <i class="fa fa-trash-o fa-lg"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
            </div><!-- /.padding-md -->
        </div><!-- /panel -->
    </div><!-- /.padding-md -->
</div><!-- /main-container -->