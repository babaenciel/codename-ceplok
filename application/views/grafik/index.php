<script type="text/javascript" src="<?php echo base_url(); ?>resources/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>resources/flot/jquery.flot.time.min.js"></script>

<div id="main-container">
    <div class="padding-md">
        <div class="panel panel-default table-responsive">
            <div class="panel-body">
                <div class="padding-md clearfix">
                    <form action="<?php echo site_url('grafik/index'); ?>" method="GET" class="form-inline" role="form">
                        <div class="form-group">
                            <label class="sr-only" for="exampleInputEmail2">Pilih Tahun</label>
                            <?php
                                echo form_dropdown(
                                    'year',
                                    $year_dropdown,
                                    ( set_value('year') ? set_value('year') : $year ),
                                    'class="form-control chzn-select"'
                                );
                            ?>
                        </div>

                        <button type="submit" class="btn btn-primary" style="margin-left: 10px">Submit</button>
                    </form>

                    <div class="clearfix"></div>
                    <br>
                    <br>

                    <div id="placeholder1" style="width: 800px; height: 300px; margin: 0 auto;"></div>
                    <div id="placeholder2" style="width: 800px; height: 300px; margin: 0 auto;"></div>
                    <div id="placeholder3" style="width: 800px; height: 300px; margin: 0 auto;"></div>
                </div>
            </div>
        </div><!-- /panel -->
    </div><!-- /.padding-md -->
</div><!-- /main-container -->

<script type="text/javascript">
    //function to format money
    Number.prototype.formatMoney = function(c, d, t){
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
       return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };

    // PUSAT
    var data_pusat = $.parseJSON('<?php echo $pusat; ?>');
    console.log(data_pusat);

    var data_grafik_pusat = [
        [(1356998400 * 1000), 1600000],
        [(1359676800 * 1000), 2000000],
        [(1362096000 * 1000), 1900000],
        [(1364774400 * 1000), 3400000],
        [(1367366400 * 1000), 1900000],
        [(1370044800 * 1000), 1600000],
        [(1372636800 * 1000), 2000000],
        [(1375315200 * 1000), 1900000],
        [(1377993600 * 1000), 3400000],
        [(1380585600 * 1000), 1900000],
        [(1383264000 * 1000), 2700000],
        [(1385856000 * 1000), 1500000],
    ];

    // console.log(data_ri);

    $.each(data_pusat, function(index, val) {
        data_grafik_pusat[index][1] = val;
    });


    // RAWAT INAP
    var data_rawat_inap = $.parseJSON('<?php echo $rawat_inap; ?>');
    console.log(data_rawat_inap);

    var data_grafik_ri = [
        [(1356998400 * 1000), 1600000],
        [(1359676800 * 1000), 2000000],
        [(1362096000 * 1000), 1900000],
        [(1364774400 * 1000), 3400000],
        [(1367366400 * 1000), 1900000],
        [(1370044800 * 1000), 1600000],
        [(1372636800 * 1000), 2000000],
        [(1375315200 * 1000), 1900000],
        [(1377993600 * 1000), 3400000],
        [(1380585600 * 1000), 1900000],
        [(1383264000 * 1000), 2700000],
        [(1385856000 * 1000), 1500000],
    ];

    // console.log(data_ri);

    $.each(data_rawat_inap, function(index, val) {
        data_grafik_ri[index][1] = val;
    });

    // RAWAT JALAN
    var data_rawat_jalan = $.parseJSON('<?php echo $rawat_jalan; ?>');
    console.log(data_rawat_jalan);

    var data_grafik_rj = [
        [(1356998400 * 1000), 1600000],
        [(1359676800 * 1000), 2000000],
        [(1362096000 * 1000), 1900000],
        [(1364774400 * 1000), 3400000],
        [(1367366400 * 1000), 1900000],
        [(1370044800 * 1000), 1600000],
        [(1372636800 * 1000), 2000000],
        [(1375315200 * 1000), 1900000],
        [(1377993600 * 1000), 3400000],
        [(1380585600 * 1000), 1900000],
        [(1383264000 * 1000), 2700000],
        [(1385856000 * 1000), 1500000],
    ];

    // console.log(data_ri);

    $.each(data_rawat_jalan, function(index, val) {
        data_grafik_rj[index][1] = val;
    });


    var data1 = [{
        label: 'Nilai Persediaan Gudang Pusat',
        color: 'red',
        data: data_grafik_pusat
    }];

    var data2 = [{
        label: 'Nilai Persediaan Gudang Rawat Inap',
        color: "green",
        data: data_grafik_ri
    }];

    var data3 = [{
        label: 'Nilai Persediaan Gudang Rawat Jalan',
        color: "blue",
        data: data_grafik_rj
    }]


    $.plot($("#placeholder1"), data1, {
        series: {
            bars: {
                barWidth: 1000 * 60 * 60 * 24 * 10,
                show: true
            }
        },
        yaxis: {
            tickDecimals: 0,
            tickFormatter: function (v, axis) {
                return (v).formatMoney(axis.tickDecimals, '.', '.');
            }
        },
        xaxis: {
            mode: "time",
            timeformat: "%b"
        },
        grid: {
            clickable: true
        }
    });

    $.plot($("#placeholder2"), data2, {
        series: {
            bars: {
                barWidth: 1000 * 60 * 60 * 24 * 10,
                show: true
            }
        },
        yaxis: {
            tickDecimals: 0,
            tickFormatter: function (v, axis) {
                return (v).formatMoney(axis.tickDecimals, '.', '.');
            }
        },
        xaxis: {
            mode: "time",
            timeformat: "%b"
        },
        grid: {
            clickable: true
        }
    });

    $.plot($("#placeholder3"), data3, {
        series: {
            bars: {
                barWidth: 1000 * 60 * 60 * 24 * 10,
                show: true
            }
        },
        yaxis: {
            tickDecimals: 0,
            tickFormatter: function (v, axis) {
                return (v).formatMoney(axis.tickDecimals, '.', '.');
            }
        },
        xaxis: {
            mode: "time",
            timeformat: "%b"
        },
        grid: {
            clickable: true
        }
    });

    var year = '<?php echo $year; ?>';

    $('#placeholder1').bind('plotclick', function (e, pos, item) {
        // console.log(item.datapoint[0]);
        // console.log(pos);
        // console.log(item);

        // if(item.datapoint[0] == 1362096000000) {
        //     console.log('hehe');
        // }
        window.location = '<?php echo site_url("grafik/detail/pusat/' + item.datapoint[0] + '/' + year + '"); ?>';
    });

    $('#placeholder2').bind('plotclick', function (e, pos, item) {
        console.log(item.datapoint[0]);
        // console.log(pos);
        // console.log(item);

        // if(item.datapoint[0] == 1362096000000) {
        //     console.log('hehe');
        // }
        window.location = '<?php echo site_url("grafik/detail/rawat_inap/' + item.datapoint[0] + '/' + year + '"); ?>';
    });

    $('#placeholder3').bind('plotclick', function (e, pos, item) {
        // console.log(item.datapoint[0]);
        // console.log(pos);
        // console.log(item);

        // if(item.datapoint[0] == 1362096000000) {
        //     console.log('hehe');
        // }
        window.location = '<?php echo site_url("grafik/detail/rawat_jalan/' + item.datapoint[0] + '/' + year + '"); ?>';
    });
</script>