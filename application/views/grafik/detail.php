<div id="main-container">
    <div class="padding-md">
        <div class="panel panel-default table-responsive">
            <div class="padding-md clearfix">
                <div style="margin-bottom: 20px;"></div>
                <div id="dataTable_wrapper" class="dataTables_wrapper" role="grid">
                    <table class="table table-striped" aria-describedby="dataTable_info">
                        <thead>
                            <tr role="row">
                                <th>Nama Obat</th>
                                <th>Jenis</th>
                                <th>Berat Netto</th>
                                <th>Jumlah Persediaan</th>
                                <th>Harga Satuan</th>
                                <th>Nilai Persediaan</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach($result as $rows): ?>
                                <tr>
                                    <td><?php echo $rows['nama_obat']; ?></td>
                                    <td><?php echo $rows['nama_kategori']; ?></td>
                                    <td><?php echo $rows['berat_netto']; ?></td>
                                    <td><?php echo $rows['sisa']; ?></td>
                                    <td><?php echo format_number($rows['harga_satuan']); ?></td>
                                    <td><?php echo format_number($rows['nilai_persediaan']);; ?></td>
                                </tr>
                            <?php endforeach; ?>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><strong>TOTAL</strong></td>
                                <td><strong><?php echo format_number($total_persediaan, TRUE); ?></strong></td>
                            </tr>
                        </tbody>
                    </table>
            </div><!-- /.padding-md -->
        </div><!-- /panel -->
    </div><!-- /.padding-md -->
</div><!-- /main-container -->