<div class="form-group">
    <label class="col-lg-2 control-label">Nama Kamar</label>
    <div class="col-lg-5">
        <?php
            echo form_dropdown(
                'kamar',
                $kamar_dropdown,
                (set_value('kamar')) ? set_value('kamar') : $form['kamar'],
                'class="form-control chzn-select"'
            );
        ?>
    </div><!-- /.col -->
</div><!-- /form-group -->
<div class="form-group">
    <label class="col-lg-2 control-label">Tanggal Mulai</label>
    <div class="col-lg-5">
        <?php
            echo form_input(
                'tanggal_mulai',
                (set_value('tanggal_mulai')) ? set_value('tanggal_mulai') : $form['tanggal_mulai'],
                'class="form-control datepicker"'
            );
        ?>
    </div><!-- /.col -->
</div><!-- /form-group -->
<?php if(isset($edit)): ?>
<div class="form-group">
    <label class="col-lg-2 control-label">Tanggal Akhir</label>
    <div class="col-lg-5">
        <?php
            echo form_input(
                'tanggal_akhir',
                (set_value('tanggal_akhir')) ? set_value('tanggal_akhir') : $form['tanggal_akhir'],
                'class="form-control datepicker"'
            );
        ?>
    </div><!-- /.col -->
</div><!-- /form-group -->
<?php endif; ?>
<br>
<div class="form-group">
    <label class="col-lg-2 control-label"></label>
    <div class="col-lg-5">
        <div class="input-group">
            <input type="submit" class="form-control btn btn-primary">
        </div>
    </div>
</div>
