<div id="main-container">
    <div class="padding-md">
        <div class="panel panel-default table-responsive">
            <div class="padding-md clearfix">
                <form action="<?php echo site_url('report/monthly'); ?>" method="POST" id="form-laporan">
                    <div class="col-lg-2">
                        <?php
                            echo form_dropdown(
                                'bulan',
                                $bulan,
                                (set_value('bulan') ? set_value('bulan') : date('m')),
                                'class="chzn-select"'
                            );
                        ?>
                    </div>
                    <div class="col-lg-2">
                        <?php
                            echo form_dropdown(
                                'tahun',
                                $tahun,
                                (set_value('tahun') ? set_value('tahun') : date('Y')),
                                'class="chzn-select"'
                            );
                        ?>
                    </div>

                    <div class="clearfix"></div>
                    <br>
                    <div class="col-lg-10">
                        <input type="submit" class="submit" id="pusat" value="Cetak Gudang Pusat" style="display: inline">
                        <input type="submit" class="submit" id="rawat_inap" value="Cetak Gudang Rawat Inap" style="display: inline">
                        <input type="submit" class="submit" id="rawat_jalan" value="Cetak Gudang Rawat Jalan" style="display: inline">
                    </div>
                </form>
            </div><!-- /.padding-md -->
        </div><!-- /panel -->
    </div><!-- /.padding-md -->
</div><!-- /main-container -->

<script type="text/javascript">
    $('.submit').on('click', function() {
        var type = $(this).attr('id');
        if(type == 'pusat') {
            $('#form-laporan').attr('action', '<?php echo site_url("report/monthly/pusat"); ?>')
        }else if(type == 'rawat_inap') {
            $('#form-laporan').attr('action', '<?php echo site_url("report/monthly/rawat_inap"); ?>')
        }else if(type == 'rawat_jalan') {
            $('#form-laporan').attr('action', '<?php echo site_url("report/monthly/rawat_jalan"); ?>')
        }

    });
</script>