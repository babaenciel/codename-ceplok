<style type="text/css">
    #nota {
        font-size: 13px;
    }

    #nota table {
        border-spacing: 0px;
    }
</style>

<div style="width: 700px" id="nota">
    <table style="margin-bottom: 10px;">
        <tr>
            <td>
                <img src="<?php echo base_url('resources/img/logo2.jpg'); ?>" style="width: 90px; height: 70px; margin-right: 20px;">
            </td>
            <td style="width: 550px">
                <div style="text-align: center; font-size: 20px;">RUMAH SAKIT</div>
                <div style="text-align: center; font-size: 20px;">BUDI SEHAT</div>
                <div style="text-align: center">Jln. WR. Supratman No 183, Cangkrep Lor, Purworejo</div>
                <div style="text-align: center">Telepon ( 0275 ) 3128272</div>
            </td>
        </tr>
    </table>

    <table>
        <tr>
            <td style="width: 700px;">
                <div style="border-top: 1px solid black;"></div>
            </td>
        </tr>
        <tr>
            <td style="width: 700px;">
                <div style="border-top: 1px solid black;"></div>
            </td>
        </tr>
    </table>

    <br>

    <table style="width: 700px;">
        <tr>
            <?php if($type == 'pusat'): ?>
                <td><h4 style="text-align: center">LAPORAN NILAI PERSEDIAAN OBAT GUDANG PUSAT</h4></td>
            <?php elseif($type == 'rawat_inap'): ?>
                <td><h4 style="text-align: center">LAPORAN NILAI PERSEDIAAN OBAT GUDANG RAWAT INAP</h4></td>
            <?php elseif($type == 'rawat_jalan'): ?>
                <td><h4 style="text-align: center">LAPORAN NILAI PERSEDIAAN OBAT GUDANG RAWAT JALAN</h4></td>
            <?php endif; ?>
        </tr>
        <tr>
            <td><h4 style="text-align: center"><?php echo $month_text . ' - ' . $year; ?></h4></td>
        </tr>
    </table>

    <table border="1" style="width: 700px">
        <tr>
            <td>No</td>
            <td>Nama</td>
            <td>Jenis</td>
            <td>Persediaan Awal</td>
            <td>Penerimaan</td>
            <td>Pengeluaran</td>
            <td>Sisa</td>
            <td>Harga Satuan</td>
            <td>Nilai Persediaan</td>
        </tr>

        <?php foreach($result as $keys => $rows): ?>
            <tr>
                <td><?php echo $keys+1; ?></td>
                <td><?php echo $rows['nama_obat']; ?></td>
                <td><?php echo $rows['nama_kategori']; ?></td>
                <td><?php echo $rows['persediaan_awal']; ?></td>
                <td><?php echo $rows['penerimaan']; ?></td>
                <td><?php echo $rows['pengeluaran']; ?></td>
                <td><?php echo $rows['sisa']; ?></td>
                <td><?php echo format_number($rows['harga_satuan']); ?></td>
                <td><?php echo format_number($rows['nilai_persediaan']); ?></td>
            </tr>
        <?php endforeach; ?>

        <tr>
            <td colspan="9" style="text-align: center;">TOTAL NILAI PERSEDIAAN</td>
            <td><?php echo format_number($total_persediaan); ?></td>
        </tr>
    </table>

    <table style="margin-top: 100px;">
        <tr>
            <td style="width: 350px;">
                <div style="text-align: center">Mengetahui</div> <br>
                <div style="text-align: center">Kepala Klinik Umum Rawat Inap</div>
                <div style="text-align: center">Budi Sehat</div>
                <div style="text-align: center"><br></div>
                <div style="text-align: center"><br></div>
                <div style="text-align: center"><br></div>
                <div style="text-align: center"><br></div>  
                <div style="text-align: center"><br></div>                
            </td>
            <td style="width: 300px;">
                <div style="text-align: center">Purworejo, <?php echo date('d-m-Y'); ?></div> <br>
                <div style="text-align: center">Yang membuat rekening</div>
                <div style="text-align: center"><br></div>
                <div style="text-align: center"><br></div>
                <div style="text-align: center"><br></div>
                <div style="text-align: center"><br></div>
                <div style="text-align: center"><br></div>  
                <div style="text-align: center"><?php echo $pembuat_laporan; ?></div>
            </td>
        </tr>
    </table>
</div>