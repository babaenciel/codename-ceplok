<?php

class MY_Controller extends CI_Controller {
	public $layout = 'main';
    public $title = 'Rumah Sakit Budi Sehat';
    public $sidebar = '';

    function __construct() {
        parent::__construct();

        // $this->output->enable_profiler(TRUE);
        
        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');

        // belum login
        if($this->session->userdata('user') === FALSE) {
            redirect('login');
        }

        // setting sidebar menu
        if($this->session->userdata('user')->jabatan === USER_ADMINISTRASI) {
            $this->sidebar = $this->load->view('layouts/sidebar_adm', '', TRUE);
        }else if($this->session->userdata('user')->jabatan === USER_GUDANG) {
            $this->sidebar = $this->load->view('layouts/sidebar_gud', '', TRUE);
        }else if($this->session->userdata('user')->jabatan === USER_MANAGER) {
            $this->sidebar = $this->load->view('layouts/sidebar_man', '', TRUE);
        }
    }

	function render($template = '', $vars = array(), $return = FALSE) {
        // Load master template
        if($this->layout){
            $vars['content'] = $this->load->view($template, $vars, true);
            $vars['sidebar'] = $this->sidebar;
            return $this->load->view('layouts/'.$this->layout, $vars, $return);
        }else{
            return $this->load->view($template, $vars, $return);
        }
    }
}