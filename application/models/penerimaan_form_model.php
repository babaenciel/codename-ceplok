<?php

class Penerimaan_form_model extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function init_data($existing_data=FALSE) {
		$data['nomor_faktur'] = ($existing_data) ? $existing_data->nomor_faktur : '';
		$data['tanggal_penerimaan'] = ($existing_data) ? conv_date_format($existing_data->tanggal_penerimaan, 'd-m-Y') : '';
		$data['supplier'] = ($existing_data) ? $existing_data->id_supplier : '';
		$data['jumlah_dus'] = ($existing_data) ? $existing_data->jumlah_dus : '';
		$data['quantity_per_dus'] = ($existing_data) ? $existing_data->quantity_per_dus : '';
		$data['harga_per_dus'] = ($existing_data) ? $existing_data->harga_per_dus : '';
		$data['jumlah_satuan'] = ($existing_data) ? $existing_data->jumlah_penerimaan : '';
		$data['harga_satuan'] = ($existing_data) ? $existing_data->harga_satuan : '';
		// $data['harga_jual_satuan'] = ($existing_data) ? $existing_data->harga_jual_satuan : '';

		return $data;
	}
}