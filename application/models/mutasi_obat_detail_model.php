<?php

class Mutasi_obat_detail_model extends MY_Model {
	public $_table = 'mutasi_obat_detail';
	protected $primary_key = 'id_mutasi_obat_detail';

	function __construct() {
		parent::__construct();
	}

	function get_all_with_obat($id_mutasi_obat) {
		$query = $this->db->select('*')
			->from($this->_table)
			->join('katalog_obat', $this->_table.'.id_katalog_obat = katalog_obat.id_katalog_obat')
			->join('kategori_obat', 'katalog_obat.id_kategori_obat = kategori_obat.id_kategori_obat')
			->where('id_mutasi_obat', $id_mutasi_obat)
			->get()
			->result();

		return $query;
	}
}