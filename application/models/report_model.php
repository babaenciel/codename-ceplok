<?php

class Report_model extends MY_Model {
    function __construct() {
        parent::__construct();
    }

    function get_pusat_persediaan_awal($month, $year) {
        // SELECT * FROM log_katalog_obat
        // WHERE tanggal_perubahan IN
        // (SELECT max(tanggal_perubahan) FROM log_katalog_obat WHERE MONTH(tanggal_perubahan) = '03' AND YEAR(tanggal_penerimaan) = '2014')
        $query = $this->db->select('*')
            ->from('perubahan_persediaan_obat')
            ->join('katalog_obat', 'katalog_obat.id_katalog_obat = perubahan_persediaan_obat.id_katalog_obat')
            ->join('kategori_obat', 'katalog_obat.id_kategori_obat = kategori_obat.id_kategori_obat')
            ->where('tanggal_perubahan IN (SELECT MAX(tanggal_perubahan) FROM perubahan_persediaan_obat WHERE MONTH(tanggal_perubahan) = '.$month.' AND YEAR(tanggal_perubahan) = '.$year.')')
            ->order_by('katalog_obat.id_katalog_obat')
            ->get()
            ->result();

        if(empty($query)) {
            $query = $this->db->select('*')
                ->from('katalog_obat')
                ->join('kategori_obat', 'katalog_obat.id_kategori_obat = kategori_obat.id_kategori_obat')
                ->get()
                ->result();

            foreach($query as $keys => $rows) {
                $query[$keys]->persediaan_pusat = 0;
            }
        }
        return $query;
    }

    function get_pusat_penerimaan($month, $year) {
        // SELECT k.id_katalog_obat, harga_satuan_penerimaan, jumlah_penerimaan
        // FROM (
        //     SELECT p.id_katalog_obat, MAX(p.harga_satuan) as harga_satuan_penerimaan,
        //         SUM(jumlah_penerimaan) as jumlah_penerimaan
        //     FROM penerimaan_obat_detail p
        //     LEFT JOIN penerimaan_obat po ON p.id_penerimaan_obat = po.id_penerimaan_obat
        //     WHERE MONTH(po.tanggal_penerimaan) = '03' AND YEAR(po.tanggal_penerimaan) = '2014'
        //     GROUP BY p.id_katalog_obat
        // ) table2
        // RIGHT JOIN katalog_obat k ON k.id_katalog_obat = table2.id_katalog_obat
        // ORDER BY k.id_katalog_obat

        $query = $this->db->query('
            SELECT k.id_katalog_obat, harga_satuan_penerimaan, sum_jumlah_penerimaan
            FROM (
                SELECT p.id_katalog_obat, MAX(p.harga_satuan) as harga_satuan_penerimaan,
                    SUM(jumlah_penerimaan) as sum_jumlah_penerimaan
                FROM penerimaan_obat_detail p
                LEFT JOIN penerimaan_obat po ON p.id_penerimaan_obat = po.id_penerimaan_obat
                WHERE MONTH(po.tanggal_penerimaan) = "'.$month.'" AND YEAR(po.tanggal_penerimaan) = "'.$year.'"
                GROUP BY p.id_katalog_obat
            ) table2
            RIGHT JOIN katalog_obat k ON k.id_katalog_obat = table2.id_katalog_obat
            ORDER BY k.id_katalog_obat
        ')->result();

        return $query;
    }

    function get_pusat_pengeluaran($month, $year) {
        // SELECT k.id_katalog_obat, sum_jumlah_mutasi
        // FROM (
        //     SELECT id_katalog_obat, SUM(jumlah_mutasi) as sum_jumlah_mutasi
        //     FROM mutasi_obat_detail
        //     LEFT JOIN mutasi_obat ON mutasi_obat.id_mutasi_obat = mutasi_obat_detail.id_mutasi_obat
        //     WHERE MONTH(tanggal_mutasi) = '03' AND YEAR(tanggal_mutasi) = '2014' AND gudang = 'rawat_inap'
        //     GROUP BY mutasi_obat_detail.id_katalog_obat
        // ) table1
        // RIGHT JOIN katalog_obat k ON k.id_katalog_obat = table1.id_katalog_obat
        // ORDER BY k.id_katalog_obat


        $query = $this->db->query('
            SELECT k.id_katalog_obat, sum_jumlah_mutasi
            FROM (
                SELECT id_katalog_obat, SUM(jumlah_mutasi) as sum_jumlah_mutasi
                FROM mutasi_obat_detail
                LEFT JOIN mutasi_obat ON mutasi_obat.id_mutasi_obat = mutasi_obat_detail.id_mutasi_obat
                WHERE MONTH(tanggal_mutasi) = "'.$month.'" AND YEAR(tanggal_mutasi) = "'.$year.'"
                GROUP BY mutasi_obat_detail.id_katalog_obat
            ) table1
            RIGHT JOIN katalog_obat k ON k.id_katalog_obat = table1.id_katalog_obat
            ORDER BY k.id_katalog_obat
        ')->result();

        return $query;
    }

    public function get_rawat_persediaan_awal($month, $year, $type) {
        $query = $this->db->select('*')
            ->from('perubahan_persediaan_obat')
            ->join('katalog_obat', 'katalog_obat.id_katalog_obat = perubahan_persediaan_obat.id_katalog_obat')
            ->join('kategori_obat', 'katalog_obat.id_kategori_obat = kategori_obat.id_kategori_obat')
            ->where('tanggal_perubahan IN (SELECT MAX(tanggal_perubahan) FROM perubahan_persediaan_obat WHERE MONTH(tanggal_perubahan) = '.$month.' AND YEAR(tanggal_perubahan) = '.$year.')')
            ->order_by('katalog_obat.id_katalog_obat')
            ->get()
            ->result();

        if(empty($query)) {
            $query = $this->db->select('*')
                ->from('katalog_obat')
                ->join('kategori_obat', 'katalog_obat.id_kategori_obat = kategori_obat.id_kategori_obat')
                ->get()
                ->result();

            if($type == 'rawat_inap') {
                foreach($query as $keys => $rows) {
                    $query[$keys]->persediaan_rawat_inap = 0;
                }
            }else {
                foreach($query as $keys => $rows) {
                    $query[$keys]->persediaan_rawat_jalan = 0;
                }
            }
        }
        return $query;
    }

    function get_mutasi($month, $year, $gudang) {
        // SELECT COALESCE(SUM(jumlah_mutasi), 0), tanggal_mutasi, nama_obat, katalog_obat.id_katalog_obat, COALESCE(MAX(penerimaan_obat_detail.harga_satuan), 0) as harga_satuan
        // FROM katalog_obat
        // LEFT JOIN penerimaan_obat_detail ON katalog_obat.id_katalog_obat = penerimaan_obat_detail.id_katalog_obat
        // LEFT JOIN mutasi_obat_detail ON mutasi_obat_detail.id_katalog_obat = katalog_obat.id_katalog_obat
        // LEFT JOIN mutasi_obat ON mutasi_obat.id_mutasi_obat = mutasi_obat_detail.id_mutasi_obat
        // AND MONTH(tanggal_mutasi) = '03' AND YEAR(tanggal_mutasi) = '2014'
        // GROUP BY katalog_obat.id_katalog_obat
        // $query = $this->db->select('COALESCE(SUM(jumlah_mutasi), 0) as sum_jumlah_mutasi, tanggal_mutasi, nama_obat, katalog_obat.id_katalog_obat, COALESCE(MAX(penerimaan_obat_detail.harga_satuan), 0) as harga_satuan', FALSE)
        //     ->from('katalog_obat')
        //     ->join('penerimaan_obat_detail', 'katalog_obat.id_katalog_obat = penerimaan_obat_detail.id_katalog_obat', 'left')
        //     ->join('mutasi_obat_detail', 'mutasi_obat_detail.id_katalog_obat = katalog_obat.id_katalog_obat', 'LEFT')
        //     ->join('mutasi_obat', 'mutasi_obat.id_mutasi_obat = mutasi_obat_detail.id_mutasi_obat AND MONTH(tanggal_mutasi) = '.$month.' AND YEAR(tanggal_mutasi) = '.$year .' AND gudang = "'.$gudang.'"', 'left')
        //     // ->group_by('katalog_obat.id_katalog_obat')
        //     ->order_by('katalog_obat.id_katalog_obat')
        //     ->get()
        //     ->result();

        $query = $this->db->query('
            SELECT ko.id_katalog_obat, COALESCE(jumlah_mutasi, 0) as jumlah_mutasi
            FROM (
                SELECT table1.id_katalog_obat, SUM(jumlah_mutasi) as jumlah_mutasi, tanggal_mutasi
                FROM (
                    SELECT k.id_katalog_obat, MAX(p.harga_satuan)
                    FROM katalog_obat k
                    LEFT JOIN penerimaan_obat_detail p ON k.id_katalog_obat = p.id_katalog_obat
                    GROUP BY k.id_katalog_obat
                ) table1
                LEFT JOIN mutasi_obat_detail m ON m.id_katalog_obat = table1.id_katalog_obat
                LEFT JOIN mutasi_obat mo ON mo.id_mutasi_obat = m.id_mutasi_obat
                WHERE MONTH(tanggal_mutasi) = '.$month.' AND YEAR(tanggal_mutasi) = '.$year.' AND gudang = "'.$gudang.'"
                GROUP BY table1.id_katalog_obat
            ) table2
            RIGHT JOIN katalog_obat ko ON ko.id_katalog_obat = table2.id_katalog_obat
            GROUP BY ko.id_katalog_obat
            ORDER BY ko.id_katalog_obat
        ')->result();

        // dump($query);
        return $query;
    }

    function get_resep($month, $year, $jenis) {
        if($jenis == 'rawat_inap') {
            $jenis1 = 'rawat_inap';
            $jenis2 = 'rawat_jalan';
        }else {
            $jenis1 = 'rawat_jalan';
            $jenis2 = 'rawat_inap';
        }

        $query = $this->db->query('
            SELECT ko.id_katalog_obat, COALESCE(jumlah_pengeluaran, 0) as jumlah_pengeluaran
            FROM (
                SELECT k.id_katalog_obat , SUM(jumlah_pengeluaran) as jumlah_pengeluaran, tanggal_resep
                FROM katalog_obat k
                LEFT JOIN pengeluaran_obat p ON p.id_katalog_obat = k.id_katalog_obat
                LEFT JOIN resep r ON r.id_resep = p.id_resep
                WHERE MONTH(tanggal_resep) = '.$month.' AND YEAR(tanggal_resep) = '.$year.'
                AND (
                    (jenis_resep = "'.$jenis1.'" AND pengambilan_gudang_lain = 0)
                    OR
                    (jenis_resep = "'.$jenis2.'" AND pengambilan_gudang_lain = 1)
                )
                GROUP BY k.id_katalog_obat
            ) table1
            RIGHT JOIN katalog_obat ko ON ko.id_katalog_obat = table1.id_katalog_obat
            GROUP BY ko.id_katalog_obat
        ')->result();

        return $query;
    }
}