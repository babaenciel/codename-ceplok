<?php

class Perawat_model extends MY_Model {
    public $_table = 'perawat';
    protected $primary_key = 'id_perawat';
    public $before_create = array('timestamps_create');
    public $before_update = array('timestamps_update');

    function __construct() {
        parent::__construct();
    }

    protected function timestamps_create($perawat) {
        $perawat['created_at'] = $perawat['updated_at'] = date('Y-m-d');
        return $perawat;
    }

    protected function timestamps_update($perawat) {
        $perawat['updated_at'] = date('Y-m-d');
        return $perawat;
    }
}