<?php

class Log_katalog_obat_model extends MY_Model {
    public $_table = 'perubahan_persediaan_obat';
    protected $primary_key = 'id_perubahan_persediaan_obat';

    function __construct() {
        parent::__construct();
    }

    protected function timestamps_create($log) {
        $log['created_at'] = $log['updated_at'] = date('Y-m-d');
        return $log;
    }

    protected function timestamps_update($log) {
        $log['updated_at'] = date('Y-m-d');
        return $log;
    }

    // $type ada di constant
    function create_log($date, $type) {
        $this->load->model('obat_model', 'obat');

        $katalog_obat = $this->obat->get_all_for_log();

        $query = $this->db->select('id_perubahan_persediaan_obat')
            ->from($this->_table)
            ->where('tanggal_perubahan', $date)
            ->get()
            ->num_rows();

        // if > 0 berarti data log sudah ada, maka lakukan update log
        $data_all = array();
        $data = array();

        if($query > 0) {            
            foreach($katalog_obat as $rows) {
                $data = array(
                    'persediaan_pusat' => $rows->jumlah_pusat,
                    'persediaan_rawat_inap' => $rows->jumlah_rawat_inap,
                    'persediaan_rawat_jalan' => $rows->jumlah_rawat_jalan,
                    'jenis_perubahan' => $type,
                    'id_katalog_obat' => $rows->id_katalog_obat
                );
                
                array_push($data_all, $data);
                // $this->db->set('persediaan_pusat', $rows->jumlah_pusat);
                // $this->db->set('persediaan_rawat_inap', $rows->jumlah_rawat_inap);
                // $this->db->set('persediaan_rawat_jalan', $rows->jumlah_rawat_jalan);
                // $this->db->set('jenis_perubahan', $type);
                // $this->db->where('id_katalog_obat', $rows->id_katalog_obat);
                // $this->db->update($this->_table);
            }
            
            $this->db->update_batch($this->_table, $data_all, 'id_katalog_obat');
        }else {
            foreach($katalog_obat as $rows) {
                $data = array(
                    'id_katalog_obat' => $rows->id_katalog_obat,
                    'tanggal_perubahan' => $date,
                    'persediaan_pusat' => $rows->jumlah_pusat,
                    'persediaan_rawat_inap' => $rows->jumlah_rawat_inap,
                    'persediaan_rawat_jalan' => $rows->jumlah_rawat_jalan,
                    'jenis_perubahan' => $type
                );

                array_push($data_all, $data);
                // $this->db->insert($this->_table, array(
                //     'id_katalog_obat' => $rows->id_katalog_obat,
                //     'tanggal_perubahan' => $date,
                //     'persediaan_pusat' => $rows->jumlah_pusat,
                //     'persediaan_rawat_inap' => $rows->jumlah_rawat_inap,
                //     'persediaan_rawat_jalan' => $rows->jumlah_rawat_jalan,
                //     'jenis_perubahan' => $type
                // ));
            }

            $this->db->insert_batch($this->_table, $data_all);
        }
    }

    // $type adalah pusat, rawat inap, atau rawat jalan
    function get_log($month, $year, $type) {
        $query = $this->db->select('MAX(tanggal_perubahan) as tanggal_perubahan, katalog_obat.id_katalog_obat,
            persediaan_pusat, persediaan_rawat_jalan, persediaan_rawat_inap, nama_obat, nama_kategori, berat_netto')
            ->from($this->_table)
            ->join('katalog_obat', $this->_table.'.id_katalog_obat = katalog_obat.id_katalog_obat', 'LEFT')
            ->join('kategori_obat', 'katalog_obat.id_kategori_obat = kategori_obat.id_kategori_obat', 'LEFT')
            ->where('YEAR(tanggal_perubahan)', $year)
            ->where('MONTH(tanggal_perubahan)', $month)
            ->group_by('id_katalog_obat')
            ->get()
            ->result();

        return $query;
    }

    function olah_tanggal($month) {
        if($month == 1356998400000) {
            $month = '01';
        }else if($month == 1359676800000) {
            $month = '02';
        }else if($month == 1362096000000) {
            $month = '03';
        }else if($month == 1364774400000) {
            $month = '04';
        }else if($month == 1367366400000) {
            $month = '05';
        }else if($month == 1370044800000) {
            $month = '06';
        }else if($month == 1372636800000) {
            $month = '07';
        }else if($month == 1375315200000) {
            $month = '08';
        }else if($month == 1377993600000) {
            $month = '09';
        }else if($month == 1380585600000) {
            $month = '10';
        }else if($month == 1383264000000) {
            $month = '11';
        }else if($month == 1385856000000) {
            $month = '12';
        }

        return $month;
    }
}