<?php

class Visit_dokter_model extends MY_Model {
    public $_table = 'visit_dokter';
    protected $primary_key = 'id_visit_dokter';
    public $before_create = array('timestamps_create');
    public $before_update = array('timestamps_update');

    function __construct() {
        parent::__construct();
    }

    protected function timestamps_create($dokter) {
        $dokter['created_at'] = $dokter['updated_at'] = date('Y-m-d');
        return $dokter;
    }

    protected function timestamps_update($dokter) {
        $dokter['updated_at'] = date('Y-m-d');
        return $dokter;
    }

    // $id = id rawat inap
    function get_all_visit($id) {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('tarif_visit', $this->_table.'.id_tarif_visit = tarif_visit.id_tarif_visit')
            ->join('kamar', 'tarif_visit.id_kamar = kamar.id_kamar')
            ->join('dokter', $this->_table.'.id_dokter = dokter.id_dokter')
            ->where('id_rawat_inap', $id)
            ->get()
            ->result();

        return $query;
    }

    // $id = id rawat inap
    function get_all_visit_grouped($id) {
        $query = $this->db->query('
            SELECT * FROM (
                SELECT tarif_visit.id_tarif_visit, 
                    sum(tarif_visit.tarif) total_tarif,
                    count(tarif_visit.id_tarif_visit) count_layanan
                FROM visit_dokter
                JOIN tarif_visit ON visit_dokter.id_tarif_visit = tarif_visit.id_tarif_visit
                WHERE id_rawat_inap =  '.$id.' 
                GROUP BY tarif_visit.id_tarif_visit
            ) A
            INNER JOIN tarif_visit B ON A.id_tarif_visit = B.id_tarif_visit
        ')->result();

        return $query;
    }

    function init_data($existing_data=NULL) {
        if(!empty($existing_data)) {
            $data['dokter'] = $existing_data->id_dokter;
            $data['jenis_visit'] = $existing_data->id_tarif_visit;
            $data['tanggal_visit'] = conv_date_format($existing_data->tanggal_visit, 'd-m-Y');
            $data['waktu_visit'] = $existing_data->waktu_visit;
        }else {
            $data['dokter'] = '';
            $data['jenis_visit'] = '';
            $data['tanggal_visit'] = '';
            $data['waktu_visit'] = '';
        }

        return $data;
    }
}