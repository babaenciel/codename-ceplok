<?php

class Penerimaan_obat_detail_model extends MY_Model {
	public $_table = 'penerimaan_obat_detail';
	protected $primary_key = 'id_penerimaan_obat_detail';

	function __construct() {
		parent::__construct();
	}

	public function get_all_by_katalog_grouped($id_katalog_obat) {
		$query = $this->db->select('*')
			->from($this->_table)
			->where('id_katalog_obat', $id_katalog_obat)
			->group_by('id_penerimaan_obat')
			->get()
			->result();

		return $query;
	}

	public function get_with_katalog_and_penerimaan_obat($id_penerimaan_obat_detail) {
		$query = $this->db->select('nomor_faktur, tanggal_penerimaan, id_supplier,
				jumlah_dus, '.$this->_table.'.quantity_per_dus, '.$this->_table.'.harga_per_dus,
				'.$this->_table.'.harga_satuan, jumlah_penerimaan, '.$this->_table.'.id_penerimaan_obat,
				'.$this->_table.'.id_katalog_obat')
			->from($this->_table)
			->join('penerimaan_obat', $this->_table.'.id_penerimaan_obat = penerimaan_obat.id_penerimaan_obat')
			->join('katalog_obat', $this->_table.'.id_katalog_obat = katalog_obat.id_katalog_obat')
			->where($this->primary_key, $id_penerimaan_obat_detail)
			->get()
			->row();

		return $query;
	}

	public function get_all_with_penerimaan_obat($id_katalog_obat) {
		$query = $this->db->select('*')
			->from($this->_table)
			->join('penerimaan_obat', $this->_table.'.id_penerimaan_obat = penerimaan_obat.id_penerimaan_obat')
			->join('supplier', $this->_table.'.id_supplier = supplier.id_supplier')
			->where('id_katalog_obat', $id_katalog_obat)
			->get()
			->result();

		return $query;
	}

	// $id = id katalog obat
	// public function get_for_view_detail_obat($id) {
	// 	$query = $this->db->select('*')
	// 		->from($this->_table)
	// 		->join('supplier', $this->_table.'.id_supplier = supplier.id_supplier')
	// 		->where('id_katalog_obat', $id)
	// 		->group_by($this->_table.'.id_supplier')
	// 		->get()
	// 		->result();

	// 	return $query;
	// }

	// $id = id katalog obat
	public function get_for_view_detail_obat($id) {
		$query = $this->db->query('
			SELECT C.nama_supplier, C.quantity_per_dus, C.harga_per_dus, 
				C.harga_satuan, C.id_supplier, C.id_katalog_obat,
				D.harga_satuan harga_jual_satuan 
			FROM (
				SELECT B.nama_supplier, A.quantity_per_dus, A.harga_per_dus, 
					A.harga_satuan, B.id_supplier, A.id_katalog_obat
				FROM penerimaan_obat_detail A
				JOIN supplier B ON A.id_supplier = B.id_supplier
				WHERE id_katalog_obat =  '.$id.'
				ORDER BY id_penerimaan_obat_detail DESC
			) C
			LEFT JOIN katalog_obat D ON C.id_katalog_obat = D.id_katalog_obat
			GROUP BY C.id_supplier 
		')->result();

		return $query;
	}	

	public function get_for_grafik_detail() {
		// SELECT COALESCE(MAX(po.harga_satuan), 0), k.id_katalog_obat
		// FROM katalog_obat k
		// LEFT JOIN penerimaan_obat_detail po ON k.id_katalog_obat = po.id_katalog_obat
		// GROUP BY k.id_katalog_obat
		$query = $this->db->select('COALESCE(MAX('.$this->_table.'.harga_satuan), 0) as harga_satuan, katalog_obat.id_katalog_obat', FALSE)
			->from('katalog_obat')
			->join($this->_table, $this->_table.'.id_katalog_obat = katalog_obat.id_katalog_obat', 'LEFT')
			->group_by('katalog_obat.id_katalog_obat')
			->get()
			->result();

		return $query;
	}
}