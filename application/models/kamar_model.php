<?php

class Kamar_model extends MY_Model {
    public $_table = 'kamar';
    protected $primary_key = 'id_kamar';
    public $before_create = array('timestamps_create');
    public $before_update = array('timestamps_update');

    function __construct() {
        parent::__construct();
    }

    protected function timestamps_create($kamar) {
        $kamar['created_at'] = $kamar['updated_at'] = date('Y-m-d');
        return $kamar;
    }

    protected function timestamps_update($kamar) {
        $kamar['updated_at'] = date('Y-m-d');
        return $kamar;
    }
}