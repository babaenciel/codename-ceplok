<?php

class Supplier_model extends MY_Model {
	public $_table = 'supplier';
	protected $primary_key = 'id_supplier';

	function __construct() {
		parent::__construct();
	}

    function init_field($existing_data=NULL) {
        if(!empty($existing_data)) {
            $data['nama_supplier'] = $existing_data->nama_supplier;
            $data['nama_medrep'] = $existing_data->nama_medrep;
            $data['telepon_medrep'] = $existing_data->telepon_medrep;
        }else {
            $data['nama_supplier'] = '';
            $data['nama_medrep'] = '';
            $data['telepon_medrep'] = '';
        }

        return $data;
    }
}