<?php

class Penerimaan_obat_model extends MY_Model {
	public $_table = 'penerimaan_obat';
	protected $primary_key = 'id_penerimaan_obat';

	function __construct() {
		parent::__construct();
	}

	public function get_like_by_nomor_faktur($nomor_faktur) {
		$query = $this->db->select('nomor_faktur, tanggal_penerimaan')
			->from($this->_table)
			->like('nomor_faktur', $nomor_faktur)
			->get()
			->result();

		return $query;
	}
}