<?php

class Pemakaian_kamar_model extends MY_Model {
    public $_table = 'pemakaian_kamar';
    protected $primary_key = 'id_pemakaian_kamar';
    public $before_create = array('timestamps_create');
    public $before_update = array('timestamps_update');

    function __construct() {
        parent::__construct();
    }

    protected function timestamps_create($pemakaian_kamar) {
        $pemakaian_kamar['created_at'] = $pemakaian_kamar['updated_at'] = date('Y-m-d');
        return $pemakaian_kamar;
    }

    protected function timestamps_update($pemakaian_kamar) {
        $pemakaian_kamar['updated_at'] = date('Y-m-d');
        return $pemakaian_kamar;
    }

    // $id = id rawat inap
    function get_all_pemakaian_kamar($id) {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('kamar', $this->_table.'.id_kamar = kamar.id_kamar')
            ->where('id_rawat_inap', $id)
            ->get()
            ->result();

        return $query;
    }

    // $id digunakan untuk mencari tanggal mulai suatu rawat inap juga sudah pernah masukin kamar
    function init_data($existing_data=NULL, $id=NULL) {
        if(!empty($existing_data)) {
            $data['kamar'] = $existing_data->id_kamar;
            $data['tanggal_mulai'] = conv_date_format($existing_data->tgl_mulai, 'd-m-Y');
            $data['tanggal_akhir'] = (!empty($existing_data->tgl_akhir) ? conv_date_format($existing_data->tgl_akhir, 'd-m-Y') : '');
        }else {
            $this->load->model('rawat_inap_model', 'rawat_inap');
            // $rawat_inap = $this->rawat_inap->get_with_pemakaian_kamar($id);
            $rawat_inap = $this->rawat_inap->get($id);

            $data['kamar'] = '';
            $data['tanggal_akhir'] = '';
            $data['tanggal_mulai'] = '';

            if(!empty($rawat_inap)) {
                $data['tanggal_mulai'] = conv_date_format($rawat_inap->tgl_masuk, 'd-m-Y');
            }
        }

        return $data;
    }

    // $id = id pemakaian kamar
    function get_pemakaian_kamar($id) {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('kamar', $this->_table.'.id_kamar = kamar.id_kamar')
            ->where('id_pemakaian_kamar', $id)
            ->get()
            ->row();

        return $query;
    }

    // $id = id rawat inap
    function set_tanggal_pulang($id) {
        $this->db->where('id_rawat_inap', $id);
        $this->db->where('tgl_akhir IS NULL');
        $this->db->update($this->_table, array('tgl_akhir'=>date('Y-m-d')));
    }

    function get_pemakaian_kamar_dropdown($id_kamar=NULL) {
        $this->load->model('kamar_model', 'kamar');

        // ambil kamar yang sudah terpakai
        $list_kamar_terpakai = $this->db->select('id_kamar')
            ->from($this->_table)
            ->where('tgl_akhir IS NULL')
            ->get()
            ->result();

        // ambil semua daftar kamar
        $list_kamar = $this->kamar->get_all();

        // jika id kamar terisi maka value pertama ditambahkan kamar dengan id tersebut
        // ini untuk penanganan edit
        if(!empty($id_kamar)) {
            $kamar = $this->kamar->get($id_kamar);
            $dropdown = array($kamar->id_kamar => $kamar->nama_kamar . ' - ' . $kamar->jenis_kamar);
        }else {
            $dropdown = array(''=>'');
        }

        foreach($list_kamar as $rows) {
            $terpakai = FALSE;

            foreach($list_kamar_terpakai as $rows2) {
                if($rows->id_kamar == $rows2->id_kamar) {
                    // tidak dimasukkan ke dalam dropdown array
                    $terpakai = TRUE;
                }
            }

            if(!$terpakai) {
                $dropdown[$rows->id_kamar] = $rows->nama_kamar . ' - ' . $rows->jenis_kamar;
            }
        }

        return $dropdown;

    }

    function is_tanggal_akhir_null($id) {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('kamar', $this->_table.'.id_kamar = kamar.id_kamar')
            ->where('id_rawat_inap', $id)
            ->where('tgl_akhir IS NULL')
            ->get()
            ->num_rows();

        if($query > 0) {
            return true;
        }

        return false;
    }
}