<?php

class Tindakan_model extends MY_Model {
    public $_table = 'tindakan_perawat';
    protected $primary_key = 'id_tindakan_perawat';
    public $before_create = array('timestamps_create');
    public $before_update = array('timestamps_update');

    function __construct() {
        parent::__construct();
    }

    protected function timestamps_create($dokter) {
        $dokter['created_at'] = $dokter['updated_at'] = date('Y-m-d');
        return $dokter;
    }

    protected function timestamps_update($dokter) {
        $dokter['updated_at'] = date('Y-m-d');
        return $dokter;
    }

    // $id = id rawat inap
    function get_all_tindakan($id) {
        $query = $this->db->select('*')
            ->from($this->_table)
            // ->join('perawat', $this->_table.'.id_perawat = perawat.id_perawat')
            ->join('tarif_tindakan', $this->_table.'.id_tarif_tindakan = tarif_tindakan.id_tarif_tindakan')
            ->join('kamar', 'tarif_tindakan.id_kamar = kamar.id_kamar', 'left')
            ->where('id_rawat_inap', $id)
            ->get()
            ->result();

        return $query;
    }

    // $id = id rawat inap
    function get_all_tindakan_grouped($id) {
        $query = $this->db->query('
            SELECT * FROM (
                SELECT tarif_tindakan.id_tarif_tindakan, 
                    sum(tarif_tindakan.tarif) total_tarif,
                    count(tarif_tindakan.id_tarif_tindakan) count_tindakan
                FROM tindakan_perawat
                JOIN tarif_tindakan ON tindakan_perawat.id_tarif_tindakan = tarif_tindakan.id_tarif_tindakan
                WHERE id_rawat_inap =  '.$id.' 
                GROUP BY tarif_tindakan.id_tarif_tindakan
            ) A
            INNER JOIN tarif_tindakan B ON A.id_tarif_tindakan = B.id_tarif_tindakan
        ')->result();
        return $query;
    }

    // $id = id tindakan perawat
    function get_tindakan_with_tarif_tindakan($id) {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('tarif_tindakan', $this->_table.'.id_tarif_tindakan = tarif_tindakan.id_tarif_tindakan')
            ->where('id_tindakan_perawat', $id)
            ->get()
            ->row();

        return $query;
    }

    function init_data($existing_data=NULL) {
        if(!empty($existing_data)) {
            // dump($existing_data);
            // $data['perawat'] = $existing_data->id_perawat;
            $data['jenis_tindakan'] = $existing_data->id_tarif_tindakan;
            $data['tanggal_tindakan'] = conv_date_format($existing_data->tgl_tindakan, 'd-m-Y');
            $data['tindakan_lainnya_checkbox'] = $existing_data->tindakan_lainnya;
            $data['waktu_tindakan'] = $existing_data->waktu_tindakan;
            if($existing_data->tindakan_lainnya == 1) {
                $data['tindakan_lainnya'] = $existing_data->nama_tindakan;
                $data['tarif'] = $existing_data->tarif;
            }else {
                $data['tindakan_lainnya'] = '';
                $data['tarif'] = '';
            }
        }else {
            // $data['perawat'] = '';
            $data['jenis_tindakan'] = '';
            $data['tanggal_tindakan'] = '';
            $data['tindakan_lainnya_checkbox'] = '';
            $data['tindakan_lainnya'] = '';
            $data['tarif'] = '';
            $data['waktu_tindakan'] = '';
        }

        return $data;
    }

}