<?php

class Tarif_fisioterapi_model extends MY_Model {
    public $_table = 'tarif_fisioterapi';
    protected $primary_key = 'id_tarif_fisioterapi';
    public $before_create = array('timestamps_create');
    public $before_update = array('timestamps_update');

    function __construct() {
        parent::__construct();
    }

    protected function timestamps_create($fisioterapi) {
        $fisioterapi['created_at'] = $fisioterapi['updated_at'] = date('Y-m-d');
        return $fisioterapi;
    }

    protected function timestamps_update($fisioterapi) {
        $fisioterapi['updated_at'] = date('Y-m-d');
        return $fisioterapi;
    }

    public function get_dropdown() {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('kamar', $this->_table.'.id_kamar = kamar.id_kamar', 'left')
            ->get()
            ->result();
        
        $dropdown = array();
        foreach($query as $rows) {            
            $dropdown[$rows->id_tarif_fisioterapi] = $rows->nama_terapi . ' - ' . $rows->jenis_kamar;
        }

        return $dropdown;
    }
}