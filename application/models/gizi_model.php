<?php

class Gizi_model extends MY_Model {
    public $_table = 'gizi';
    protected $primary_key = 'id_gizi';
    public $before_create = array('timestamps_create');
    public $before_update = array('timestamps_update');

    function __construct() {
        parent::__construct();
    }

    protected function timestamps_create($gizi) {
        $gizi['created_at'] = $gizi['updated_at'] = date('Y-m-d');
        return $gizi;
    }

    protected function timestamps_update($gizi) {
        $gizi['updated_at'] = date('Y-m-d');
        return $gizi;
    }

    // $id = id rawat inap
    function get_all_gizi($id) {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('tarif_gizi', $this->_table.'.id_tarif_gizi = tarif_gizi.id_tarif_gizi')
            ->join('kamar', 'tarif_gizi.id_kamar = kamar.id_kamar')
            ->where('id_rawat_inap', $id)
            ->get()
            ->result();

        return $query;
    }

    function init_data($existing_data=NULL) {
        if(!empty($existing_data)) {
            $data['gizi'] = $existing_data->id_tarif_gizi;
            $data['tanggal_mulai'] = conv_date_format($existing_data->tgl_mulai, 'd-m-Y');                       
            $data['tanggal_akhir'] = !empty($existing_data->tgl_akhir) ? conv_date_format($existing_data->tgl_akhir, 'd-m-Y') : '';                       
        }else {
            $data['gizi'] = '';
            $data['tanggal_mulai'] = '';            
            $data['tanggal_akhir'] = '';            
        }

        return $data;
    }

    // $id = id rawat inap
    function set_tanggal_akhir($id) {
        $this->db->where('id_rawat_inap', $id);
        $this->db->update($this->_table, array('tgl_akhir'=>date('Y-m-d')));
    }

    function is_tanggal_akhir_null($id) {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('tarif_gizi', $this->_table.'.id_tarif_gizi = tarif_gizi.id_tarif_gizi')
            ->join('kamar', 'tarif_gizi.id_kamar = kamar.id_kamar')
            ->where('id_rawat_inap', $id)
            ->where('tgl_akhir IS NULL')
            ->get()
            ->num_rows();

        if($query > 0) {
            return true;
        }

        return false;
    }
}