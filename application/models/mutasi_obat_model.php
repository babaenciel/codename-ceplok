<?php

class Mutasi_obat_model extends MY_Model {
	public $_table = 'mutasi_obat';
	protected $primary_key = 'id_mutasi_obat';
    public $before_create = array('timestamps_create');
    public $before_update = array('timestamps_update');

    function __construct() {
        parent::__construct();
    }

    protected function timestamps_create($mutasi_obat) {
        $mutasi_obat['created_at'] = $mutasi_obat['updated_at'] = date('Y-m-d');
        return $mutasi_obat;
    }

    protected function timestamps_update($mutasi_obat) {
        $mutasi_obat['updated_at'] = date('Y-m-d');
        return $mutasi_obat;
    }

	// $existing_data keisi ketika proses edit
    function init_form_field($existing_data=NULL) {
    	if(!empty($existing_data)) {
            $this->load->model('mutasi_obat_model', 'mutasi_obat');

            $mutasi = $this->mutasi_obat->get($existing_data[0]->id_mutasi_obat);
            $data['id_mutasi'] = $existing_data[0]->id_mutasi_obat;
            $data['tanggal_mutasi'] = conv_date_format($mutasi->tanggal_mutasi, 'd-m-Y');

            foreach($existing_data as $rows) {
                $data['obat[]'][] = $rows->id_katalog_obat;
                $data['jumlah[]'][] = $rows->jumlah_mutasi;
            }
    	}else {
            $data['id_mutasi'] = '';
    		$data['tanggal_mutasi'] = '';
    		$data['obat[]'][] = '';
    		$data['jumlah[]'][] = '';
    	}

    	return $data;
    }

    // @params: $obat, array, dari post
    //          $jumlah, array, dari post
    // @return: array gabungan obat dan jumlah
    function create_obat_jumlah_array($obat, $jumlah) {
        $obat_jumlah = array();

        foreach($obat as $keys=>$rows) {
            $obat_jumlah[$keys]['obat'] = $rows;

            if(empty($jumlah[$keys])) {
                $obat_jumlah[$keys]['jumlah'] = 0;
            }else {
                $obat_jumlah[$keys]['jumlah'] = $jumlah[$keys];
            }
        }

        return $obat_jumlah;
    }
}