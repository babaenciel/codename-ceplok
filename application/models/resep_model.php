<?php

class Resep_model extends MY_Model {
	public $_table = 'resep';
	protected $primary_key = 'id_resep';

	function __construct() {
		parent::__construct();
	}

	// $jenis = jenis resep
	public function get_all_with_pasien($jenis) {
		$query = $this->db->select('*')
			->from($this->_table)
            ->join('rawat_inap', $this->_table.'.id_rawat_inap = rawat_inap.id_rawat_inap', 'left')
			->join('pasien', 'rawat_inap.id_pasien = pasien.id_pasien', 'left')
            ->join('dokter', $this->_table.'.id_dokter = dokter.id_dokter', 'left')
			->where('jenis_resep', $jenis)
			->get()
			->result();

		return $query;
	}

    public function get_with_pasien_rawat_inap($id) {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('rawat_inap', $this->_table.'.id_rawat_inap = rawat_inap.id_rawat_inap', 'LEFT')
            ->join('pasien', 'rawat_inap.id_pasien = pasien.id_pasien', 'left', 'LEFT')
            ->join('dokter', $this->_table.'.id_dokter = dokter.id_dokter', 'left')
            ->where('id_resep', $id)
            ->get()
            ->row();

        return $query;
    }

	public function init_data($existing_data=NULL, $i=0) {
		if(!empty($existing_data)) {
			$this->load->model('resep_model', 'resep');

			$resep = $this->resep->get($existing_data[0]->id_resep);

            if($resep->jenis_resep == 'rawat_inap') {
                $data['pasien'] = $resep->id_rawat_inap;
            }else {
                $data['pasien'] = $resep->nama_pasien_rawat_jalan;
            }
            $data['tanggal_resep'] = conv_date_format($resep->tanggal_resep, 'd-m-Y');
            $data['waktu_resep'] = $resep->waktu_resep;
            $data['nomor_resep'] = $resep->nomor_resep;
            $data['nama_dokter'] = $resep->id_dokter;

            foreach($existing_data as $keys => $rows) {
            	$data['obat'][$keys] = $rows->id_katalog_obat;
            	$data['jumlah'][$keys] = $rows->jumlah_pengeluaran;
            	$data['gudang_lain'][$keys] = (($rows->pengambilan_gudang_lain == 1) ? 'checked' : '');
                $data['obat_pulang'][$keys] = (($rows->sebagai_obat_pulang == 1) ? 'checked' : '');

            	$data['i'][$keys] = $keys;
            }
		}else {
            $data['nomor_resep'] = '';
            $data['nama_dokter'] = '';
			$data['pasien'] = '';
			$data['tanggal_resep'] = '';
            $data['waktu_resep'] = '';
			$data['obat'][$i] = '';
    		$data['jumlah'][$i] = '';
    		$data['gudang_lain'][$i] = '';
            $data['obat_pulang'][$i] = '';
		}

		return $data;
	}

	// @params: $obat, array, dari post
    //          $jumlah, array, dari post
    //			$gudang_lain, array, dari post
    //          $obat_pulang, array, dari post
    // @return: array gabungan obat dan jumlah
    function create_array($obat, $jumlah, $gudang_lain, $obat_pulang) {
        $obat_jumlah = array();

        foreach($obat as $keys=>$rows) {
            $obat_jumlah[$keys]['obat'] = $rows;

            if(empty($gudang_lain[$keys])) {
            	$obat_jumlah[$keys]['gudang_lain'] = NULL;
            }else {
            	$obat_jumlah[$keys]['gudang_lain'] = 1;
            }

            if(empty($jumlah[$keys])) {
                $obat_jumlah[$keys]['jumlah'] = 0;
            }else {
                $obat_jumlah[$keys]['jumlah'] = $jumlah[$keys];
            }

            if(empty($obat_pulang[$keys])) {
                $obat_jumlah[$keys]['obat_pulang'] = 0;
            }else {
                $obat_jumlah[$keys]['obat_pulang'] = 1;
            }
        }

        return $obat_jumlah;
    }

    function get_resep_by_nomor_and_jenis($nomor_resep, $jenis_resep) {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->where('nomor_resep', $nomor_resep)
            ->where('jenis_resep', $jenis_resep)
            ->get()
            ->row();

        return $query;
    }

    function get_resep_pasien_rawat_inap($id_rawat_inap) {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('pengeluaran_obat', $this->_table.'.id_resep = pengeluaran_obat.id_resep', 'left')
            ->join('katalog_obat', 'pengeluaran_obat.id_katalog_obat = katalog_obat.id_katalog_obat', 'left')
            ->join('kategori_obat', 'katalog_obat.id_kategori_obat = kategori_obat.id_kategori_obat')
            ->where($this->_table.'.id_rawat_inap', $id_rawat_inap)
            ->get()
            ->result();

        return $query;
    }

    function get_pengeluaran_obat_for_retur($id_rawat_inap) {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('pengeluaran_obat', $this->_table.'.id_resep = pengeluaran_obat.id_resep', 'left')
            ->join('katalog_obat', 'pengeluaran_obat.id_katalog_obat = katalog_obat.id_katalog_obat', 'left')
            // ->join('rawat_inap', $this->_table.'.id_rawat_inap = rawat_inap.id_rawat_inap', 'left')
            // ->join('pasien', 'rawat_inap.id_pasien = pasien.id_pasien', 'left')
            // ->join('dokter', $this->_table.'.id_dokter = dokter.id_dokter', 'left')
            ->where($this->_table.'.id_rawat_inap', $id_rawat_inap)
            ->get()
            ->result();

        return $query;
    }

    function update_obat_retur($id_pengeluaran_obat, $data) {
        $this->db->where('id_pengeluaran_obat', $id_pengeluaran_obat);
        $this->db->update('pengeluaran_obat', $data);
    }

    function is_greater_than_pengeluaran_obat($id_pengeluaran_obat, $retur) {
        $query = $this->db->select('id_pengeluaran_obat')
            ->from('pengeluaran_obat')
            ->where('id_pengeluaran_obat', $id_pengeluaran_obat)
            ->where('jumlah_pengeluaran < ', $retur)
            ->get()
            ->num_rows();

        // jika jumlah pengeluaran obat > inputan, maka true
        if($query > 0) {
            return TRUE;
        }

        return FALSE;
    }

    function get_pengeluaran_obat_array($id_pengeluaran_obat_array) {
        $this->db->_protect_identifiers=false;
        $query = $this->db->select('id_pengeluaran_obat,
                jumlah_pengeluaran, id_resep, id_katalog_obat,
                IFNULL(jumlah_pengeluaran - sisa, jumlah_pengeluaran) as available,
                IFNULL(sisa, 0) as sisa')
            ->from('pengeluaran_obat')
            ->where_in('id_pengeluaran_obat', $id_pengeluaran_obat_array)
            ->get()
            ->result();

        return $query;
    }

}