<?php

class Sp_detail_model extends MY_Model {
    public $_table = 'sp_detail';
    protected $primary_key = 'id_sp_detail';
    public $before_create = array('timestamps_create');
    public $before_update = array('timestamps_update');

    function __construct() {
        parent::__construct();
    }

    protected function timestamps_create($sp_detail) {
        $sp_detail['created_at'] = $sp_detail['updated_at'] = date('Y-m-d');
        return $sp_detail;
    }

    protected function timestamps_update($sp_detail) {
        $sp_detail['updated_at'] = date('Y-m-d');
        return $sp_detail;
    }

    // $id = id surat permintaan
    public function get_sp_detail_with_katalog_obat($id) {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('katalog_obat', $this->_table.'.id_katalog_obat = katalog_obat.id_katalog_obat')
            ->join('kategori_obat', 'katalog_obat.id_kategori_obat = kategori_obat.id_kategori_obat')
            ->where('id_surat_permintaan', $id)
            ->get()
            ->result();

        return $query;
    }
}