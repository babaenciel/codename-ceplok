<?php

class Tarif_tindakan_model extends MY_Model {
    public $_table = 'tarif_tindakan';
    protected $primary_key = 'id_tarif_tindakan';
    public $before_create = array('timestamps_create');
    public $before_update = array('timestamps_update');

    function __construct() {
        parent::__construct();
    }

    protected function timestamps_create($tarif_tindakan) {
        $tarif_tindakan['created_at'] = $tarif_tindakan['updated_at'] = date('Y-m-d');
        return $tarif_tindakan;
    }

    protected function timestamps_update($tarif_tindakan) {
        $tarif_tindakan['updated_at'] = date('Y-m-d');
        return $tarif_tindakan;
    }

    function get_tindakan_dropdown($tindakan_lainnya=NULL) {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('kamar', $this->_table.'.id_kamar = kamar.id_kamar')
            ->where('tindakan_lainnya', $tindakan_lainnya)
            ->get()
            ->result();

        $arr = array();
        foreach($query as $rows) {
            $arr[$rows->id_tarif_tindakan] = $rows->nama_tindakan . '-' . $rows->jenis_kamar;
        }

        return $arr;
    }
}