<?php

class Dokter_model extends MY_Model {
	public $_table = 'dokter';
	protected $primary_key = 'id_dokter';
	public $before_create = array('timestamps_create');
	public $before_update = array('timestamps_update');

	function __construct() {
		parent::__construct();
	}

	protected function timestamps_create($dokter) {
        $dokter['created_at'] = $dokter['updated_at'] = date('Y-m-d');
        return $dokter;
    }

    protected function timestamps_update($dokter) {
    	$dokter['updated_at'] = date('Y-m-d');
    	return $dokter;
    }

    function get_dropdown() {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->get()
            ->result();

        $dropdown = array();
        foreach($query as $rows) {
            $dropdown[$rows->id_dokter] = $rows->nama_dokter . ' - ' . $rows->jenis_dokter;
        }

        return $dropdown;
    }
}