<?php

class Obat_retur_model extends MY_Model {
    public $_table = 'obat_retur';
    protected $primary_key = 'id_obat_retur';
    public $before_create = array('timestamps_create');
    public $before_update = array('timestamps_update');

    function __construct() {
        parent::__construct();
    }

    protected function timestamps_create($obat_retur) {
        $obat_retur['created_at'] = $obat_retur['updated_at'] = date('Y-m-d');
        return $obat_retur;
    }

    protected function timestamps_update($obat_retur) {
        $obat_retur['updated_at'] = date('Y-m-d');
        return $obat_retur;
    }

    function get_all_with_obat() {
        $query = $this->db->select('*, '.$this->_table.'.jumlah as jumlah_retur')
            ->from($this->_table)
            ->join('katalog_obat', $this->_table.'.id_katalog_obat = katalog_obat.id_katalog_obat')
            ->get()
            ->result();

        return $query;
    }
}