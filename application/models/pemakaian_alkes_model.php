<?php

class Pemakaian_alkes_model extends MY_Model {
    public $_table = 'pemakaian_alkes';
    protected $primary_key = 'id_pemakaian_alkes';

    public $before_create = array('timestamps_create');
    public $before_update = array('timestamps_update');

    function __construct() {
        parent::__construct();
    }

    protected function timestamps_create($pemakaian_alkes) {
        $pemakaian_alkes['created_at'] = $pemakaian_alkes['updated_at'] = date('Y-m-d');
        return $pemakaian_alkes;
    }

    protected function timestamps_update($pemakaian_alkes) {
        $pemakaian_alkes['updated_at'] = date('Y-m-d');
        return $pemakaian_alkes;
    }

    // $id = id rawat inap
    function get_all_pemakaian_alkes($id) {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('katalog_obat', $this->_table.'.id_katalog_obat = katalog_obat.id_katalog_obat')
            ->where('id_rawat_inap', $id)
            ->get()
            ->result();

        return $query;
    }

    function init_data($existing_data=NULL) {
        if(!empty($existing_data)) {
            $data['obat_dan_alkes'] = $existing_data->id_katalog_obat;
            $data['jumlah'] = $existing_data->jumlah;
            $data['tanggal'] = conv_date_format($existing_data->tanggal, 'd-m-Y');
            $data['sebagai_obat_pulang'] = $existing_data->sebagai_obat_pulang;
        }else {
            $data['obat_dan_alkes'] = '';
            $data['jumlah'] = '';
            $data['tanggal'] = '';
            $data['sebagai_obat_pulang'] = '';
        }

        return $data;
    }

    // $id = id_rawat_inap
    function get_pemakaian_obat($id, $jenis=NULL) {
        $this->db->select('*');
        $this->db->from($this->_table);
        $this->db->join('katalog_obat', $this->_table.'.id_katalog_obat = katalog_obat.id_katalog_obat');
        $this->db->join('kategori_obat', 'katalog_obat.id_kategori_obat = kategori_obat.id_kategori_obat');
        $this->db->where('id_rawat_inap', $id);

        if($jenis == 'obat_pulang') {
            $this->db->where('sebagai_obat_pulang', 1);
        }else {
            $this->db->where('katalog_obat.jenis', $jenis);
            $this->db->where('sebagai_obat_pulang', 0);
        }

        $query = $this->db->get()->result();

        return $query;
    }

    function update_obat_retur($id_pemakaian_alkes, $data) {
        $this->db->where('id_pemakaian_alkes', $id_pemakaian_alkes);
        $this->db->update($this->_table, $data);
    }

    function get_all_obat_retur() {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('katalog_obat', $this->_table.'.id_katalog_obat = katalog_obat.id_katalog_obat')
            ->where('sisa IS NOT NULL')
            ->get()
            ->result();

        return $query;
    }

    // $id = id pemakaian alkes
    function get_all_with_obat($id) {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('katalog_obat', $this->_table.'.id_katalog_obat = katalog_obat.id_katalog_obat')
            ->where('id_pemakaian_alkes', $id)
            ->get()
            ->result();

        return $query;
    }
}