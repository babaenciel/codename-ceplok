<?php

class Laboratorium_model extends MY_Model {
    public $_table = 'laboratorium';
    protected $primary_key = 'id_laboratorium';
    public $before_create = array('timestamps_create');
    public $before_update = array('timestamps_update');

    function __construct() {
        parent::__construct();
    }

    protected function timestamps_create($laboratorium) {
        $laboratorium['created_at'] = $laboratorium['updated_at'] = date('Y-m-d');
        return $laboratorium;
    }

    protected function timestamps_update($laboratorium) {
        $laboratorium['updated_at'] = date('Y-m-d');
        return $laboratorium;
    }

    // $id = id rawat inap
    function get_all_laboratorium($id) {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('tarif_laboratorium', $this->_table.'.id_tarif_laboratorium = tarif_laboratorium.id_tarif_laboratorium')
            ->join('kamar', 'tarif_laboratorium.id_kamar = kamar.id_kamar', 'left')
            ->where('id_rawat_inap', $id)
            ->get()
            ->result();

        return $query;
    }

    // $id = id rawat inap
    function get_all_laboratorium_grouped($id) {
        $query = $this->db->query('
            SELECT * FROM (
                SELECT tarif_laboratorium.id_tarif_laboratorium, 
                    sum(tarif_laboratorium.tarif) total_tarif,
                    count(tarif_laboratorium.id_tarif_laboratorium) count_layanan
                FROM (laboratorium)
                JOIN tarif_laboratorium ON laboratorium.id_tarif_laboratorium = tarif_laboratorium.id_tarif_laboratorium
                WHERE id_rawat_inap =  '.$id.' 
                GROUP BY tarif_laboratorium.id_tarif_laboratorium
            ) A
            INNER JOIN tarif_laboratorium B ON A.id_tarif_laboratorium = B.id_tarif_laboratorium
        ')->result();

        return $query;
    }

    function init_data($existing_data=NULL) {
        if(!empty($existing_data)) {
            $data['layanan'] = $existing_data->id_tarif_laboratorium;
            $data['tanggal_laboratorium'] = conv_date_format($existing_data->tgl_laboratorium, 'd-m-Y');
            $data['waktu_laboratorium'] = $existing_data->waktu_laboratorium;
        }else {
            $data['layanan'] = '';
            $data['tanggal_laboratorium'] = '';
            $data['waktu_laboratorium'] = '';
        }

        return $data;
    }
}