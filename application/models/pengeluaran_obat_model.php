<?php

class Pengeluaran_obat_model extends MY_Model {
	public $_table = 'pengeluaran_obat';
	protected $primary_key = 'id_pengeluaran_obat';

	function __construct() {
		parent::__construct();
	}

	function get_all_obat_retur() {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('katalog_obat', $this->_table.'.id_katalog_obat = katalog_obat.id_katalog_obat')
            ->join('kategori_obat', 'katalog_obat.id_kategori_obat = kategori_obat.id_kategori_obat')
            ->join('resep', $this->_table.'.id_resep = resep.id_resep')
            ->join('rawat_inap', 'resep.id_rawat_inap = rawat_inap.id_rawat_inap')
            ->join('pasien', 'rawat_inap.id_pasien = pasien.id_pasien')
            ->where('sisa IS NOT NULL')
            ->get()
            ->result();

        return $query;
    }

    // $id = id pengeluaran obat
    function get_all_with_obat($id) {
    	$this->db->_protect_identifiers=false;
        $query = $this->db->select('*, IFNULL(jumlah_pengeluaran - sisa, jumlah_pengeluaran) as available')
            ->from($this->_table)
            ->join('katalog_obat', $this->_table.'.id_katalog_obat = katalog_obat.id_katalog_obat')
            ->join('kategori_obat', 'katalog_obat.id_kategori_obat = kategori_obat.id_kategori_obat')
            ->where($this->_table.'.id_pengeluaran_obat', $id)
            ->get()
            ->result();
		return $query;
	}

    // $id = id pengeluaran obat
    function get_with_obat($id) {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('katalog_obat', $this->_table.'.id_katalog_obat = katalog_obat.id_katalog_obat')
            ->where('id_pengeluaran_obat', $id)
            ->get()
            ->row();
        return $query;
    }

	function get_pemakaian_obat($id, $jenis=NULL) {
		$this->db->_protect_identifiers=false;

        $this->db->select('*, COALESCE(sisa, 0) as sisa');
        $this->db->from($this->_table);
        $this->db->join('resep', $this->_table.'.id_resep  = resep.id_resep');
        $this->db->join('katalog_obat', $this->_table.'.id_katalog_obat = katalog_obat.id_katalog_obat');
        $this->db->join('kategori_obat', 'katalog_obat.id_kategori_obat = kategori_obat.id_kategori_obat');
        $this->db->where('id_rawat_inap', $id);

        if($jenis == 'obat_pulang') {
            $this->db->where('sebagai_obat_pulang', 1);
        }else {
            $this->db->where('katalog_obat.jenis', $jenis);
            $this->db->where('sebagai_obat_pulang', 0);
        }

        $query = $this->db->get()->result();

		return $query;
	}


	function get_pengeluaran_obat_setahun() {
		$query = $this->db->select($this->_table.'.id_katalog_obat,nama_obat,nama_generik,kelas_terapi,jenis,ved,
satuan,nama_kategori, sum(jumlah_pengeluaran) as total')
			->from($this->_table)
			->join('resep', $this->_table.'.id_resep = resep.id_resep')
			->join('katalog_obat', $this->_table.'.id_katalog_obat = katalog_obat.id_katalog_obat')
			->join('kategori_obat', 'katalog_obat.id_kategori_obat = kategori_obat.id_kategori_obat')
			->where('tanggal_resep >= DATE_SUB(curdate(),INTERVAL 1 YEAR)')
			->group_by('pengeluaran_obat.id_katalog_obat')
			->get()
			->result();

		return $query;
	}

	function get_pengeluaran_obat() {
		/**
		 * harga satuan masih diambil dari tabel katalog obat karena
		 * data penerimaan obat pada database tidak lengkap
		 * */
		$query = $this->db->select('id_katalog_obat, nama_obat, ved,lead_time, jenis, satuan, nama_kategori, harga_satuan, GROUP_CONCAT(bulan) as bulan, GROUP_CONCAT(total) as total, GROUP_CONCAT(total_harga) as total_harga ')
			->from('view_pengeluaran_obat')
			->group_by('id_katalog_obat')
			->order_by('bulan')
			->get()
			->result();

		return $query;
	}

	function get_pengeluaran_obat_with($id) {
		$query = $this->db->select($this->_table.'.id_katalog_obat,nama_obat,jenis,ved,
satuan,nama_kategori, tanggal_resep, sum(jumlah_pengeluaran) as total')
			->from($this->_table)
			->join('resep', $this->_table.'.id_resep = resep.id_resep')
			->join('katalog_obat', $this->_table.'.id_katalog_obat = katalog_obat.id_katalog_obat')
			->join('kategori_obat', 'katalog_obat.id_kategori_obat = kategori_obat.id_kategori_obat')
			//where order_date >= DATE_SUB(NOW(),INTERVAL 1 YEAR);
			->where($this->_table.'.id_katalog_obat = '.$id.' AND  (tanggal_resep >= DATE_SUB(curdate(),INTERVAL 1 YEAR))')
			->group_by('YEAR(resep.tanggal_resep), MONTH(resep.tanggal_resep)')
			->order_by('resep.tanggal_resep')
			->get()
			->result();

		return $query;
	}

	function get_periode() {
		$query = $this->db->select('tanggal_resep')
			->from($this->_table)
			->join('resep', $this->_table.'.id_resep = resep.id_resep')
			->where('tanggal_resep >= DATE_SUB(curdate(),INTERVAL 1 YEAR)')
			->group_by('YEAR(resep.tanggal_resep), MONTH(resep.tanggal_resep)')
			->order_by('resep.tanggal_resep')
			->get()
			->result();

		return $query;
	}

}