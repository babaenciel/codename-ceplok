<?php

class Rontgen_model extends MY_Model {
    public $_table = 'rontgen';
    protected $primary_key = 'id_rontgen';
    public $before_create = array('timestamps_create');
    public $before_update = array('timestamps_update');

    function __construct() {
        parent::__construct();
    }

    protected function timestamps_create($rontgen) {
        $rontgen['created_at'] = $rontgen['updated_at'] = date('Y-m-d');
        return $rontgen;
    }

    protected function timestamps_update($rontgen) {
        $rontgen['updated_at'] = date('Y-m-d');
        return $rontgen;
    }

    // $id = id rawat inap
    function get_all_rontgen($id) {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('tarif_rontgen', $this->_table.'.id_tarif_rontgen = tarif_rontgen.id_tarif_rontgen')
            ->join('kamar', 'tarif_rontgen.id_kamar = kamar.id_kamar', 'left')
            ->where('id_rawat_inap', $id)
            ->get()
            ->result();

        return $query;
    }

    // $id = id rawat inap
    function get_all_rontgen_grouped($id) {
        $query = $this->db->query('
            SELECT * FROM (
                SELECT tarif_rontgen.id_tarif_rontgen, 
                    sum(tarif_rontgen.tarif) total_tarif,
                    count(tarif_rontgen.id_tarif_rontgen) count_layanan
                FROM (rontgen)
                JOIN tarif_rontgen ON rontgen.id_tarif_rontgen = tarif_rontgen.id_tarif_rontgen
                WHERE id_rawat_inap =  '.$id.' 
                GROUP BY tarif_rontgen.id_tarif_rontgen
            ) A
            INNER JOIN tarif_rontgen B ON A.id_tarif_rontgen = B.id_tarif_rontgen
        ')->result();

        return $query;
    }

    function init_data($existing_data=NULL) {
        if(!empty($existing_data)) {
            $data['layanan'] = $existing_data->id_tarif_rontgen;
            $data['tanggal_rontgen'] = conv_date_format($existing_data->tgl_rontgen, 'd-m-Y');            
        }else {
            $data['layanan'] = '';
            $data['tanggal_rontgen'] = '';            
        }

        return $data;
    }
}    