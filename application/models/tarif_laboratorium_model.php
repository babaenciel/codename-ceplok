<?php

class Tarif_laboratorium_model extends MY_Model {
    public $_table = 'tarif_laboratorium';
    protected $primary_key = 'id_tarif_laboratorium';
    public $before_create = array('timestamps_create');
    public $before_update = array('timestamps_update');

    function __construct() {
        parent::__construct();
    }

    protected function timestamps_create($dokter) {
        $dokter['created_at'] = $dokter['updated_at'] = date('Y-m-d');
        return $dokter;
    }

    protected function timestamps_update($dokter) {
        $dokter['updated_at'] = date('Y-m-d');
        return $dokter;
    }

    public function get_dropdown() {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('kamar', $this->_table.'.id_kamar = kamar.id_kamar', 'left')
            ->get()
            ->result();
        
        $dropdown = array();
        foreach($query as $rows) {            
            $dropdown[$rows->id_tarif_laboratorium] = $rows->nama_layanan . ' - ' . $rows->jenis_kamar;
        }

        return $dropdown;
    }
}