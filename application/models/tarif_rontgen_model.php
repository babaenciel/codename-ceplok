<?php

class Tarif_rontgen_model extends MY_Model {
    public $_table = 'tarif_rontgen';
    protected $primary_key = 'id_tarif_rontgen';
    public $before_create = array('timestamps_create');
    public $before_update = array('timestamps_update');

    function __construct() {
        parent::__construct();
    }

    protected function timestamps_create($rontgen) {
        $rontgen['created_at'] = $rontgen['updated_at'] = date('Y-m-d');
        return $rontgen;
    }

    protected function timestamps_update($rontgen) {
        $rontgen['updated_at'] = date('Y-m-d');
        return $rontgen;
    }

    public function get_dropdown() {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('kamar', $this->_table.'.id_kamar = kamar.id_kamar', 'left')
            ->get()
            ->result();
        
        $dropdown = array();
        foreach($query as $rows) {            
            $dropdown[$rows->id_tarif_rontgen] = $rows->nama_layanan . ' - ' . $rows->jenis_kamar;
        }

        return $dropdown;
    }
}