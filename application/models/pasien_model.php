<?php

class Pasien_model extends MY_Model {
	public $_table = 'pasien';
	protected $primary_key = 'id_pasien';
	public $before_create = array('timestamps_create');
	public $before_update = array('timestamps_update');

	function __construct() {
		parent::__construct();
	}

	protected function timestamps_create($pasien) {
        $pasien['created_at'] = $pasien['updated_at'] = date('Y-m-d');
        return $pasien;
    }

    protected function timestamps_update($pasien) {
    	$pasien['updated_at'] = date('Y-m-d');
    	return $pasien;
    }

    // $existing_data keisi ketika proses edit
    function init_form_field($existing_data=NULL) {
    	if(!empty($existing_data)) {
            $data['id_pasien'] = $existing_data->id_pasien;
    		$data['nama'] = $existing_data->nama_pasien;
    		$data['alamat'] = $existing_data->alamat_pasien;
    		$data['umur'] = $existing_data->umur_pasien; //get_age($existing_data->tanggal_lahir);
    		$data['jenis_kelamin'] = $existing_data->jenis_kelamin;
            $data['tanggal_lahir'] = !empty($existing_data->tanggal_lahir) ? conv_date_format($existing_data->tanggal_lahir, 'd-m-Y') : '';
            $data['umur_pasien'] = !empty($existing_data->umur_pasien) ? $existing_data->umur_pasien : '';
            $data['telepon'] = $existing_data->telepon;
            $data['nama_keluarga'] = $existing_data->nama_keluarga;
            $data['telepon_keluarga'] = $existing_data->telepon_keluarga;
            $data['nomor_bpjs'] = $existing_data->nomor_bpjs;
            $data['tinggi_badan'] = $existing_data->tinggi_badan;
            $data['berat_badan'] = $existing_data->berat_badan;
            $data['sistole'] = $existing_data->sistole;
            $data['diastole'] = $existing_data->diastole;
            $data['respiratory_rate'] = $existing_data->respiratory_rate;
            $data['heart_rate'] = $existing_data->heart_rate;
    	}else {
            $data['id_pasien'] = $this->get_last_id_pasien() + 1;
    		$data['nama'] = '';
    		$data['alamat'] = '';
    		$data['umur'] = '';
    		$data['jenis_kelamin'] = '';
            $data['tanggal_lahir'] = '';
            $data['umur_pasien'] = '';
            $data['telepon'] = '';
            $data['nama_keluarga'] = '';
            $data['telepon_keluarga'] = '';
            $data['nomor_bpjs'] = '';
            $data['tinggi_badan'] = '';
            $data['berat_badan'] = '';
            $data['sistole'] = '';
            $data['diastole'] = '';
            $data['respiratory_rate'] = '';
            $data['heart_rate'] = '';
    	}

    	return $data;
    }

    function populate_form_field($input, $type='add') {
        $data = array(
            'id_pasien' => $input['id_pasien'],
            'nama_pasien' => $input['nama'],
            'alamat_pasien' => $input['alamat'],
            'umur_pasien' => $input['umur'],
            'jenis_kelamin' => $input['jenis_kelamin'],
            'tanggal_lahir' => !empty($input['tanggal_lahir']) ? conv_date_format($input['tanggal_lahir'], 'Y-m-d') : NULL,
            'telepon' => $input['telepon'],
            'nama_keluarga' => $input['nama_keluarga'],
            'telepon_keluarga' => $input['telepon_keluarga'],
            'nomor_bpjs' => $input['nomor_bpjs'],
            'tinggi_badan' => $input['tinggi_badan'],
            'berat_badan' => $input['berat_badan'],
            'sistole' => $input['sistole'],
            'diastole' => $input['diastole'],
            'respiratory_rate' => $input['respiratory_rate'],
            'heart_rate' => $input['heart_rate']
        );

        if($type === 'add') {
            $data['status'] = PASIEN_STATUS_HIDUP;
        }else if($type === 'edit') {
            // $data['id_pasien'] = $input['id_pasien'];
        }

        return $data;
    }

    function get_pasien_with_rawat_inap() {
        $query = $this->db->query('
            SELECT * FROM (
                SELECT pasien.id_pasien as id_pasien_ordered, nama_pasien, jenis_kelamin, alamat_pasien,
                    umur_pasien, tgl_masuk, tgl_keluar, rawat_inap.id_rawat_inap, rawat_inap.status, nama_kamar, jenis_kamar
                FROM pasien
                LEFT JOIN rawat_inap ON pasien.id_pasien = rawat_inap.id_pasien
                LEFT JOIN pemakaian_kamar ON rawat_inap.id_rawat_inap = pemakaian_kamar.id_rawat_inap
                LEFT JOIN kamar ON pemakaian_kamar.id_kamar = kamar.id_kamar
                ORDER BY tgl_masuk DESC
            ) AS tmp_table
            WHERE status = 1
            GROUP BY id_pasien_ordered
        ')->result();

        return $query;
    }

    function count_pasien_rawat_inap_by_month_year($month, $year) {
        $query = $this->db->query('
            SELECT * FROM (
                SELECT pasien.id_pasien as id_pasien_ordered, nama_pasien, jenis_kelamin, alamat_pasien,
                    umur_pasien, tgl_masuk, tgl_keluar, id_rawat_inap, rawat_inap.status
                FROM pasien
                LEFT JOIN rawat_inap ON pasien.id_pasien = rawat_inap.id_pasien
                ORDER BY tgl_masuk DESC
            ) AS tmp_table
            WHERE status = 1 AND MONTH(tgl_masuk) = "'.$month.'" AND YEAR(tgl_masuk) = "'.$year.'"
            GROUP BY id_pasien_ordered
        ')->num_rows();

        return $query;
    }

    function count_all_pasien_rawat_inap() {
        $query = $this->db->query('
            SELECT * FROM (
                SELECT pasien.id_pasien as id_pasien_ordered, nama_pasien, jenis_kelamin, alamat_pasien,
                    umur_pasien, tgl_masuk, tgl_keluar, id_rawat_inap, rawat_inap.status
                FROM pasien
                LEFT JOIN rawat_inap ON pasien.id_pasien = rawat_inap.id_pasien
                ORDER BY tgl_masuk DESC
            ) AS tmp_table
            WHERE status = 1 OR status = 0
            GROUP BY id_pasien_ordered
        ')->num_rows();

        return $query;
    }

    function change_status($id, $status) {
        $this->db->where('id_pasien', $id);
        $this->db->update($this->_table, array('status'=>$status));
    }

    function get_dropdown() {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('rawat_inap', $this->_table.'.id_pasien = rawat_inap.id_pasien AND rawat_inap.status = 1')
            ->where('pasien.status', 1)
            ->get()
            ->result();

        $arr = array();
        if(!empty($query)) {
            foreach($query as $rows) {
                $arr[$rows->id_rawat_inap] = $rows->nama_pasien . ' - ' . $rows->alamat_pasien;
            }
        }

        return $arr;
    }

    function get_dropdown_rawat_inap() {
        $query = $this->db->query('select A.id_pasien, A.umur_pasien, A.nama_pasien from pasien A
            left join (select P.id_pasien, P.nama_pasien, RI.status status_rawat_inap from pasien P
            left join rawat_inap RI on P.id_pasien = RI.id_pasien
            where RI.status = 1) B on A.id_pasien = B.id_pasien
        where B.status_rawat_inap != 1 OR B.status_rawat_inap IS NULL')->result();

        $arr = array();
        if(!empty($query)) {
            foreach($query as $rows) {
                $arr[$rows->id_pasien] = $rows->nama_pasien . ' - ' . $rows->umur_pasien . ' tahun';
            }
        }

        return $arr;
    }

    function get_last_id_pasien() {
        $query = $this->db->select('id_pasien')
            ->from($this->_table)
            ->order_by('id_pasien', 'desc')
            ->limit(1)
            ->get()
            ->row();

        if(count($query) > 0) {
            return $query->id_pasien;
        }

        return 0;
    }
}