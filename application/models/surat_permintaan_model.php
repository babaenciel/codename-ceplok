<?php

class Surat_permintaan_model extends MY_Model {
    public $_table = 'surat_permintaan';
    protected $primary_key = 'id_surat_permintaan';
    public $before_create = array('timestamps_create');
    public $before_update = array('timestamps_update');

    function __construct() {
        parent::__construct();
    }

    protected function timestamps_create($surat_permintaan) {
        $surat_permintaan['created_at'] = $surat_permintaan['updated_at'] = date('Y-m-d');
        return $surat_permintaan;
    }

    protected function timestamps_update($surat_permintaan) {
        $surat_permintaan['updated_at'] = date('Y-m-d');
        return $surat_permintaan;
    }
}