<?php

class Fisioterapi_model extends MY_Model {
    public $_table = 'fisioterapi';
    protected $primary_key = 'id_fisioterapi';
    public $before_create = array('timestamps_create');
    public $before_update = array('timestamps_update');

    function __construct() {
        parent::__construct();
    }

    protected function timestamps_create($fisioterapi) {
        $fisioterapi['created_at'] = $fisioterapi['updated_at'] = date('Y-m-d');
        return $fisioterapi;
    }

    protected function timestamps_update($fisioterapi) {
        $fisioterapi['updated_at'] = date('Y-m-d');
        return $fisioterapi;
    }

    // $id = id rawat inap
    function get_all_fisioterapi($id) {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('tarif_fisioterapi', $this->_table.'.id_tarif_fisioterapi = tarif_fisioterapi.id_tarif_fisioterapi')
            ->join('kamar', 'tarif_fisioterapi.id_kamar = kamar.id_kamar', 'left')
            ->where('id_rawat_inap', $id)
            ->get()
            ->result();

        return $query;
    }

    // $id = id rawat inap
    function get_all_fisioterapi_grouped($id) {
        $query = $this->db->query('
            SELECT * FROM (
                SELECT tarif_fisioterapi.id_tarif_fisioterapi, 
                    sum(tarif_fisioterapi.tarif) total_tarif,
                    count(tarif_fisioterapi.id_tarif_fisioterapi) count_layanan
                FROM (fisioterapi)
                JOIN tarif_fisioterapi ON fisioterapi.id_tarif_fisioterapi = tarif_fisioterapi.id_tarif_fisioterapi
                WHERE id_rawat_inap =  '.$id.' 
                GROUP BY tarif_fisioterapi.id_tarif_fisioterapi
            ) A
            INNER JOIN tarif_fisioterapi B ON A.id_tarif_fisioterapi = B.id_tarif_fisioterapi
        ')->result();

        return $query;
    }

    function init_data($existing_data=NULL) {
        if(!empty($existing_data)) {
            $data['layanan'] = $existing_data->id_tarif_fisioterapi;
            $data['tanggal_fisioterapi'] = conv_date_format($existing_data->tgl_fisioterapi, 'd-m-Y');            
        }else {
            $data['layanan'] = '';
            $data['tanggal_fisioterapi'] = '';            
        }

        return $data;
    }
}    