<?php

class Nota_model extends MY_Model {
    function __construct() {
        parent::__construct();
    }

    // $id = id rawat inap
    function get_data($id, $for_nota=TRUE) {
        $this->load->model('rawat_inap_model', 'rawat_inap');
        $this->load->model('pemakaian_kamar_model', 'pemakaian_kamar');
        $this->load->model('pemakaian_alkes_model', 'pemakaian_alkes');
        $this->load->model('visit_dokter_model', 'visit_dokter');
        $this->load->model('laboratorium_model', 'laboratorium');
        $this->load->model('tindakan_model', 'tindakan');
        $this->load->model('pengeluaran_obat_model', 'pengeluaran_obat');
        $this->load->model('pasien_model', 'pasien');
        $this->load->model('gizi_model', 'gizi');
        $this->load->model('fisioterapi_model', 'fisioterapi');
        $this->load->model('rontgen_model', 'rontgen');

        $rawat_inap_data = $this->rawat_inap->get($id);
        $pasien = $this->pasien->get_by('id_pasien', $rawat_inap_data->id_pasien);

        // HITUNG KAMAR
        $data['kamar'] = $this->pemakaian_kamar->get_all_pemakaian_kamar($id);

        $total_satu_kamar = 0;
        $total_semua_kamar = 0;
        foreach($data['kamar'] as $keys=>$rows) {
            if(!empty($rows->tgl_akhir)) {
                $tgl_mulai = new DateTime($rows->tgl_mulai);
                $tgl_akhir = new DateTime($rows->tgl_akhir);
                $diff_date = $tgl_mulai->diff($tgl_akhir)->days;

                if($diff_date == 0) {
                    $diff_date = 1;
                }else {
                    $diff_date += 1;
                }
            }else {
                $diff_date = 1;
            }

            $total_satu_kamar = $diff_date * $rows->tarif_kamar;

            $data['kamar'][$keys]->total_satu_kamar = $total_satu_kamar;
            $data['kamar'][$keys]->diff_date = $diff_date;

            $total_semua_kamar += $total_satu_kamar;
        }
        // dump($data['kamar']);
        // dump($total_semua_kamar);
        $data['kamar_total'] = $total_semua_kamar;

        // END HITUNG KAMAR

        // HITUNG OBAT
        $data['obat'] = $this->pengeluaran_obat->get_pemakaian_obat($id, 'obat');
        $total_semua_obat = 0;
        $total_satu_obat = 0;
        foreach($data['obat'] as $keys=>$rows) {
            $total_satu_obat = $rows->harga_satuan * ($rows->jumlah_pengeluaran - $rows->sisa);

            $data['obat'][$keys]->total_satu_obat = $total_satu_obat;
            $total_semua_obat += $total_satu_obat;

            $obat_desc = $rows->nama_obat . ' @[' . format_number($rows->harga_satuan, TRUE) . ']'.
            ', ' . ($rows->jumlah_pengeluaran - $rows->sisa) . ' ' . $rows->satuan;
            $data['obat'][$keys]->obat_desc = $obat_desc;
            $clean = str_replace('[', ' ', $rows->obat_desc);
            $clean = str_replace(']', ' ', $clean);
            $data['obat'][$keys]->obat_desc_clean = $clean;

            $data['obat'][$keys]->satuan = $rows->jumlah_pengeluaran - $rows->sisa;
        }

        $data['obat_total'] = $total_semua_obat;
        // dump($data['obat_total']);

        // END HITUNG OBAT

        // HITUNG OBAT PULANG
        $data['obat_pulang'] = $this->pengeluaran_obat->get_pemakaian_obat($id, 'obat_pulang');
        $total_semua_obat_pulang = 0;
        $total_satu_obat_pulang = 0;
        foreach($data['obat_pulang'] as $keys=>$rows) {
            $total_satu_obat_pulang = $rows->harga_satuan * ($rows->jumlah_pengeluaran - $rows->sisa);

            $data['obat_pulang'][$keys]->total_satu_obat_pulang = $total_satu_obat_pulang;
            $total_semua_obat_pulang += $total_satu_obat_pulang;

            $obat_pulang_desc = $rows->nama_obat . ' @[' . format_number($rows->harga_satuan, TRUE) . ']' .
            ', ' . ($rows->jumlah_pengeluaran - $rows->sisa) . ' ' . $rows->satuan;
            $data['obat_pulang'][$keys]->obat_pulang_desc = $obat_pulang_desc;
            $clean = str_replace('[', ' ', $rows->obat_pulang_desc);
            $clean = str_replace(']', ' ', $clean);
            $data['obat_pulang'][$keys]->obat_pulang_desc_clean = $clean;

            $data['obat_pulang'][$keys]->satuan = $rows->jumlah_pengeluaran - $rows->sisa;
        }

        $data['obat_pulang_total'] = $total_semua_obat_pulang;

        // END HITUNG OBAT PULANG

        // HITUNG ALKES
        // $data['alkes'] = $this->pemakaian_alkes->get_pemakaian_obat($id, 'alkes');
        // $total_semua_alkes = 0;
        // $total_satu_alkes = 0;
        // foreach($data['alkes'] as $keys=>$rows) {
        //     $total_satu_alkes = $rows->harga_satuan * $rows->jumlah;

        //     $data['alkes'][$keys]->total_satu_alkes = $total_satu_alkes;
        //     $total_semua_alkes += $total_satu_alkes;
        // }

        // $data['alkes_total'] = $total_semua_alkes;
        $data['alkes'] = $this->pengeluaran_obat->get_pemakaian_obat($id, 'alkes');
        $total_semua_alkes = 0;
        $total_satu_alkes = 0;
        foreach($data['alkes'] as $keys=>$rows) {
            $total_satu_alkes = $rows->harga_satuan * ($rows->jumlah_pengeluaran - $rows->sisa);

            $data['alkes'][$keys]->total_satu_alkes = $total_satu_alkes;
            $total_semua_alkes += $total_satu_alkes;

            $alkes_desc = $rows->nama_obat . ' @' . format_number($rows->harga_satuan, TRUE) .
            ', ' . ($rows->jumlah_pengeluaran - $rows->sisa) . ' ' . $rows->satuan;
            $data['alkes'][$keys]->alkes_desc = $alkes_desc;
        }

        $data['alkes_total'] = $total_semua_alkes;
        // dump($alkes);

        // END HITUNG ALKES

        // HITUNG GIZI
        // HITUNG KAMAR
        $data['gizi'] = $this->gizi->get_all_gizi($id);

        $total_satu_gizi = 0;
        $total_semua_gizi = 0;
        foreach($data['gizi'] as $keys=>$rows) {
            if(!empty($rows->tgl_akhir)) {
                $tgl_mulai = new DateTime($rows->tgl_mulai);
                $tgl_akhir = new DateTime($rows->tgl_akhir);
                $diff_date = $tgl_mulai->diff($tgl_akhir)->days;

                if($diff_date == 0) {
                    $diff_date = 1;
                }else {
                    $diff_date += 1;
                }
            }else {
                $diff_date = 1;
            }

            $total_satu_gizi = $diff_date * $rows->tarif;

            $data['gizi'][$keys]->total_satu_gizi = $total_satu_gizi;
            $data['gizi'][$keys]->diff_date = $diff_date;

            $total_semua_gizi += $total_satu_gizi;
        }
        // dump($data['kamar']);
        // dump($total_semua_kamar);
        $data['gizi_total'] = $total_semua_gizi;

        // END GIZI

        // HITUNG VISIT DOKTER
        if($for_nota) {
            $data['visit_dokter'] = $this->visit_dokter->get_all_visit_grouped($id);

            $total_visit_dokter = 0;
            foreach($data['visit_dokter'] as $rows) {
                $total_visit_dokter += $rows->total_tarif;
            }
        }else {
            $data['visit_dokter'] = $this->visit_dokter->get_all_visit($id);

            $total_visit_dokter = 0;
            foreach($data['visit_dokter'] as $rows) {
                $total_visit_dokter += $rows->tarif;
            }
        }

        $data['visit_dokter_total'] = $total_visit_dokter;

        // END VISIT DOKTER

        // HITUNG LABORATORIUM
        if($for_nota) {
            $data['laboratorium'] = $this->laboratorium->get_all_laboratorium_grouped($id);
            $total_laboratorium = 0;
            foreach($data['laboratorium'] as $rows) {
                $total_laboratorium += $rows->total_tarif;
            }
        }else {
            $data['laboratorium'] = $this->laboratorium->get_all_laboratorium($id);
            $total_laboratorium = 0;
            foreach($data['laboratorium'] as $rows) {
                $total_laboratorium += $rows->tarif;
            }
        }

        $data['laboratorium_total'] = $total_laboratorium;

        // END HITUNG LABORATORIUM

        // HITUNG fisioterapi
        if($for_nota) {
            $data['fisioterapi'] = $this->fisioterapi->get_all_fisioterapi_grouped($id);
            $total_fisioterapi = 0;
            foreach($data['fisioterapi'] as $rows) {
                $total_fisioterapi += $rows->total_tarif;
            }
        }else {
            $data['fisioterapi'] = $this->fisioterapi->get_all_fisioterapi($id);
            $total_fisioterapi = 0;
            foreach($data['fisioterapi'] as $rows) {
                $total_fisioterapi += $rows->tarif;
            }
        }

        $data['fisioterapi_total'] = $total_fisioterapi;

        // END HITUNG fisioterapi

        // HITUNG rontgen
        if($for_nota) {
            $data['rontgen'] = $this->rontgen->get_all_rontgen_grouped($id);
            $total_rontgen = 0;
            foreach($data['rontgen'] as $rows) {
                $total_rontgen += $rows->total_tarif;
            }
        }else {
            $data['rontgen'] = $this->rontgen->get_all_rontgen($id);
            $total_rontgen = 0;
            foreach($data['rontgen'] as $rows) {
                $total_rontgen += $rows->tarif;
            }
        }

        $data['rontgen_total'] = $total_rontgen;

        // END HITUNG fisioterapi

        // HITUNG TINDAKAN
        if($for_nota) {
            $data['tindakan'] = $this->tindakan->get_all_tindakan_grouped($id);
            $total_tindakan = 0;
            foreach($data['tindakan'] as $rows) {
                $total_tindakan += $rows->total_tarif;
            }
        }else {
            $data['tindakan'] = $this->tindakan->get_all_tindakan($id);
            $total_tindakan = 0;
            foreach($data['tindakan'] as $rows) {
                $total_tindakan += $rows->tarif;
            }
        }

        $data['tindakan_total'] = $total_tindakan;
        // END HITUNG TINDAKAN

        $data['administrasi'] = BIAYA_ADMINISTRASI;

        $data['jumlah_nota'] = $total_semua_kamar + $total_semua_obat + $total_semua_obat_pulang + $total_semua_alkes
            + $total_laboratorium + $total_visit_dokter + $total_tindakan
            + $total_semua_gizi + $total_fisioterapi + $total_rontgen + $data['administrasi'];

        $data['form']['sudah_terima_dari'] = '';
        $data['form']['alamat_dari'] = '';
        $data['form']['untuk'] = $pasien->nama_pasien;
        $data['form']['alamat_untuk'] = '';
        $data['form']['atas_permintaan_sendiri'] = '';

        $data['id'] = $id;

        return $data;
    }

    function populate_input($input) {
        $data['sudah_terima_dari'] = $input['sudah_terima_dari'];
        $data['alamat_dari'] = $input['alamat_dari'];
        $data['untuk'] = $input['untuk'];
        // $data['alamat_untuk'] = $input['alamat_untuk'];
        $data['atas_permintaan_sendiri'] = (isset($input['atas_permintaan_sendiri']) ? $input['atas_permintaan_sendiri'] : '');

        if(!empty($input['kamar'])) {
            foreach($input['nama_kamar'] as $key => $val) {
                $input['nama_kamar'][$key] = str_replace("[", "", str_replace("]", "", $val));
            }

            $data['kamar'] = $input['kamar'];
            $data['nama_kamar'] = $input['nama_kamar'];
            $data['total_kamar'] = $input['total_kamar'];
        }

        if(!empty($input['obat'])) {
            foreach($input['obat_desc'] as $key => $val) {
                $input['obat_desc'][$key] = str_replace("[", "", str_replace("]", "", $val));
            }

            $data['obat'] = $input['obat'];
            $data['obat_desc'] = $input['obat_desc'];
            $data['total_obat'] = $input['total_obat'];
        }

        if(!empty($input['obat_pulang'])) {
            foreach($input['obat_pulang_desc'] as $key => $val) {
                $input['obat_pulang_desc'][$key] = str_replace("[", "", str_replace("]", "", $val));
            }

            $data['obat_pulang'] = $input['obat_pulang'];
            $data['obat_pulang_desc'] = $input['obat_pulang_desc'];
            $data['total_obat_pulang'] = $input['total_obat_pulang'];
        }

        if(!empty($input['alkes'])) {
            $data['alkes'] = $input['alkes'];
            $data['nama_alkes'] = $input['nama_alkes'];
            $data['total_alkes'] = $input['total_alkes'];
        }

        if(!empty($input['gizi'])) {
            $data['gizi'] = $input['gizi'];
            $data['nama_gizi'] = $input['nama_gizi'];
            $data['total_gizi'] = $input['total_gizi'];
        }

        if(!empty($input['visit_dokter'])) {
            $data['visit_dokter'] = $input['visit_dokter'];
            $data['nama_visit_dokter'] = $input['nama_visit_dokter'];
            $data['total_visit_dokter'] = $input['total_visit_dokter'];
        }

        if(!empty($input['laboratorium'])) {
            $data['laboratorium'] = $input['laboratorium'];
            $data['nama_laboratorium'] = $input['nama_laboratorium'];
            $data['total_laboratorium'] = $input['total_laboratorium'];
        }

        if(!empty($input['fisioterapi'])) {
            $data['fisioterapi'] = $input['fisioterapi'];
            $data['nama_fisioterapi'] = $input['nama_fisioterapi'];
            $data['total_fisioterapi'] = $input['total_fisioterapi'];
        }

        if(!empty($input['rontgen'])) {
            $data['rontgen'] = $input['rontgen'];
            $data['nama_rontgen'] = $input['nama_rontgen'];
            $data['total_rontgen'] = $input['total_rontgen'];
        }

        if(!empty($input['tindakan'])) {
            $data['tindakan'] = $input['tindakan'];
            $data['nama_tindakan'] = $input['nama_tindakan'];
            $data['total_tindakan'] = $input['total_tindakan'];
        }

        if(!empty($input['bpjs_total'])) {
            $data['bpjs_hari'] = $input['bpjs_hari'];
            $data['bpjs_tarif'] = $input['bpjs_tarif'];
            $data['bpjs_total'] = $input['bpjs_total'];
        }

        $data['administrasi'] = $input['administrasi'];
        $data['total_keseluruhan'] = $input['total_keseluruhan'];
        $data['pembuat_rekening'] = $this->session->userdata('user')->username;

        return $data;
    }

    // $id = id rawat inap
    // no: nota-no pasien-nama pasien-tanggal
    function get_nomor_nota($id) {
        $this->load->model('rawat_inap_model', 'rawat_inap');

        $rawat_inap = $this->rawat_inap->get_with_pasien($id);
        // dump($rawat_inap);

        $nomor_nota = str_pad($rawat_inap->id_rawat_inap, 6, "0", STR_PAD_LEFT);

        return $nomor_nota;
    }

    function is_tanggal_akhir_null($id) {
        $this->load->model('pemakaian_kamar_model', 'pemakaian_kamar');
        $this->load->model('gizi_model', 'gizi');

        if($this->pemakaian_kamar->is_tanggal_akhir_null($id)) {
            return true;
        }

        if($this->gizi->is_tanggal_akhir_null($id)) {
            return true;
        }

        return false;
    }
}