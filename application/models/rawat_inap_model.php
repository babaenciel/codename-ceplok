<?php

class Rawat_inap_model extends MY_Model {
	public $_table = 'rawat_inap';
	protected $primary_key = 'id_rawat_inap';
	public $before_create = array('timestamps_create');
	public $before_update = array('timestamps_update');

	function __construct() {
		parent::__construct();
	}

	protected function timestamps_create($rawat_inap) {
        $rawat_inap['created_at'] = $rawat_inap['updated_at'] = date('Y-m-d');
        return $rawat_inap;
    }

    protected function timestamps_update($rawat_inap) {
    	$rawat_inap['updated_at'] = date('Y-m-d');
    	return $rawat_inap;
    }

    function init_form_field($existing_data=NULL) {
        if(!empty($existing_data)) {
            $data['pasien'] = $existing_data->id_pasien;
            $data['tanggal_masuk'] = conv_date_format($existing_data->tgl_masuk, 'd-m-Y');
            $data['tanggal_keluar'] = (!empty($existing_data->tgl_keluar) ? conv_date_format($existing_data->tgl_keluar, 'd-m-Y') : '');
            $data['dokter'] = $existing_data->id_dokter;
            $data['catatan_medik'] = $existing_data->catatan_medik;
            $data['kode_diagnosa'] = $existing_data->kode_diagnosa;
            $data['keluhan'] = $existing_data->keluhan;
            $data['kesadaran'] = $existing_data->kesadaran;
        }else {
            $data['pasien'] = '';
            $data['tanggal_masuk'] = date('d-m-Y');
            $data['tanggal_keluar'] = '';
            $data['dokter'] = '';
            $data['catatan_medik'] = '';
            $data['kode_diagnosa'] = '';
            $data['keluhan'] = '';
            $data['kesadaran'] = '';
        }

        return $data;
    }

    function populate_form_field($input, $type="add") {
        $data = array(
            'id_pasien' => $input['pasien'],
            'tgl_masuk' => ( !empty($input['tanggal_masuk']) ? conv_date_format($input['tanggal_masuk'], 'Y-m-d') : NULL),
            'tgl_keluar' => ( !empty($input['tanggal_keluar']) ? conv_date_format($input['tanggal_keluar'], 'Y-m-d') : NULL),
            'catatan_medik' => $input['catatan_medik'],
            'id_dokter' => $input['dokter'],
            'kode_diagnosa' => $input['kode_diagnosa'],
            'keluhan' => $input['keluhan'],
            'kesadaran' => $input['kesadaran']
        );

        if($type == "add") {
            $data['status'] = 1;
        }

        return $data;
    }

    // $id = id rawat inap
    function get_with_pasien($id) {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('pasien', $this->_table.'.id_pasien = pasien.id_pasien')
            ->where($this->_table.'.id_rawat_inap', $id)
            ->get()
            ->row();

        return $query;
    }

    // $id = id pasien
    function get_all_with_dokter($id) {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('dokter', $this->_table.'.id_dokter = dokter.id_dokter')
            ->where($this->_table.'.id_pasien', $id)
            ->order_by('tgl_masuk', 'DESC')
            ->get()
            ->result();

        return $query;
    }

    // $id = id pasien
    function get_pasien_rawat_inap_by_pasien($id) {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('pasien', $this->_table.'.id_pasien = pasien.id_pasien')
            ->where($this->_table.'.id_pasien', $id)
            ->where($this->_table.'.status', 1)
            ->get()
            ->row();

        return $query;
    }

    // $id = id pasien
    function get_pasien_rawat_inap_by_pasien_only($id) {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->where('id_pasien', $id)
            ->get()
            ->row();

        return $query;
    }

    // $id = id rawat inap
    function get_with_pemakaian_kamar($id) {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('pemakaian_kamar', $this->_table.'.id_rawat_inap = pemakaian_kamar.id_rawat_inap')
            ->where($this->_table.'.id_rawat_inap', $id)
            ->get()
            ->row();

        return $query;
    }

    function get_total_pendapatan_by_month_year($month, $year) {
        $query = $this->db->select('sum(total_biaya) as total_pendapatan')
            ->from($this->_table)
            ->where('MONTH(tgl_keluar)', $month)
            ->where('YEAR(tgl_keluar)', $year)
            ->where('status', RAWAT_INAP_TIDAK)
            ->get()
            ->row();

        return $query;
    }

    function get_kesadaran_dropdown() {
        // Compos mentis, Somnolence, Sopor, Coma
        return array(
            'compos mentis' => 'Compos Mentis',
            'somnolence' => 'Somnolence',
            'sopor' => 'Sopor',
            'coma' => 'Coma',
            'apatis' => 'Apatis'
        );
    }
}