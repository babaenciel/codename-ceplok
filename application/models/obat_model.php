<?php

class Obat_model extends MY_Model {
	public $_table = 'katalog_obat';
	protected $primary_key = 'id_katalog_obat';
	public $before_create = array( 'timestamps' );

	function __construct() {
		parent::__construct();
	}

    protected function timestamps($obat) {
        $obat['created_at'] = $obat['updated_at'] = date('Y-m-d');
        return $obat;
    }

    public function get_all_for_log() {
        $query = $this->db->select('id_katalog_obat,jumlah_pusat,jumlah_rawat_inap,jumlah_rawat_jalan')
            ->from($this->_table)
            ->get()
            ->result();

        return $query;
    }

    public function get_all_with_penerimaan() {
    	$query = $this->db->select('*')
    		->from($this->_table)
    		->join('penerimaan_obat_detail pod', $this->_table.'.id_katalog_obat = pod.id_katalog_obat')
    		->join('penerimaan_obat po', 'pod.id_penerimaan_obat = po.id_penerimaan_obat')
    		->get()
    		->result();

    	return $query;
    }

    public function get_all_with_kategori() {
    	$query = $this->db->select('*')
    		->from($this->_table)
    		->join('kategori_obat', $this->_table.'.id_kategori_obat = kategori_obat.id_kategori_obat')
    		->get()
    		->result();

    	return $query;
    }

    public function get_with_kategori($id) {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('kategori_obat', $this->_table.'.id_kategori_obat = kategori_obat.id_kategori_obat')
            ->where('id_katalog_obat', $id)
            ->get()
            ->row();

        return $query;
    }

    public function get_by_id($id) {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->where('id_katalog_obat', $id)
            ->get()
            ->row();

        return $query;
    }

    // $id = id katalog obat
    public function add_jumlah_pusat($id, $quantity) {
        $this->db->set('jumlah_pusat', 'jumlah_pusat + ' . $quantity, FALSE);
        $this->db->where('id_katalog_obat', $id);
        $this->db->update($this->_table);
    }

    // $quantity_lama = jumlah penerimaan obat detail lama
    // $quantity_baru = jumlah penerimaan obat detail baru
    public function edit_jumlah_pusat($id_katalog_obat, $quantity_lama, $quantity_baru) {
        // ambil jumlah pusat
        $katalog_obat = $this->get($id_katalog_obat);

        // kurangi jumlah pusat dengan jumlah penerimaan penerimaan obat detail lama
        $result = $katalog_obat->jumlah_pusat - $quantity_lama;

        // tambahkan jumlah pusat dengan jumlah satuan baru
        $result += $quantity_baru;

        // update jumlah pusat
        $this->obat->update($katalog_obat->id_katalog_obat, array(
            'jumlah_pusat' => $result
        ));
    }

    public function add_jumlah_mutasi($id_katalog_obat, $gudang, $quantity) {
        // ambil data obat
        $katalog_obat = $this->get($id_katalog_obat);

        // kurangi jumlah pusat
        $jumlah_pusat = $katalog_obat->jumlah_pusat - $quantity;
        $this->update($id_katalog_obat, array(
            'jumlah_pusat' => $jumlah_pusat
        ));

        //tambah jumlah gudang
        if($gudang == 'rawat_inap') {
            $jumlah_gudang = $katalog_obat->jumlah_rawat_inap + $quantity;
            $this->update($id_katalog_obat, array(
                'jumlah_rawat_inap' => $jumlah_gudang
            ));
        }else if($gudang == 'rawat_jalan') {
            $jumlah_gudang = $katalog_obat->jumlah_rawat_jalan + $quantity;
            $this->update($id_katalog_obat, array(
                'jumlah_rawat_jalan' => $jumlah_gudang
            ));
        }
    }

    public function minus_jumlah_mutasi($id_katalog_obat, $gudang, $quantity) {
        // ambil data obat
        $katalog_obat = $this->get($id_katalog_obat);

        // kurangi jumlah gudang
        if($gudang == 'rawat_inap') {
            $jumlah_gudang = $katalog_obat->jumlah_rawat_inap - $quantity;
            $this->update($id_katalog_obat, array(
                'jumlah_rawat_inap' => $jumlah_gudang
            ));
        }else if($gudang == 'rawat_jalan') {
            $jumlah_gudang = $katalog_obat->jumlah_rawat_jalan - $quantity;
            $this->update($id_katalog_obat, array(
                'jumlah_rawat_jalan' => $jumlah_gudang
            ));
        }
    }

    public function add_jumlah_mutasi_for_pengeluaran($id_katalog_obat, $jenis, $is_gudang_lain, $quantity) {
        // ambil data obat
        $katalog_obat = $this->get($id_katalog_obat);

        //tambah jumlah gudang
        if($jenis == 'rawat_inap') {
            if($is_gudang_lain == 0) {
                $jumlah_gudang = $katalog_obat->jumlah_rawat_inap + $quantity;
                $this->update($id_katalog_obat, array(
                    'jumlah_rawat_inap' => $jumlah_gudang
                ));
            }else {
                $jumlah_gudang = $katalog_obat->jumlah_rawat_jalan + $quantity;
                $this->update($id_katalog_obat, array(
                    'jumlah_rawat_jalan' => $jumlah_gudang
                ));
            }
        }else if($jenis == 'rawat_jalan') {
            if($is_gudang_lain == 0) {
                $jumlah_gudang = $katalog_obat->jumlah_rawat_jalan + $quantity;
                $this->update($id_katalog_obat, array(
                    'jumlah_rawat_jalan' => $jumlah_gudang
                ));
            }else {
                $jumlah_gudang = $katalog_obat->jumlah_rawat_inap + $quantity;
                $this->update($id_katalog_obat, array(
                    'jumlah_rawat_inap' => $jumlah_gudang
                ));
            }
        }
    }

    public function minus_jumlah_mutasi_for_pengeluaran($id_katalog_obat, $jenis, $is_gudang_lain, $quantity) {
        // ambil data obat
        $katalog_obat = $this->get($id_katalog_obat);

        if($jenis == 'rawat_inap') {
            if($is_gudang_lain == 0) {
                $jumlah = $katalog_obat->jumlah_rawat_inap - $quantity;
                $this->update($id_katalog_obat, array(
                    'jumlah_rawat_inap' => $jumlah
                ));
            }else {
                $jumlah = $katalog_obat->jumlah_rawat_jalan - $quantity;
                $this->update($id_katalog_obat, array(
                    'jumlah_rawat_jalan' => $jumlah
                ));
            }
        }else if($jenis == 'rawat_jalan'){
            if($is_gudang_lain == 0) {
                $jumlah = $katalog_obat->jumlah_rawat_jalan - $quantity;
                $this->update($id_katalog_obat, array(
                    'jumlah_rawat_jalan' => $jumlah
                ));
            }else {
                $jumlah = $katalog_obat->jumlah_rawat_inap - $quantity;
                $this->update($id_katalog_obat, array(
                    'jumlah_rawat_inap' => $jumlah
                ));
            }
        }
    }

    public function get_all_reorder_point_limit() {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('kategori_obat', $this->_table.'.id_kategori_obat = kategori_obat.id_kategori_obat')
            ->where('jumlah_pusat <= reorder_point')
            ->get()
            ->result();

        return $query;
    }

    // $obats = array, kumpulan ID katalog obat
    public function get_obat_for_permintaan($obats) {
        $this->db->select($this->_table.'.id_katalog_obat, nama_obat, nama_kategori,
            economic_order_quantity, GROUP_CONCAT(supplier.nama_supplier) AS nama_supplier,
            GROUP_CONCAT(supplier.nama_medrep) as nama_medrep,
            GROUP_CONCAT(supplier.telepon_medrep) as telepon_medrep,
            GROUP_CONCAT(penerimaan_obat_detail.id_supplier) as id_supplier,
            GROUP_CONCAT(penerimaan_obat_detail.quantity_per_dus) as quantity_per_dus,
            GROUP_CONCAT(penerimaan_obat_detail.harga_per_dus) as harga_beli_per_dus, lead_time');
        $this->db->where_in($this->_table.'.id_katalog_obat', $obats);
        $this->db->join('kategori_obat', $this->_table.'.id_kategori_obat = kategori_obat.id_kategori_obat');
        $this->db->join('penerimaan_obat_detail', $this->_table.'.id_katalog_obat = penerimaan_obat_detail.id_katalog_obat', 'LEFT');
        $this->db->join('supplier', 'penerimaan_obat_detail.id_supplier = supplier.id_supplier', 'LEFT');
        $this->db->group_by($this->_table.'.id_katalog_obat');
        return $this->db->get($this->_table)->result();
        // dump($this->db->last_query());
    }

    function get_count_obat_reorder_point() {
        $query = $this->db->select('id_katalog_obat')
            ->from($this->_table)
            ->where('jumlah_pusat < reorder_point')
            ->get()
            ->num_rows();

        return $query;
    }

    function get_dropdown() {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('kategori_obat', $this->_table.'.id_kategori_obat = kategori_obat.id_kategori_obat')
            ->get()
            ->result();

        $dropdown = array(''=>'');
        foreach($query as $rows) {
            $dropdown[$rows->id_katalog_obat] = $rows->nama_obat . ' - ' . $rows->nama_generik . ' - ' . $rows->satuan;
        }

        return $dropdown;
    }
}