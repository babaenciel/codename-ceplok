<?php

class Tarif_gizi_model extends MY_Model {
    public $_table = 'tarif_gizi';
    protected $primary_key = 'id_tarif_gizi';
    public $before_create = array('timestamps_create');
    public $before_update = array('timestamps_update');

    function __construct() {
        parent::__construct();
    }

    protected function timestamps_create($gizi) {
        $gizi['created_at'] = $gizi['updated_at'] = date('Y-m-d');
        return $gizi;
    }

    protected function timestamps_update($gizi) {
        $gizi['updated_at'] = date('Y-m-d');
        return $gizi;
    }

    public function get_dropdown() {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->join('kamar', $this->_table.'.id_kamar = kamar.id_kamar', 'left')
            ->get()
            ->result();
        
        $dropdown = array();
        foreach($query as $rows) {            
            $dropdown[$rows->id_tarif_gizi] = $rows->nama_gizi . ' - ' . $rows->jenis_kamar;
        }

        return $dropdown;
    }
}