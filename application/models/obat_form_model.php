<?php

class Obat_form_model extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function init_data($existing_data=FALSE) {
		$data['jenis'] = ($existing_data) ? $existing_data->id_kategori_obat : '';
		$data['jenis_obat'] = ($existing_data) ? $existing_data->jenis : '';
		$data['nama'] = ($existing_data) ? $existing_data->nama_obat : '';
		$data['nama_generik'] = ($existing_data) ? $existing_data->nama_generik : '';
		$data['kelas_terapi'] = ($existing_data) ? $existing_data->kelas_terapi : '';
		// $data['berat_netto'] = ($existing_data) ? $existing_data->berat_netto : '';
		$data['ved'] = ($existing_data) ? $existing_data->ved : '';
		$data['lead_time'] = ($existing_data) ? $existing_data->lead_time : '';
		// $data['quantity_per_dus'] = ($existing_data) ? $existing_data->quantity_per_dus : '';
		$data['jumlah_pusat'] = ($existing_data) ? $existing_data->jumlah_pusat : '';
		$data['jumlah_rawat_inap'] = ($existing_data) ? $existing_data->jumlah_rawat_inap : '';
		$data['jumlah_rawat_jalan'] = ($existing_data) ? $existing_data->jumlah_rawat_jalan : '';
		$data['harga_per_dus'] = ($existing_data) ? $existing_data->harga_per_dus : '';
		$data['harga_satuan'] = ($existing_data) ? $existing_data->harga_satuan : '';
		$data['reorder_point'] = ($existing_data) ? $existing_data->reorder_point : '';
		$data['catatan'] = ($existing_data) ? $existing_data->catatan : '';

		// $data['nomor_faktur'] = ($existing_data) ? $existing_data->nomor_faktur : '';
		// $data['tanggal_penerimaan'] = ($existing_data) ? $existing_data->tanggal_penerimaan : '';
		// $data['supplier'] = ($existing_data) ? $existing_data->supplier : '';
		// $data['jumlah_dus'] = ($existing_data) ? $existing_data->jumlah_dus : '';
		// $data['quantity_per_dus'] = ($existing_data) ? $existing_data->quantity_per_dus : '';
		// $data['harga_per_dus'] = ($existing_data) ? $existing_data->harga_per_dus : '';
		// $data['jumlah_satuan'] = ($existing_data) ? $existing_data->jumlah_satuan : '';
		// $data['harga_satuan'] = ($existing_data) ? $existing_data->harga_satuan : '';

		return $data;
	}
}