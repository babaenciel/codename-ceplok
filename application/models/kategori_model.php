<?php

class Kategori_model extends MY_Model {
	public $_table = 'kategori_obat';
	protected $primary_key = 'id_kategori_obat';

	function __construct() {
		parent::__construct();
	}

    function get_dropdown() {
        $query = $this->db->select('*')
            ->from($this->_table)
            ->get()
            ->result();

        $arr = array();
        foreach($query as $rows) {
            $arr[$rows->id_kategori_obat] = $rows->nama_kategori.' - '.$rows->satuan;
        }

        return $arr;
    }
}