<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('USER_MANAGER', 'MAN');
define('USER_ADMINISTRASI', 'ADM');
define('USER_GUDANG', 'GUD');

define('PASIEN_STATUS_MATI', 0);
define('PASIEN_STATUS_HIDUP', 1);

define('RAWAT_INAP_SEDANG', 1);
define('RAWAT_INAP_TIDAK', 0);

define('BIAYA_ADMINISTRASI', 15000);

define('TYPE_LOG_PENERIMAAN', 1);
define('TYPE_LOG_MUTASI_RI', 2);
define('TYPE_LOG_MUTASI_RJ', 3);
define('TYPE_LOG_RESEP_RI', 4);
define('TYPE_LOG_RESEP_RJ', 5);

/* End of file constants.php */
/* Location: ./application/config/constants.php */