<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "login";
$route['404_override'] = '';

$route['mutasi/rawat_inap/index'] = 'mutasi/index';
$route['mutasi/rawat_inap/add'] = 'mutasi/add';
$route['mutasi/rawat_inap/edit/(:num)'] = 'mutasi/edit/$1';
$route['mutasi/rawat_inap/delete/(:num)'] = 'mutasi/delete/$1';
$route['mutasi_detail/rawat_inap/index/(:num)'] = 'mutasi_detail/index/$1';
$route['mutasi_detail/rawat_inap/delete/(:num)'] = 'mutasi_detail/delete/$1';

$route['mutasi/rawat_jalan/index'] = 'mutasi/index';
$route['mutasi/rawat_jalan/add'] = 'mutasi/add';
$route['mutasi/rawat_jalan/edit/(:num)'] = 'mutasi/edit/$1';
$route['mutasi/rawat_jalan/delete/(:num)'] = 'mutasi/delete/$1';
$route['mutasi_detail/rawat_jalan/index/(:num)'] = 'mutasi_detail/index/$1';
$route['mutasi_detail/rawat_jalan/delete/(:num)'] = 'mutasi_detail/delete/$1';

$route['resep/rawat_inap/index'] = 'resep/index';
$route['resep/rawat_inap/add'] = 'resep/add';
$route['resep/rawat_inap/add/(:num)'] = 'resep/add/$1';
$route['resep/rawat_inap/edit/(:num)'] = 'resep/edit/$1';
$route['resep/rawat_inap/edit/(:num)/(:num)'] = 'resep/edit/$1/$2';
$route['resep/rawat_inap/delete/(:num)'] = 'resep/delete/$1';
$route['resep_detail/rawat_inap/index/(:num)'] = 'resep_detail/index/$1';
$route['resep_detail/rawat_inap/delete/(:num)'] = 'resep_detail/delete/$1';

$route['resep/rawat_jalan/index'] = 'resep/index';
$route['resep/rawat_jalan/add'] = 'resep/add';
$route['resep/rawat_jalan/edit/(:num)'] = 'resep/edit/$1';
$route['resep/rawat_jalan/delete/(:num)'] = 'resep/delete/$1';
$route['resep_detail/rawat_jalan/index/(:num)'] = 'resep_detail/index/$1';
$route['resep_detail/rawat_jalan/delete/(:num)'] = 'resep_detail/delete/$1';

/* End of file routes.php */
/* Location: ./application/config/routes.php */