<?php

$config = array(
	'login' => array(
        array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'required'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required'
        )
    ),
    'obat/add' => array(
        array(
            'field' => 'jenis',
            'label' => 'Jenis',
            'rules' => 'required'
        ),
        array(
            'field' => 'jenis_obat',
            'label' => 'Jenis Obat',
            'rules' => 'required'
        ),
        array(
            'field' => 'nama',
            'label' => 'Nama Dagang',
            'rules' => 'required'
        ),
        array(
            'field' => 'nama_generik',
            'label' => 'Nama Generik',
            'rules' => 'required'
        ),
        array(
            'field' => 'kelas_terapi',
            'label' => 'Kelas Terapi',
            'rules' => ''
        ),
        // array(
        //     'field' => 'berat_netto',
        //     'label' => 'Berat Netto',
        //     'rules' => ''
        // ),
        array(
            'field' => 'ved',
            'label' => 'VED',
            'rules' => 'required'
        ),
        array(
            'field' => 'lead_time',
            'label' => 'Lead Time',
            'rules' => 'required'
        ),
        array(
            'field' => 'quantity_per_dus',
            'label' => 'Quantity Per Dus',
            'rules' => '',
        ),
        array(
            'field' => 'harga_per_dus',
            'label' => 'Harga Jual Per Dus',
            'rules' => ''
        ),
        array(
            'field' => 'harga_satuan',
            'label' => 'Harga Jual Satuan',
            'rules' => ''
        ),
        array(
            'field' => 'reorder_point',
            'label' => 'Reorder Point',
            'rules' => 'required'
        ),
        array(
            'field' => 'catatan',
            'label' => 'Catatan',
            'rules' => ''
        )
    ),
    'penerimaan_obat/add' => array(
        array(
            'field' => 'nomor_faktur',
            'label' => 'Nomor Faktur',
            'rules' => 'required'
        ),
        array(
            'field' => 'tanggal_penerimaan',
            'label' => 'Tanggal Penerimaan',
            'rules' => 'required'
        ),
        array(
            'field' => 'jumlah_dus',
            'label' => 'Jumlah Dus',
            'rules' => 'required',
        ),
        array(
            'field' => 'quantity_per_dus',
            'label' => 'Quantity Per Dus',
            'rules' => '',
        ),
    ),
    'penerimaan_obat_detail/add' => array(
        array(
            'field' => 'jumlah_satuan',
            'label' => 'Jumlah Satuan',
            'rules' => 'required'
        ),
        array(
            'field' => 'harga_per_dus',
            'label' => 'Harga Per Dus',
            'rules' => 'required',
        ),
        array(
            'field' => 'harga_satuan',
            'label' => 'Harga Satuan',
            'rules' => 'required'
        ),
        array(
            'field' => 'supplier',
            'label' => 'Kode Pabrik',
            'rules' => 'required'
        ),
    ),
    'mutasi/add' => array(
        array(
            'field' => 'tanggal_mutasi',
            'label' => 'Tanggal Mutasi',
            'rules' => 'required',
        ),
        array(
            'field' => 'obat[]',
            'label' => 'Obat',
            'rules' => '',
        ),
        array(
            'field' => 'jumlah[]',
            'label' => 'Jumlah',
            'rules' => '',
        ),
        array(
            'field' => 'id_mutasi',
            'label' => 'ID Mutasi',
            'rules' => 'required|numeric'
        )
    ),
    'resep_rawat_inap/add' => array(
        array(
            'field' => 'nomor_resep',
            'label' => 'Nomor Resep',
            'rules' => 'required'
        ),
        array(
            'field' => 'pasien',
            'label' => 'Nama Pasien',
            'rules' => 'required'
        ),
        array(
            'field' => 'nama_dokter',
            'label' => 'Nama Dokter',
            'rules' => 'required'
        ),
        array(
            'field' => 'tanggal_resep',
            'label' => 'Tanggal Resep',
            'rules' => 'required'
        ),
        array(
            'field' => 'obat[]',
            'label' => 'Obat',
            'rules' => '',
        ),
        array(
            'field' => 'jumlah[]',
            'label' => 'Jumlah',
            'rules' => '',
        ),
        array(
            'field' => 'gudang_lain[]',
            'label' => 'Gudang Lain',
            'rules' => '',
        ),
        // array(
        //     'field' => 'gudang_lain_hidden[]',
        //     'label' => 'Gudang Lain Hidden',
        //     'rules' => '',
        // )
        array(
            'field' => 'obat_pulang[]',
            'label' => 'Obat Pulang',
            'rules' => ''
        )
    ),
    'pasien' => array(
        array(
            'field' => 'id_pasien',
            'label' => 'ID Pasien',
            'rules' => 'required'
        ),
        array(
            'field' => 'nama',
            'label' => 'Nama Pasien',
            'rules' => 'required'
        ),
        array(
            'field' => 'nomor_bpjs',
            'label' => 'Nomor BPJS',
            'rules' => ''
        ),
        array(
            'field' => 'alamat',
            'label' => 'Alamat Pasien',
            'rules' => 'required'
        ),
        array(
            'field' => 'umur',
            'label' => 'Umur Pasien',
            'rules' => ''
        ),
        array(
            'field' => 'tinggi_badan',
            'label' => 'Tinggi Badan',
            'rules' => 'numeric'
        ),
        array(
            'field' => 'berat_badan',
            'label' => 'Berat Badan',
            'rules' => 'numeric'
        ),
        array(
            'field' => 'sistole',
            'label' => 'sistole',
            'rules' => 'numeric'
        ),
        array(
            'field' => 'diastole',
            'label' => 'Diastole',
            'rules' => 'numeric'
        ),
        array(
            'field' => 'respiratory_rate',
            'label' => 'Respiratory Rate',
            'rules' => 'numeric'
        ),
        array(
            'field' => 'heart_rate',
            'label' => 'Heart Rate',
            'rules' => 'numeric'
        ),
        array(
            'field' => 'tanggal_lahir',
            'label' => 'Tanggal Lahir',
            'rules' => ''
        ),
        array(
            'field' => 'jenis_kelamin',
            'label' => 'Jenis Kelamin',
            'rules' => 'required'
        ),
        array(
            'field' => 'nama_keluarga',
            'label' => 'Nama Keluarga',
            'rules' => ''
        ),
        array(
            'field' => 'telepon_keluarga',
            'label' => 'No Telepon Keluarga',
            'rules' => ''
        )
    ),
    'permintaan/edit' => array(
        array(
            'field' => 'reorder_point',
            'label' => 'Reorder Point',
            'rules' => 'required',
        ),
        array(
            'field' => 'economic_order_quantity',
            'label' => 'Economic Order Quantity',
            'rules' => 'required'
        )
    ),
    'pasien_rawat_inap/add' => array(
        array(
            'field' => 'pasien',
            'label' => 'Pasien',
            'rules' => 'required'
        ),
        array(
            'field' => 'tanggal_masuk',
            'label' => 'Tanggal Masuk',
            'rules' => 'required'
        ),
        array(
            'field' => 'tanggal_keluar',
            'label' => 'Tanggal Keluar',
            'rules' => ''
        ),
        array(
            'field' => 'dokter',
            'label' => 'Dokter',
            'rules' => 'required'
        ),
        array(
            'field' => 'catatan_medik',
            'label' => 'Diagnosa',
            'rules' => ''
        ),
        array(
            'field' => 'kode_diagnosa',
            'label' => 'Kode Diagnosa',
            'rules' => ''
        ),
        array(
            'field' => 'kesadaran',
            'label' => 'Kesadaran',
            'rules' => 'required'
        ),
        array(
            'field' => 'keluhan',
            'label' => 'Keluhan',
            'rules' => 'required'
        )
    ),
    'visit_dokter/add' => array(
        // array(
        //     'field' => '',
        //     'label' => '',
        //     'rules' => ''
        // ),
        array(
            'field' => 'dokter',
            'label' => 'Nama Dokter',
            'rules' => 'required'
        ),
        array(
            'field' => 'jenis_visit',
            'label' => 'Jenis Visit',
            'rules' => 'required'
        ),
        array(
            'field' => 'tanggal_visit',
            'label' => 'Tanggal Visit',
            'rules' => 'required'
        ),
        array(
            'field' => 'waktu_visit',
            'label' => 'Waktu Visit',
            'rules' => ''
        )
    ),
    'laboratorium/add' => array(
        array(
            'field' => 'layanan',
            'label' => 'Nama Layanan',
            'rules' => 'required'
        ),
        array(
            'field' => 'tanggal_laboratorium',
            'label' => 'Tanggal',
            'rules' => 'required'
        ),
    ),
    'fisioterapi/add' => array(
        array(
            'field' => 'layanan',
            'label' => 'Nama Layanan',
            'rules' => 'required'
        ),
        array(
            'field' => 'tanggal_fisioterapi',
            'label' => 'Tanggal',
            'rules' => 'required'
        ),
    ),
    'rontgen/add' => array(
        array(
            'field' => 'layanan',
            'label' => 'Nama Layanan',
            'rules' => 'required'
        ),
        array(
            'field' => 'tanggal_rontgen',
            'label' => 'Tanggal',
            'rules' => 'required'
        ),
    ),
    'tindakan/add' => array(
        array(
            'field' => 'perawat',
            'label' => 'Perawat',
            'rules' => ''
        ),
        array(
            'field' => 'tanggal_tindakan',
            'label' => 'Tanggal',
            'rules' => 'required'
        ),
        array(
            'field' => 'tindakan_lainnya_checkbox',
            'label' => '',
            'rules' => ''
        )
    ),
    'gizi/add' => array(
        array(
            'field' => 'gizi',
            'label' => 'Gizi',
            'rules' => 'required'
        ),
        array(
            'field' => 'tanggal_mulai',
            'label' => 'Tanggal Mulai',
            'rules' => 'required'
        ),
        array(
            'field' => 'tanggal_akhir',
            'label' => 'Tanggal Akhir',
            'rules' => ''
        )
    ),
    'obat_dan_alkes/add' => array(
        array(
            'field' => 'obat_dan_alkes',
            'label' => 'Obat/Alkes',
            'rules' => 'required'
        ),
        array(
            'field' => 'jumlah',
            'label' => 'Jumlah',
            'rules' => 'required'
        ),
        array(
            'field' => 'tanggal',
            'label' => 'Tanggal',
            'rules' => 'required'
        ),
        array(
            'field' => 'sebagai_obat_pulang',
            'label' => 'Sebagai Obat Pulang',
            'rules' => ''
        )
    ),
    'pemakaian_kamar/add' => array(
        array(
            'field' => 'kamar',
            'label' => 'Kamar',
            'rules' => 'required'
        ),
        array(
            'field' => 'tanggal_mulai',
            'label' => 'Tanggal Mulai',
            'rules' => 'required'
        ),
        array(
            'field' => 'tanggal_akhir',
            'label' => 'Tanggal Akhir',
            'rules' => ''
        )
    ),
    'obat_retur/add' => array(

    ),
    'supplier/add' => array(
        array(
            'field' => 'nama_supplier',
            'label' => 'Kode Pabrik',
            'rules' => 'required'
        ),
        array(
            'field' => 'nama_medrep',
            'label' => 'Nama Medrep',
            'rules' => 'required'
        ),
        array(
            'field' => 'telepon_medrep',
            'label' => 'Telepon Medrep',
            'rules' => 'required'
        ),
    )

);