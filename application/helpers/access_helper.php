<?php

function access_redirect() {
    $CI = &get_instance();
    if($CI->session->userdata('user')->jabatan === USER_ADMINISTRASI) {
        redirect('pasien');
    }else if($CI->session->userdata('user')->jabatan === USER_GUDANG) {
        redirect('obat');
    }else if($CI->session->userdata('user')->jabatan === USER_MANAGER) {
        redirect('grafik');
    }
}

function access_check($jabatan) {
    $CI = &get_instance();
    if(!in_array($CI->session->userdata('user')->jabatan, $jabatan)) {
        show_error('Maaf anda tidak berhak mengakses halaman ini.');
    }
    // if($CI->session->userdata('user')->jabatan !== $jabatan) {
    //     show_error('Maaf anda tidak berhak mengakses halaman ini.');
    // }
}