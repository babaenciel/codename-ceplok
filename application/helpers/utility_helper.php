<?php

function dump($var){
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
    exit;
}

function conv_date_format($oldformat,$format) {
    $time = strtotime($oldformat);
    $newformat = date($format,$time);

    return $newformat;
}

function get_age($birthdate) {
    return date_diff(date_create($birthdate), date_create('today'))->y;
}

if ( ! function_exists('format_number'))
{
    function format_number($number, $price=FALSE)
    {
    	$number = intval($number);

    	if($price) {
    		return 'Rp ' . number_format($number, 0, ',', '.');
    	}else {
    		return number_format($number, 0, ',', '.');
    	}
    }
}

function date_diff_with_today($date) {
    $dStart = new DateTime($date);
    $dEnd  = new DateTime(date('Y-m-d'));
    $dDiff = $dStart->diff($dEnd);
    // echo $dDiff->format('%R'); // use for point out relation: smaller/greater
    // echo $dDiff->days;
    return $dDiff;
}