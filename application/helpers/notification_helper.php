<?php

function get_count_permintaan_obat() {
    $CI = &get_instance();
    $CI->load->model('obat_model', 'obat');
    $jumlah_obat = $CI->obat->get_count_obat_reorder_point();

    return $jumlah_obat;
}