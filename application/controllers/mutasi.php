<?php

class Mutasi extends MY_Controller {
      private $gudang = 'rawat_inap';
      private $label_gudang = 'Rawat Inap';

	function __construct() {
		parent::__construct();
            access_check(array(USER_GUDANG));

            $this->load->model('mutasi_obat_model', 'mutasi_obat');
            $this->load->model('log_katalog_obat_model', 'log_katalog_obat');

            if($this->uri->segment(2) === 'rawat_inap') {
                  $this->gudang = 'rawat_inap';
                  $this->label_gudang = 'Rawat Inap';
            }else if($this->uri->segment(2) === 'rawat_jalan') {
                  $this->gudang = 'rawat_jalan';
                  $this->label_gudang = 'Rawat Jalan';
            }
	}

	function index() {
            $this->title = 'Mutasi Obat '.$this->label_gudang;

		$data['data'] = $this->mutasi_obat->get_many_by('gudang', $this->gudang);
            $data['gudang'] = $this->gudang;

		$this->render('mutasi/index', $data);
	}

	function add() {
            $this->title = 'Tambah Mutasi Obat '.$this->label_gudang;

		$this->load->model('obat_model', 'obat');
		$this->load->model('mutasi_obat_model', 'mutasi_obat');
		$this->load->model('mutasi_obat_detail_model', 'mutasi_obat_detail');

		if($_POST) {
			// print_r($this->input->post('obat'));
			// print_r($this->input->post('jumlah'));

			$this->config->load('form_validation');
                  $rules = $this->config->item('mutasi/add');

                  $this->form_validation->set_rules($rules);

                  if($this->form_validation->run()) {
                        $data_mutasi = $this->mutasi_obat->get($this->input->post('id_mutasi'));

                        if(empty($data_mutasi)) {
                              $obat_jumlah = $this->mutasi_obat->create_obat_jumlah_array($this->input->post('obat'), $this->input->post('jumlah'));

                              // check apakah tanggal mutasinya sudah ada di database
                              // $mutasi = $this->mutasi_obat->get_by(
                              //       'tanggal_mutasi = "' . conv_date_format($this->input->post('tanggal_mutasi'), 'Y-m-d') . '"
                              //       AND gudang = "'.$this->gudang.'"'
                              // );

                              // jika sudah ada, gunakan id mutasi obat yang sudah ada
                              // jika belom ada, insert dan gunakan id mutasi obat dari last insert
                              // if(!empty($mutasi)) {
                              //       $id_mutasi_obat = $mutasi->id_mutasi_obat;
                              // }else {
                              //       $id_mutasi_obat = $this->mutasi_obat->insert(array(
                              //             'id_mutasi' => $this->input->post('id_mutasi'),
                              //             'gudang' => $this->gudang,
                              //             'tanggal_mutasi' => conv_date_format($this->input->post('tanggal_mutasi'), 'Y-m-d')
                              //       ));
                              // }
                              $id_mutasi_obat = $this->input->post('id_mutasi');

                              $this->mutasi_obat->insert(array(
                                    'id_mutasi_obat' => $id_mutasi_obat,
                                    'gudang' => $this->gudang,
                                    'tanggal_mutasi' => conv_date_format($this->input->post('tanggal_mutasi'), 'Y-m-d')
                              ));

                              // kemudian baru masukin data mutasi obat detail
                              foreach($obat_jumlah as $rows) {
                                    $this->mutasi_obat_detail->insert(array(
                                          'jumlah_mutasi' => $rows['jumlah'],
                                          'id_katalog_obat' => $rows['obat'],
                                          'id_mutasi_obat' => $id_mutasi_obat
                                    ));

                                    $this->obat->add_jumlah_mutasi($rows['obat'], $this->gudang, $rows['jumlah']);
                              }

                              // create log katalog obat
                              if($this->gudang == 'rawat_inap') {
                                    $this->log_katalog_obat->create_log(conv_date_format($this->input->post('tanggal_mutasi'), 'Y-m-d'), TYPE_LOG_MUTASI_RI);
                              }else {
                                    $this->log_katalog_obat->create_log(conv_date_format($this->input->post('tanggal_mutasi'), 'Y-m-d'), TYPE_LOG_MUTASI_RJ);
                              }

                              redirect('mutasi/'.$this->gudang.'/index');
                        }else {
                              $obat = $this->input->post('obat');
                              $jumlah = $this->input->post('jumlah');

                              // populate data obat dan jumlah kembali
                              $i = 0;
                              foreach($obat as $rows) {
                                    $form['obat'][$i] = $rows;
                                    $i++;
                              }

                              $i = 0;
                              foreach($jumlah as $rows) {
                                    $form['jumlah'][$i] = $rows;
                                    $i++;
                              }

                              $data['form_repopulate'] = $form;
                              $data['total_field_obat'] = count($obat);
                              $data['lebih_dari_satu'] = TRUE;

                              $data['errors'] = 'ID Mutasi sudah pernah digunakan. Mohon gunakan ID yang lain.';
                        }
                  }else {
                  	$obat = $this->input->post('obat');
                  	$jumlah = $this->input->post('jumlah');

                  	// populate data obat dan jumlah kembali
                  	$i = 0;
                  	foreach($obat as $rows) {
                  		$form['obat'][$i] = $rows;
                  		$i++;
                  	}

                  	$i = 0;
                  	foreach($jumlah as $rows) {
                  		$form['jumlah'][$i] = $rows;
                  		$i++;
                  	}

                        $data['form_repopulate'] = $form;
                        $data['total_field_obat'] = count($obat);
                  	$data['lebih_dari_satu'] = TRUE;

                  	$data['errors'] = validation_errors();
                  }
		}

		$data['obat_dropdown'] = $this->obat->get_dropdown();
		$data['form'] = $this->mutasi_obat->init_form_field();
            $data['gudang'] = $this->gudang;

		$this->render('mutasi/add', $data);
	}

	// $id = id mutasi obat
	function edit($id) {
            $this->title = 'Edit Mutasi Obat '.$this->label_gudang;

            $this->load->model('obat_model', 'obat');
            $this->load->model('mutasi_obat_detail_model', 'mutasi_obat_detail');

            $list_obat = $this->mutasi_obat_detail->get_many_by('id_mutasi_obat', $id);

            $data['obat_dropdown'] = $this->obat->get_dropdown();
            $data['form'] = $this->mutasi_obat->init_form_field($list_obat);

            $data['lebih_dari_satu'] = TRUE;
            $data['total_field_obat'] = count($data['form']['obat[]']);

		if($_POST) {
			$this->config->load('form_validation');
                  $rules = $this->config->item('mutasi/add');

                  $this->form_validation->set_rules($rules);

                  if($this->form_validation->run()) {
                        if($this->input->post('id_mutasi') == $id) {
                              $obat_jumlah = $this->mutasi_obat->create_obat_jumlah_array($this->input->post('obat'), $this->input->post('jumlah'));

                              // update mutasi obat tanggal mutasinya
                              $this->mutasi_obat->update($id, array(
                                    'tanggal_mutasi' => conv_date_format($this->input->post('tanggal_mutasi'), 'Y-m-d')
                              ));

                              // sebelum masukin data mutasi baru,
                              // hapus dulu semua data mutasi detail by id mutasi obat

                              // dimulai dengan mengambil data, untuk mengembalikan
                              // nilai jumlah mutasi yang di katalog obat
                              $list_obat_lama = $this->mutasi_obat_detail->get_many_by('id_mutasi_obat', $id);
                              foreach($list_obat_lama as $rows) {
                                    $this->obat->add_jumlah_pusat($rows->id_katalog_obat, $rows->jumlah_mutasi);
                                    $this->obat->minus_jumlah_mutasi($rows->id_katalog_obat, $this->gudang, $rows->jumlah_mutasi);
                              }

                              $this->mutasi_obat_detail->delete_by('id_mutasi_obat', $id);

                              // kemudian masukin data mutasi obat detail baru
                              foreach($obat_jumlah as $rows) {
                                    $this->mutasi_obat_detail->insert(array(
                                          'jumlah_mutasi' => $rows['jumlah'],
                                          'id_katalog_obat' => $rows['obat'],
                                          'id_mutasi_obat' => $id
                                    ));

                                    $this->obat->add_jumlah_mutasi($rows['obat'], $this->gudang, $rows['jumlah']);
                              }

                              // create log katalog obat
                              if($this->gudang == 'rawat_inap') {
                                    $this->log_katalog_obat->create_log(conv_date_format($this->input->post('tanggal_mutasi'), 'Y-m-d'), TYPE_LOG_MUTASI_RI);
                              }else {
                                    $this->log_katalog_obat->create_log(conv_date_format($this->input->post('tanggal_mutasi'), 'Y-m-d'), TYPE_LOG_MUTASI_RJ);
                              }

                              redirect('mutasi/'.$this->gudang.'/index');
                        }else {
                              $mutasi = $this->mutasi_obat->get($this->input->post('id_mutasi'));

                              if(empty($mutasi)) {
                                    $obat_jumlah = $this->mutasi_obat->create_obat_jumlah_array($this->input->post('obat'), $this->input->post('jumlah'));

                                    // update mutasi obat tanggal mutasinya
                                    $this->mutasi_obat->update($id, array(
                                          'id_mutasi_obat' => $this->input->post('id_mutasi'),
                                          'tanggal_mutasi' => conv_date_format($this->input->post('tanggal_mutasi'), 'Y-m-d')
                                    ));

                                    // sebelum masukin data mutasi baru,
                                    // hapus dulu semua data mutasi detail by id mutasi obat

                                    // dimulai dengan mengambil data, untuk mengembalikan
                                    // nilai jumlah mutasi yang di katalog obat
                                    $list_obat_lama = $this->mutasi_obat_detail->get_many_by('id_mutasi_obat', $this->input->post('id_mutasi'));
                                    foreach($list_obat_lama as $rows) {
                                          $this->obat->add_jumlah_pusat($rows->id_katalog_obat, $rows->jumlah_mutasi);
                                          $this->obat->minus_jumlah_mutasi($rows->id_katalog_obat, $this->gudang, $rows->jumlah_mutasi);
                                    }

                                    $this->mutasi_obat_detail->delete_by('id_mutasi_obat', $this->input->post('id_mutasi'));

                                    // kemudian masukin data mutasi obat detail baru
                                    foreach($obat_jumlah as $rows) {
                                          $this->mutasi_obat_detail->insert(array(
                                                'jumlah_mutasi' => $rows['jumlah'],
                                                'id_katalog_obat' => $rows['obat'],
                                                'id_mutasi_obat' => $this->input->post('id_mutasi')
                                          ));

                                          $this->obat->add_jumlah_mutasi($rows['obat'], $this->gudang, $rows['jumlah']);
                                    }

                                    // create log katalog obat
                                    if($this->gudang == 'rawat_inap') {
                                          $this->log_katalog_obat->create_log(conv_date_format($this->input->post('tanggal_mutasi'), 'Y-m-d'), TYPE_LOG_MUTASI_RI);
                                    }else {
                                          $this->log_katalog_obat->create_log(conv_date_format($this->input->post('tanggal_mutasi'), 'Y-m-d'), TYPE_LOG_MUTASI_RJ);
                                    }

                                    redirect('mutasi/'.$this->gudang.'/index');
                              }else {
                                    $obat = $this->input->post('obat');
                                    $jumlah = $this->input->post('jumlah');
                                    $data['total_field_obat'] = count($obat);

                                    // populate data obat dan jumlah kembali
                                    $i = 0;
                                    foreach($obat as $rows) {
                                          $form['obat'][$i] = $rows;
                                          $i++;
                                    }

                                    $i = 0;
                                    foreach($jumlah as $rows) {
                                          $form['jumlah'][$i] = $rows;
                                          $i++;
                                    }

                                    $data['form_repopulate'] = $form;

                                    $data['errors'] = 'ID Mutasi sudah pernah digunakan. Mohon gunakan ID yang lain.';
                              }
                        }
                  }else {
                        $obat = $this->input->post('obat');
                        $jumlah = $this->input->post('jumlah');
                        $data['total_field_obat'] = count($obat);

                        // populate data obat dan jumlah kembali
                        $i = 0;
                        foreach($obat as $rows) {
                              $form['obat'][$i] = $rows;
                              $i++;
                        }

                        $i = 0;
                        foreach($jumlah as $rows) {
                              $form['jumlah'][$i] = $rows;
                              $i++;
                        }

                        $data['form_repopulate'] = $form;

                  	$data['errors'] = validation_errors();
                  }
		}

		$data['id'] = $id;
            $data['gudang'] = $this->gudang;

		$this->render('mutasi/edit', $data);
	}

	// $id = id mutasi obat
	function delete($id) {
		$this->load->model('mutasi_obat_detail_model', 'mutasi_obat_detail');
		$this->load->model('obat_model', 'obat');

		$result = $this->mutasi_obat_detail->get_many_by('id_mutasi_obat', $id);

		foreach($result as $rows) {
			$this->obat->add_jumlah_pusat($rows->id_katalog_obat, $rows->jumlah_mutasi);
			$this->obat->minus_jumlah_mutasi($rows->id_katalog_obat, $this->gudang, $rows->jumlah_mutasi);
		}

		$this->mutasi_obat_detail->delete_by('id_mutasi_obat', $id);
            $mutasi_obat = $this->mutasi_obat->get($id);
		$this->mutasi_obat->delete($id);

            // create log katalog obat
            if($this->gudang == 'rawat_inap') {
                  $this->log_katalog_obat->create_log(conv_date_format($mutasi_obat->tanggal_mutasi, 'Y-m-d'), TYPE_LOG_MUTASI_RI);
            }else {
                  $this->log_katalog_obat->create_log(conv_date_format($mutasi_obat->tanggal_mutasi, 'Y-m-d'), TYPE_LOG_MUTASI_RJ);
            }

		redirect('mutasi/'.$this->gudang.'/index');
	}

	function get_field_obat() {
		$this->load->model('obat_model', 'obat');

		$data['obat_dropdown'] = $this->obat->get_dropdown();
		$data['form'] = $this->mutasi_obat->init_form_field();
		$data['i'] = 0;

		$data['view'] = $this->load->view('mutasi/_field_obat', $data, TRUE);

		echo json_encode($data);
	}
}