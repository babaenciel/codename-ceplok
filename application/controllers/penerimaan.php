<?php

class Penerimaan extends MY_Controller {
	function __construct() {
		parent::__construct();
		access_check(array(USER_GUDANG, USER_ADMINISTRASI));

        $this->load->model('obat_model', 'obat');
	}

	// $id = id katalog obat
	function index($id) {
		$this->load->model('penerimaan_obat_detail_model', 'penerimaan_obat_detail');

		$data['katalog_obat'] = $this->obat->get($id);
		$data['data'] = $this->penerimaan_obat_detail->get_all_with_penerimaan_obat($id);

		$this->title = 'Daftar History Penerimaan Obat: ' . $data['katalog_obat']->nama_obat;

		// dump($data);
		$this->render('penerimaan/index', $data);
	}

	// $id = id katalog obat
	function add($id) {
        $katalog_obat = $this->obat->get($id);
		$this->title = 'Form Penerimaan Obat: ' . $katalog_obat->nama_obat;

		$this->load->model('supplier_model', 'supplier');
		$this->load->model('penerimaan_form_model', 'penerimaan_form');
		$this->load->model('obat_model', 'obat');
		$this->load->model('penerimaan_obat_model', 'penerimaan_obat');
		$this->load->model('penerimaan_obat_detail_model', 'penerimaan_obat_detail');
        $this->load->model('log_katalog_obat_model', 'log_katalog_obat');

		$data['supplier_dropdown'] = $this->supplier->dropdown('nama_supplier');

		$data['form'] = $this->penerimaan_form->init_data();

		$data_obat = $this->obat->get($id);
		// $data['form']['quantity_per_dus'] = $data_obat->quantity_per_dus;

		if($_POST) {
			$this->config->load('form_validation');
            $rules = $this->config->item('penerimaan_obat/add');

            foreach($this->config->item('penerimaan_obat_detail/add') as $rows) {
            	array_push($rules, $rows);
            }

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
            	$this->db->trans_start();

            	// tambahkan jumlah pusat
            	$this->obat->add_jumlah_pusat($id, $this->input->post('jumlah_satuan'));

                // create log katalog obat
                $this->log_katalog_obat->create_log(conv_date_format($this->input->post('tanggal_penerimaan'), 'Y-m-d'), TYPE_LOG_PENERIMAAN);

            	// jika nomor faktur sudah pernah diinput, maka tidak diinput lagi.
            	// untuk menangani kasus dimana dalam satu faktur ada beberapa obat
            	$penerimaan_obat = $this->penerimaan_obat->get_by('nomor_faktur', $this->input->post('nomor_faktur'));

				if(!$penerimaan_obat) {
					$id_penerimaan_obat = $this->penerimaan_obat->insert(array(
						'tanggal_penerimaan' => conv_date_format($this->input->post('tanggal_penerimaan'), 'Y-m-d'),
						'nomor_faktur' => $this->input->post('nomor_faktur')
					));
				}else {
					$id_penerimaan_obat = $penerimaan_obat->id_penerimaan_obat;
				}

				$this->penerimaan_obat_detail->insert(array(
					'jumlah_penerimaan' => $this->input->post('jumlah_satuan'),
					'jumlah_dus' => $this->input->post('jumlah_dus'),
					'harga_per_dus' => $this->input->post('harga_per_dus'),
					'harga_satuan' => $this->input->post('harga_satuan'),
                    'quantity_per_dus' => $this->input->post('quantity_per_dus'),
                    // 'harga_jual_satuan' => $this->input->post('harga_jual_satuan'),
					'id_supplier' => $this->input->post('supplier'),
					'id_katalog_obat' => $id,
					'id_penerimaan_obat' => $id_penerimaan_obat,
				));

				$this->db->trans_complete();

				$this->session->set_flashdata('message', 'Insert Obat Sukses!');

				redirect('obat');
            }else {
            	$data['errors'] = validation_errors();
            }
		}

		$data['id_katalog_obat'] = $id;

		$this->render('penerimaan/add', $data);
	}

	// $id = id penerimaan obat detail
	function edit($id) {
		$this->load->model('penerimaan_obat_detail_model', 'penerimaan_obat_detail');
		$this->load->model('penerimaan_obat_model', 'penerimaan_obat');
		$this->load->model('penerimaan_form_model', 'penerimaan_form');
		$this->load->model('supplier_model', 'supplier');
		$this->load->model('obat_model', 'obat');
        $this->load->model('log_katalog_obat_model', 'log_katalog_obat');

		$data['supplier_dropdown'] = $this->supplier->dropdown('nama_supplier');

		// $this->penerimaan_form->get($id);

		$data_penerimaan_obat_detail = $this->penerimaan_obat_detail->get_with_katalog_and_penerimaan_obat($id);
        $data['form'] = $this->penerimaan_form->init_data($data_penerimaan_obat_detail);

        $katalog_obat = $this->obat->get($data_penerimaan_obat_detail->id_katalog_obat);
        $this->title = 'Edit Penerimaan Obat: ' . $katalog_obat->nama_obat;

		if($_POST) {
			$this->config->load('form_validation');
            $rules = $this->config->item('penerimaan_obat/add');

            foreach($this->config->item('penerimaan_obat_detail/add') as $rows) {
            	array_push($rules, $rows);
            }

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
            	// dump($this->input->post());
            	$this->db->trans_start();

            	// jika berubah
            	if($this->input->post('jumlah_satuan') != $data_penerimaan_obat_detail->jumlah_penerimaan) {
            		$this->obat->edit_jumlah_pusat(
            			$data_penerimaan_obat_detail->id_katalog_obat,
            			$data_penerimaan_obat_detail->jumlah_penerimaan,
            			$this->input->post('jumlah_satuan')
            		);

                    $this->log_katalog_obat->create_log(conv_date_format($this->input->post('tanggal_penerimaan'), 'Y-m-d'), TYPE_LOG_PENERIMAAN);
            	}

            	// update penerimaan obat
            	$this->penerimaan_obat->update($data_penerimaan_obat_detail->id_penerimaan_obat, array(
            		'nomor_faktur' => $this->input->post('nomor_faktur'),
            		'tanggal_penerimaan' => conv_date_format($this->input->post('tanggal_penerimaan'), 'Y-m-d')
            	));

            	$this->penerimaan_obat_detail->update($id, array(
            		'jumlah_penerimaan' => $this->input->post('jumlah_satuan'),
            		'jumlah_dus' => $this->input->post('jumlah_dus'),
					'harga_per_dus' => $this->input->post('harga_per_dus'),
					'harga_satuan' => $this->input->post('harga_satuan'),
                    'quantity_per_dus' => $this->input->post('quantity_per_dus'),
                    // 'harga_jual_satuan' => $this->input->post('harga_jual_satuan'),
					'id_supplier' => $this->input->post('supplier'),
					'id_katalog_obat' => $data_penerimaan_obat_detail->id_katalog_obat,
					'id_penerimaan_obat' => $data_penerimaan_obat_detail->id_penerimaan_obat,
            	));

				$this->db->trans_complete();

				$this->session->set_flashdata('message', 'Insert Obat Sukses!');

				redirect('penerimaan/index/'.$data_penerimaan_obat_detail->id_katalog_obat);
            }else {
            	$data['errors'] = validation_errors();
            }
		}

		$data['id_penerimaan_obat_detail'] = $id;

		$this->render('penerimaan/edit', $data);
	}

	//$id = id penerimaan obat detail
	function delete($id) {
		$this->load->model('penerimaan_obat_detail_model', 'penerimaan_obat_detail');
		$this->load->model('penerimaan_obat_model', 'penerimaan_obat');
		$this->load->model('obat_model', 'obat');
        $this->load->model('log_katalog_obat_model', 'log_katalog_obat');

		$data_penerimaan_obat_detail = $this->penerimaan_obat_detail->get($id);
        $data_penerimaan_obat = $this->penerimaan_obat->get($data_penerimaan_obat_detail->id_penerimaan_obat);

		$this->penerimaan_obat_detail->delete($id);

		// cek apakah ada data penerimaan obat detail
		// yang memiliki id_penerimaan_obat sama dengan data yang didelete.
		// jika tidak ada maka lakukan delete pada data penerimaan obat
		$result = $this->penerimaan_obat_detail->get_by('id_penerimaan_obat', $data_penerimaan_obat_detail->id_penerimaan_obat);
		if(!$result) {
			$this->penerimaan_obat->delete($data_penerimaan_obat_detail->id_penerimaan_obat);
		}

		$this->obat->edit_jumlah_pusat(
			$data_penerimaan_obat_detail->id_katalog_obat,
			$data_penerimaan_obat_detail->jumlah_penerimaan,
			0
		);

        $this->log_katalog_obat->create_log($data_penerimaan_obat->tanggal_penerimaan, TYPE_LOG_PENERIMAAN);

        redirect('penerimaan/index/'.$data_penerimaan_obat_detail->id_katalog_obat);
	}
}