<?php

class Fisioterapi extends MY_Controller {
	function __construct() {
        parent::__construct();
        access_check(array(USER_ADMINISTRASI));

        $this->load->model('fisioterapi_model', 'fisioterapi');
        $this->load->model('rawat_inap_model', 'rawat_inap');

        $this->title = 'Fisioterapi';
    }

    // $id = id rawat inap
    function index($id) {
        $rawat_inap = $this->rawat_inap->get_with_pasien($id);        
        $this->title .= ' - ' . $rawat_inap->nama_pasien . ' - ' . conv_date_format($rawat_inap->tgl_masuk, 'd-m-Y');       
        
    	$data['data'] = $this->fisioterapi->get_all_fisioterapi($id);
    	$data['id'] = $id;

    	$this->render('fisioterapi/index', $data);
    }

    // $id = id rawat inap
    function add($id) {
        $this->load->model('tarif_fisioterapi_model', 'tarif_fisioterapi');

        $rawat_inap = $this->rawat_inap->get_with_pasien($id);        
        $this->title .= ' - ' . $rawat_inap->nama_pasien . ' - ' . conv_date_format($rawat_inap->tgl_masuk, 'd-m-Y');               

        if($_POST) {
            $this->config->load('form_validation');
            $rules = $this->config->item('fisioterapi/add');

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                $this->fisioterapi->insert(array(
                    'id_tarif_fisioterapi' => $this->input->post('layanan'),
                    'tgl_fisioterapi' => conv_date_format($this->input->post('tanggal_fisioterapi'), 'Y-m-d'),                    
                    'id_rawat_inap' => $id
                ));

                redirect('fisioterapi/index/'.$id);
            }else {
                $data['errors'] = validation_errors();
            }
        }

        $data['layanan_dropdown'] = $this->tarif_fisioterapi->get_dropdown();        
        $data['form'] = $this->fisioterapi->init_data();
        $data['id'] = $id;

        $this->render('fisioterapi/add', $data);
    }

    // $id = id fisioterapi
    function edit($id) {
        $this->load->model('tarif_fisioterapi_model', 'tarif_fisioterapi');

        $data['layanan_dropdown'] = $this->tarif_fisioterapi->get_dropdown();
        $data['id'] = $id;

        $data_fisioterapi = $this->fisioterapi->get($id);
        $data['form'] = $this->fisioterapi->init_data($data_fisioterapi);

        $rawat_inap = $this->rawat_inap->get_with_pasien($data_fisioterapi->id_rawat_inap);        
        $this->title .= ' - ' . $rawat_inap->nama_pasien . ' - ' . conv_date_format($rawat_inap->tgl_masuk, 'd-m-Y');               

        if($_POST) {
            $this->config->load('form_validation');
            $rules = $this->config->item('fisioterapi/add');

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                $this->fisioterapi->update($id, array(
                    'id_tarif_fisioterapi' => $this->input->post('layanan'),
                    'tgl_fisioterapi' => conv_date_format($this->input->post('tanggal_fisioterapi'), 'Y-m-d'),                    
                    'id_rawat_inap' => $data_fisioterapi->id_rawat_inap
                ));

                redirect('fisioterapi/index/'.$data_fisioterapi->id_rawat_inap);
            }else {
                $data['errors'] = validation_errors();
            }
        }

        $this->render('fisioterapi/edit', $data);
    }

    // $id = id fisioterapi
    function delete($id) {
        $data_fisioterapi = $this->fisioterapi->get($id);
        $this->fisioterapi->delete($id);

        redirect('fisioterapi/index/'.$data_fisioterapi->id_rawat_inap);
    }
}