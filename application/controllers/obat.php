<?php

class Obat extends MY_Controller {
	function __construct() {
		parent::__construct();
		access_check(array(USER_GUDANG, USER_ADMINISTRASI));
	}

	function index() {
		$this->title = 'Daftar Obat';

		$this->load->model('obat_model', 'obat');
		$data['data'] = $this->obat->get_all_with_kategori();

		$this->render('obat/index', $data);
	}

	function add() {
		$this->title = 'Tambah Obat';

		$this->load->model('kategori_model', 'kategori');
		$this->load->model('supplier_model', 'supplier');
		$this->load->model('obat_model', 'obat');
		$this->load->model('obat_form_model', 'obat_form');

		$data['jenis_dropdown'] = $this->kategori->get_dropdown();
		$data['supplier_dropdown'] = $this->supplier->dropdown('nama_supplier');
		$data['jenis_obat_dropdown'] = array('obat'=>'Obat', 'alkes'=>'Alat Kesehatan');

		$data['form'] = $this->obat_form->init_data();

		if($_POST) {
			$this->config->load('form_validation');
            $rules = $this->config->item('obat/add');

            if($this->input->post('jenis_obat') == 'alkes') {
            	// unset rule berat netto untuk tipe alkes
            	unset($rules[3]);
            }

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
            	$id_obat = $this->obat->insert(array(
				    'id_kategori_obat' => $this->input->post('jenis'),
				    'jenis' => $this->input->post('jenis_obat'),
				    'nama_obat' => $this->input->post('nama'),
				    'nama_generik' => $this->input->post('nama_generik'),
				    'kelas_terapi' => $this->input->post('kelas_terapi'),
				    // 'berat_netto' => $this->input->post('berat_netto'),
				    'ved' => $this->input->post('ved'),
				    'lead_time' => $this->input->post('lead_time'),
				    // 'quantity_per_dus' => $this->input->post('quantity_per_dus'),
				    'jumlah_pusat' => $this->input->post('jumlah_satuan'),
				    'jumlah_rawat_inap' => 0,
				    'jumlah_rawat_jalan' => 0,
				    'harga_per_dus' => $this->input->post('harga_per_dus'),
				    'harga_satuan' => ($this->input->post('harga_satuan') ? $this->input->post('harga_satuan') : 0),
				    'reorder_point' => $this->input->post('reorder_point'),
				    'catatan' => $this->input->post('catatan')
				));

				$this->session->set_flashdata('message', 'Insert Obat Sukses!');

				redirect('obat');
            }else {
            	$data['errors'] = validation_errors();
            }
		}

		$this->render('obat/add', $data);
	}

	// $id = id katalog obat
	function edit($id) {
		$this->title = 'Edit Obat';

		$this->load->model('kategori_model', 'kategori');
		$this->load->model('obat_model', 'obat');
		$this->load->model('obat_form_model', 'obat_form');

		$data['jenis_dropdown'] = $this->kategori->get_dropdown();
		$data['jenis_obat_dropdown'] = array('obat'=>'Obat', 'alkes'=>'Alat Kesehatan');

		$data_obat = $this->obat->get($id);
		$data['form'] = $this->obat_form->init_data($data_obat);

		if($_POST) {
			$this->config->load('form_validation');
            $rules = $this->config->item('obat/add');

            if($this->input->post('jenis_obat') == 'alkes') {
            	// unset rule berat netto untuk tipe alkes
            	unset($rules[3]);
            }

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
            	$id_obat = $this->obat->update($id, array(
				    'id_kategori_obat' => $this->input->post('jenis'),
				    'jenis' => $this->input->post('jenis_obat'),
				    'nama_obat' => $this->input->post('nama'),
				    'nama_generik' => $this->input->post('nama_generik'),
				    'kelas_terapi' => $this->input->post('kelas_terapi'),
				    // 'berat_netto' => $this->input->post('berat_netto'),
				    'ved' => $this->input->post('ved'),
				    'lead_time' => $this->input->post('lead_time'),
				    // 'quantity_per_dus' => $this->input->post('quantity_per_dus'),
				    'jumlah_pusat' => $this->input->post('jumlah_pusat'),
				    'jumlah_rawat_inap' => $this->input->post('jumlah_rawat_inap'),
				    'jumlah_rawat_jalan' => $this->input->post('jumlah_rawat_jalan'),
				    'harga_per_dus' => $this->input->post('harga_per_dus'),
				    'harga_satuan' => ($this->input->post('harga_satuan') ? $this->input->post('harga_satuan') : 0),
				    'reorder_point' => $this->input->post('reorder_point'),
				    'catatan' => $this->input->post('catatan')
				));

				$this->session->set_flashdata('message', 'Insert Obat Sukses!');

				redirect('obat');
            }else {
            	$data['errors'] = validation_errors();
            }
		}

		$data['id'] = $id;

		$this->render('obat/edit', $data);
	}

	function delete($id) {
		$this->load->model('obat_model', 'obat');
		$this->load->model('penerimaan_obat_detail_model', 'penerimaan_obat_detail');
		$this->load->model('penerimaan_obat_model', 'penerimaan_obat');

		$this->db->trans_start();

		$data_penerimaan_obat_detail = $this->penerimaan_obat_detail->get_all_by_katalog_grouped($id);
		foreach($data_penerimaan_obat_detail as $rows) {
			$this->penerimaan_obat->delete($rows->id_penerimaan_obat);
		}

		$this->obat->delete($id);

		$this->db->trans_complete();

		redirect('obat');
	}

	function view($id) {
		$this->title = 'Detail Obat';

		$this->load->model('obat_model', 'obat');
		$this->load->model('penerimaan_obat_detail_model', 'penerimaan_obat_detail');

		$data['data'] = $this->obat->get_with_kategori($id);
		$data['supplier'] = $this->penerimaan_obat_detail->get_for_view_detail_obat($id);

		$this->render('obat/view', $data);
	}

	function get_nomor_faktur() {
		$this->load->model('penerimaan_obat_model', 'penerimaan_obat');
		$result = $this->penerimaan_obat->get_like_by_nomor_faktur($this->input->get('query'));

		$data['suggestions'] = array();
		foreach($result as $keys => $rows) {
			$data['suggestions'][$keys]['value'] = $rows->nomor_faktur;
			$data['suggestions'][$keys]['tanggal'] = conv_date_format($rows->tanggal_penerimaan, 'd-m-Y');
		}

		echo json_encode($data);
	}
}