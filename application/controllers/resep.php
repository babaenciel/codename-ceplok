<?php

class Resep extends MY_Controller {
	private $jenis = 'rawat_inap'; // default
    private $jenis_label = 'Rawat Inap';

	function __construct() {
		parent::__construct();
        access_check(array(USER_GUDANG, USER_ADMINISTRASI));

		$this->load->model('resep_model', 'resep');
        $this->load->model('log_katalog_obat_model', 'log_katalog_obat');

		if($this->uri->segment(2) === 'rawat_inap') {
			$this->jenis = 'rawat_inap';
            $this->jenis_label = 'Rawat Inap';
		}else if($this->uri->segment(2) === 'rawat_jalan') {
			$this->jenis = 'rawat_jalan';
            $this->jenis_label = 'Rawat Jalan';
		}
	}

	function index() {
        $this->title = 'Resep Obat ' . $this->jenis_label;

		$data['data'] = $this->resep->get_all_with_pasien($this->jenis);

		$data['jenis'] = $this->jenis;

		$this->render('resep/index', $data);
	}

    // $id = id rawat inap. untuk di action obat dan alkes add resep
	function add($id=NULL) {
        $this->title = 'Tambah Resep Obat ' . $this->jenis_label;

		$this->load->model('pasien_model', 'pasien');
		$this->load->model('obat_model', 'obat');
		$this->load->model('pengeluaran_obat_model', 'pengeluaran_obat');
        $this->load->model('dokter_model', 'dokter');

		if($_POST) {
			$this->config->load('form_validation');
			$rules = $this->config->item('resep_rawat_inap/add');

			$this->form_validation->set_rules($rules);

			if($this->form_validation->run()) {
                $resep = $this->resep->get_resep_by_nomor_and_jenis($this->input->post('nomor_resep'), $this->jenis);

                if(empty($resep)) {
                    $obat_array = $this->resep->create_array($this->input->post('obat'), $this->input->post('jumlah'), $this->input->post('gudang_lain'), $this->input->post('obat_pulang'));

                    // check apakah tanggal resepnya sudah ada di database
                    // $resep = $this->resep->get_by(
                    //     'tanggal_resep = "' . conv_date_format($this->input->post('tanggal_resep'), 'Y-m-d') . '"
                    //     AND jenis_resep = "'.$this->jenis.'"'
                    // );

                    // jika sudah ada, gunakan id resep yang sudah ada
                    // jika belom ada, insert dan gunakan id resep dari last insert
                    // if(!empty($resep)) {
                    //     $id_resep = $resep->id_resep;
                    // }else {
                    //     // Jika dia rawat_jalan, maka tidak menggunakan id_pasien, tapi nama_pasien_rawat_jalan
                    //     if($this->jenis == 'rawat_jalan') {
                    //         $id_resep = $this->resep->insert(array(
                    //             'nama_pasien_rawat_jalan' => $this->input->post('pasien'),
                    //             'jenis_resep' => $this->jenis,
                    //             'tanggal_resep' => conv_date_format($this->input->post('tanggal_resep'), 'Y-m-d')
                    //         ));
                    //     }else {
                    //         $id_resep = $this->resep->insert(array(
                    //             'id_pasien' => $this->input->post('pasien'),
                    //             'jenis_resep' => $this->jenis,
                    //             'tanggal_resep' => conv_date_format($this->input->post('tanggal_resep'), 'Y-m-d')
                    //         ));
                    //     }
                    // }

                    // Jika dia rawat_jalan, maka tidak menggunakan id_rawat_inap, tapi nama_pasien_rawat_jalan
                    if($this->jenis == 'rawat_jalan') {
                        $id_resep = $this->resep->insert(array(
                            'nomor_resep' => $this->input->post('nomor_resep'),
                            'id_dokter' => $this->input->post('nama_dokter'),
                            'nama_pasien_rawat_jalan' => $this->input->post('pasien'),
                            'jenis_resep' => $this->jenis,
                            'waktu_resep' => $this->input->post('waktu_resep'),
                            'tanggal_resep' => conv_date_format($this->input->post('tanggal_resep'), 'Y-m-d')
                        ));
                    }else {
                        $id_resep = $this->resep->insert(array(
                            'nomor_resep' => $this->input->post('nomor_resep'),
                            'id_dokter' => $this->input->post('nama_dokter'),
                            // post id pasien itu sebenernya isinya id rawat inap
                            'id_rawat_inap' => $this->input->post('pasien'),
                            'jenis_resep' => $this->jenis,
                            'waktu_resep' => $this->input->post('waktu_resep'),
                            'tanggal_resep' => conv_date_format($this->input->post('tanggal_resep'), 'Y-m-d')
                        ));
                    }

                    // kemudian baru masukin data obat resep
                    foreach($obat_array as $rows) {
                        $pengambilan_gudang_lain = (empty($rows['gudang_lain']) ? 0 : 1);

                        $this->pengeluaran_obat->insert(array(
                            'jumlah_pengeluaran' => $rows['jumlah'],
                            'pengambilan_gudang_lain' => $pengambilan_gudang_lain,
                            'id_katalog_obat' => $rows['obat'],
                            'id_resep' => $id_resep,
                            'sebagai_obat_pulang' => $rows['obat_pulang']
                        ));

                        $this->obat->minus_jumlah_mutasi_for_pengeluaran($rows['obat'], $this->jenis, $pengambilan_gudang_lain, $rows['jumlah']);
                    }

                    // create log katalog obat
                    if($this->jenis == 'rawat_inap') {
                        $this->log_katalog_obat->create_log(conv_date_format($this->input->post('tanggal_resep'), 'Y-m-d'), TYPE_LOG_RESEP_RI);
                    }else {
                        $this->log_katalog_obat->create_log(conv_date_format($this->input->post('tanggal_resep'), 'Y-m-d'), TYPE_LOG_RESEP_RJ);
                    }

                    // di redirect ke pasien rawat inap lagi
                    if($id !== NULL) {
                        redirect('obat_dan_alkes/index/'.$id);
                    }else {
                        redirect('resep/'.$this->jenis.'/index');
                    }
                }else {
                    $data = $this->_repopulate_form();
                    $data['errors'] = 'Nomor Resep sudah pernah digunakan. Mohon gunakan nomor resep yang lain.';
                }
			}else {
                $data = $this->_repopulate_form();
                $data['errors'] = validation_errors();
			}
		}

		$data['pasien_dropdown'] = $this->pasien->get_dropdown();
        $data['dokter_dropdown'] = $this->dokter->get_dropdown();
		$data['obat_dropdown'] = $this->obat->get_dropdown();
		$data['form'] = $this->resep->init_data();

        if($id !== NULL) {
            $data['form']['pasien'] = $id;
        }

        $data['jenis'] = $this->jenis;
        $data['id'] = (($id !== NULL) ? $id : NULL);

		$this->render('resep/add', $data);
	}

    function _repopulate_form() {
        $obat = ($this->input->post('obat') ? $this->input->post('obat') : array());
        $jumlah = ($this->input->post('jumlah') ? $this->input->post('jumlah') : array());
        $gudang_lain = ($this->input->post('gudang_lain') ? $this->input->post('gudang_lain') : array());
        $obat_pulang = ($this->input->post('obat_pulang') ? $this->input->post('obat_pulang') : array());

        // populate data obat dan jumlah kembali
        // looping obat berdasarkan keys,
        // dimana keys telah diset berdasarkan increment i
        foreach($obat as $keys => $rows) {
            $form['obat'][$keys] = $rows;
            $data['i'][] = $keys;

            // last count digunakan untuk menandai
            // jumlah terakhir i yang telah dilooping ketika
            // melakukan proses add delete field obat tadi.
            // nantinya ini akan digunakan untuk melanjutkan
            // i untuk field obat yang akan ditambahkan
            $data['last_count'] = $keys;
        }

        foreach($jumlah as $keys => $rows) {
            $form['jumlah'][$keys] = $rows;
        }

        if(empty($gudang_lain)) {
            for($i = 0; $i <= $data['last_count']; $i++) {
                $form['gudang_lain'][$i] = FALSE;
            }
        }else {
            for($i = 0; $i <= $data['last_count']; $i++) {
                if(isset($gudang_lain[$i])) {
                    $form['gudang_lain'][$i] = 'checked';
                }else {
                    $form['gudang_lain'][$i] = FALSE;
                }
            }
        }

        if(empty($obat_pulang)) {
            for($i = 0; $i <= $data['last_count']; $i++) {
                $form['obat_pulang'][$i] = FALSE;
            }
        }else {
            for($i = 0; $i <= $data['last_count']; $i++) {
                if(isset($obat_pulang[$i])) {
                    $form['obat_pulang'][$i] = 'checked';
                }else {
                    $form['obat_pulang'][$i] = FALSE;
                }
            }
        }

        $data['form_repopulate'] = $form;
        $data['total_field_obat'] = count($obat);
        $data['lebih_dari_satu'] = TRUE;

        return $data;
    }

	// $id = id resep
	function edit($id, $from_ri=NULL) {
        $this->title = 'Edit Resep Obat ' . $this->jenis_label;

        $this->load->model('pasien_model', 'pasien');
        $this->load->model('obat_model', 'obat');
        $this->load->model('pengeluaran_obat_model', 'pengeluaran_obat');
        $this->load->model('dokter_model', 'dokter');

        $list_obat = $this->pengeluaran_obat->get_many_by('id_resep', $id);
        $data['form'] = $this->resep->init_data($list_obat);
        $data['lebih_dari_satu'] = TRUE;
        $data['last_count'] = count($data['form']['obat']) - 1;
        $data['i'] = $data['form']['i'];

        if($_POST) {
            $this->config->load('form_validation');
            $rules = $this->config->item('resep_rawat_inap/add');

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                // dump($data['form']['nomor_resep']);
                // dump($this->input->post('nomor_resep'));
                if($this->input->post('nomor_resep') == $data['form']['nomor_resep']) {
                    $obat_array = $this->resep->create_array($this->input->post('obat'), $this->input->post('jumlah'), $this->input->post('gudang_lain'), $this->input->post('obat_pulang'));

                    // update data resep
                    if($this->jenis == 'rawat_jalan') {
                        $this->resep->update($id, array(
                            'nomor_resep' => $this->input->post('nomor_resep'),
                            'id_dokter' => $this->input->post('nama_dokter'),
                            // 'id_pasien' => $this->input->post('pasien'),
                            'nama_pasien_rawat_jalan' => $this->input->post('pasien'),
                            'waktu_resep' => $this->input->post('waktu_resep'),
                            'tanggal_resep' => conv_date_format($this->input->post('tanggal_resep'), 'Y-m-d')
                        ));
                    }else {
                        $this->resep->update($id, array(
                            'nomor_resep' => $this->input->post('nomor_resep'),
                            'id_dokter' => $this->input->post('nama_dokter'),
                            // post pasien sebenernya isinya id rawat inap
                            'id_rawat_inap' => $this->input->post('pasien'),
                            // 'nama_pasien_rawat_jalan' => $this->input->post('pasien'),
                            'waktu_resep' => $this->input->post('waktu_resep'),
                            'tanggal_resep' => conv_date_format($this->input->post('tanggal_resep'), 'Y-m-d')
                        ));
                    }

                    // sebelum masukin data resep baru, hapus semua data pengeluaran obat
                    // dengan resep yang akan diupdate dulu

                    // dimulai dengan mengambil data untuk mengembalikan nilai jumlah di katalog obat
                    $list_obat_lama = $this->pengeluaran_obat->get_many_by('id_resep', $id);

                    foreach($list_obat_lama as $rows) {
                        $this->obat->add_jumlah_mutasi_for_pengeluaran($rows->id_katalog_obat, $this->jenis, $rows->pengambilan_gudang_lain, $rows->jumlah_pengeluaran);
                    }

                    $this->pengeluaran_obat->delete_by('id_resep', $id);

                    // kemudian baru masukin data obat resep updatetan baru
                    foreach($obat_array as $rows) {
                        $pengambilan_gudang_lain = (empty($rows['gudang_lain']) ? 0 : 1);

                        $this->pengeluaran_obat->insert(array(
                            'jumlah_pengeluaran' => $rows['jumlah'],
                            'pengambilan_gudang_lain' => $pengambilan_gudang_lain,
                            'id_katalog_obat' => $rows['obat'],
                            'id_resep' => $id,
                            'sebagai_obat_pulang' => $rows['obat_pulang']
                        ));

                        $this->obat->minus_jumlah_mutasi_for_pengeluaran($rows['obat'], $this->jenis, $pengambilan_gudang_lain, $rows['jumlah']);
                    }

                    // create log katalog obat
                    if($this->jenis == 'rawat_inap') {
                        $this->log_katalog_obat->create_log(conv_date_format($this->input->post('tanggal_resep'), 'Y-m-d'), TYPE_LOG_RESEP_RI);
                    }else {
                        $this->log_katalog_obat->create_log(conv_date_format($this->input->post('tanggal_resep'), 'Y-m-d'), TYPE_LOG_RESEP_RJ);
                    }

                    // di redirect ke pasien rawat inap lagi
                    if($from_ri !== NULL) {
                        redirect('obat_dan_alkes/index/'.$this->input->post('pasien'));
                    }else {
                        redirect('resep/'.$this->jenis.'/index');
                    }
                }else {
                    $resep = $this->resep->get_resep_by_nomor_and_jenis($this->input->post('nomor_resep'), $this->jenis);

                    if(empty($resep)) {
                        $obat_array = $this->resep->create_array($this->input->post('obat'), $this->input->post('jumlah'), $this->input->post('gudang_lain'), $this->input->post('obat_pulang'));

                        // update data resep
                        if($this->jenis == 'rawat_jalan') {
                            $this->resep->update($id, array(
                                'nomor_resep' => $this->input->post('nomor_resep'),
                                'id_dokter' => $this->input->post('nama_dokter'),
                                // 'id_pasien' => $this->input->post('pasien'),
                                'nama_pasien_rawat_jalan' => $this->input->post('pasien'),
                                'waktu_resep' => $this->input->post('waktu_resep'),
                                'tanggal_resep' => conv_date_format($this->input->post('tanggal_resep'), 'Y-m-d')
                            ));
                        }else {
                            $this->resep->update($id, array(
                                'nomor_resep' => $this->input->post('nomor_resep'),
                                'id_dokter' => $this->input->post('nama_dokter'),
                                // post pasien sebenernya isinya id rawat inap
                                'id_rawat_inap' => $this->input->post('pasien'),
                                // 'nama_pasien_rawat_jalan' => $this->input->post('pasien'),
                                'waktu_resep' => $this->input->post('waktu_resep'),
                                'tanggal_resep' => conv_date_format($this->input->post('tanggal_resep'), 'Y-m-d')
                            ));
                        }

                        // sebelum masukin data resep baru, hapus semua data pengeluaran obat
                        // dengan resep yang akan diupdate dulu

                        // dimulai dengan mengambil data untuk mengembalikan nilai jumlah di katalog obat
                        $list_obat_lama = $this->pengeluaran_obat->get_many_by('id_resep', $id);

                        foreach($list_obat_lama as $rows) {
                            $this->obat->add_jumlah_mutasi_for_pengeluaran($rows->id_katalog_obat, $this->jenis, $rows->pengambilan_gudang_lain, $rows->jumlah_pengeluaran);
                        }

                        $this->pengeluaran_obat->delete_by('id_resep', $id);

                        // kemudian baru masukin data obat resep updatetan baru
                        foreach($obat_array as $rows) {
                            $pengambilan_gudang_lain = (empty($rows['gudang_lain']) ? 0 : 1);

                            $this->pengeluaran_obat->insert(array(
                                'jumlah_pengeluaran' => $rows['jumlah'],
                                'pengambilan_gudang_lain' => $pengambilan_gudang_lain,
                                'id_katalog_obat' => $rows['obat'],
                                'id_resep' => $id,
                                'sebagai_obat_pulang' => $rows['obat_pulang']
                            ));

                            $this->obat->minus_jumlah_mutasi_for_pengeluaran($rows['obat'], $this->jenis, $pengambilan_gudang_lain, $rows['jumlah']);
                        }

                        // di redirect ke pasien rawat inap lagi
                        if($from_ri !== NULL) {
                            redirect('obat_dan_alkes/index/'.$this->input->post('pasien'));
                        }else {
                            redirect('resep/'.$this->jenis.'/index');
                        }
                    }else {
                        array_push($data, $this->_repopulate_form_edit());
                        $data['errors'] = 'Nomor Resep sudah pernah digunakan. Mohon gunakan nomor resep yang lain.';
                    }
                }
            }else {
                array_push($data, $this->_repopulate_form_edit());
                $data['errors'] = validation_errors();
            }
        }

        $data['dokter_dropdown'] = $this->dokter->get_dropdown();
        $data['pasien_dropdown'] = $this->pasien->get_dropdown();
        $data['obat_dropdown'] = $this->obat->get_dropdown();
        $data['jenis'] = $this->jenis;
        $data['id'] = $id;
        $data['from_ri'] = (($from_ri !== NULL) ? $from_ri : NULL);

        $this->render('resep/edit', $data);
	}

    function _repopulate_form_edit() {
        $obat = ($this->input->post('obat') ? $this->input->post('obat') : array());
        $jumlah = ($this->input->post('jumlah') ? $this->input->post('jumlah') : array());
        $gudang_lain = ($this->input->post('gudang_lain') ? $this->input->post('gudang_lain') : array());

        // populate data obat dan jumlah kembali
        // looping obat berdasarkan keys,
        // dimana keys telah diset berdasarkan increment i
        foreach($obat as $keys => $rows) {
            $form['obat'][$keys] = $rows;
            $data['i'][] = $keys;

            // last count digunakan untuk menandai
            // jumlah terakhir i yang telah dilooping ketika
            // melakukan proses add delete field obat tadi.
            // nantinya ini akan digunakan untuk melanjutkan
            // i untuk field obat yang akan ditambahkan
            $data['last_count'] = $keys;
        }

        foreach($jumlah as $keys => $rows) {
            $form['jumlah'][$keys] = $rows;
        }

        if(empty($gudang_lain)) {
            for($i = 0; $i < $data['last_count']; $i++) {
                $form['gudang_lain'][$keys] = FALSE;
            }
        }else {
            for($i = 0; $i <= $data['last_count']; $i++) {
                if(isset($gudang_lain[$i])) {
                    $form['gudang_lain'][$i] = 'checked';
                }else {
                    $form['gudang_lain'][$i] = FALSE;
                }
            }
        }

        $data['form_repopulate'] = $form;
        $data['total_field_obat'] = count($obat);
        $data['lebih_dari_satu'] = TRUE;

        return $data;
    }

    // $id = id resep
    function delete($id) {
        $this->load->model('pengeluaran_obat_model', 'pengeluaran_obat');
        $this->load->model('resep_model', 'resep');
        $this->load->model('obat_model', 'obat');

        $result = $this->pengeluaran_obat->get_many_by('id_resep', $id);

        foreach($result as $rows) {
            $this->obat->add_jumlah_mutasi_for_pengeluaran($rows->id_katalog_obat, $this->jenis, $rows->pengambilan_gudang_lain, $rows->jumlah_pengeluaran);
        }

        $this->pengeluaran_obat->delete_by('id_resep', $id);
        $resep = $this->resep->get($id);
        $this->resep->delete($id);

        // create log katalog obat
        if($this->jenis == 'rawat_inap') {
            $this->log_katalog_obat->create_log(conv_date_format($resep->tanggal_resep, 'Y-m-d'), TYPE_LOG_RESEP_RI);
        }else {
            $this->log_katalog_obat->create_log(conv_date_format($resep->tanggal_resep, 'Y-m-d'), TYPE_LOG_RESEP_RJ);
        }

        redirect('resep/'.$this->jenis.'/index');
    }

	function get_field_obat() {
		$this->load->model('obat_model', 'obat');
		$this->load->model('resep_model', 'resep');

		$data['obat_dropdown'] = $this->obat->get_dropdown();
		$data['form'] = $this->resep->init_data(NULL, $this->input->post('i'));
		$data['i'] = $this->input->post('i');
        $data['jenis'] = $this->input->post('jenis');

		$data['view'] = $this->load->view('resep/_field_obat', $data, TRUE);

		echo json_encode($data);
	}
}