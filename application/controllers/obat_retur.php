<?php

class Obat_retur extends MY_Controller {
    function __construct() {
        parent::__construct();
        access_check(array(USER_GUDANG, USER_ADMINISTRASI));

        $this->load->model('rawat_inap_model', 'rawat_inap');
        $this->load->model('pemakaian_alkes_model', 'pemakaian_alkes');
        $this->load->model('obat_retur_model', 'obat_retur');
        $this->load->model('obat_model', 'obat');
        $this->load->model('resep_model', 'resep');
        $this->load->model('pengeluaran_obat_model', 'pengeluaran_obat');
    }

    function index() {
        $this->title = 'History Obat Retur';

        $data['data'] = $this->pengeluaran_obat->get_all_obat_retur();

        $this->render('obat_retur/index', $data);
    }

    function list_pasien() {
        $this->load->model('pasien_model', 'pasien');

        $this->title = 'Daftar Pasien Rawat Inap';

        $data['data'] = $this->pasien->get_pasien_with_rawat_inap();

        $this->render('obat_retur/list_pasien', $data);
    }

    // $id = id pasien
    function add($id) {
        $this->title = 'Tambah Obat Retur';

        $data_rawat_inap = $this->rawat_inap->get_pasien_rawat_inap_by_pasien($id);
        $pemakaian_obat = $this->resep->get_resep_pasien_rawat_inap($data_rawat_inap->id_rawat_inap);

        foreach($pemakaian_obat as $keys=>$rows) {
            $data['pemakaian_obat'][$rows->id_katalog_obat]['nama_obat'] = $rows->nama_obat;
            $data['pemakaian_obat'][$rows->id_katalog_obat]['id_katalog_obat'] = $rows->id_katalog_obat;
            $data['pemakaian_obat'][$rows->id_katalog_obat]['id_pengeluaran_obat'][] = $rows->id_pengeluaran_obat;
            $data['pemakaian_obat'][$rows->id_katalog_obat]['sisa'] = '';
            $data['pemakaian_obat'][$rows->id_katalog_obat]['nama_kategori'] = $rows->nama_kategori;

            $sisa = !empty($rows->sisa) ? intval($rows->sisa) : 0;
            if(isset($data['pemakaian_obat'][$rows->id_katalog_obat]['sisa_pengeluaran'])) {
                $data['pemakaian_obat'][$rows->id_katalog_obat]['sisa_pengeluaran'] += $rows->jumlah_pengeluaran - $sisa;
            } else {
                $data['pemakaian_obat'][$rows->id_katalog_obat]['sisa_pengeluaran'] = $rows->jumlah_pengeluaran - $sisa;
            }
        }

        $this->title .= ' - ' . $data_rawat_inap->nama_pasien . ' - ' . conv_date_format($data_rawat_inap->tgl_masuk, 'd-m-Y');

        $data['id'] = $id;

        if($_POST) {
            $this->config->load('form_validation');
            $rules = $this->config->item('obat_retur/add');

            foreach($pemakaian_obat as $keys=>$rows) {
                array_push($rules, array(
                    'field' => 'jumlah['.$rows->id_katalog_obat.']',
                    'label' => 'Jumlah Retur',
                    'rules' => ''
                ));
            }

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                $id_rawat_inap = $pemakaian_obat[0]->id_rawat_inap;
                $obat_id = $this->input->post('obat_id');
                $id_pengeluaran_obat = $this->input->post('id_pengeluaran_obat');
                // print_r($id_pengeluaran_obat);exit;
                // print_r($this->input->post('jumlah'));exit;
                $data['errors'] = [];
                $data['success'] = [];

                foreach($this->input->post('jumlah') as $keys=>$rows) {
                    $error_messages = '';
                    $success_messages = '';

                    if(!empty($rows)) {
                        $pengeluaran_obats = $this->resep->get_pengeluaran_obat_array($id_pengeluaran_obat[$keys]);

                        $returTemp = $rows;
                        $pengeluaran_obat_available = array();
                        foreach($pengeluaran_obats as $pengeluaran_obat) {
                            if($pengeluaran_obat->available < $returTemp) {
                                array_push($pengeluaran_obat_available, array(
                                    'id_pengeluaran_obat' => $pengeluaran_obat->id_pengeluaran_obat,
                                    'current_sisa' => $pengeluaran_obat->sisa + $pengeluaran_obat->available,
                                ));

                                $returTemp -= $pengeluaran_obat->available;
                            } else {
                                array_push($pengeluaran_obat_available, array(
                                    'id_pengeluaran_obat' => $pengeluaran_obat->id_pengeluaran_obat,
                                    'current_sisa' => $pengeluaran_obat->sisa + $returTemp
                                ));

                                $returTemp = 0;
                                break;
                            }
                        }

                        if($returTemp == 0) {
                            $this->obat->add_jumlah_mutasi_for_pengeluaran($keys, 'rawat_inap', 0, $rows);

                            foreach($pengeluaran_obat_available as $pengeluaran_calculated) {
                                $this->resep->update_obat_retur($pengeluaran_calculated['id_pengeluaran_obat'], array(
                                    'tanggal_retur' => date('Y-m-d'),
                                    'sisa' => $pengeluaran_calculated['current_sisa']
                                ));
                            }

                            $data_obat = $this->obat->get_by_id($keys);
                            $success_messages = 'Retur obat (' . $data_obat->nama_obat.') sejumlah ' . $rows . ' berhasil!';
                            array_push($data['success'], $success_messages);
                        } else {
                            $data_obat = $this->obat->get_by_id($keys);
                            $error_messages = 'Error! Jumlah retur obat ('.$data_obat->nama_obat.') melebihi pemakaian<br>';
                            array_push($data['errors'], $error_messages);

                            // repopulate field jumlah
                            $input_jumlah = $this->input->post('jumlah');
                            $form = array();
                            foreach($input_jumlah as $keys=>$rows) {
                                $data['form_repopulate'][$keys]['jumlah'] = $rows;
                            }
                        }
                    }
                }

                if(count($data['success']) > 0) {
                    $this->session->set_flashdata('success', $data['success']);
                }

                if(count($data['errors']) > 0) {
                    $this->session->set_flashdata('errors', $data['errors']);
                }

                redirect('obat_retur/add/' . $id);
            }else {
                $data['errors'] = validation_errors();

                // repopulate field jumlah
                $input_jumlah = $this->input->post('jumlah');
                $form = array();
                foreach($input_jumlah as $keys=>$rows) {
                    $form['jumlah'][$keys][] = $input_jumlah[$keys][0];
                }

                $data['form_repopulate'] = $form;
            }
        }

        $this->render('obat_retur/add', $data);
    }

    // $id = id pengeluaran obat
    function edit($id) {
        $this->title = 'Edit Obat Retur';
        $pemakaian_obat = $this->pengeluaran_obat->get_all_with_obat($id);

        foreach($pemakaian_obat as $keys=>$rows) {
            $data['pemakaian_obat'][$rows->id_katalog_obat]['nama_obat'] = $rows->nama_obat;
            $data['pemakaian_obat'][$rows->id_katalog_obat]['id_katalog_obat'] = $rows->id_katalog_obat;
            $data['pemakaian_obat'][$rows->id_katalog_obat]['id_pengeluaran_obat'][] = $rows->id_pengeluaran_obat;
            $data['pemakaian_obat'][$rows->id_katalog_obat]['sisa'] = $rows->sisa;

            $sisa = !empty($rows->sisa) ? intval($rows->sisa) : 0;
            if(isset($data['pemakaian_obat'][$rows->id_katalog_obat]['sisa_pengeluaran'])) {
                $data['pemakaian_obat'][$rows->id_katalog_obat]['sisa_pengeluaran'] += $rows->jumlah_pengeluaran - $sisa;
            } else {
                $data['pemakaian_obat'][$rows->id_katalog_obat]['sisa_pengeluaran'] = $rows->jumlah_pengeluaran - $sisa;
            }
        }

        $data['id'] = $id;

        if($_POST) {
            $this->config->load('form_validation');
            $rules = $this->config->item('obat_retur/add');

            foreach($data['pemakaian_obat'] as $keys=>$rows) {
                array_push($rules, array(
                    'field' => 'jumlah['.$keys.'][]',
                    'label' => 'Jumlah Retur',
                    'rules' => ''
                ));
            }

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                $obat_id = $this->input->post('obat_id');
                $id_pengeluaran_obat = $this->input->post('id_pengeluaran_obat');
                $error_messages = '';

                foreach($this->input->post('jumlah') as $keys=>$rows) {
                    if($this->resep->is_greater_than_pengeluaran_obat($id_pengeluaran_obat[$keys][0], $rows)) {
                        $data_obat = $this->pengeluaran_obat->get_with_obat($id_pengeluaran_obat[$keys][0]);
                        $error_messages .= 'Error! Jumlah retur obat ('.$data_obat->nama_obat.') melebihi pemakaian<br>';

                        // repopulate field jumlah
                        $input_jumlah = $this->input->post('jumlah');
                        $form = array();
                        foreach($input_jumlah as $keys=>$rows) {
                            $form['jumlah'][$keys][] = $input_jumlah[$keys][0];
                        }

                        $data['form_repopulate'] = $form;
                    }else {
                        $this->obat->minus_jumlah_mutasi_for_pengeluaran($obat_id[$keys], 'rawat_inap', 0, $data['pemakaian_obat'][$keys]['sisa']);
                        $this->obat->add_jumlah_mutasi_for_pengeluaran($obat_id[$keys], 'rawat_inap', 0, $rows);

                        $this->resep->update_obat_retur($id_pengeluaran_obat[$keys][0], array(
                            'tanggal_retur' => date('Y-m-d'),
                            'sisa' => $rows,
                        ));

                        redirect('obat_retur');
                    }

                    $data['errors'] = $error_messages;
                }
            }else {
                $data['errors'] = validation_errors();

                // repopulate field jumlah
                $input_jumlah = $this->input->post('jumlah');
                $form = array();
                foreach($input_jumlah as $keys=>$rows) {
                    $form['jumlah'][$keys][] = $input_jumlah[$keys][0];
                }

                $data['form_repopulate'] = $form;
            }
        }

        $this->render('obat_retur/edit', $data);
    }

    // $id = id pemakaian alkes
    function delete($id) {
        $data_pemakaian = $this->pengeluaran_obat->get($id);
        $this->obat->minus_jumlah_mutasi_for_pengeluaran($data_pemakaian->id_katalog_obat, 'rawat_inap', 0, $data_pemakaian->sisa);

        $this->pengeluaran_obat->update($id, array(
            'sisa' => NULL,
            'tanggal_retur' => NULL
        ));

        redirect('obat_retur');
    }
}