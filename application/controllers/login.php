<?php

class Login extends CI_Controller {
    function __construct() {
        parent::__construct();

        header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0',false);
        header('Pragma: no-cache');
    }

    public function index() {
        // jika sudah login maka tidak bisa masuk halaman ini.
        if($this->session->userdata('user') !== FALSE) {
            access_redirect();
        }

        $data = array();

        if($_POST) {
            $this->config->load('form_validation');
            $rules = $this->config->item('login');
            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                $this->load->model('user_model', 'user');
                $username = $this->input->post('username');
                $password = $this->input->post('password');

                $result = $this->user->get_by('username = "'.$username.'" AND password = "'.$password.'"');
                if(!empty($result)) {
                    // $this->session->set_userdata('username', $username);
                    // $this->session->set_userdata('jabatan', $result->jabatan);
                    $this->session->set_userdata('user', $result);

                    access_redirect();
                }else {
                    $data['errors'] = 'Username atau Password Salah!';
                }
            }else {
                $data['errors'] = validation_errors();
            }
        }

        $this->load->view('login/index', $data);
    }

    function logout() {
        $this->session->unset_userdata('user');
        $this->session->sess_destroy();

        redirect('login');
    }
}