<?php

class Rontgen extends MY_Controller {
	function __construct() {
        parent::__construct();
        access_check(array(USER_ADMINISTRASI));

        $this->load->model('rontgen_model', 'rontgen');
        $this->load->model('rawat_inap_model', 'rawat_inap');

        $this->title = 'Rontgen';
    }

    // $id = id rawat inap
    function index($id) {
        $rawat_inap = $this->rawat_inap->get_with_pasien($id);        
        $this->title .= ' - ' . $rawat_inap->nama_pasien . ' - ' . conv_date_format($rawat_inap->tgl_masuk, 'd-m-Y');       
        
    	$data['data'] = $this->rontgen->get_all_rontgen($id);
    	$data['id'] = $id;

    	$this->render('rontgen/index', $data);
    }

    // $id = id rawat inap
    function add($id) {
        $this->load->model('tarif_rontgen_model', 'tarif_rontgen');

        $rawat_inap = $this->rawat_inap->get_with_pasien($id);        
        $this->title .= ' - ' . $rawat_inap->nama_pasien . ' - ' . conv_date_format($rawat_inap->tgl_masuk, 'd-m-Y');               

        if($_POST) {
            $this->config->load('form_validation');
            $rules = $this->config->item('rontgen/add');

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                $this->rontgen->insert(array(
                    'id_tarif_rontgen' => $this->input->post('layanan'),
                    'tgl_rontgen' => conv_date_format($this->input->post('tanggal_rontgen'), 'Y-m-d'),                    
                    'id_rawat_inap' => $id
                ));

                redirect('rontgen/index/'.$id);
            }else {
                $data['errors'] = validation_errors();
            }
        }

        $data['layanan_dropdown'] = $this->tarif_rontgen->get_dropdown();        
        $data['form'] = $this->rontgen->init_data();
        $data['id'] = $id;

        $this->render('rontgen/add', $data);
    }

    // $id = id fisioterapi
    function edit($id) {
        $this->load->model('tarif_rontgen_model', 'tarif_rontgen');

        $data['layanan_dropdown'] = $this->tarif_rontgen->get_dropdown();
        $data['id'] = $id;

        $data_rontgen = $this->rontgen->get($id);
        $data['form'] = $this->rontgen->init_data($data_rontgen);

        $rawat_inap = $this->rawat_inap->get_with_pasien($data_rontgen->id_rawat_inap);        
        $this->title .= ' - ' . $rawat_inap->nama_pasien . ' - ' . conv_date_format($rawat_inap->tgl_masuk, 'd-m-Y');               

        if($_POST) {
            $this->config->load('form_validation');
            $rules = $this->config->item('rontgen/add');

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                $this->rontgen->update($id, array(
                    'id_tarif_rontgen' => $this->input->post('layanan'),
                    'tgl_rontgen' => conv_date_format($this->input->post('tanggal_rontgen'), 'Y-m-d'),                    
                    'id_rawat_inap' => $data_rontgen->id_rawat_inap
                ));

                redirect('rontgen/index/'.$data_rontgen->id_rawat_inap);
            }else {
                $data['errors'] = validation_errors();
            }
        }

        $this->render('rontgen/edit', $data);
    }

    // $id = id rontgen
    function delete($id) {
        $data_rontgen = $this->rontgen->get($id);
        $this->rontgen->delete($id);

        redirect('rontgen/index/'.$data_rontgen->id_rawat_inap);
    }
}