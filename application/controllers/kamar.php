<?php

class Kamar extends MY_Controller {
    function __construct() {
        parent::__construct();
        access_check(array(USER_ADMINISTRASI));

        $this->load->model('pemakaian_kamar_model', 'pemakaian_kamar');
    }

    // $id = id rawat inap
    function index($id) {
        $data['data'] = $this->pemakaian_kamar->get_all_pemakaian_kamar($id);
        // dump($data);
        $data['id'] = $id;

        $this->render('pemakaian_kamar/index', $data);
    }

    // $id = id rawat inap
    function add($id) {
        $this->load->model('pemakaian_kamar_model', 'pemakaian_kamar');

        if($_POST) {
            $this->config->load('form_validation');
            $rules = $this->config->item('pemakaian_kamar/add');

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                $this->pemakaian_kamar->insert(array(
                    'id_tarif_laboratorium' => $this->input->post('layanan'),
                    'tgl_laboratorium' => conv_date_format($this->input->post('tanggal_laboratorium'), 'Y-m-d'),
                    'id_rawat_inap' => $id
                ));

                redirect('laboratorium/index/'.$id);
            }else {
                $data['errors'] = validation_errors();
            }
        }

        $data['layanan_dropdown'] = $this->tarif_laboratorium->dropdown('nama_layanan');
        $data['form'] = $this->laboratorium->init_data();
        $data['id'] = $id;

        $this->render('pemakaian_kamar/add', $data);
    }

    // $id = id laboratorium
    function edit($id) {
        $this->load->model('tarif_laboratorium_model', 'tarif_laboratorium');

        $data['layanan_dropdown'] = $this->tarif_laboratorium->dropdown('nama_layanan');
        $data['id'] = $id;

        $data_laboratorium = $this->laboratorium->get($id);
        $data['form'] = $this->laboratorium->init_data($data_laboratorium);
        $data['id'] = $id;

        if($_POST) {
            $this->config->load('form_validation');
            $rules = $this->config->item('laboratorium/add');

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                $this->laboratorium->update($id, array(
                    'id_tarif_laboratorium' => $this->input->post('layanan'),
                    'tgl_laboratorium' => conv_date_format($this->input->post('tanggal_laboratorium'), 'Y-m-d'),
                    'id_rawat_inap' => $data_laboratorium->id_rawat_inap
                ));

                redirect('laboratorium/index/'.$data_laboratorium->id_rawat_inap);
            }else {
                $data['errors'] = validation_errors();
            }
        }

        $this->render('laboratorium/edit', $data);
    }

    // $id = id laboratorium
    function delete($id) {
        $data_laboratorium = $this->laboratorium->get($id);
        $this->laboratorium->delete($id);

        redirect('laboratorium/index/'.$data_laboratorium->id_rawat_inap);
    }
}