<?php

class Pasien extends MY_Controller {
	function __construct() {
		parent::__construct();
		access_check(array(USER_ADMINISTRASI));

		$this->load->model('pasien_model', 'pasien');
	}

	function index() {
		$this->title = 'Daftar Pasien';

		$data['data'] = $this->pasien->get_all();
        // dump($data);
		$this->render('pasien/index', $data);
	}

	function add() {
		$this->title = 'Pendaftaran Pasien';

		$data = array();

		if($_POST) {
			$this->config->load('form_validation');
            $rules = $this->config->item('pasien');
            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                $pasien = $this->pasien->get($this->input->post('id_pasien'));
                if(empty($pasien)) {                        
                    $input = $this->pasien->populate_form_field($this->input->post());
                    $this->pasien->insert($input);                        
                    redirect('pasien');
                }else {
                    $data['errors'] = 'ID sudah digunakan. Mohon gunakan ID yang lain.';
                }            	
            }else {
            	$data['errors'] = validation_errors();
            }
		}

		$data['form'] = $this->pasien->init_form_field();

		$this->render('pasien/add', $data);
	}

	function edit($id) {
		$this->title = 'Edit Pasien';

		if($_POST) {
			$this->config->load('form_validation');
            $rules = $this->config->item('pasien');

            array_push($rules, array(
                'field' => 'id_pasien',
                'label' => 'ID Pasien',
                'rules' => 'required|numeric'
            ));

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                if($this->input->post('id_pasien') == $id) {
                    $input = $this->pasien->populate_form_field($this->input->post(), 'edit');
                    $this->pasien->update($id, $input);
                    redirect('pasien');
                }else {
                    $pasien = $this->pasien->get($this->input->post('id_pasien'));
                    if(empty($pasien)) {
                        $input = $this->pasien->populate_form_field($this->input->post(), 'edit');
                        $this->pasien->update($id, $input);
                        redirect('pasien');
                    }else {
                        $data['errors'] = 'ID sudah digunakan. Mohon gunakan ID yang lain.';
                    }
                }
            }else {
            	$data['errors'] = validation_errors();
            }
		}

		$data['id'] = $id;

		$pasien = $this->pasien->get($id);
		$data['form'] = $this->pasien->init_form_field($pasien);

		$this->render('pasien/edit', $data);
	}

	function delete($id) {
        $this->load->model('rawat_inap_model', 'rawat_inap');

        $pasien = $this->rawat_inap->get_pasien_rawat_inap_by_pasien_only($id);
        
        if($pasien != null) {        
            $data['data'] = $this->pasien->get_all();    
            $data['errors'] = 'Hapus pasien gagal karena data pasien sudah digunakan oleh data lain.';                        
            
            $this->render('pasien/index', $data);            
        }else {
            $this->pasien->delete($id);
            
            redirect('pasien');
        }		
	}

    // $id = id pasien
    function change_status($id, $status) {
        $this->pasien->change_status($id, $status);
        redirect('pasien');
    }

    // function id_check($id) {
    //     $pasien = $this->pasien->get($id);
    //     if(!empty($pasien)) {
    //         $this->form_validation->set_message('id_check', 'ID yang dimasukkan sudah digunakan. Mohon gunakan ID yang lain.');
    //         return FALSE;
    //     }else {
    //         return TRUE;
    //     }
    // }

}