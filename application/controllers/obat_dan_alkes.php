<?php

// controller ini adalah untuk menu crud obat dan alkes untuk pasien rawat inap
// bukan untuk manajemen obat
class Obat_dan_alkes extends MY_Controller {
    function __construct() {
        parent::__construct();
        access_check(array(USER_ADMINISTRASI));

        $this->load->model('resep_model', 'resep');
        $this->load->model('rawat_inap_model', 'rawat_inap');

        $this->title = 'Obat dan Alkes';
    }

    // $id = id rawat inap
    function index($id) {
        $rawat_inap = $this->rawat_inap->get_with_pasien($id);        
        $this->title .= ' - ' . $rawat_inap->nama_pasien . ' - ' . conv_date_format($rawat_inap->tgl_masuk, 'd-m-Y');       
        
        $data['data'] = $this->resep->get_resep_pasien_rawat_inap($id);
        $data['id'] = $id;

        $this->render('obat_dan_alkes/index', $data);
    }

    // $id = id rawat inap
    function add($id) {
        $this->load->model('obat_model', 'obat');

        $rawat_inap = $this->rawat_inap->get_with_pasien($id);        
        $this->title .= ' - ' . $rawat_inap->nama_pasien . ' - ' . conv_date_format($rawat_inap->tgl_masuk, 'd-m-Y');               

        if($_POST) {
            $this->config->load('form_validation');
            $rules = $this->config->item('obat_dan_alkes/add');

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                $sebagai_obat_pulang = $this->input->post('sebagai_obat_pulang') ? $this->input->post('sebagai_obat_pulang') : 0;

                $this->pemakaian_alkes->insert(array(
                    'id_katalog_obat' => $this->input->post('obat_dan_alkes'),
                    'jumlah' => $this->input->post('jumlah'),
                    'tanggal' => conv_date_format($this->input->post('tanggal'), 'Y-m-d'),
                    'sebagai_obat_pulang' => $sebagai_obat_pulang,
                    'id_rawat_inap' => $id
                ));

                redirect('obat_dan_alkes/index/'.$id);
            }else {
                $data['errors'] = validation_errors();
            }
        }

        $data['obat_dan_alkes_dropdown'] = $this->obat->dropdown('nama_obat');
        $data['form'] = $this->pemakaian_alkes->init_data();
        $data['id'] = $id;

        $this->render('obat_dan_alkes/add', $data);
    }

    // $id = id pemakaian alkes
    function edit($id) {
        $this->load->model('obat_model', 'obat');
        
        $data['obat_dan_alkes_dropdown'] = $this->obat->dropdown('nama_obat');
        $data['form'] = $this->pemakaian_alkes->init_data();

        $data_pemakaian = $this->pemakaian_alkes->get($id);
        $data['form'] = $this->pemakaian_alkes->init_data($data_pemakaian);

        $data['id'] = $id;

        $rawat_inap = $this->rawat_inap->get_with_pasien($data_pemakaian->id_rawat_inap);        
        $this->title .= ' - ' . $rawat_inap->nama_pasien . ' - ' . conv_date_format($rawat_inap->tgl_masuk, 'd-m-Y');               

        if($_POST) {
            $this->config->load('form_validation');
            $rules = $this->config->item('obat_dan_alkes/add');

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                $sebagai_obat_pulang = $this->input->post('sebagai_obat_pulang') ? $this->input->post('sebagai_obat_pulang') : 0;

                $this->pemakaian_alkes->update($id, array(
                    'id_katalog_obat' => $this->input->post('obat_dan_alkes'),
                    'jumlah' => $this->input->post('jumlah'),
                    'tanggal' => conv_date_format($this->input->post('tanggal'), 'Y-m-d'),
                    'sebagai_obat_pulang' => $sebagai_obat_pulang,
                    'id_rawat_inap' => $data_pemakaian->id_rawat_inap
                ));

                redirect('obat_dan_alkes/index/'.$data_pemakaian->id_rawat_inap);
            }else {
                $data['errors'] = validation_errors();
            }
        }

        $this->render('obat_dan_alkes/edit', $data);
    }

    // $id = id pemakaian alkes
    function delete($id) {
        $data_pemakaian = $this->pemakaian_alkes->get($id);
        $this->pemakaian_alkes->delete($id);

        redirect('obat_dan_alkes/index/'.$data_pemakaian->id_rawat_inap);
    }
}