<?php

class Pemakaian_kamar extends MY_Controller {
    function __construct() {
        parent::__construct();
        access_check(array(USER_ADMINISTRASI));

        $this->load->model('pemakaian_kamar_model', 'pemakaian_kamar');
        $this->load->model('rawat_inap_model', 'rawat_inap');

        $this->title = 'Pemakaian Kamar Pasien';
    }

    // $id = id rawat inap
    function index($id) {   
        $rawat_inap = $this->rawat_inap->get_with_pasien($id);        
        $this->title .= ' - ' . $rawat_inap->nama_pasien . ' - ' . conv_date_format($rawat_inap->tgl_masuk, 'd-m-Y');       
     
        $data['data'] = $this->pemakaian_kamar->get_all_pemakaian_kamar($id);                
        $data['id'] = $id;

        $this->render('pemakaian_kamar/index', $data);
    }

    // $id = id rawat inap
    function add($id) {        
        $this->load->model('kamar_model', 'kamar');

        $rawat_inap = $this->rawat_inap->get_with_pasien($id);        
        $this->title .= ' - ' . $rawat_inap->nama_pasien . ' - ' . conv_date_format($rawat_inap->tgl_masuk, 'd-m-Y');       

        if($_POST) {
            $this->config->load('form_validation');
            $rules = $this->config->item('pemakaian_kamar/add');

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                // jika ngeadd, tidak perlu ada tgl akhir.
                // tgl akhir ada di menu edit aja
                $this->pemakaian_kamar->insert(array(
                    'id_kamar' => $this->input->post('kamar'),
                    'tgl_mulai' => conv_date_format($this->input->post('tanggal_mulai'), 'Y-m-d'),
                    // 'tgl_akhir' => conv_date_format($this->input->post('tanggal_akhir'), 'Y-m-d'),
                    'id_rawat_inap' => $id
                ));

                redirect('pemakaian_kamar/index/'.$id);
            }else {
                $data['errors'] = validation_errors();
            }
        }

        // $data['kamar_dropdown'] = $this->kamar->dropdown('nama_kamar');
        $data['kamar_dropdown'] = $this->pemakaian_kamar->get_pemakaian_kamar_dropdown();
        $data['form'] = $this->pemakaian_kamar->init_data(NULL, $id);
        $data['id'] = $id;

        $this->render('pemakaian_kamar/add', $data);
    }

    // $id = id pemakaian kamar
    function edit($id) {
        $this->load->model('kamar_model', 'kamar');

        $data_kamar = $this->pemakaian_kamar->get_pemakaian_kamar($id);

        $data['kamar_dropdown'] = $this->pemakaian_kamar->get_pemakaian_kamar_dropdown($data_kamar->id_kamar);

        $data['form'] = $this->pemakaian_kamar->init_data($data_kamar);

        $data['id'] = $id;
        $data['edit'] = TRUE;

        $rawat_inap = $this->rawat_inap->get_with_pasien($data_kamar->id_rawat_inap);
        $this->title .= ' - ' . $rawat_inap->nama_pasien . ' - ' . conv_date_format($rawat_inap->tgl_masuk, 'd-m-Y');

        if($_POST) {
            $this->config->load('form_validation');
            $rules = $this->config->item('pemakaian_kamar/add');

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                if($this->input->post('tanggal_akhir')) {
                    $this->pemakaian_kamar->update($id, array(
                        'id_kamar' => $this->input->post('kamar'),
                        'tgl_mulai' => conv_date_format($this->input->post('tanggal_mulai'), 'Y-m-d'),
                        'tgl_akhir' => conv_date_format($this->input->post('tanggal_akhir'), 'Y-m-d'),
                        'id_rawat_inap' => $data_kamar->id_rawat_inap
                    ));
                }else {
                    $this->pemakaian_kamar->update($id, array(
                        'id_kamar' => $this->input->post('kamar'),
                        'tgl_mulai' => conv_date_format($this->input->post('tanggal_mulai'), 'Y-m-d'),
                        'id_rawat_inap' => $data_kamar->id_rawat_inap
                    ));
                }

                redirect('pemakaian_kamar/index/'.$data_kamar->id_rawat_inap);
            }else {
                $data['errors'] = validation_errors();
            }
        }

        $this->render('pemakaian_kamar/edit', $data);
    }

    // $id = id pemakaian kamar
    function delete($id) {
        $data_kamar = $this->pemakaian_kamar->get($id);
        $this->pemakaian_kamar->delete($id);

        redirect('pemakaian_kamar/index/'.$data_kamar->id_rawat_inap);
    }
}