<?php

class Pendapatan extends MY_Controller {
    function __construct() {
        parent::__construct();
        access_check(array(USER_ADMINISTRASI));
    }

    function index() {
        $this->title = 'Pendapatan Bulanan';

        $this->load->model('rawat_inap_model', 'rawat_inap');
        $this->load->model('pasien_model', 'pasien');

        if($_POST) {
            $this->form_validation->set_rules('bulan', 'Bulan', '');
            $this->form_validation->set_rules('tahun', 'Tahun', '');

            if($this->form_validation->run()) {
                $month = $this->input->post('bulan');
                $year = $this->input->post('tahun');

                $data['total_pendapatan'] = $this->rawat_inap->get_total_pendapatan_by_month_year($month, $year)->total_pendapatan;
                $data['total_pasien_dirawat'] = $this->pasien->count_pasien_rawat_inap_by_month_year($month, $year);
                $data['total_pasien_keseluruhan'] = $this->pasien->count_all_pasien_rawat_inap();
            }
        }

        $data['bulan'] = array('01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei', '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'November', '12'=>'Desember');

        for($i = 1990; $i < 2050; $i++) {
            $data['tahun'][$i] = $i;
        }

        $this->render('pendapatan/index', $data);
    }

}