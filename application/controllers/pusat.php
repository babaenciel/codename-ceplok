<?php

class Pusat extends MY_Controller {
	function __construct() {
		parent::__construct();
		access_check(array(USER_ADMINISTRASI));
	}

	function index() {
		$this->title = 'Daftar Obat';

		$this->load->model('obat_model', 'obat');
		$data['data'] = $this->obat->get_all_with_kategori();

		$this->render('pusat/index', $data);
	}
}