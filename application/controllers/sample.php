<?php

class Sample extends MY_Controller {
	function __construct() {
		parent::__construct();
	}

	function adm_pusat() {
		$this->title = "Daftar Obat Gudang Farmasi Pusat";
		$this->render('samples/adm_table');
	}

	function adm_form() {
		$this->title = "Input Data Obat";
		$this->render('samples/adm_form');
	}

	function adm_pasien() {
		$this->title = 'Daftar Pasien';
		$this->render('samples/adm_pasien');
	}

	function adm_form_pasien() {
		$this->title = 'Input Pasien';
		$this->render('samples/adm_form_pasien');
	}

	function adm_table_nota() {
		$this->title = 'Daftar Nota';
		$this->render('samples/adm_table_nota');
	}

	function adm_form_nota() {
		$this->title = 'Buat Nota';
		$this->render('samples/adm_form_nota');
	}

	function adm_form_pasien_rawat_inap() {
		$this->title = 'Form Pendaftaran Pasien Rawat Inap';
		$this->render('samples/adm_form_pasien_rawat_inap');
	}

	function adm_visit_dokter() {
		$this->title = 'Visit Dokter';
		$this->render('samples/adm_visit_dokter');
	}

	function adm_form_visit_dokter() {
		$this->title = 'Form Visit Dokter';
		$this->render('samples/adm_form_visit_dokter');
	}

	function adm_obat_dan_alkes() {
		$this->title = 'Daftar Obat dan Alkes';
		$this->render('samples/adm_obat_dan_alkes');
	}

	function adm_form_obat_dan_alkes() {
		$this->title = 'Form Obat dan Alkes';
		$this->render('samples/adm_form_obat_dan_alkes');
	}

	function adm_laboratorium() {
		$this->title = 'Daftar Layanan Laboratorium';
		$this->render('samples/adm_laboratorium');
	}

	function adm_form_laboratorium() {
		$this->title = 'Form Layanan Laboratorium';
		$this->render('samples/adm_form_laboratorium');
	}

	function adm_tindakan() {
		$this->title = 'Daftar Tindakan';
		$this->render('samples/adm_tindakan');
	}

	function adm_form_tindakan() {
		$this->title = 'Form Tindakan';
		$this->render('samples/adm_form_tindakan');
	}

	// function adm_pasien_rawat_inap() {
	// 	$this->title = 'Daftar Pasien Rawat Inap';
	// 	$this->render('samples/adm_pasien_rawat_inap');
	// }


	function pg_obat() {
		$this->title = "Daftar Obat";
		$this->layout = 'main_pg';
		$this->render('samples/pg_table');
	}

	function pg_pengambilan_rawat_inap() {
		$this->title = "Daftar History Pengambilan Obat Ke Gudang Rawat Inap";
		$this->layout = 'main_pg';
		$this->render('samples/pg_table_pengambilan_rawat_inap');
	}

	function pg_detail_pengambilan_rawat_inap() {
		$this->title = "Daftar Detail Pengambilan Obat Ke Gudang Rawat Inap";
		$this->layout = 'main_pg';
		$this->render('samples/pg_detail_table_pengambilan_rawat_inap');
	}

	function pg_pengambilan_rawat_jalan() {
		$this->title = "Daftar History Pengambilan Obat Ke Gudang Rawat Jalan";
		$this->layout = 'main_pg';
		$this->render('samples/pg_table_pengambilan_rawat_jalan');
	}

	function pg_detail_pengambilan_rawat_jalan() {
		$this->title = "Daftar Detail Pengambilan Obat Ke Gudang Rawat Jalan";
		$this->layout = 'main_pg';
		$this->render('samples/pg_detail_table_pengambilan_rawat_jalan');
	}

	function pg_form_pengambilan_rawat_inap() {
		$this->title = 'Form Pengambilan Obat Ke Gudang Rawat Inap';
		$this->layout = 'main_pg';
		$this->render('samples/pg_form_pengambilan_rawat_inap');
	}

	function pg_form_pengambilan_rawat_jalan() {
		$this->title = 'Form Pengambilan Obat Ke Gudang Rawat Jalan';
		$this->layout = 'main_pg';
		$this->render('samples/pg_form_pengambilan_rawat_jalan');
	}

	function pg_table_resep_rawat_inap() {
		$this->title = 'Daftar History Resep Rawat Inap';
		$this->layout = 'main_pg';
		$this->render('samples/pg_table_resep_rawat_inap');
	}

	function pg_detail_table_resep_rawat_inap() {
		$this->title = 'Daftar Detail Obat Resep Rawat Inap';
		$this->layout = 'main_pg';
		$this->render('samples/pg_detail_table_resep_rawat_inap');
	}

	function pg_form_resep_rawat_inap() {
		$this->title = 'Input Resep Rawat Inap';
		$this->layout = 'main_pg';
		$this->render('samples/pg_form_resep_rawat_inap');
	}

	function pg_table_resep_rawat_jalan() {
		$this->title = 'Daftar History Resep Rawat Jalan';
		$this->layout = 'main_pg';
		$this->render('samples/pg_table_resep_rawat_jalan');
	}

	function pg_detail_table_resep_rawat_jalan() {
		$this->title = 'Daftar Detail Obat Resep Rawat Jalan';
		$this->layout = 'main_pg';
		$this->render('samples/pg_detail_table_resep_rawat_jalan');
	}

	function pg_form_resep_rawat_jalan() {
		$this->title = 'Input Resep Rawat Jalan';
		$this->layout = 'main_pg';
		$this->render('samples/pg_form_resep_rawat_jalan');
	}

	function pg_table_pemesanan() {
		$this->title = 'Daftar Permintaan';
		$this->layout = 'main_pg';
		$this->render('samples/pg_table_pemesanan');
	}

	function pg_table_history_pemesanan() {
		$this->title = 'Daftar History Permintaan';
		$this->layout = 'main_pg';
		$this->render('samples/pg_table_history_pemesanan');
	}

	function pg_table_detail_history_pemesanan() {
		$this->title = 'Detail History Pemesanan';
		$this->layout = 'main_pg';
		$this->render('samples/pg_detail_table_history_pemesanan');
	}

	function login() {
		$this->load->view('samples/login');
	}

	function man_grafik() {
		$this->title = 'Nilai Persediaan';
		$this->layout = 'main_man';
		$this->render('samples/man_grafik');
	}
}