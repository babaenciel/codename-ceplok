<?php

class Visit_dokter extends MY_Controller {
    function __construct() {
        parent::__construct();
        access_check(array(USER_ADMINISTRASI));

        $this->load->model('visit_dokter_model', 'visit_dokter');
        $this->load->model('rawat_inap_model', 'rawat_inap');
        $this->title = 'Visit Dokter';
    }

    // $id = id rawat inap
    function index($id) { 
        $rawat_inap = $this->rawat_inap->get_with_pasien($id);        
        $this->title .= ' - ' . $rawat_inap->nama_pasien . ' - ' . conv_date_format($rawat_inap->tgl_masuk, 'd-m-Y');       

        $data['data'] = $this->visit_dokter->get_all_visit($id);        
        $data['id'] = $id;        

        $this->render('visit_dokter/index', $data);
    }

    // $id = id rawat inap
    function add($id) {
        $this->load->model('dokter_model', 'dokter');
        $this->load->model('tarif_visit_model', 'tarif_visit');

        $rawat_inap = $this->rawat_inap->get_with_pasien($id);
        $this->title .= ' - ' . $rawat_inap->nama_pasien . ' - ' . conv_date_format($rawat_inap->tgl_masuk, 'd-m-Y');

        if($_POST) {
            $this->config->load('form_validation');
            $rules = $this->config->item('visit_dokter/add');

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                $this->visit_dokter->insert(array(
                    'id_dokter' => $this->input->post('dokter'),
                    'id_tarif_visit' => $this->input->post('jenis_visit'),
                    'id_rawat_inap' => $id,
                    'tanggal_visit' => conv_date_format($this->input->post('tanggal_visit'), 'Y-m-d')
                    // 'waktu_visit' => $this->input->post('waktu_visit')
                ));

                redirect('visit_dokter/index/'.$id);
            }else {
                $data['errors'] = validation_errors();
            }
        }

        $data['dokter_dropdown'] = $this->dokter->get_dropdown();
        $data['jenis_visit_dropdown'] = $this->tarif_visit->get_dropdown();        
        $data['form'] = $this->visit_dokter->init_data();
        $data['id'] = $id;

        $this->render('visit_dokter/add', $data);
    }

    // $id = id visit dokter
    function edit($id) {
        $this->load->model('dokter_model', 'dokter');
        $this->load->model('tarif_visit_model', 'tarif_visit');
        
        $data['dokter_dropdown'] = $this->dokter->get_dropdown();
        $data['jenis_visit_dropdown'] = $this->tarif_visit->get_dropdown();

        $data_visit = $this->visit_dokter->get($id);
        $data['form'] = $this->visit_dokter->init_data($data_visit);
        $data['id'] = $id;

        $rawat_inap = $this->rawat_inap->get_with_pasien($data_visit->id_rawat_inap);
        $this->title .= ' - ' . $rawat_inap->nama_pasien . ' - ' . conv_date_format($rawat_inap->tgl_masuk, 'd-m-Y');

        if($_POST) {
            $this->config->load('form_validation');
            $rules = $this->config->item('visit_dokter/add');

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                $this->visit_dokter->update($id, array(
                    'id_dokter' => $this->input->post('dokter'),
                    'id_tarif_visit' => $this->input->post('jenis_visit'),
                    'id_rawat_inap' => $data_visit->id_rawat_inap,
                    'tanggal_visit' => conv_date_format($this->input->post('tanggal_visit'), 'Y-m-d')
                    // 'waktu_visit' => $this->input->post('waktu_visit')
                ));

                redirect('visit_dokter/index/'.$data_visit->id_rawat_inap);
            }else {
                $data['errors'] = validation_errors();
            }
        }

        $this->render('visit_dokter/edit', $data);
    }

    // $id = id visit dokter
    function delete($id) {
        $data_visit = $this->visit_dokter->get($id);
        $this->visit_dokter->delete($id);

        redirect('visit_dokter/index/'.$data_visit->id_rawat_inap);
    }
}