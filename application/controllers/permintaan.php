<?php

class Permintaan extends MY_Controller {
	function __construct() {
		parent::__construct();
		access_check(array(USER_GUDANG));

        $this->load->model('obat_model', 'obat');
	}

	function index() {
        $this->title = 'Daftar Permintaan';

		$data['data'] = $this->obat->get_all_reorder_point_limit();

		$this->render('permintaan/index', $data);
	}

	// $id = id katalog obat
	function edit($id) {        
		$data['data'] = $this->obat->get($id);

        $this->title = 'Form Permintaan - ' . $data['data']->nama_obat;

		if($_POST) {
			$this->config->load('form_validation');
            $rules = $this->config->item('permintaan/edit');
            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
            	$this->obat->update($id, array(
            		'reorder_point' => $this->input->post('reorder_point'),
            		'economic_order_quantity' => $this->input->post('economic_order_quantity')
            	));

            	redirect('permintaan');
            }else {
            	$data['errors'] = validation_errors();
            }
		}

		$data['id'] = $id;

		$this->render('permintaan/edit', $data);
	}

    function cetak() {
        if($_POST) {
            $this->load->model('surat_permintaan_model', 'surat_permintaan');
            $this->load->model('sp_detail_model', 'sp_detail');

            $data_obat = $this->input->post('check');
            $data['daftar_permintaan'] = $this->obat->get_obat_for_permintaan($data_obat);
            // dump($data['daftar_permintaan']);

            $id_surat_permintaan = $this->surat_permintaan->insert(array(
                 'tanggal_sp' => date('Y-m-d')
            ));

            foreach($data['daftar_permintaan'] as $rows) {
                $this->sp_detail->insert(array(
                    'id_surat_permintaan' => $id_surat_permintaan,
                    'id_katalog_obat' => $rows->id_katalog_obat
                ));
            }

            // dump($data['daftar_permintaan']);

            $data['tanggal_sp'] = date('d-m-Y');
            $data['no_sp'] = $id_surat_permintaan;

            foreach($data['daftar_permintaan'] as $keys => $rows) {
                if(!empty($rows->id_supplier)) {
                    $arr = explode(',', $rows->id_supplier);
                    $temp = array();
                    $new_string1 = $new_string2 = $new_string3 = $new_string4 = $new_string5 = $new_string6 = '';

                    foreach($arr as $keys2=>$rows2) {
                        if(in_array($rows2, $temp)) {
                            // skip
                        }else {
                            $temp[] = $rows2;

                            $arr1 = explode(',', $rows->nama_supplier);
                            $arr2 = explode(',', $rows->harga_beli_per_dus);
                            $arr3 = explode(',', $rows->nama_medrep);
                            $arr4 = explode(',', $rows->telepon_medrep);
                            $arr5 = explode(',', $rows->quantity_per_dus);

                            $new_string1 .= $arr1[$keys2] . '/ ';
                            $new_string2 .= format_number($arr2[$keys2], TRUE) . '/ ';
                            $new_string3 .= $arr3[$keys2] . '/ ';
                            $new_string4 .= $arr4[$keys2] . '/ ';
                            $new_string5 .= $arr5[$keys2] . '/ ';

                            $jumlah_dus = ceil($rows->economic_order_quantity / $arr5[$keys2]);
                            $new_string6 .= $jumlah_dus . '/ ';
                        }
                    }

                    $new_string1 = substr($new_string1, 0, -2);
                    $new_string2 = substr($new_string2, 0, -2);
                    $new_string3 = substr($new_string3, 0, -2);
                    $new_string4 = substr($new_string4, 0, -2);
                    $new_string5 = substr($new_string5, 0, -2);
                    $new_string6 = substr($new_string6, 0, -2);

                    $data['daftar_permintaan'][$keys]->nama_supplier = $new_string1;
                    $data['daftar_permintaan'][$keys]->harga_beli_per_dus = $new_string2;
                    $data['daftar_permintaan'][$keys]->nama_medrep = $new_string3;
                    $data['daftar_permintaan'][$keys]->telepon_medrep = $new_string4;
                    $data['daftar_permintaan'][$keys]->quantity_per_dus = $new_string5;
                    $data['daftar_permintaan'][$keys]->jumlah_dus = $new_string6;
                }

                // $new_string = '';
                // if(!empty($rows->nama_supplier)) {
                //     $arr = explode(',', $rows->nama_supplier);
                //     $arr = array_unique($arr);
                //     $temp = array();
                //     foreach($arr as $rows2) {
                //         $new_string .= $rows2 . '/ ';
                //     }
                //     $new_string = substr($new_string, 0, -2);

                //     $data['daftar_permintaan'][$keys]->nama_supplier = $new_string;
                // }

                // $new_string = '';
                // if(!empty($rows->harga_beli_per_dus)) {
                //     $arr = explode(',', $rows->harga_beli_per_dus);
                //     $arr = array_unique($arr);
                //     foreach($arr as $rows2) {
                //         $new_string .= format_number($rows2, TRUE) . '/ ';
                //     }
                //     $new_string = substr($new_string, 0, -2);

                //     $data['daftar_permintaan'][$keys]->harga_beli_per_dus = $new_string;
                // }

                // $new_string = '';
                // if(!empty($rows->nama_medrep)) {
                //     $arr = explode(',', $rows->nama_medrep);
                //     $arr = array_unique($arr);
                //     foreach($arr as $rows2) {
                //         $new_string .= $rows2 . '/ ';
                //     }
                //     $new_string = substr($new_string, 0, -2);

                //     $data['daftar_permintaan'][$keys]->nama_medrep = $new_string;
                // }

                // $new_string = '';
                // if(!empty($rows->telepon_medrep)) {
                //     $arr = explode(',', $rows->telepon_medrep);
                //     $arr = array_unique($arr);
                //     foreach($arr as $rows2) {
                //         $new_string .= $rows2 . '/ ';
                //     }
                //     $new_string = substr($new_string, 0, -2);

                //     $data['daftar_permintaan'][$keys]->telepon_medrep = $new_string;
                // }

                // $new_string = '';
                // if(!empty($rows->quantity_per_dus)) {
                //     $arr = explode(',', $rows->quantity_per_dus);
                //     $arr = array_unique($arr);
                //     foreach($arr as $rows2) {
                //         $new_string .= $rows2 . '/ ';
                //     }
                //     $new_string = substr($new_string, 0, -2);

                //     $data['daftar_permintaan'][$keys]->quantity_per_dus = $new_string;
                // }

                // $new_string = '';
                // if(!empty($rows->lead_time)) {
                //     $arr = explode(',', $rows->lead_time);
                //     foreach($arr as $rows2) {
                //         $new_string .= $rows2 . ' Minggu/ ';
                //     }
                //     $new_string = substr($new_string, 0, -2);

                //     $data['daftar_permintaan'][$keys]->lead_time = $new_string;
                // }
            }

            $data['pembuat_nota'] = $this->session->userdata('user')->username;

            $this->load->view('permintaan/_nota_permintaan', $data);
            $this->load->helper('dompdf');
            // page info here, db calls, etc.
            $html = $this->load->view('permintaan/_nota_permintaan', $data, true);
            // dump($html);
            pdf_create($html, 'Surat_permintaan-'.date('d-m-Y'));
        }
    }

    function history() {
        $this->title = 'History Permintaan';

        $this->load->model('surat_permintaan_model', 'surat_permintaan');

        $data['data'] = $this->surat_permintaan->get_all();
        $this->render('permintaan/history', $data);
    }

    // $id = id surat permintaan
    function detail_history($id) {
        $this->title = 'Detail History Permintaan';

        $this->load->model('sp_detail_model', 'sp_detail');

        $data['data'] = $this->sp_detail->get_sp_detail_with_katalog_obat($id);
        $this->render('permintaan/detail_history', $data);
    }

    function delete($id) {
        $this->load->model('surat_permintaan_model', 'surat_permintaan');
        $this->surat_permintaan->delete($id);
        redirect('permintaan/history');
    }
}