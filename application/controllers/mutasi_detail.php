<?php

class Mutasi_detail extends MY_Controller {
	private $gudang = 'rawat_inap';
    private $label_gudang = 'Rawat Inap';

	function __construct() {
		parent::__construct();

		access_check(array(USER_GUDANG));

        $this->load->model('mutasi_obat_model', 'mutasi_obat');

        if($this->uri->segment(2) === 'rawat_inap') {
              $this->gudang = 'rawat_inap';
              $this->label_gudang = 'Rawat Inap';
        }else if($this->uri->segment(2) === 'rawat_jalan') {
              $this->gudang = 'rawat_jalan';
              $this->label_gudang = 'Rawat Jalan';
        }
	}
	// $id = id mutasi obat
	function index($id) {
        $mutasi = $this->mutasi_obat->get($id);

        $this->title = 'Mutasi '.$this->label_gudang.' Detail - ' . $id . ' - ' . conv_date_format($mutasi->tanggal_mutasi, 'd-m-Y');

		$this->load->model('mutasi_obat_detail_model', 'mutasi_obat_detail');

		$data['data'] = $this->mutasi_obat_detail->get_all_with_obat($id);
		$data['id'] = $id;
		$data['gudang'] = $this->gudang;

		$this->render('mutasi_detail/index', $data);
	}

	// $id = id mutasi obat detail
	function delete($id) {
		$this->load->model('obat_model', 'obat');
		$this->load->model('mutasi_obat_detail_model', 'mutasi_obat_detail');

		$mutasi_obat_detail = $this->mutasi_obat_detail->get($id);

		$this->obat->add_jumlah_pusat($mutasi_obat_detail->id_katalog_obat, $mutasi_obat_detail->jumlah_mutasi);
		$this->obat->minus_jumlah_mutasi($mutasi_obat_detail->id_katalog_obat, $this->gudang, $mutasi_obat_detail->jumlah_mutasi);

		$this->mutasi_obat_detail->delete($id);

		redirect('mutasi_detail/'.$this->gudang.'/index/'.$mutasi_obat_detail->id_mutasi_obat);
	}
}