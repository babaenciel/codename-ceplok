<?php

class Administration extends MY_Controller {
    function __construct() {
        parent::__construct();
        access_check(array(USER_GUDANG));
    }

    function supplier_manage() {
        $this->title = 'Manajemen Supplier';

        $this->load->model('supplier_model', 'supplier');

        $data['data'] = $this->supplier->get_all();
        $this->render('administration/supplier/manage', $data);
    }

    function supplier_add() {
        $this->title = 'Tambah Supplier';

        $this->load->model('supplier_model', 'supplier');

        if($_POST) {
            $this->config->load('form_validation');
            $rules = $this->config->item('supplier/add');

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                $this->supplier->insert(array(
                    'nama_supplier' => $this->input->post('nama_supplier'),
                    'nama_medrep' => $this->input->post('nama_medrep'),
                    'telepon_medrep' => $this->input->post('telepon_medrep')
                ));

                redirect('administration/supplier_manage');
            }else {
                $data['errors'] = validation_errors();
            }
        }

        $data['form'] = $this->supplier->init_field();
        $this->render('administration/supplier/add', $data);
    }

    // $id = id supplier
    function supplier_edit($id) {
        $this->title = 'Edit Supplier';

        $this->load->model('supplier_model', 'supplier');

        $supplier = $this->supplier->get($id);
        $data['form'] = $this->supplier->init_field($supplier);
        $data['id'] = $id;

        if($_POST) {
            $this->config->load('form_validation');
            $rules = $this->config->item('supplier/add');

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                $this->supplier->update($id, array(
                    'nama_supplier' => $this->input->post('nama_supplier'),
                    'nama_medrep' => $this->input->post('nama_medrep'),
                    'telepon_medrep' => $this->input->post('telepon_medrep')
                ));

                redirect('administration/supplier_manage');
            }else {
                $data['errors'] = validation_errors();
            }
        }

        $this->render('administration/supplier/edit', $data);
    }

    function supplier_delete($id) {
        $this->load->model('supplier_model', 'supplier');

        $this->supplier->delete($id);

        redirect('administration/supplier_manage');
    }
}