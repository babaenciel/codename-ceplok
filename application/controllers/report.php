<?php

class Report extends MY_Controller {
    function __construct() {
        parent::__construct();
        access_check(array(USER_GUDANG));
        ini_set('memory_limit', '2048M');
    }

    function monthly($type=NULL) {
        $this->title = 'Laporan Bulanan';

        if($_POST) {
            $this->form_validation->set_rules('bulan', 'Bulan', '');
            $this->form_validation->set_rules('tahun', 'Tahun', '');

            if($this->form_validation->run()) {
                $this->load->model('report_model', 'report');
                $this->load->helper('dompdf');

                $month = $this->input->post('bulan');
                $year = $this->input->post('tahun');

                $d = new DateTime( $year.'-'.$month.'-01' );
                $data['month_text'] = $d->format('F');
                $data['year'] = $year;
                $d->modify( 'previous month' );
                $month_before = $d->format('m');
                $year_before = $d->format('Y');

                if($type === 'pusat') {
                    $result = $this->_hitung_pusat($month, $year, $month_before, $year_before, $type);
                }else if($type == 'rawat_inap') {
                    $result = $this->_hitung_rawat($month, $year, $month_before, $year_before, $type);
                }else if($type == 'rawat_jalan') {
                    $result = $this->_hitung_rawat($month, $year, $month_before, $year_before, $type);
                }

                $data['result'] = $result['result'];
                $data['total_persediaan'] = $result['total_persediaan'];
                $data['type'] = $type;
                $data['pembuat_laporan'] = $this->session->userdata('user')->username;

                // dump($data);
                // // testing
                // $this->load->view('report/_report_monthly', $data);

                // page info here, db calls, etc.
                $html = $this->load->view('report/_report_monthly', $data, TRUE);
                // dump($html);
                pdf_create($html, 'laporan_persediaan_'.$type.'_'.$month.'-'.$year);
            }
        }

        $data['bulan'] = array('01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei', '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'November', '12'=>'Desember');

        for($i = 1990; $i < 2050; $i++) {
            $data['tahun'][$i] = $i;
        }

        $this->render('report/monthly', $data);
    }

    function _hitung_pusat($month, $year, $month_before, $year_before, $type) {
        $pusat_persediaan_awal = $this->report->get_pusat_persediaan_awal($month_before, $year_before, $type);
        $pusat_penerimaan = $this->report->get_pusat_penerimaan($month, $year);
        $pusat_pengeluaran = $this->report->get_pusat_pengeluaran($month, $year);
        // dump($pusat_persediaan_awal);
        // dump($pusat_penerimaan);

        $result = array();
        $total_persediaan = 0;
        foreach($pusat_persediaan_awal as $keys => $rows) {
            $result[$keys]['id_katalog_obat'] = $rows->id_katalog_obat;
            $result[$keys]['nama_obat'] = $rows->nama_obat;
            $result[$keys]['nama_kategori'] = $rows->nama_kategori;
            $result[$keys]['persediaan_awal'] = $rows->persediaan_pusat;
            $result[$keys]['penerimaan'] = $pusat_penerimaan[$keys]->sum_jumlah_penerimaan;
            $result[$keys]['pengeluaran'] = $pusat_pengeluaran[$keys]->sum_jumlah_mutasi;
            $result[$keys]['sisa'] = $result[$keys]['persediaan_awal'] + $result[$keys]['penerimaan'] - $result[$keys]['pengeluaran'];
            $result[$keys]['harga_satuan'] = $rows->harga_satuan;
            $result[$keys]['nilai_persediaan'] = $result[$keys]['sisa'] * $result[$keys]['harga_satuan'];
            $total_persediaan += $result[$keys]['nilai_persediaan'];
        }
        // dump($result);
        $data['result'] = $result;
        $data['total_persediaan'] = $total_persediaan;

        return $data;
    }

    function _hitung_rawat($month, $year, $month_before, $year_before, $type) {
        $rawat_inap_persediaan_awal = $this->report->get_rawat_persediaan_awal($month_before, $year_before, $type);
        // dump($rawat_inap_persediaan_awal);
        $rawat_inap_penerimaan = $this->report->get_mutasi($month, $year, $type);
        // dump($rawat_inap_penerimaan);
        $rawat_inap_pengeluaran = $this->report->get_resep($month, $year, $type);
        // dump($rawat_inap_pengeluaran);

        $result = array();
        $total_persediaan = 0;
        foreach($rawat_inap_persediaan_awal as $keys => $rows) {
            $result[$keys]['id_katalog_obat'] = $rows->id_katalog_obat;
            $result[$keys]['nama_obat'] = $rows->nama_obat;
            $result[$keys]['nama_kategori'] = $rows->nama_kategori;

            if($type == 'rawat_inap') {
                $result[$keys]['persediaan_awal'] = $rows->persediaan_rawat_inap;
            }else {
                $result[$keys]['persediaan_awal'] = $rows->persediaan_rawat_jalan;
            }

            $result[$keys]['penerimaan'] = $rawat_inap_penerimaan[$keys]->jumlah_mutasi;
            $result[$keys]['pengeluaran'] = $rawat_inap_pengeluaran[$keys]->jumlah_pengeluaran;
            $result[$keys]['sisa'] = $result[$keys]['persediaan_awal'] + $result[$keys]['penerimaan'] - $result[$keys]['pengeluaran'];
            $result[$keys]['harga_satuan'] = $rows->harga_satuan;
            $result[$keys]['nilai_persediaan'] = $result[$keys]['sisa'] * $result[$keys]['harga_satuan'];
            $total_persediaan += $result[$keys]['nilai_persediaan'];
        }
        // dump($result);
        $data['result'] = $result;
        $data['total_persediaan'] = $total_persediaan;

        return $data;
    }
}