<?php

class Pasien_rawat_inap extends MY_Controller {
    function __construct() {
        parent::__construct();
        access_check(array(USER_ADMINISTRASI));

        $this->load->model('pasien_model', 'pasien');
        $this->load->model('rawat_inap_model', 'rawat_inap');
    }

    function index() {
        $this->title = 'Daftar Pasien Rawat Inap';
        $data['data'] = $this->pasien->get_pasien_with_rawat_inap();        

        $this->render('pasien_rawat_inap/index', $data);
    }

    // $id = id pasien
    function view($id) {
        $this->title = 'Detail Pasien';
        $this->load->model('rawat_inap_model', 'rawat_inap');

        $data['data'] = $this->pasien->get($id);
        $data['rawat_inap'] = $this->rawat_inap->get_all_with_dokter($id);
        // dump($data['rawat_inap']);
        $this->render('pasien/view', $data);
    }

    function add() {
        $this->title = 'Tambah Pasien Rawat Inap';

        $this->load->model('pasien_model', 'pasien');
        $this->load->model('dokter_model', 'dokter');
        $this->load->model('rawat_inap_model', 'rawat_inap');

        if($_POST) {
            $this->config->load('form_validation');
            $rules = $this->config->item('pasien_rawat_inap/add');

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                $input = $this->rawat_inap->populate_form_field($this->input->post());
                $this->rawat_inap->insert($input);

                redirect('pasien_rawat_inap');
            }else {
                $data['errors'] = validation_errors();
            }
        }

        $data['pasien_dropdown'] = $this->pasien->get_dropdown_rawat_inap();
        $data['dokter_dropdown'] = $this->dokter->dropdown('nama_dokter');
        $data['kesadaran_dropdown'] = $this->rawat_inap->get_kesadaran_dropdown();
        $data['form'] = $this->rawat_inap->init_form_field();

        // if($id !== NULL) {
        //     $data['form']['pasien'] = $id;
        // }

        $this->render('pasien_rawat_inap/add', $data);
    }
    // $id = id rawat inap
    function edit($id) {
        $this->title = 'Edit Pasien Rawat Inap';

        $this->load->model('pasien_model', 'pasien');
        $this->load->model('dokter_model', 'dokter');
        $this->load->model('rawat_inap_model', 'rawat_inap');

        $pasien_rawat_inap = $this->rawat_inap->get_with_pasien($id);
        $data['form'] = $this->rawat_inap->init_form_field($pasien_rawat_inap);

        if($_POST) {
            $this->config->load('form_validation');
            $rules = $this->config->item('pasien_rawat_inap/add');

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                $input = $this->rawat_inap->populate_form_field($this->input->post());
                $this->rawat_inap->update($id, $input);

                redirect('pasien_rawat_inap');
            }else {
                $data['errors'] = validation_errors();
            }
        }

        $data['pasien_dropdown'] = $this->pasien->get_dropdown_rawat_inap();
        $data['dokter_dropdown'] = $this->dokter->dropdown('nama_dokter');
        $data['kesadaran_dropdown'] = $this->rawat_inap->get_kesadaran_dropdown();
        $data['id'] = $id;

        $this->render('pasien_rawat_inap/edit', $data);
    }

    // $id = id rawat inap
    function pulang($id) {
        $this->load->model('pemakaian_kamar_model', 'pemakaian_kamar');
        $this->load->model('gizi_model', 'gizi');

        $this->rawat_inap->update($id, array(
            'tgl_keluar'=> date('Y-m-d'),
            'status' => 0
        ));

        $this->gizi->set_tanggal_akhir($id);
        $this->pemakaian_kamar->set_tanggal_pulang($id);

        // redirect('pasien_rawat_inap');
    }

    // $id = id rawat inap
    function history_pelayanan($id) {
        $this->title = 'History Pelayanan';

        $this->load->model('nota_model', 'nota');

        $data['rawat_inap'] = $this->rawat_inap->get_with_pasien($id);
        $data['history'] = $this->nota->get_data($id, FALSE);
        
        // dump($data['history']);
        $this->render('pasien_rawat_inap/history_pelayanan', $data);
    }
}