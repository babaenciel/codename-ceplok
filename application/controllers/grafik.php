<?php

class Grafik extends MY_Controller {
    function index() {
        $year = date('Y');

        if($_GET) {
            $this->form_validation->set_rules('year', 'Tahun', 'required');
            $this->form_validation->run();
            $year = $this->input->get('year');
        }

        $this->load->model('report_model', 'report');
        $type = array('pusat', 'rawat_inap', 'rawat_jalan');
        $month_a_year = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
        // $nilai_persediaan = array('01' => 0, '02' => 0, '03' => 0, '04' => 0, '05' => 0, '06' => 0, '07' => 0, '08' => 0, '09' => 0, '10' => 0, '11' => 0, '12' => 0);
        $nilai_persediaan = array();

        foreach($type as $rows) {
            foreach($month_a_year as $keys=>$month) {
                $d = new DateTime( $year.'-'.$month.'-01' );
                $data['month_text'] = $d->format('F');
                $data['year'] = $year;
                $d->modify( 'previous month' );
                $month_before = $d->format('m');
                $year_before = $d->format('Y');
                // echo $month;
                // echo $year;
                // echo "<br>";
                if($rows === 'pusat') {
                    $result = $this->_hitung_pusat($month, $year, $month_before, $year_before);
                }else if($rows == 'rawat_inap') {
                    $result = $this->_hitung_rawat($month, $year, $month_before, $year_before, $rows);
                }else if($rows == 'rawat_jalan') {
                    $result = $this->_hitung_rawat($month, $year, $month_before, $year_before, $rows);
                }

                // $data['result'] = $result['result'];
                // $data['total_persediaan'] = $result['total_persediaan'];
                // $data['type'] = $type;
                // echo $result['total_persediaan'];
                // echo "<br>";
                $nilai_persediaan[$rows][$keys] = $result['total_persediaan'];
            }
        }

        $data['pusat'] = json_encode($nilai_persediaan['pusat']);
        $data['rawat_inap'] = json_encode($nilai_persediaan['rawat_inap']);
        $data['rawat_jalan'] = json_encode($nilai_persediaan['rawat_jalan']);

        $year_dropdown = array();
        for($i = 1990; $i < 2050; $i++) {
            $year_dropdown[$i] = $i;
        }

        $data['year_dropdown'] = $year_dropdown;
        $data['year'] = $year;

        $this->render('grafik/index', $data);
    }

    function detail($type, $month, $year) {
        // $this->load->model('log_katalog_obat_model', 'log_katalog_obat');
        // $this->load->model('penerimaan_obat_detail_model', 'penerimaan_obat_detail');
        // $data = array();

        // if($type == '1') {
        //     $month = $this->log_katalog_obat->olah_tanggal($month);
        //     $log = $this->log_katalog_obat->get_log($month, $year, $type);
        //     $harga_satuan_obat = $this->penerimaan_obat_detail->get_for_grafik_detail();

        //     foreach($log as $keys=>$rows) {
        //         $data[$keys]['nama_obat'] = $rows->nama_obat;
        //         $data[$keys]['berat_netto'] = $rows->berat_netto;
        //         $data[$keys]['jenis'] = $rows->nama_kategori;
        //         $data[$keys]['jumlah_persediaan'] = $rows->persediaan_pusat;
        //         $data[$keys]['harga_satuan'] = $harga_satuan_obat[$keys]->harga_satuan;
        //         $data[$keys]['nilai_persediaan'] = $data[$keys]['jumlah_persediaan'] * $data[$keys]['harga_satuan'];
        //     }
        // }else if($type == '2') {

        // }else {

        // }

        // $data['data'] = $data;
        // $this->render('grafik/detail', $data);

        if($type == 'pusat') {
            $type_label = 'Pusat';
        }else if($type == 'rawat_inap') {
            $type_label = 'Rawat Inap';
        }else if($type == 'rawat_jalan') {
            $type_label = 'Rawat Jalan';
        }

        $this->load->model('log_katalog_obat_model', 'log_katalog_obat');
        $this->load->model('report_model', 'report');

        $month = $this->log_katalog_obat->olah_tanggal($month);

        $d = new DateTime( $year.'-'.$month.'-01' );
        $data['month_text'] = $d->format('F');
        $data['year'] = $year;
        $d->modify( 'previous month' );
        $month_before = $d->format('m');
        $year_before = $d->format('Y');

        $this->title = 'Detail Grafik ' . $type_label . ' - ' . $data['month_text'] . ' ' . $data['year'] ;

        if($type === 'pusat') {
            $result = $this->_hitung_pusat($month, $year, $month_before, $year_before, $type);
        }else if($type == 'rawat_inap') {
            $result = $this->_hitung_rawat($month, $year, $month_before, $year_before, $type);
        }else if($type == 'rawat_jalan') {
            $result = $this->_hitung_rawat($month, $year, $month_before, $year_before, $type);
        }

        $data['result'] = $result['result'];
        $data['total_persediaan'] = $result['total_persediaan'];
        $data['type'] = $type;

        // dump($data);
        $this->render('grafik/detail', $data);
    }

    function _hitung_pusat($month, $year, $month_before, $year_before) {
        $pusat_persediaan_awal = $this->report->get_pusat_persediaan_awal($month_before, $year_before);
        $pusat_penerimaan = $this->report->get_pusat_penerimaan($month, $year);
        $pusat_pengeluaran = $this->report->get_pusat_pengeluaran($month, $year);
        // dump($pusat_persediaan_awal);
        // if($month == '04') {
        //     dump($pusat_penerimaan);
        // }
        // dump($month);

        $result = array();
        $total_persediaan = 0;
        foreach($pusat_persediaan_awal as $keys => $rows) {
            $result[$keys]['id_katalog_obat'] = $rows->id_katalog_obat;
            $result[$keys]['nama_obat'] = $rows->nama_obat;
            $result[$keys]['nama_kategori'] = $rows->nama_kategori;
            // $result[$keys]['berat_netto'] = $rows->berat_netto;
            $result[$keys]['persediaan_awal'] = $rows->persediaan_pusat;
            $result[$keys]['penerimaan'] = $pusat_penerimaan[$keys]->sum_jumlah_penerimaan;
            $result[$keys]['pengeluaran'] = $pusat_pengeluaran[$keys]->sum_jumlah_mutasi;
            $result[$keys]['sisa'] = $result[$keys]['persediaan_awal'] + $result[$keys]['penerimaan'] - $result[$keys]['pengeluaran'];
            $result[$keys]['harga_satuan'] = $rows->harga_satuan;
            $result[$keys]['nilai_persediaan'] = $result[$keys]['sisa'] * $result[$keys]['harga_satuan'];
            $total_persediaan += $result[$keys]['nilai_persediaan'];
        }
        // dump($result);
        $data['result'] = $result;
        $data['total_persediaan'] = $total_persediaan;

        return $data;
    }

    function _hitung_rawat($month, $year, $month_before, $year_before, $type) {
        $rawat_inap_persediaan_awal = $this->report->get_rawat_persediaan_awal($month_before, $year_before, $type);
        // dump($rawat_inap_persediaan_awal);
        $rawat_inap_penerimaan = $this->report->get_mutasi($month, $year, $type);
        // dump($rawat_inap_penerimaan);
        $rawat_inap_pengeluaran = $this->report->get_resep($month, $year, $type);
        // dump($rawat_inap_pengeluaran);

        $result = array();
        $total_persediaan = 0;
        foreach($rawat_inap_persediaan_awal as $keys => $rows) {
            $result[$keys]['id_katalog_obat'] = $rows->id_katalog_obat;
            $result[$keys]['nama_obat'] = $rows->nama_obat;
            $result[$keys]['nama_kategori'] = $rows->nama_kategori;
            // $result[$keys]['berat_netto'] = $rows->berat_netto;

            if($type == 'rawat_inap') {
                $result[$keys]['persediaan_awal'] = $rows->persediaan_rawat_inap;
            }else {
                $result[$keys]['persediaan_awal'] = $rows->persediaan_rawat_jalan;
            }

            $result[$keys]['penerimaan'] = $rawat_inap_penerimaan[$keys]->jumlah_mutasi;
            $result[$keys]['pengeluaran'] = $rawat_inap_pengeluaran[$keys]->jumlah_pengeluaran;
            $result[$keys]['sisa'] = $result[$keys]['persediaan_awal'] + $result[$keys]['penerimaan'] - $result[$keys]['pengeluaran'];
            $result[$keys]['harga_satuan'] = $rows->harga_satuan;
            $result[$keys]['nilai_persediaan'] = $result[$keys]['sisa'] * $result[$keys]['harga_satuan'];
            $total_persediaan += $result[$keys]['nilai_persediaan'];
        }
        // dump($result);
        $data['result'] = $result;
        $data['total_persediaan'] = $total_persediaan;

        return $data;
    }
}