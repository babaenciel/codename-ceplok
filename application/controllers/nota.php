<?php

class Nota extends MY_Controller {
    function __construct() {
        parent::__construct();
        access_check(array(USER_GUDANG, USER_ADMINISTRASI));

        $this->load->model('nota_model', 'nota');
    }

    // id = id rawat inap
    function index($id) {
        $this->title = 'Nota Pasien';
        $data = $this->nota->get_data($id);
        $data['nomor_nota'] = $this->nota->get_nomor_nota($id);

        $this->render('nota/index', $data);
    }

    // id = id rawat inap
    function rincian($id) {
        $data['form']['sudah_terima_dari'] = 'Agus';
        $data['form']['alamat_dari'] = 'Bandung';
        $data['form']['untuk'] = 'Bebi';
        $data['form']['alamat_untuk'] = 'Bandung Timur';

        $this->render('nota/rincian', $data);
    }

    function cetak_umum($id) {
        if($_POST) {
            $this->load->helper('dompdf');
            $this->load->model('rawat_inap_model', 'rawat_inap');

            $data_rawat_inap = $this->rawat_inap->get_with_pasien($id);

            if($this->nota->is_tanggal_akhir_null($id)) {
                $this->session->set_flashdata('errors', 'Cetak nota gagal! Pastikan tanggal akhir Kamar dan Gizi sudah diisi.');

                redirect('nota/index/' . $id);
            } else {
                // set tgl_keluar untuk nama nota
                if(!empty($data_rawat_inap->tgl_keluar)) {
                    $tgl_keluar = $data_rawat_inap->tgl_keluar;
                }else {
                    $tgl_keluar = date('d-m-Y');
                }

                $input = $this->nota->populate_input($this->input->post());
                $data['input'] = $input;

                // update total_biaya rawat inap
                $this->rawat_inap->update($id, array('total_biaya' => $input['total_keseluruhan']));

                $data['nomor_nota'] = $this->nota->get_nomor_nota($id);

                // page info here, db calls, etc.
                $html = $this->load->view('nota/_nota_umum', $data, true);
                // dump($html);
                // date('d/m/Y');

                pdf_create($html, $data['nomor_nota'].'-'.$data_rawat_inap->nama_pasien.'-'.$tgl_keluar);


                // // testing
                // // dump($this->input->post());
                // $input = $this->nota->populate_input($this->input->post());
                // $data['input'] = $input;
                // // dump($data['input']);
                // $this->load->view('nota/_nota_umum', $data);
            }
        }
    }

    function cetak_rincian($id) {
        if($_POST) {
            $this->load->helper('dompdf');
            $this->load->model('rawat_inap_model', 'rawat_inap');

            $data_rawat_inap = $this->rawat_inap->get_with_pasien($id);

            if($this->nota->is_tanggal_akhir_null($id)) {
                $this->session->set_flashdata('errors', 'Cetak nota gagal! Pastikan tanggal akhir Kamar dan Gizi sudah diisi.');

                redirect('nota/index/' . $id);
            } else {
                // set tgl_keluar untuk nama nota
                if(!empty($data_rawat_inap->tgl_keluar)) {
                    $tgl_keluar = $data_rawat_inap->tgl_keluar;
                }else {
                    $tgl_keluar = date('d-m-Y');
                }

                $input = $this->nota->populate_input($this->input->post());
                $data['input'] = $input;

                // update total_biaya rawat inap
                $this->rawat_inap->update($id, array('total_biaya' => $input['total_keseluruhan']));

                $data['nomor_nota'] = $this->nota->get_nomor_nota($id);

                // page info here, db calls, etc.
                $html = $this->load->view('nota/_nota_rincian', $data, true);
                // dump($html);
                pdf_create($html, 'R-'.$data['nomor_nota'].'-'.$data_rawat_inap->nama_pasien.'-'.$tgl_keluar);


                // // testing
                // // dump($this->input->post());
                // $input = $this->nota->populate_input($this->input->post());
                // $data['input'] = $input;
                // // dump($data['input']);
                // $this->load->view('nota/_nota_rincian', $data);
            }
        }
    }

    function test() {

    }
}