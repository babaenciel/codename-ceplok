<?php

class Laboratorium extends MY_Controller {
    function __construct() {
        parent::__construct();
        access_check(array(USER_ADMINISTRASI));

        $this->load->model('laboratorium_model', 'laboratorium');
        $this->load->model('rawat_inap_model', 'rawat_inap');

        $this->title = 'Laboratorium';
    }

    // $id = id rawat inap
    function index($id) {
        $rawat_inap = $this->rawat_inap->get_with_pasien($id);        
        $this->title .= ' - ' . $rawat_inap->nama_pasien . ' - ' . conv_date_format($rawat_inap->tgl_masuk, 'd-m-Y');       
        
        $data['data'] = $this->laboratorium->get_all_laboratorium($id);
        $data['id'] = $id;

        $this->render('laboratorium/index', $data);
    }

    // $id = id rawat inap
    function add($id) {
        $this->load->model('tarif_laboratorium_model', 'tarif_laboratorium');

        $rawat_inap = $this->rawat_inap->get_with_pasien($id);        
        $this->title .= ' - ' . $rawat_inap->nama_pasien . ' - ' . conv_date_format($rawat_inap->tgl_masuk, 'd-m-Y');               

        if($_POST) {
            $this->config->load('form_validation');
            $rules = $this->config->item('laboratorium/add');

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                $this->laboratorium->insert(array(
                    'id_tarif_laboratorium' => $this->input->post('layanan'),
                    'tgl_laboratorium' => conv_date_format($this->input->post('tanggal_laboratorium'), 'Y-m-d'),
                    'waktu_laboratorium' => $this->input->post('waktu_laboratorium'),
                    'id_rawat_inap' => $id
                ));

                redirect('laboratorium/index/'.$id);
            }else {
                $data['errors'] = validation_errors();
            }
        }

        $data['layanan_dropdown'] = $this->tarif_laboratorium->get_dropdown();        
        $data['form'] = $this->laboratorium->init_data();
        $data['id'] = $id;

        $this->render('laboratorium/add', $data);
    }

    // $id = id laboratorium
    function edit($id) {
        $this->load->model('tarif_laboratorium_model', 'tarif_laboratorium');
       
        $data['layanan_dropdown'] = $this->tarif_laboratorium->get_dropdown();
        $data['id'] = $id;

        $data_laboratorium = $this->laboratorium->get($id);
        $data['form'] = $this->laboratorium->init_data($data_laboratorium);

        $rawat_inap = $this->rawat_inap->get_with_pasien($data_laboratorium->id_rawat_inap);        
        $this->title .= ' - ' . $rawat_inap->nama_pasien . ' - ' . conv_date_format($rawat_inap->tgl_masuk, 'd-m-Y');               

        if($_POST) {
            $this->config->load('form_validation');
            $rules = $this->config->item('laboratorium/add');

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                $this->laboratorium->update($id, array(
                    'id_tarif_laboratorium' => $this->input->post('layanan'),
                    'tgl_laboratorium' => conv_date_format($this->input->post('tanggal_laboratorium'), 'Y-m-d'),
                    'waktu_laboratorium' => $this->input->post('waktu_laboratorium'),
                    'id_rawat_inap' => $data_laboratorium->id_rawat_inap
                ));

                redirect('laboratorium/index/'.$data_laboratorium->id_rawat_inap);
            }else {
                $data['errors'] = validation_errors();
            }
        }

        $this->render('laboratorium/edit', $data);
    }

    // $id = id laboratorium
    function delete($id) {
        $data_laboratorium = $this->laboratorium->get($id);
        $this->laboratorium->delete($id);

        redirect('laboratorium/index/'.$data_laboratorium->id_rawat_inap);
    }
}