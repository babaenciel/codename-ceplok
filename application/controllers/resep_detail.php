<?php

class Resep_detail extends MY_Controller {
	private $jenis = 'rawat_inap'; // default
	private $jenis_label = 'Rawat Inap';

	function __construct() {
		parent::__construct();
		access_check(array(USER_GUDANG));

		if($this->uri->segment(2) === 'rawat_inap') {
			$this->jenis = 'rawat_inap';
			$this->jenis_label = 'Rawat Inap';
		}else if($this->uri->segment(2) === 'rawat_jalan') {
			$this->jenis = 'rawat_jalan';
			$this->jenis_label = 'Rawat Jalan';
		}
	}

	// $id = id resep
	function index($id) {
		$this->load->model('resep_model', 'resep');
		$resep = $this->resep->get_with_pasien_rawat_inap($id);
		$this->title = 'Detail Resep ' . $this->jenis_label . ' - ' . $resep->nomor_resep . ' - ' . conv_date_format($resep->tanggal_resep, 'd-m-Y');

		$this->load->model('pengeluaran_obat_model', 'pengeluaran_obat');

		$data['id'] = $id;
		$data['jenis'] = $this->jenis;
		$data['data'] = $this->pengeluaran_obat->get_all_with_obat($id);

		if($this->jenis == 'rawat_inap') {
			$data['nama_pasien'] = $resep->nama_pasien;
		}else {
			$data['nama_pasien'] = $resep->nama_pasien_rawat_jalan;
		}
		$data['nama_dokter'] = $resep->nama_dokter;

		$this->render('resep_detail/index', $data);
	}

	// $id = id pengeluaran obat
	function delete($id) {
		$this->load->model('obat_model', 'obat');
		$this->load->model('pengeluaran_obat_model', 'pengeluaran_obat');

		$pengeluaran_obat = $this->pengeluaran_obat->get($id);
		$this->obat->add_jumlah_mutasi_for_pengeluaran($pengeluaran_obat->id_katalog_obat, $this->jenis, $pengeluaran_obat->is_gudang_lain, $pengeluaran_obat->jumlah_pengeluaran);

		$this->pengeluaran_obat->delete($id);

		redirect('resep_detail/'.$this->jenis.'/index/'.$pengeluaran_obat->id_resep);
	}
}