<?php

class Gizi extends MY_Controller {
	function __construct() {
        parent::__construct();
        access_check(array(USER_ADMINISTRASI));

        $this->load->model('pasien_model', 'pasien');
        $this->load->model('rawat_inap_model', 'rawat_inap');
        $this->load->model('gizi_model', 'gizi');

        $this->title = 'Gizi';
    }

    // $id = id rawat inap
    function index($id) {
        $rawat_inap = $this->rawat_inap->get_with_pasien($id);        
        $this->title .= ' - ' . $rawat_inap->nama_pasien . ' - ' . conv_date_format($rawat_inap->tgl_masuk, 'd-m-Y');       
        
    	$data['data'] = $this->gizi->get_all_gizi($id);
        $data['id'] = $id;

    	$this->render('gizi/index', $data);
    }

    // $id = id rawat inap
    function add($id) {
        $this->load->model('tarif_gizi_model', 'tarif_gizi');

        $rawat_inap = $this->rawat_inap->get_with_pasien($id);        
        $this->title .= ' - ' . $rawat_inap->nama_pasien . ' - ' . conv_date_format($rawat_inap->tgl_masuk, 'd-m-Y');               

        if($_POST) {
            $this->config->load('form_validation');
            $rules = $this->config->item('gizi/add');

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                $this->gizi->insert(array(
                    'id_tarif_gizi' => $this->input->post('gizi'),
                    'tgl_mulai' => conv_date_format($this->input->post('tanggal_mulai'), 'Y-m-d'),                    
                    'id_rawat_inap' => $id
                ));

                redirect('gizi/index/'.$id);
            }else {
                $data['errors'] = validation_errors();
            }
        }

        $data['gizi_dropdown'] = $this->tarif_gizi->get_dropdown();
        $data['form'] = $this->gizi->init_data();
        $data['id'] = $id;

        $this->render('gizi/add', $data);
    }

    // $id = id gizi
    function edit($id) {
        $this->load->model('tarif_gizi_model', 'tarif_gizi');

        $data['gizi_dropdown'] = $this->tarif_gizi->get_dropdown();
        $data['id'] = $id;
        $data['edit'] = TRUE;

        $data_gizi = $this->gizi->get($id);
        $data['form'] = $this->gizi->init_data($data_gizi);

        $rawat_inap = $this->rawat_inap->get_with_pasien($data_gizi->id_rawat_inap);        
        $this->title .= ' - ' . $rawat_inap->nama_pasien . ' - ' . conv_date_format($rawat_inap->tgl_masuk, 'd-m-Y');       

        if($_POST) {
            $this->config->load('form_validation');
            $rules = $this->config->item('gizi/add');

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                if($this->input->post('tanggal_akhir')) {
                    $this->gizi->update($id, array(
                        'id_tarif_gizi' => $this->input->post('gizi'),
                        'tgl_mulai' => conv_date_format($this->input->post('tanggal_mulai'), 'Y-m-d'),
                        'tgl_akhir' => conv_date_format($this->input->post('tanggal_akhir'), 'Y-m-d'),
                        'id_rawat_inap' => $data_gizi->id_rawat_inap
                    ));
                }else {
                    $this->gizi->update($id, array(
                        'id_tarif_gizi' => $this->input->post('gizi'),
                        'tgl_mulai' => conv_date_format($this->input->post('tanggal_mulai'), 'Y-m-d'),                    
                        'id_rawat_inap' => $data_gizi->id_rawat_inap
                    ));
                }                                

                redirect('gizi/index/'.$data_gizi->id_rawat_inap);
            }else {
                $data['errors'] = validation_errors();
            }
        }

        $this->render('gizi/edit', $data);
    }

    // $id = id gizi
    function delete($id) {
        $data_gizi = $this->gizi->get($id);
        $this->gizi->delete($id);

        redirect('gizi/index/'.$data_gizi->id_rawat_inap);
    }
}