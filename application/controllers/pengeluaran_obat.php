<?php

class Pengeluaran_obat extends MY_Controller {
	function __construct() {
		parent::__construct();
		access_check(array(USER_GUDANG, USER_ADMINISTRASI));
	}

	function index() {
		$this->title = 'Rekap Pengeluaran Obat Setahun';

		$this->load->model('pengeluaran_obat_model', 'pengeluaran');
		$data['data'] = $this->pengeluaran->get_pengeluaran_obat_setahun();

		$this->render('pengeluaran_obat/index', $data);
	}

	function chart($id) {
		$this->title = 'Grafik Pengeluaran Obat';

		$this->load->model('obat_model', 'obat');
		$obat = $this->obat->get_with_kategori($id);

		$this->load->model('pengeluaran_obat_model', 'pengeluaran');
		$pemakaian = $this->pengeluaran->get_pengeluaran_obat_with($id);
		/*print_r($data['data']);
		exit();*/
		$this->load->library('graph');
        $data_1 = array();
        $xlabel = array();
        foreach ($pemakaian as $key => $value) {
        	$data_1[] = $value->total;
            $xlabel[] = date("Y-m", strtotime($value->tanggal_resep));
        }
      
        $ff = new graph();
        $ff->set_data( $data_1 );
        $ff->title( 'Pengeluaran obat '.$obat->nama_obat.' dalam 1 tahun terakhir', '{font-size: 14px; color: #3D5570;font-family:calibri;}' );
        $ff->line_dot( 3, 5, '#8B6122', $obat->satuan, 10 );
        $ff->bg_colour = '#FFFFFF';
        $ff->x_axis_colour( '#818D9D', '#ADB5C7' );
        $ff->y_axis_colour( '#818D9D', '#ADB5C7' );
        $ff->set_x_labels($xlabel);
        $ff->set_y_max( max($data_1) + 50 );
        $ff->y_label_steps( 10 );
        $ff->set_y_legend( 'Jumlah obat dikeluarkan', 12, '#3D5570' );
        $ff->set_x_legend( 'Bulan', 12, '#3D5570' );
        $ff->set_output_type('js');
        $ff->width = '100%';
        $ff->height = '400';
        $data['graph1'] = $ff->render();

		$this->render('pengeluaran_obat/chart', $data);
	}

    function forecast() {
        $this->load->model('pengeluaran_obat_model', 'pengeluaran');
        //get periode penggunaan - khawatir ada bulan yg semua obat tidak terjual
        $tanggalresep = $this->pengeluaran->get_periode();
        $totalperiode=array();
        $totalhargaperiode=array();
        $i=1;
        foreach ($tanggalresep as $value) {
            $periode[]=date("Y-m", strtotime($value->tanggal_resep));
            //mendefinisikan array untuk menghitung total pengeluaran perperiode
            $totalperiode[$i]=0;
            $totalhargaperiode[$i]=0;
            $i++;
        }

        $this->title = 'Estimasi Kebutuhan Obat Selama '.count($periode).' Bulan';

        //hitung nilai perngeluaran tiap obat
        $dt_pengeluaran = $this->pengeluaran->get_pengeluaran_obat();
        foreach ($dt_pengeluaran as $key => $value) {
            $totalperobat=0;
            $bln = explode(',', $value->bulan);
            $tot = explode(',', $value->total);
            $hrg = explode(',', $value->total_harga);
            foreach ($periode as $period) {
                foreach ($bln as $keybln => $valbln) {
                    if($period == $valbln){
                        //add to array nilai pengeluaran bulanan
                        $dt_pengeluaran[$key]->pengeluaran[$period] = array('total'=>$tot[$keybln],'harga'=>$hrg[$keybln]);
                            $totalperobat = $totalperobat +  $hrg[$keybln];
                             break; 
                    }else{
                        $dt_pengeluaran[$key]->pengeluaran[$period] = array('total'=>0,'harga'=>0);

                    }    

                }

            }
            //nilai total pengeluaran tiap obat
            $dt_pengeluaran[$key]->totalperobat=$totalperobat; 
        }

        //hitung nilai total pengeluaran tiap periode
        foreach ($dt_pengeluaran as $key => $value) {
            $i=1;
            foreach ($value->pengeluaran as $kunci => $pengeluaran) {
                $totalperiode[$i]=$totalperiode[$i]+$pengeluaran['total'];
                $totalhargaperiode[$i]=$totalhargaperiode[$i]+$pengeluaran['harga'];
                $i++;
            }
        }

        //membentuk rumus regresi linear
        //sum indek, sum (indek * value), sum(indek^2) 
        $sumkey=0; $sumkeyvalue=0; $sumkey2 =0;  $sumvalue=0;
        foreach ($totalhargaperiode as $indek => $value) {
            $sumkey = $sumkey+ $indek;
            $sumkeyvalue = $sumkeyvalue + ($indek*$value);
            $sumkey2 = $sumkey2 + pow($indek,2);
        }
        $sumvalue = array_sum($totalhargaperiode);

        //b = (n * sumkeyvalue - sumkey*sumvalue)/(n*sumkey2 - sumkey^2)
        $b = (((count($totalhargaperiode))*$sumkeyvalue) - ($sumkey*$sumvalue)) / (((count($totalhargaperiode))*$sumkey2) - pow($sumkey,2));
        // a = (sumvalue - (b*sumkey)) / n
        $a = ($sumvalue - ($b*$sumkey)) / count($totalhargaperiode) ;
        // y = a + bx
        $y = $a + $b;


        //mendapatkan nilai peramalan
        $forcast=array();
        $forcast_total_next_periode=0;
        for ($j=1; $j <= (count($totalhargaperiode) * 2) ; $j++) { 
            $forcast[$j] = ceil($a + $b*$j);
            if($j> count($totalhargaperiode)) {
                $forcast_total_next_periode=$forcast_total_next_periode+$forcast[$j];
            }
        }

        //HItung MAD
        $MAD=0;
        for ($j=1; $j <= (count($totalhargaperiode)) ; $j++) { 
            $MAD = $MAD + (abs($totalhargaperiode[$j] - $forcast[$j]));
            //echo (abs($totalhargaperiode[$j] - $forcast[$j]))."<br>";
        }
        //Standar Deviasi
        $stdev_ss = ceil(1.25 * $MAD);


        //hitung rasio produk
        $rasio = array();  //rasio disagregasi
        $forcast_obat_rp = array();  //total kebutuhan obat dalam rp
        $forcast_obat = array(); //total kebutuhan obat dalam satuan
        $forcast_obat_seminggu = array(); //kebutuhan obat dalam semiggu
        $stdev_obat_rp = array(); //standar deviasi tiap obat dalam rp 
        $stdev_obat = array(); //standar deviasi tiap obat dalam satuan 
        $SS_obat = array(); //Safety stok tiap obat dalam satuan 
        $ROP_obat = array(); //Reorder point tiap obat dalam satuan 
        $IC_obat = array(); //Biaya SImpan tiap obat dalam rp 
        $EOQ_obat = array(); //EOQ tiap obat dalam rp 

        foreach ($dt_pengeluaran as $key3 => $value3) {
             //rasio disagregasi
            $rasio[$value3->id_katalog_obat] = ($value3->totalperobat) / array_sum($totalhargaperiode);
            //total kebutuhan obat dalam rp
            $forcast_obat_rp[$value3->id_katalog_obat] = ceil( ($rasio[$value3->id_katalog_obat] ) * $forcast_total_next_periode);
            //total kebutuhan obat dalam satuan
            $forcast_obat[$value3->id_katalog_obat] = round( $forcast_obat_rp[$value3->id_katalog_obat] / $value3->harga_satuan );
            //kebutuhan obat dalam semiggu
            $forcast_obat_seminggu [$value3->id_katalog_obat] = round( $forcast_obat[$value3->id_katalog_obat] / (count($periode)*4) );
            //standar deviasi tiap obat dalam rp 
            $stdev_obat_rp [$value3->id_katalog_obat] = $rasio[$value3->id_katalog_obat] * $stdev_ss ;
            //standar deviasi tiap obat dalam satuan 
             $stdev_obat [$value3->id_katalog_obat] = round( $stdev_obat_rp [$value3->id_katalog_obat] / $value3->harga_satuan );
             //Safety stok tiap obat dalam satuan 
             $SS_obat [$value3->id_katalog_obat] = ceil(  $stdev_obat [$value3->id_katalog_obat] * 2.326 );
             //Reorder point tiap obat dalam satuan 
             $ROP_obat [$value3->id_katalog_obat] =  ($forcast_obat_seminggu [$value3->id_katalog_obat] * $value3->lead_time ) +  $SS_obat [$value3->id_katalog_obat];
             //biaya simpan tiap obat
              $IC_obat [$value3->id_katalog_obat] = ceil( 43 + 0.075 *  $value3->harga_satuan );
              //EOQ tiap obat
              $EOQ_obat [$value3->id_katalog_obat] = ceil( SQRT((2*$forcast_obat[$value3->id_katalog_obat] * 5000 )/ $IC_obat [$value3->id_katalog_obat] ));

        }

       /* echo "<pre>";
        // print_r($dt_pengeluaran);
        print_r($rasio);
        print_r($forcast_obat_rp);
        print_r($forcast_obat);
        print_r($forcast_obat_seminggu);
        print_r($stdev_obat_rp);
        print_r($stdev_obat);
        print_r($SS_obat);
        print_r($ROP_obat);
        print_r($IC_obat);
        echo "EOQ";
        print_r($EOQ_obat);
        exit();*/

        $data['dt_pengeluaran'] = $dt_pengeluaran;
        $data['forcast_obat'] = $forcast_obat;
        $data['SS_obat'] = $SS_obat;
        $data['ROP_obat'] = $ROP_obat;
        $data['EOQ_obat'] = $EOQ_obat;

        //set session
        $this->session->unset_userdata('sesi_forecast');
        $dt_forecast = array('forcast_obat'=>$forcast_obat,
                        'safety_stock'=>$SS_obat,
                        'reorder_point'=>$ROP_obat,
                        'economic_order_quantity'=>$EOQ_obat);
        $this->session->set_userdata('sesi_forecast',$dt_forecast);

        $this->render('pengeluaran_obat/forecast', $data);
    }

     function forecast_save() {
        $dt_forecast = $this->session->userdata('sesi_forecast');      
        $data = array();
        foreach ($dt_forecast as $key => $value) {
           if ($key == 'forcast_obat') {
               foreach ($value as $idobat => $frc) {
                $data[] = array('id_katalog_obat'=>$idobat,
                                 'safety_stock'=>0,   
                                 'reorder_point'=>0,
                                 'economic_order_quantity'=>0);
                }
            }else{
                break;
            }           
        }
        
        foreach ($dt_forecast as $key => $value) {
           if ($key != 'forcast_obat'){
                $i=0;
                foreach ($value as $frc) {
                    $data[$i][$key]= $frc;
                    $i++;                      
                }
            }           
        }

        /*echo "<pre>";
        print_r($dt_forecast);
        print_r($data);
        exit();*/
       
        $this->db->update_batch('katalog_obat', $data, 'id_katalog_obat'); 

        redirect ('pengeluaran_obat');

     }
  
}