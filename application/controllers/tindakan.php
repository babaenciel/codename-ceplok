<?php

class Tindakan extends MY_Controller {
    function __construct() {
        parent::__construct();
        access_check(array(USER_ADMINISTRASI));

        $this->load->model('tindakan_model', 'tindakan');
        $this->load->model('rawat_inap_model', 'rawat_inap');

        $this->title = 'Tindakan';
    }

    // $id = id rawat inap
    function index($id) {
        $rawat_inap = $this->rawat_inap->get_with_pasien($id);        
        $this->title .= ' - ' . $rawat_inap->nama_pasien . ' - ' . conv_date_format($rawat_inap->tgl_masuk, 'd-m-Y');       
        
        $data['data'] = $this->tindakan->get_all_tindakan($id);
        $data['id'] = $id;

        $this->render('tindakan/index', $data);
    }

    // $id = id rawat inap
    function add($id) {
        $this->load->model('perawat_model', 'perawat');
        $this->load->model('tarif_tindakan_model', 'tarif_tindakan');

        $rawat_inap = $this->rawat_inap->get_with_pasien($id);        
        $this->title .= ' - ' . $rawat_inap->nama_pasien . ' - ' . conv_date_format($rawat_inap->tgl_masuk, 'd-m-Y');               

        if($_POST) {
            $this->config->load('form_validation');
            $rules = $this->config->item('tindakan/add');

            if($this->input->post('tindakan_lainnya_checkbox')) {
                array_push($rules, array(
                    'field' => 'tindakan_lainnya',
                    'label' => 'Nama Tindakan Lainnya',
                    'rules' => 'required'
                ));
                array_push($rules, array(
                    'field' => 'tarif',
                    'label' => 'Tarif Tindakan Lainnya',
                    'rules' => 'required|numeric'
                ));
            }else {
                array_push($rules, array(
                    'field' => 'jenis_tindakan',
                    'label' => 'Jenis Tindakan',
                    'rules' => 'required'
                ));
            }

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                // dump($this->input->post());
                if($this->input->post('tindakan_lainnya_checkbox')) {
                    $id_tarif_tindakan = $this->tarif_tindakan->insert(array(
                        'nama_tindakan' => $this->input->post('tindakan_lainnya'),
                        'tarif' => $this->input->post('tarif'),
                        'tindakan_lainnya' => 1,
                    ));

                    $this->tindakan->insert(array(
                        // 'id_perawat' => $this->input->post('perawat'),
                        'id_tarif_tindakan' => $id_tarif_tindakan,
                        'tgl_tindakan' => conv_date_format($this->input->post('tanggal_tindakan'), 'Y-m-d'),
                        'waktu_tindakan' => $this->input->post('waktu_tindakan'),
                        'id_rawat_inap' => $id,
                    ));
                }else {
                    $this->tindakan->insert(array(
                        // 'id_perawat' => $this->input->post('perawat'),
                        'id_tarif_tindakan' => $this->input->post('jenis_tindakan'),
                        'tgl_tindakan' => conv_date_format($this->input->post('tanggal_tindakan'), 'Y-m-d'),
                        'waktu_tindakan' => $this->input->post('waktu_tindakan'),
                        'id_rawat_inap' => $id,
                    ));
                }

                redirect('tindakan/index/'.$id);
            }else {
                $data['errors'] = validation_errors();
                // dump(set_value('tindakan_lainnya'));
            }
        }

        // $data['perawat_dropdown'] = $this->perawat->dropdown('nama_perawat');
        $data['jenis_tindakan_dropdown'] = $this->tarif_tindakan->get_tindakan_dropdown(0);
        $data['form'] = $this->tindakan->init_data();
        $data['id'] = $id;

        $this->render('tindakan/add', $data);
    }

    // $id = id tindakan
    function edit($id) {
        $this->load->model('perawat_model', 'perawat');
        $this->load->model('tarif_tindakan_model', 'tarif_tindakan');
        
        // $data['perawat_dropdown'] = $this->perawat->dropdown('nama_perawat');
        $data['jenis_tindakan_dropdown'] = $this->tarif_tindakan->get_tindakan_dropdown(0);

        $data_tindakan = $this->tindakan->get_tindakan_with_tarif_tindakan($id);
        $data['form'] = $this->tindakan->init_data($data_tindakan);
        $data['id'] = $id;

        $rawat_inap = $this->rawat_inap->get_with_pasien($data_tindakan->id_rawat_inap);        
        $this->title .= ' - ' . $rawat_inap->nama_pasien . ' - ' . conv_date_format($rawat_inap->tgl_masuk, 'd-m-Y');               

        if($_POST) {
            $this->config->load('form_validation');
            $rules = $this->config->item('tindakan/add');

            if($this->input->post('tindakan_lainnya_checkbox')) {
                array_push($rules, array(
                    'field' => 'tindakan_lainnya',
                    'label' => 'Nama Tindakan Lainnya',
                    'rules' => 'required'
                ));
                array_push($rules, array(
                    'field' => 'tarif',
                    'label' => 'Tarif Tindakan Lainnya',
                    'rules' => 'required|numeric'
                ));
            }else {
                array_push($rules, array(
                    'field' => 'jenis_tindakan',
                    'label' => 'Jenis Tindakan',
                    'rules' => 'required'
                ));
            }

            $this->form_validation->set_rules($rules);

            if($this->form_validation->run()) {
                if($this->input->post('tindakan_lainnya_checkbox')) {
                    $this->tarif_tindakan->update($data_tindakan->id_tarif_tindakan, array(
                        'nama_tindakan' => $this->input->post('tindakan_lainnya'),
                        'tarif' => $this->input->post('tarif'),
                        'tindakan_lainnya' => 1
                    ));

                    $this->tindakan->update($id, array(
                        // 'id_perawat' => $this->input->post('perawat'),
                        'id_tarif_tindakan' => $data_tindakan->id_tarif_tindakan,
                        'tgl_tindakan' => conv_date_format($this->input->post('tanggal_tindakan'), 'Y-m-d'),
                        'waktu_tindakan' => $this->input->post('waktu_tindakan'),
                        'id_rawat_inap' => $data_tindakan->id_rawat_inap
                    ));
                }else {
                    $this->tindakan->update($id, array(
                        // 'id_perawat' => $this->input->post('perawat'),
                        'id_tarif_tindakan' => $this->input->post('jenis_tindakan'),
                        'tgl_tindakan' => conv_date_format($this->input->post('tanggal_tindakan'), 'Y-m-d'),
                        'waktu_tindakan' => $this->input->post('waktu_tindakan'),
                        'id_rawat_inap' => $data_tindakan->id_rawat_inap
                    ));
                }

                redirect('tindakan/index/'.$data_tindakan->id_rawat_inap);
            }else {
                $data['errors'] = validation_errors();
            }
        }

        $this->render('tindakan/edit', $data);
    }

    // $id = id visit dokter
    function delete($id) {
        $data_tindakan = $this->tindakan->get($id);
        $this->tindakan->delete($id);

        redirect('tindakan/index/'.$data_tindakan->id_rawat_inap);
    }
}